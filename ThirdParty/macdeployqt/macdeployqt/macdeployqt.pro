TEMPLATE = app
TARGET = macdeployqt

# DESTDIR = $$QT.designer.bins

SOURCES += main.cpp
CONFIG -= app_bundle

CONFIG += c++11

macx {
    # Objective-C++ does not recognize C++11 syntax despite the CONFIG += c++11 option
    # https://bugreports.qt.io/browse/QTBUG-39057
    # https://bugreports.qt.io/browse/QTBUG-36575
    # https://codereview.qt-project.org/#/c/122199/

    QMAKE_OBJCXXFLAGS_PRECOMPILE += -std=c++11 -stdlib=libc++
}

include(../shared/shared.pri)

# target.path = $$[QT_INSTALL_BINS]
# INSTALLS += target

QMAKE_CLEAN  += $$TARGET
