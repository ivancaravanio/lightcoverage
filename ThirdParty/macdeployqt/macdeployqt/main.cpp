/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "logger.h"

#include "deployer.h"

#include <iostream>

#include <QDir>
#include <QHash>
#include <QString>

enum ArgumentType
{
    ArgumentTypeUnknown                      = -1,
    ArgumentTypeNoPlugins                    = 0,
    ArgumentTypeDMG                          = 1,
    ArgumentTypeNoStrip                      = 2,
    ArgumentTypeUseDebugLibs                 = 3,
    ArgumentTypeVerbose                      = 4,
    ArgumentTypeExecutable                   = 5,
    ArgumentTypeQmlDir                       = 6,
    ArgumentTypeAlwaysOverwrite              = 7,
    ArgumentTypeAdditionalLibrarySearchPaths = 8,
    ArgumentTypesCount                       = 9
};

using namespace std;
using namespace MacDeployQt::Shared;

int main(int argc, char **argv)
{
    QString modulePath;
    if (argc > 1)
        modulePath = QString::fromLocal8Bit(argv[1]);

    Logger::instance().init(stdout, Logger::verbosityLevelToSeverityLevels(VerbosityLevelInfo));

    static const QChar argumentStart(QLatin1Char('-'));
    if (argc < 2 || modulePath.startsWith(argumentStart)) {
        Logger::instance().logInfo()
                << "Usage: macdeployqt app-bundle [options]" << endl
                << endl
                << "Options:" << endl
                << "   -verbose=<0-3>                           : 0 = no output, 1 = errors, 2 = errors/warnings (default), 3 = info" << endl
                << "   -no-plugins                              : Skip plugin deployment" << endl
                << "   -dmg                                     : Create a .dmg disk image" << endl
                << "   -no-strip                                : Don't run 'strip' on the binaries" << endl
                << "   -use-debug-libs                          : Deploy with debug versions of frameworks and plugins (implies -no-strip)" << endl
                << "   -executable=<path>                       : Let the given executable use the deployed frameworks too" << endl
                << "   -qmldir=<path>                           : Deploy imports used by .qml files in the given path" << endl
                << "   -always-overwrite                        : Copy files even if the target file exists" << endl
                << "   -additional-library-search-paths=<paths> : Specify additional paths where macdeployqt will search for dependent libraries\' binaries." << endl
                << "                                              The paths should be separated using colon (;):" << endl
                << "                                              -additional-library-search-paths=<path-1>;<path-2>" << endl
                << "                                              If either of the paths contains whitespace, it should be escaped" << endl
                << "                                              by prepending a backward slash (\\) to the whitespace character." << endl
                << "                                              The tool allows for specifying numerous -additional-library-paths switches." << endl
                << endl
                << "macdeployqt takes an application bundle as input and makes it" << endl
                << "self-contained by copying in the Qt frameworks and plugins that" << endl
                << "the application uses." << endl
                << endl
                << "Plugins related to a framework are copied in with the" << endl
                << "framework. The accessibilty, image formats, and text codec" << endl
                << "plugins are always copied, unless \"-no-plugins\" is specified." << endl
                << endl
                << "See the \"Deploying an Application on Qt/Mac\" topic in the" << endl
                << "documentation for more information about deployment on Mac OS X.";

        return EXIT_SUCCESS;
    }

    const ModuleInfo mainModuleInfo(modulePath);
    if (!mainModuleInfo.isValid()) {
        Logger::instance().logError()
                << "Either the provided module "
                << modulePath
                << " does not exist or its type is not correct.";
        return EXIT_SUCCESS;
    }

    VerbosityLevel logVerbosityLevel = VerbosityLevelErrorsAndWarnings;
    bool deployPlugins = true;
    bool createDmg = false;
    bool useDebugLibs = false;
    bool runStrip = false;
    QList<ModuleInfo> additionalExecutables;
    QString qmlDir;
    bool alwaysOverwrite = false;
    QStringList additionalLibrarySearchPaths;

    QHash<QString, ArgumentType> argumentTypes;
    argumentTypes.reserve(ArgumentTypesCount);
    argumentTypes.insert("no-plugins", ArgumentTypeNoPlugins);
    argumentTypes.insert("dmg", ArgumentTypeDMG);
    argumentTypes.insert("no-strip", ArgumentTypeNoStrip);
    argumentTypes.insert("use-debug-libs", ArgumentTypeUseDebugLibs);
    argumentTypes.insert("verbose", ArgumentTypeVerbose);
    argumentTypes.insert("executable", ArgumentTypeExecutable);
    argumentTypes.insert("qmldir", ArgumentTypeQmlDir);
    argumentTypes.insert("always-overwrite", ArgumentTypeAlwaysOverwrite);
    argumentTypes.insert("additional-library-search-paths", ArgumentTypeAdditionalLibrarySearchPaths);

    static const QChar argumentParametersStart(QLatin1Char('='));
    for (int i = 2; i < argc; ++i) {
        const QString argComplete = QString::fromLatin1(argv[i]);
        if (argComplete.startsWith(argumentStart)) {
            const QString arg = argComplete.mid(1);
            const int argTypeParamSepIndex = arg.indexOf(argumentParametersStart);
            QString argType;
            QString argParam;
            if (argTypeParamSepIndex == -1) {
                argType = arg;
            } else {
                argType = arg.left(argTypeParamSepIndex);
                argParam = arg.mid(argTypeParamSepIndex + 1);
            }

            const ArgumentType argTypeNum = argumentTypes.value(argType, ArgumentTypeUnknown);
            if (argTypeNum != ArgumentTypeUnknown) {
                Logger::instance().logInfo() << "Argument found:" << arg;
            }

            switch (argTypeNum) {
            case ArgumentTypeUnknown:
                Logger::instance().logError() << "Unknown argument" << arg << endl;
                return EXIT_SUCCESS;
            case ArgumentTypeNoPlugins:
                deployPlugins = false;
                break;
            case ArgumentTypeDMG:
                createDmg = true;
                break;
            case ArgumentTypeNoStrip:
                runStrip = false;
                break;
            case ArgumentTypeUseDebugLibs:
                useDebugLibs = true;
                runStrip = false;
                break;
            case ArgumentTypeVerbose: {
                bool ok = false;
                int number = argParam.toInt(&ok);
                if (ok)
                    logVerbosityLevel = static_cast<VerbosityLevel>(number);
                else
                    Logger::instance().logError() << "Could not parse verbose level";
                break;
            }
            case ArgumentTypeExecutable:
                if (argParam.isEmpty()) {
                    Logger::instance().logError() << "Missing executable path";
                } else {
                    const ModuleInfo additionaExecutableInfo(argParam);
                    if (additionaExecutableInfo.type() == ModuleTypeApplicationExecutable) {
                        additionalExecutables.append(additionaExecutableInfo);
                    } else {
                        Logger::instance().logError()
                                << "The provided additional executable file path "
                                   "does not point to a valid executable file.";
                    }
                }
                break;
            case ArgumentTypeQmlDir:
                if (argParam.isEmpty()) {
                    Logger::instance().logError() << "Missing qml directory path";
                } else {
                    qmlDir = argParam;
                }
                break;
            case ArgumentTypeAlwaysOverwrite:
                alwaysOverwrite = true;
                break;
            case ArgumentTypeAdditionalLibrarySearchPaths:
                if (argParam.isEmpty()) {
                    Logger::instance().logError() << "Missing additional library paths";
                } else {
                    static const QChar pathsSeparator(QLatin1Char(':'));
                    const QStringList dirPaths = argParam.split(pathsSeparator);
                    foreach (const QString &dirPath, dirPaths) {
                        const QDir dir(dirPath);
                        if (dir.exists())
                            additionalLibrarySearchPaths.append(dir.canonicalPath());
                    }
                }
                break;
            default:
                break;
            }
        } else {
            Logger::instance().logError() << "Unknown argument" << argComplete << endl;
            return EXIT_SUCCESS;
        }
    }

    Logger::instance().setSeverityLevels(Logger::verbosityLevelToSeverityLevels(logVerbosityLevel));

    Deployer::deployQtFrameworks(mainModuleInfo,
                                 additionalExecutables,
                                 useDebugLibs,
                                 deployPlugins,
                                 createDmg,
                                 runStrip,
                                 qmlDir,
                                 alwaysOverwrite,
                                 additionalLibrarySearchPaths);

    return EXIT_SUCCESS;
}

