/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "deployer.h"

#include "logger.h"

#include <QByteArray>
#include <QDebug>
#include <QDir>
#include <QDirIterator>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QLibraryInfo>
#include <QProcess>
#include <QSet>
#include <QStringBuilder>

using namespace MacDeployQt::Shared;

bool FrameworkInfo::operator==(const FrameworkInfo &info) const
{
    return ((frameworkPath == info.frameworkPath)
            && (binaryPath == info.binaryPath));
}

bool FrameworkInfo::operator!=(const FrameworkInfo &info) const
{
    return !(*this == info);
}

void Deployer::changeQtFrameworks(
        const ModuleInfo &mainModuleInfo,
        const QString &qtPath,
        const bool useDebugLibs,
        const QStringList &additionalLibraryPaths)
{
    QList<ModuleInfo> modules;
    modules << mainModuleInfo
            << mainModuleInfo.bundleLibraryModules();
    const QList<FrameworkInfo> frameworks = getQtFrameworksForPaths(mainModuleInfo, modules, useDebugLibs, additionalLibraryPaths);
    if (frameworks.isEmpty()) {
        Logger::instance().logWarning() << "Could not find any _external_ Qt frameworks to change in" << mainModuleInfo;
        return;
    }

    const QString absoluteQtPath = QDir(qtPath).absolutePath();
    changeQtFrameworks(frameworks, modules, absoluteQtPath);
}

void Deployer::deployQtFrameworks(
        const ModuleInfo &mainModuleInfo,
        const QList<ModuleInfo> &additionalExecutables,
        const bool useDebugLibs,
        const bool deployPlugins,
        const bool createDmg,
        const bool runStrip,
        const QString &qmlDir,
        const bool alwaysOverwrite,
        const QStringList &additionalLibraryPaths)
{
    DeploymentInfo deploymentInfo = deployQtFrameworks(mainModuleInfo,
                                                       additionalExecutables,
                                                       useDebugLibs,
                                                       runStrip,
                                                       alwaysOverwrite,
                                                       additionalLibraryPaths);
    if (deployPlugins && !deploymentInfo.qtPath.isEmpty()) {
        deploymentInfo.pluginPath = deploymentInfo.qtPath % QDir::separator() % "plugins";

        Deployer::deployPlugins(mainModuleInfo,
                                deploymentInfo,
                                useDebugLibs,
                                runStrip,
                                alwaysOverwrite,
                                additionalLibraryPaths);
        createQtConf(mainModuleInfo);
    }

    QStringList qmlDirs;
    // Convenience: Look for .qml files in the current directoty if no -qmldir specified.
    if (qmlDir.isEmpty()) {
        QDir dir;
        if (!dir.entryList(QStringList() << QStringLiteral("*.qml")).isEmpty()) {
            qmlDirs.append(QStringLiteral("."));
        }
    } else {
        qmlDirs.append(qmlDir);
    }

    if (!qmlDirs.isEmpty())
        deployQmlImports(mainModuleInfo,
                         qmlDirs,
                         useDebugLibs,
                         deploymentInfo.useLoaderPath,
                         runStrip,
                         alwaysOverwrite,
                         additionalLibraryPaths);

    if (createDmg)
        createDiskImage(mainModuleInfo, alwaysOverwrite);
}

Deployer::DeploymentInfo::DeploymentInfo()
    : useLoaderPath(false)
{
}

bool Deployer::copyFilePrintStatus(
        const QString &from,
        const QString &to,
        const bool alwaysOverwrite)
{
    if (QFile(to).exists()) {
        if (alwaysOverwrite) {
            QFile(to).remove();
        } else {
            Logger::instance().logInfo() << "File exists, skip copy:" << to;
            return false;
        }
    }

    if (QFile::copy(from, to)) {
        QFile dest(to);
        dest.setPermissions(dest.permissions() | QFile::WriteOwner | QFile::WriteUser);
        Logger::instance().logInfo() << " copied:" << from << endl
                                     << " to" << to;

        // The source file might not have write permissions set. Set the
        // write permission on the target file to make sure we can use
        // install_name_tool on it later.
        QFile toFile(to);
        if (toFile.permissions() & QFile::WriteOwner)
            return true;

        if (!toFile.setPermissions(toFile.permissions() | QFile::WriteOwner)) {
            Logger::instance().logError() << "Failed to set u+w permissions on target file: " << to;
            return false;
        }

        return true;
    } else {
        Logger::instance().logError() << "file copy failed from" << from << endl
                                      << " to" << to;
        return false;
    }
    return true;
}

FrameworkInfo Deployer::parseOtoolLibraryLine(
        const ModuleInfo &mainModuleInfo,
        const QString &line,
        const bool useDebugLibs,
        const QStringList &additionalLibraryPaths)
{
    FrameworkInfo info;
    const QString trimmed = line.trimmed();

    if (trimmed.isEmpty())
        return info;

    // Don't deploy system libraries.
    if (trimmed.startsWith("/System/Library/")
        || (trimmed.startsWith("/usr/lib/") && trimmed.contains("libQt") == false) // exception for libQtuitools and libQtlucene
        || trimmed.startsWith(ModuleInfo::exePath)
        || trimmed.startsWith(ModuleInfo::loaderPath)
        || trimmed.startsWith(ModuleInfo::rPath)) {
        return info;
    }

    enum State {QtPath, FrameworkName, DylibName, Version, End};
    State state = QtPath;
    int part = 0;
    QString name;
    QString qtPath;

    static const QString debugSuffix = "_debug";
    QString suffix = useDebugLibs ? debugSuffix : QString();

    // Split the line into [Qt-path]/lib/qt[Module].framework/Versions/[Version]/
    QStringList parts = trimmed.split(QDir::separator());
    while (part < parts.count()) {
        const QString currentPart = parts.at(part).simplified() ;
        ++part;
        if (currentPart.isEmpty())
            continue;

        static const QString dynamicLibraryBinaryExtension = QChar(QLatin1Char('.'))
                                                             + ModuleInfo::moduleExtension(ModuleTypeLibraryDynamicBinary)
                                                             + QChar(QLatin1Char(' '));
        static const QString libraryBundleFolderExtension = QChar(QLatin1Char('.'))
                                                            + ModuleInfo::moduleExtension(ModuleTypeLibraryBundle);
        if (state == QtPath) {
            // Check for library name part
            if (part < parts.count() && parts.at(part).contains(dynamicLibraryBinaryExtension)) {
                state = DylibName;
                info.installName += QDir::separator() + (qtPath + "lib/").simplified();
                info.frameworkDirectory = info.installName;
                state = DylibName;
                continue;
            }

            if (part < parts.count() && parts.at(part).endsWith(libraryBundleFolderExtension)) {
                info.installName += QDir::separator() + (qtPath + "lib/").simplified();
                info.frameworkDirectory = info.installName;
                state = FrameworkName;
                continue;
            }

            if (trimmed.startsWith(QDir::separator()) == false) {      // If the line does not contain a full path, the app is using a binary Qt package.
                if (currentPart.contains(libraryBundleFolderExtension)) {
                    info.frameworkDirectory = "/Library/Frameworks/";
                    state = FrameworkName;
                } else {
                    info.frameworkDirectory = "/usr/lib/";
                    state = DylibName;
                }

                --part;
                continue;
            }

            qtPath += (currentPart + QDir::separator());
        }

        if (state == FrameworkName) {
            // remove ".framework"
            name = currentPart;
            name.chop(libraryBundleFolderExtension.length());
            info.frameworkName = currentPart;
            state = Version;
            ++part;
            continue;
        }

        if (state == DylibName) {
            name = currentPart.split(" (compatibility").first();
            info.frameworkName = name;
            info.binaryName = name.contains("Qt") && (suffix.isEmpty() ? true : !name.contains(suffix))
                              ? name.left(name.indexOf('.')) + suffix + name.mid(name.indexOf('.'))
                              : name;

            // search the additional directories for dependent libraries' binaries
            if (!QFileInfo(QDir(info.frameworkDirectory), info.binaryName).exists()) {
                foreach (const QString &additionalLibraryPath, additionalLibraryPaths) {
                    if (QFileInfo(QDir(additionalLibraryPath), info.binaryName).exists()) {
                        info.frameworkDirectory = additionalLibraryPath + QDir::separator();
                        break;
                    }
                }
            }

            info.installName += name;
            info.deployedInstallName = mainModuleInfo.librariesInstallNameDirPath()
                                       + QDir::separator()
                                       + info.binaryName;
            info.frameworkPath = info.frameworkDirectory + info.binaryName;
            info.sourceFilePath = info.frameworkPath;
            info.destinationDirectory = mainModuleInfo.librariesRelativeDirPath()
                                        + QDir::separator();
            info.binaryDirectory = info.frameworkDirectory;
            info.binaryPath = info.frameworkPath;
            state = End;
            ++part;
            continue;
        }

        if (state == Version) {
            info.version = currentPart;
            info.binaryDirectory = "Versions/" + info.version;
            info.binaryName = name + suffix;
            info.binaryPath = QDir::separator() + info.binaryDirectory + QDir::separator() + info.binaryName;
            info.installName += info.frameworkName + QDir::separator() + info.binaryDirectory + QDir::separator() + name;
            info.deployedInstallName = mainModuleInfo.librariesInstallNameDirPath()
                                       + QDir::separator()
                                       + info.frameworkName
                                       + info.binaryPath;
            // search the additional directories for dependent frameworks' bundles
            if (!QDir(info.frameworkDirectory + info.frameworkName).exists()) {
                foreach (const QString &additionalLibraryPath, additionalLibraryPaths) {
                    if (QDir(additionalLibraryPath + QDir::separator() + info.frameworkName).exists()) {
                        info.frameworkDirectory = additionalLibraryPath + QDir::separator();
                        break;
                    }
                }
            }
            info.frameworkPath = info.frameworkDirectory + info.frameworkName;
            info.sourceFilePath = info.frameworkPath + info.binaryPath;
            info.destinationDirectory = mainModuleInfo.librariesRelativeDirPath()
                                        + QDir::separator()
                                        + info.frameworkName
                                        + QDir::separator()
                                        + info.binaryDirectory;
            state = End;
        } else if (state == End) {
            break;
        }
    }

    return info;
}


QList<FrameworkInfo> Deployer::getQtFrameworks(
        const ModuleInfo &mainModuleInfo,
        const QStringList &otoolLines,
        const bool useDebugLibs,
        const QStringList &additionalLibraryPaths)
{
    QList<FrameworkInfo> libraries;
    foreach (const QString &line, otoolLines) {
        const FrameworkInfo info = parseOtoolLibraryLine(mainModuleInfo, line, useDebugLibs, additionalLibraryPaths);
        if (info.frameworkName.isEmpty() == false) {
            Logger::instance().logInfo()
                    << "Adding framework:" << endl
                    << info;
            libraries.append(info);
        }
    }
    return libraries;
}

QList<FrameworkInfo> Deployer::getQtFrameworks(
        const ModuleInfo &mainModuleInfo,
        const ModuleInfo &moduleInfo,
        bool useDebugLibs,
        const QStringList &additionalLibraryPaths)
{
    if (!moduleInfo.isValid())
        return QList<FrameworkInfo>();

    const QString binaryFilePath = moduleInfo.binaryFilePath();

    Logger::instance().logInfo()
            << "Using otool:" << endl
            << " inspecting" << binaryFilePath;
    QProcess otool;
    otool.start("otool", QStringList() << "-L" << binaryFilePath);
    otool.waitForFinished();

    if (otool.exitStatus() != QProcess::NormalExit
        || otool.exitCode() != EXIT_SUCCESS) {
        Logger::instance().logError() << otool.readAllStandardError();
    }

    QString output = otool.readAllStandardOutput();
    QStringList outputLines = output.split("\n");
    outputLines.removeFirst(); // remove line containing the binary path
    if (moduleInfo.isLibrary())
        outputLines.removeFirst(); // frameworks and dylibs print install name of themselves first.

    return getQtFrameworks(mainModuleInfo, outputLines, useDebugLibs, additionalLibraryPaths);
}

QList<FrameworkInfo> Deployer::getQtFrameworksForPaths(
        const ModuleInfo &mainModuleInfo,
        const QList<ModuleInfo> &modules,
        const bool useDebugLibs,
        const QStringList &additionalLibraryPaths)
{
    QList<FrameworkInfo> result;
    QSet<QString> existing;
    foreach (const ModuleInfo &module, modules) {
        foreach (const FrameworkInfo &info, getQtFrameworks(mainModuleInfo, module, useDebugLibs, additionalLibraryPaths)) {
            if (!existing.contains(info.frameworkPath)) { // avoid duplicates
                existing.insert(info.frameworkPath);
                result << info;
            }
        }
    }
    return result;
}

// copies everything _inside_ sourcePath to destinationPath
void Deployer::recursiveCopy(
        const QString &sourcePath,
        const QString &destinationPath,
        const bool alwaysOverwrite)
{
    static const QStringList allEntities("*");
    const QStringList files = QDir(sourcePath).entryList(allEntities, QDir::Files | QDir::NoDotAndDotDot);
    const QStringList subdirs = QDir(sourcePath).entryList(allEntities, QDir::Dirs | QDir::NoDotAndDotDot);
    if (files.empty() && subdirs.empty())
        return;

    QDir().mkpath(destinationPath);

    foreach (const QString &file, files) {
        const QString fileSourcePath = sourcePath + QDir::separator() + file;
        const QString fileDestinationPath = destinationPath + QDir::separator() + file;
        copyFilePrintStatus(fileSourcePath, fileDestinationPath, alwaysOverwrite);
    }

    foreach (const QString &dir, subdirs) {
        recursiveCopy(sourcePath + QDir::separator() + dir,
                      destinationPath + QDir::separator() + dir,
                      alwaysOverwrite);
    }
}

void Deployer::recursiveCopyAndDeploy(
        const ModuleInfo &mainModuleInfo,
        const QString &sourcePath,
        const QString &destinationPath,
        const bool useDebugLibs,
        const bool useLoaderPath,
        const bool runStrip,
        const bool alwaysOverwrite,
        const QStringList &additionalLibraryPaths)
{
    QDir().mkpath(destinationPath);

    Logger::instance().logInfo() << "copy:" << sourcePath << destinationPath;

    const QStringList files = QDir(sourcePath).entryList(QStringList() << QStringLiteral("*"), QDir::Files | QDir::NoDotAndDotDot);
    foreach (const QString &file, files) {
        const QString fileSourcePath = sourcePath % QDir::separator() % file;
        const QString fileDestinationPath = destinationPath % QDir::separator() % file;

        if (file.endsWith("_debug.dylib")) {
            continue; // Skip debug versions
        } else if (file.endsWith(QStringLiteral(".dylib"))) {
            if (copyFilePrintStatus(fileSourcePath, fileDestinationPath, alwaysOverwrite)) {
                const QStringList destinationFilePaths(fileDestinationPath);
                QList<FrameworkInfo> frameworks = getQtFrameworks(mainModuleInfo, destinationFilePaths, useDebugLibs);

                const int destinationFilesCount = destinationFilePaths.size();
                QList< ModuleInfo > destionationModules;
                destionationModules.reserve(destinationFilesCount);
                for (int i = 0; i < destinationFilesCount; ++ i) {
                    destionationModules.append(ModuleInfo(destinationFilePaths[i]));
                }

                deployQtFrameworks(frameworks,
                                   mainModuleInfo,
                                   destionationModules,
                                   useDebugLibs,
                                   useLoaderPath,
                                   runStrip,
                                   alwaysOverwrite,
                                   additionalLibraryPaths);
            }
        } else {
            copyFilePrintStatus(fileSourcePath, fileDestinationPath, alwaysOverwrite);
        }
    }

    const QStringList subdirs = QDir(sourcePath).entryList(QStringList() << QStringLiteral("*"), QDir::Dirs | QDir::NoDotAndDotDot);
    foreach (const QString &dir, subdirs) {
        recursiveCopyAndDeploy(mainModuleInfo,
                               sourcePath % QDir::separator() % dir,
                               destinationPath % QDir::separator() % dir,
                               useDebugLibs,
                               useLoaderPath,
                               runStrip,
                               alwaysOverwrite,
                               additionalLibraryPaths);
    }
}

QString Deployer::copyFramework(
        const FrameworkInfo &framework,
        const ModuleInfo &mainModuleInfo,
        const bool alwaysOverwrite)
{
    const QString &from = framework.sourceFilePath;
    if (!QFile::exists(from)) {
        Logger::instance().logError() << "no file at" << from;
        return QString();
    }

    const QChar dirSep = QDir::separator();
    const QFileInfo fromDirInfo(framework.frameworkPath
                                + dirSep
                                + framework.binaryDirectory);
    const bool fromDirIsSymLink = fromDirInfo.isSymLink();
    QString unresolvedToDir = mainModuleInfo.workingDirPath()
                              + dirSep
                              + framework.destinationDirectory;
    QString resolvedToDir;
    QString relativeLinkTarget; // will contain the link from Current to e.g. 4 in the Versions directory
    if (fromDirIsSymLink) {
        // handle the case where framework is referenced with Versions/Current
        // which is a symbolic link, so copy to target and recreate as symbolic link
        relativeLinkTarget = QDir(fromDirInfo.canonicalPath())
                .relativeFilePath(QFileInfo(fromDirInfo.symLinkTarget()).canonicalFilePath());
        resolvedToDir = QFileInfo(unresolvedToDir).path() + dirSep + relativeLinkTarget;
    } else {
        resolvedToDir = unresolvedToDir;
    }

    const QString to = resolvedToDir
                       + dirSep
                       + framework.binaryName;

    // create the (non-symlink) dir
    QDir dir;
    if (!dir.mkpath(resolvedToDir)) {
        Logger::instance().logError() << "could not create destination directory" << to;
        return QString();
    }

    if (!QFile::exists(to) || alwaysOverwrite) { // copy the binary and resources if that wasn't done before
        copyFilePrintStatus(from, to, alwaysOverwrite);

        const QFileInfo fromInfo(framework.frameworkPath);
        if (fromInfo.isDir() && fromInfo.isBundle()) {
            const QString resourcesSourcePath = framework.frameworkPath
                                                + dirSep
                                                + ModuleInfo::resourcesDirName;
            const QString resourcesDestianationPath = mainModuleInfo.librariesAbsoluteDirPath()
                                                      + dirSep
                                                      + framework.frameworkName
                                                      + dirSep
                                                      + ModuleInfo::resourcesDirName;
            recursiveCopy(resourcesSourcePath,
                          resourcesDestianationPath,
                          alwaysOverwrite);
        }
    }

    // create the Versions/Current symlink dir if necessary
    if (fromDirIsSymLink) {
        QFile::link(relativeLinkTarget, unresolvedToDir);
        Logger::instance().logInfo()
                << " linked:" << unresolvedToDir << endl
                << " to" << resolvedToDir << "(" << relativeLinkTarget << ")";
    }
    return to;
}

void Deployer::runInstallNameTool(const QStringList &options)
{
    QProcess installNametool;
    installNametool.start("install_name_tool", options);
    installNametool.waitForFinished();
    if (installNametool.exitStatus() != QProcess::NormalExit
        || installNametool.exitCode() != EXIT_SUCCESS) {
        Logger::instance().logError()
                << installNametool.readAllStandardError() << endl
                << installNametool.readAllStandardOutput();
    }
}

void Deployer::changeIdentification(const QString &id, const QString &binaryPath)
{
    Logger::instance().logInfo()
            << "Using install_name_tool:" << endl
            << " change identification in" << binaryPath << endl
            << " to" << id;
    runInstallNameTool(QStringList() << "-id" << id << binaryPath);
}

void Deployer::changeInstallName(
        const ModuleInfo &moduleInfo,
        const FrameworkInfo &framework,
        const QList<ModuleInfo> &modules,
        const bool useLoaderPath)
{
    foreach (const ModuleInfo &module, modules) {
        QString deployedInstallName;
        if (useLoaderPath) {
            const QChar dirSep = QDir::separator();
            const QString dependencyBinaryPath = moduleInfo.workingDirPath()
                                                 + dirSep
                                                 + framework.destinationDirectory
                                                 + dirSep
                                                 + framework.binaryName;

            deployedInstallName =
                    ModuleInfo::loaderPath
                    + dirSep
                    + QFileInfo(module.binaryFilePath())
                      .absoluteDir()
                      .relativeFilePath(dependencyBinaryPath);
        } else {
            deployedInstallName = framework.deployedInstallName;
        }
        changeInstallName(framework.installName, deployedInstallName, module);
    }
}

void Deployer::changeInstallName(const QString &oldName, const QString &newName, const ModuleInfo &moduleInfo)
{
    Logger::instance().logInfo()
            << "Using install_name_tool:" << endl
            << " in" << moduleInfo << endl
            << " change reference" << oldName << endl
            << " to" << newName;
    runInstallNameTool(QStringList() << "-change" << oldName << newName << moduleInfo.binaryFilePath());
}

void Deployer::runStrip(const QString &binaryPath)
{
    Logger::instance().logInfo()
            << "Using strip:" << endl
            << " stripped" << binaryPath;
    QProcess strip;
    strip.start("strip", QStringList() << "-x" << binaryPath);
    strip.waitForFinished();
    if (strip.exitStatus() != QProcess::NormalExit
        || strip.exitCode() != EXIT_SUCCESS) {
        Logger::instance().logError()
                << strip.readAllStandardError() << endl
                << strip.readAllStandardOutput();
    }
}

/*
    Deploys the the listed frameworks listed into an app bundle.
    The frameworks are searched for dependencies, which are also deployed.
    (deploying Qt3Support will also deploy QtNetwork and QtSql for example.)
    Returns a DeploymentInfo structure containing the Qt path used and a
    a list of actually deployed frameworks.
*/
Deployer::DeploymentInfo Deployer::deployQtFrameworks(
        const QList<FrameworkInfo> &frameworks,
        const ModuleInfo &mainModuleInfo,
        const QList<ModuleInfo> &modules,
        const bool useDebugLibs,
        const bool useLoaderPath,
        const bool runStrip,
        const bool alwaysOverwrite,
        const QStringList &additionalLibraryPaths)
{
    Logger::instance().logInfo() << "Deploying Qt frameworks found inside:" << modules;
    QStringList copiedFrameworks;
    DeploymentInfo deploymentInfo;
    deploymentInfo.useLoaderPath = useLoaderPath;

    QList<FrameworkInfo> frameworksInfo = frameworks;
    while (frameworksInfo.isEmpty() == false) {
        const FrameworkInfo framework = frameworksInfo.takeFirst();
        copiedFrameworks.append(framework.frameworkName);

        /* Get the qt path from the QtCore module, since all other Qt modules depend on it
         * Do not check using framework.frameworkName.contains("Qt"),
         * since Qt addon modules also start with Qt and they could be located outside the
         * Qt library's directory used by the module being deployed.
         */
        if (deploymentInfo.qtPath.isNull()
            && framework.frameworkName.startsWith("QtCore")
            && framework.frameworkDirectory.contains("/lib")) {
            deploymentInfo.qtPath = framework.frameworkDirectory;
            deploymentInfo.qtPath.chop(5); // remove "/lib/"
        }

        static const QString separatedExePath = QDir::separator()
                                                + ModuleInfo::exePath
                                                + QDir::separator();
        if (framework.installName.startsWith(separatedExePath)) {
            Logger::instance().logError() << framework.frameworkName << "already deployed, skipping.";
            continue;
        }

        // Install_name_tool the new id into the binaries
        changeInstallName(mainModuleInfo, framework, modules, useLoaderPath);

        // Copy framework to app bundle.
        const QString deployedBinaryPath = copyFramework(framework, mainModuleInfo, alwaysOverwrite);
        // Skip the rest if already was deployed.
        if (deployedBinaryPath.isNull())
            continue;

        if (runStrip)
            Deployer::runStrip(deployedBinaryPath);

        // Install_name_tool it a new id.
        changeIdentification(framework.deployedInstallName, deployedBinaryPath);
        // Check for framework dependencies

        const ModuleInfo deployedFramework(deployedBinaryPath);
        const QList<FrameworkInfo> dependencies = getQtFrameworks(mainModuleInfo, deployedFramework, useDebugLibs, additionalLibraryPaths);

        QList<ModuleInfo> deployedFrameworks;
        deployedFrameworks << deployedFramework;
        foreach (const FrameworkInfo &dependency, dependencies) {
            changeInstallName(mainModuleInfo, dependency, deployedFrameworks, useLoaderPath);

            // Deploy framework if necessary.
            if (copiedFrameworks.contains(dependency.frameworkName) == false
                && frameworksInfo.contains(dependency) == false) {
                frameworksInfo.append(dependency);
            }
        }
    }
    deploymentInfo.deployedFrameworks = copiedFrameworks;
    return deploymentInfo;
}

Deployer::DeploymentInfo Deployer::deployQtFrameworks(
        const ModuleInfo &mainModuleInfo,
        const QList<ModuleInfo> &additionalExecutables,
        const bool useDebugLibs,
        const bool runStrip,
        const bool alwaysOverwrite,
        const QStringList &additionalLibraryPaths)
{
    QList<ModuleInfo> modules;
    modules << mainModuleInfo
            << mainModuleInfo.bundleLibraryModules()
            << additionalExecutables;

    const QList<FrameworkInfo> frameworks = getQtFrameworksForPaths(mainModuleInfo, modules, useDebugLibs, additionalLibraryPaths);
    if (frameworks.isEmpty()) {
        Logger::instance().logWarning()
                << "Could not find any external Qt frameworks to deploy for" << mainModuleInfo.path() << endl
                << "Perhaps macdeployqt was already used on" << mainModuleInfo.path() << "?" << endl
                << "If so, you will need to rebuild" << mainModuleInfo.path() << "before trying again.";
        return DeploymentInfo();
    }

    return deployQtFrameworks(frameworks,
                              mainModuleInfo,
                              modules,
                              useDebugLibs,
                              !additionalExecutables.isEmpty() || mainModuleInfo.isLibrary(),
                              runStrip,
                              alwaysOverwrite,
                              additionalLibraryPaths);
}

#if QT_VERSION >= QT_VERSION_CHECK(5,0,0)
void Deployer::deployPlugins(
        const ModuleInfo &mainModuleInfo,
        const QString &pluginSourcePath,
        const QString &pluginDestinationPath,
        const DeploymentInfo &deploymentInfo,
        const bool useDebugLibs,
        const bool runStrip,
        const bool alwaysOverwrite,
        const QStringList &additionalLibraryPaths)
{
    Logger::instance().logInfo() << "Deploying plugins from" << pluginSourcePath;

    if (!pluginSourcePath.contains(deploymentInfo.pluginPath))
        return;

    // Plugin white list:
    QStringList pluginList;

    // Platform plugin:
    pluginList.append("platforms/libqcocoa.dylib");

    // Cocoa print support
    pluginList.append("printsupport/libcocoaprintersupport.dylib");

    // Accessibility
    if (deploymentInfo.deployedFrameworks.contains(QStringLiteral("QtWidgets.framework")))
        pluginList.append("accessible/libqtaccessiblewidgets.dylib");
    if (deploymentInfo.deployedFrameworks.contains(QStringLiteral("QtQuick.framework")))
        pluginList.append("accessible/libqtaccessiblequick.dylib");

    // All image formats (svg if QtSvg.framework is used)
    QStringList imagePlugins = QDir(pluginSourcePath +  QStringLiteral("/imageformats")).entryList(QStringList() << QStringLiteral("*.dylib"));
    foreach (const QString &plugin, imagePlugins) {
        if (plugin.contains(QStringLiteral("qsvg"))) {
            if (deploymentInfo.deployedFrameworks.contains(QStringLiteral("QtSvg.framework")))
                pluginList.append(QStringLiteral("imageformats/") + plugin);
        } else if (!plugin.endsWith(QStringLiteral("_debug.dylib"))) {
            pluginList.append(QStringLiteral("imageformats/") + plugin);
        }
    }

    // Sql plugins if QtSql.framework is in use
    if (deploymentInfo.deployedFrameworks.contains(QStringLiteral("QtSql.framework"))) {
        QStringList sqlPlugins = QDir(pluginSourcePath +  QStringLiteral("/sqldrivers")).entryList(QStringList() << QStringLiteral("*.dylib"));
        foreach (const QString &plugin, sqlPlugins) {
            if (!plugin.endsWith(QStringLiteral("_debug.dylib")))
                pluginList.append(QStringLiteral("sqldrivers/") + plugin);
        }
    }

    // multimedia plugins if QtMultimedia.framework is in use
    if (deploymentInfo.deployedFrameworks.contains(QStringLiteral("QtMultimedia.framework"))) {
        QStringList plugins = QDir(pluginSourcePath + QStringLiteral("/mediaservice")).entryList(QStringList() << QStringLiteral("*.dylib"));
        foreach (const QString &plugin, plugins) {
            if (!plugin.endsWith(QStringLiteral("_debug.dylib")))
                pluginList.append(QStringLiteral("mediaservice/") + plugin);
        }
        plugins = QDir(pluginSourcePath + QStringLiteral("/audio")).entryList(QStringList() << QStringLiteral("*.dylib"));
        foreach (const QString &plugin, plugins) {
            if (!plugin.endsWith(QStringLiteral("_debug.dylib")))
                pluginList.append(QStringLiteral("audio/") + plugin);
        }
    }

    foreach (const QString &plugin, pluginList) {
        QString sourcePath = pluginSourcePath % QDir::separator() % plugin;
        if (useDebugLibs) {
            // Use debug plugins if found.
            QString debugSourcePath = sourcePath.replace(".dylib", "_debug.dylib");
            if (QFile::exists(debugSourcePath))
                sourcePath = debugSourcePath;
        }

        const QString destinationPath = pluginDestinationPath % QDir::separator() % plugin;
        QDir dir;
        dir.mkpath(QFileInfo(destinationPath).path());

        if (copyFilePrintStatus(sourcePath, destinationPath, alwaysOverwrite)) {
            if (runStrip)
                Deployer::runStrip(destinationPath);
            const ModuleInfo destinationModule(destinationPath);
            const QList<FrameworkInfo> frameworks = getQtFrameworks(mainModuleInfo,
                                                                    destinationModule,
                                                                    useDebugLibs,
                                                                    additionalLibraryPaths);
            deployQtFrameworks(frameworks,
                               mainModuleInfo,
                               QList<ModuleInfo>() << destinationModule,
                               useDebugLibs,
                               deploymentInfo.useLoaderPath,
                               runStrip,
                               alwaysOverwrite,
                               additionalLibraryPaths);
        }
    }
}

void Deployer::deployQmlImport(
        const ModuleInfo &mainModuleInfo,
        const QString &importSourcePath,
        const QString &importName,
        const bool useDebugLibs,
        const bool useLoaderPath,
        const bool runStrip,
        const bool alwaysOverwrite,
        const QStringList &additionalLibraryPaths)
{
    const QString importDestinationPath = mainModuleInfo.resourcesAbsoluteDirPath()
                                          % QDir::separator() % "qml"
                                          % QDir::separator() % importName;
    recursiveCopyAndDeploy(mainModuleInfo,
                           importSourcePath,
                           importDestinationPath,
                           useDebugLibs,
                           useLoaderPath,
                           runStrip,
                           alwaysOverwrite,
                           additionalLibraryPaths);
}

// Scan qml files in qmldirs for import statements, deploy used imports from Qml2ImportsPath to Contents/Resources/qml.
void Deployer::deployQmlImports(
        const ModuleInfo &mainModuleInfo,
        QStringList &qmlDirs,
        const bool useDebugLibs,
        const bool useLoaderPath,
        const bool runStrip,
        const bool alwaysOverwrite,
        const QStringList &additionalLibraryPaths)
{
    // verify that qmlimportscanner is in BinariesPath
    const QString qmlImportScannerPath = QDir::cleanPath(QLibraryInfo::location(QLibraryInfo::BinariesPath)
                                         % QDir::separator() % "qmlimportscanner");
    if (!QFile(qmlImportScannerPath).exists()) {
        Logger::instance().logError() << "qmlimportscanner not found at" << qmlImportScannerPath << endl
                                      << "Rebuild qtdeclarative/tools/qmlimportscanner";
        return;
    }

    // run qmlimportscanner
    QString qmlImportsPath = QLibraryInfo::location(QLibraryInfo::Qml2ImportsPath);
    QProcess qmlImportScanner;
    qmlImportScanner.setProcessChannelMode(QProcess::MergedChannels);
    qmlImportScanner.start(qmlImportScannerPath, QStringList() << qmlDirs << "-importPath" << qmlImportsPath);
    if (!qmlImportScanner.waitForStarted()) {
        Logger::instance().logError() << "Could not start qmlimpoortscanner. Process error is" << qmlImportScanner.errorString();
        return;
    }

    qmlImportScanner.waitForFinished();
    QByteArray json = qmlImportScanner.readAll();

    // parse qmlimportscanner json
    QJsonDocument doc = QJsonDocument::fromJson(json);
    if (!doc.isArray()) {
        Logger::instance().logError() << "qmlimportscanner output error. Expected json array, got:" << endl
                                      << json;
        return;
    }

    // deploy each import
    foreach (const QJsonValue &importValue, doc.array()) {
        if (!importValue.isObject())
            continue;

        QJsonObject import = importValue.toObject();
        QString name = import["name"].toString();
        QString path = import["path"].toString();

        // Create the destination path from the name
        // and version (grabbed from the source path)
        // ### let qmlimportscanner provide this.
        name.replace(QLatin1Char('.'), QLatin1Char('/'));
        int secondTolast = path.length() - 2;
        QString version = path.mid(secondTolast);
        if (version.startsWith(QLatin1Char('.')))
            name.append(version);

        deployQmlImport(mainModuleInfo,
                        path,
                        name,
                        useDebugLibs,
                        useLoaderPath,
                        runStrip,
                        alwaysOverwrite,
                        additionalLibraryPaths);
    }
}
#else
void Deployer::deployPlugins(
        const ModuleInfo &mainModuleInfo,
        const QString &pluginSourcePath,
        const QString &pluginDestinationPath,
        const DeploymentInfo &deploymentInfo,
        const bool useDebugLibs,
        const bool runStrip,
        const QStringList &additionalLibraryPaths)
{
    Logger::instance().logInfo() << "Deploying plugins from" << pluginSourcePath;
    const QStringList plugins = QDir(pluginSourcePath).entryList(QStringList("*." + ModuleInfo::moduleExtension(ModuleTypeLibraryDynamicBinary)));

    foreach (const QString &pluginName, plugins) {
        if (pluginSourcePath.contains(deploymentInfo.pluginPath)) {
            QStringList deployedFrameworks = deploymentInfo.deployedFrameworks;

            // Skip the debug versions of the plugins, unless specified otherwise.
            if (!useDebugLibs && pluginName.endsWith("_debug.dylib"))
                continue;

            // Skip the release versions of the plugins, unless specified otherwise.
            if (useDebugLibs && !pluginName.endsWith("_debug.dylib"))
                continue;

            // Skip the qmltooling plugins in release mode or when QtDeclarative is not used.
            if (pluginSourcePath.contains("qmltooling") && (!useDebugLibs || deployedFrameworks.indexOf("QtDeclarative.framework") == -1))
                continue;

            // Skip the designer plugins
            if (pluginSourcePath.contains("plugins/designer"))
                continue;

            // Skip the tracing graphics system
            if (pluginName.contains("libqtracegraphicssystem"))
                continue;

#ifndef QT_GRAPHICSSYSTEM_OPENGL
            // Skip the opengl graphicssystem plugin when not in use.
            if (pluginName.contains("libqglgraphicssystem"))
                continue;
#endif
            // Deploy accessibility for Qt3Support only if the Qt3Support.framework is in use
            if (deployedFrameworks.indexOf("Qt3Support.framework") == -1 && pluginName.contains("accessiblecompatwidgets"))
                continue;

            // Deploy the svg icon plugin if QtSvg.framework is in use.
            if (deployedFrameworks.indexOf("QtSvg.framework") == -1 && pluginName.contains("svg"))
                continue;

            // Deploy the phonon plugins if phonon.framework is in use
            if (deployedFrameworks.indexOf("phonon.framework") == -1 && pluginName.contains("phonon"))
                continue;

            // Deploy the sql plugins if QtSql.framework is in use
            if (deployedFrameworks.indexOf("QtSql.framework") == -1 && pluginName.contains("sql"))
                continue;

            // Deploy the script plugins if QtScript.framework is in use
            if (deployedFrameworks.indexOf("QtScript.framework") == -1 && pluginName.contains("script"))
                continue;

            // Deploy the bearer plugins if QtNetwork.framework is in use
            if (deployedFrameworks.indexOf("QtNetwork.framework") == -1 && pluginName.contains("bearer"))
                continue;
        }

        QDir dir;
        dir.mkpath(pluginDestinationPath);

        const QString sourcePath = pluginSourcePath + QDir::separator() + pluginName;
        const QString destinationPath = pluginDestinationPath + QDir::separator() + pluginName;
        if (copyFilePrintStatus(sourcePath, destinationPath)) {
            if (runStrip)
                Deployer::runStrip(destinationPath);

            // Special case for the phonon plugin: CoreVideo is not available as a separate framework
            // on panther, link against the QuartzCore framework instead. (QuartzCore contians CoreVideo.)
            if (pluginName.contains("libphonon_qt7")) {
                changeInstallName("/System/Library/Frameworks/CoreVideo.framework/Versions/A/CoreVideo",
                        "/System/Library/Frameworks/QuartzCore.framework/Versions/A/QuartzCore",
                        ModuleInfo(destinationPath));
                // TODO: be careful when wrapping the path using ModuleInfo
            }

            // TODO: detect if bundle to wrap the bundle
            const ModuleInfo destinationModule(destinationPath);
            const QList<FrameworkInfo> frameworks = getQtFrameworks(mainModuleInfo,
                                                                    destinationModule,
                                                                    useDebugLibs,
                                                                    additionalLibraryPaths);
            deployQtFrameworks(frameworks,
                               mainModuleInfo,
                               QList<ModuleInfo>() << destinationModule,
                               useDebugLibs,
                               deploymentInfo.useLoaderPath,
                               runStrip,
                               additionalLibraryPaths);
        }
    } // foreach plugins

    QStringList subdirs = QDir(pluginSourcePath).entryList(QStringList() << "*", QDir::Dirs | QDir::NoDotAndDotDot);
    foreach (const QString &subdir, subdirs) {
        deployPlugins(mainModuleInfo,
                      pluginSourcePath + QDir::separator() + subdir,
                      pluginDestinationPath + QDir::separator() + subdir,
                      deploymentInfo,
                      useDebugLibs,
                      runStrip,
                      additionalLibraryPaths);
    }
}
#endif

void Deployer::createQtConf(const ModuleInfo &moduleInfo)
{
    if (!moduleInfo.isValid() || !moduleInfo.isBundle())
        return;

    static const QString qtConfFileName = "qt.conf";
    static const QByteArray qtConfFileContents = "[Paths]\nPlugins = PlugIns\n";
    const QString filePath = moduleInfo.resourcesAbsoluteDirPath();
    const QString fileName = filePath
                             + QDir::separator()
                             + qtConfFileName;

    QDir().mkpath(filePath);

    QFile qtconf(fileName);
    if (qtconf.exists()) {
        Logger::instance().logWarning()
                << fileName << "already exists, will not overwrite." << endl
                << "To make sure the plugins are loaded from the correct location," << endl
                << "please make sure qt.conf contains the following lines:" << endl
                << "[Paths]" << endl
                << "  Plugins = PlugIns";
        return;
    }

    if (qtconf.open(QIODevice::WriteOnly)) {
        if (qtconf.write(qtConfFileContents) != -1) {
            Logger::instance().logInfo()
                    << "Created configuration file:" << fileName << endl
                    << "This file sets the plugin search path to" << filePath;
        }
    } else {
        Logger::instance().logError() << "Failed to open the file: \"" << fileName << "\" for writing.";
    }
}

void Deployer::deployPlugins(
        const ModuleInfo &mainModuleInfo,
        const DeploymentInfo &deploymentInfo,
        const bool useDebugLibs,
        const bool runStrip,
        const bool alwaysOverwrite,
        const QStringList &additionalLibraryPaths)
{
    if (!mainModuleInfo.isValid())
        return;

    const QString pluginDestinationPath = mainModuleInfo.plugInsAbsoluteDirPath();
    deployPlugins(mainModuleInfo,
                  deploymentInfo.pluginPath,
                  pluginDestinationPath,
                  deploymentInfo,
                  useDebugLibs,
                  runStrip,
                  alwaysOverwrite,
                  additionalLibraryPaths);
}

void Deployer::changeQtFrameworks(
        const QList<FrameworkInfo> &frameworks,
        const QList<ModuleInfo> &modules,
        const QString &qtPath)
{
    Logger::instance().logInfo()
            << "Changing" << modules << "to link against" << endl
            << "Qt in" << qtPath;
    QString finalQtPath = qtPath;

    if (!qtPath.startsWith("/Library/Frameworks"))
        finalQtPath += "/lib/";

    foreach (const FrameworkInfo &framework, frameworks) {
        const QString oldBinaryId = framework.installName;
        const QString newBinaryId = finalQtPath + framework.frameworkName + framework.binaryPath;
        foreach (const ModuleInfo &module, modules)
            changeInstallName(oldBinaryId, newBinaryId, module);
    }
}

void Deployer::createDiskImage(const ModuleInfo &moduleInfo, const bool alwaysOverwrite)
{
    if (!moduleInfo.isValid())
        return;

    const QFileInfo moduleFileInfo(moduleInfo.path());
    const QString moduleBaseName = moduleFileInfo.completeBaseName();
    const QString dmgFilePath = moduleBaseName + ".dmg";

    QFile dmgFile(dmgFilePath);
    if (dmgFile.exists() && alwaysOverwrite)
        dmgFile.remove();

    if (dmgFile.exists()) {
        Logger::instance().logInfo() << "Disk image already exists, skipping .dmg creation for" << dmgFile.fileName();
        return;
    }

    Logger::instance().logInfo() << "Creating disk image (.dmg) for" << moduleInfo.path();

    // More dmg options can be found in the hdiutil man page.
    const QStringList options = QStringList()
            << "create" << dmgFilePath
            << "-srcfolder" << moduleInfo.path()
            << "-format" << "UDZO"
            << "-volname" << moduleBaseName;

    QProcess hdutil;
    hdutil.start("hdiutil", options);
    hdutil.waitForFinished(-1);
}

MacDeployQt::Shared::AbstractLoggerStream &operator<<(MacDeployQt::Shared::AbstractLoggerStream &stream, const FrameworkInfo &info)
{
    stream << "Framework name: " << info.frameworkName << endl
           << "Framework directory: " << info.frameworkDirectory << endl
           << "Framework path: " << info.frameworkPath << endl
           << "Binary directory: " << info.binaryDirectory << endl
           << "Binary name: " << info.binaryName << endl
           << "Binary path: " << info.binaryPath << endl
           << "Version: " << info.version << endl
           << "Install name: " << info.installName << endl
           << "Deployed install name: " << info.deployedInstallName << endl
           << "Source file path: " << info.sourceFilePath << endl
           << "Destination directory (relative to bundle): " << info.destinationDirectory << endl;

    return stream;
}
