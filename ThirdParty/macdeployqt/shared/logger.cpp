/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "logger.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QStringBuilder>

using namespace MacDeployQt::Shared;

Logger::Logger()
{
}

Logger::Logger(const QString &filePath, const SeverityLevels &severityLevels)
{
    init(filePath, severityLevels);
}

Logger::Logger(QIODevice *device, const SeverityLevels &severityLevels)
{
    init(device, severityLevels);
}

Logger::Logger(FILE *fileHandle, const SeverityLevels &severityLevels)
{
    init(fileHandle, severityLevels);
}

Logger::Logger(QString &string, const SeverityLevels &severityLevels)
{
    init(string, severityLevels);
}

Logger::~Logger()
{
}

Logger &Logger::instance()
{
    static Logger logger;
    return logger;
}

void Logger::init(const QString &filePath, const SeverityLevels &severityLevels)
{
    if (m_file.isOpen()) {
#if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
        m_stream.reset(0);
#else
        m_stream = QSharedPointer<BufferStream>(0);
#endif
        m_file.close();
    }

    const QFileInfo fileInfo(filePath);
    QDir dir = fileInfo.dir();
    if (!dir.exists()) {
        if (!QDir().mkpath(dir.path()))
            return;
    }

    m_file.setFileName(filePath);
    if (m_file.open(QIODevice::Append)) {
        BufferStream *const stream = new BufferStream(&m_file);
#if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
        m_stream.reset(stream);
#else
        m_stream = QSharedPointer<BufferStream>(stream);
#endif
        setSeverityLevels(severityLevels);
    }
}

void Logger::init(QIODevice *device, const SeverityLevels &severityLevels)
{
    BufferStream *const stream = new BufferStream(device);
#if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
    m_stream.reset(stream);
#else
    m_stream = QSharedPointer<BufferStream>(stream);
#endif
    setSeverityLevels(severityLevels);
}

void Logger::init(FILE *fileHandle, const SeverityLevels &severityLevels)
{
    BufferStream *const stream = new BufferStream(fileHandle);
#if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
    m_stream.reset(stream);
#else
    m_stream = QSharedPointer<BufferStream>(stream);
#endif
    setSeverityLevels(severityLevels);
}

void Logger::init(QString &string, const SeverityLevels &severityLevels)
{
    BufferStream *const stream = new BufferStream(&string);
#if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
    m_stream.reset(stream);
#else
    m_stream = QSharedPointer<BufferStream>(stream);
#endif
    setSeverityLevels(severityLevels);
}

SeverityLevels Logger::verbosityLevelToSeverityLevels(const VerbosityLevel verbosityLevel)
{
    SeverityLevels severityLevels = SeverityLevelNone;
    switch (verbosityLevel) {
        case VerbosityLevelInfo:
            severityLevels |= SeverityLevelInfo;
            // don't break
        case VerbosityLevelErrorsAndWarnings:
            severityLevels |= SeverityLevelWarning;
            // don't break
        case VerbosityLevelErrors:
            severityLevels |= SeverityLevelError;
            // don't break
        case VerbosityLevelNone:
            break;
    }

    return severityLevels;
}

void Logger::setSeverityLevels(const SeverityLevels &severityLevels)
{
    QHash<SeverityLevel, LineWriterStream *> streams;
    streams.reserve(3);
    streams.insert(SeverityLevelInfo, &m_infoStream);
    streams.insert(SeverityLevelWarning, &m_warningStream);
    streams.insert(SeverityLevelError, &m_errorStream);

    const QWeakPointer<BufferStream> noStream;
    const QWeakPointer<BufferStream> textStream = m_stream.toWeakRef();
    const QList<SeverityLevel> severityLevelsList = streams.keys();
    foreach (const SeverityLevel severityLevel, severityLevelsList) {
        LineWriterStream *const loggerStream = streams.value(severityLevel, 0);
        if (loggerStream != 0) {
            loggerStream->setStream(severityLevels.testFlag(severityLevel)
                                    ? textStream
                                    : noStream);
        }
    }
}

AbstractLoggerStream &Logger::logInfo()
{
    return m_infoStream << commonPrefix(SeverityLevelInfo);
}

AbstractLoggerStream &Logger::logWarning()
{
    return m_warningStream << commonPrefix(SeverityLevelWarning);
}

AbstractLoggerStream &Logger::logError()
{
    return m_errorStream << commonPrefix(SeverityLevelError);
}

QString Logger::commonPrefix() const
{
    QString commonPrefixString;
    if (m_stream->wasWrittenTo()) {
        QTextStream commonPrefixStream(&commonPrefixString);
        commonPrefixStream << endl;
    }

    return commonPrefixString;
}

QString Logger::commonPrefix(const SeverityLevel severityLevel) const
{
    return Logger::commonPrefix()
           % QLatin1Char(' ')
           % Logger::severityLevelString(severityLevel)
           % QLatin1String(": ");
}

QString Logger::severityLevelString(const SeverityLevel severityLevel)
{
    if (severityLevel == SeverityLevelInfo)
        return "INFO";
    else if (severityLevel == SeverityLevelWarning)
        return "WARNING";
    else if (severityLevel == SeverityLevelError)
        return "ERROR";

    return QString();
}

Logger::LineWriterStream::LineWriterStream()
{
}

Logger::LineWriterStream::LineWriterStream(const QWeakPointer<Logger::BufferStream> &bufferStream)
    : m_bufferStream(bufferStream)
{
}

Logger::LineWriterStream::~LineWriterStream()
{
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(QChar ch)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << ch;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(char ch)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << ch;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(signed short i)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << i;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(unsigned short i)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << i;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(signed int i)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << i;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(unsigned int i)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << i;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(signed long i)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << i;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(unsigned long i)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << i;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(qlonglong i)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << i;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(qulonglong i)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << i;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(float f)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << f;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(double f)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << f;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(const QString &s)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << s;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(QLatin1String s)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << s;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(const QByteArray &array)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << array;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(const char *c)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << c;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(const void *ptr)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << ptr;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(QTextStreamFunction f)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << f;

    return *this;
}

AbstractLoggerStream &Logger::LineWriterStream::operator<<(QTextStreamManipulator m)
{
    if (!m_bufferStream.isNull())
        *(m_bufferStream.data()) << m;

    return *this;
}

void Logger::LineWriterStream::setStream(const QWeakPointer<Logger::BufferStream> &bufferStream)
{
    m_bufferStream = bufferStream;
}


Logger::BufferStream::BufferStream()
{
}

Logger::BufferStream::BufferStream(QIODevice *device)
    : m_textStream(device),
      m_wasWrittenTo(false)
{
}

Logger::BufferStream::BufferStream(FILE *fileHandle, QIODevice::OpenMode openMode)
    : m_textStream(fileHandle, openMode),
      m_wasWrittenTo(false)
{
}

Logger::BufferStream::BufferStream(QString *string, QIODevice::OpenMode openMode)
    : m_textStream(string, openMode),
      m_wasWrittenTo(false)
{
}

Logger::BufferStream::BufferStream(QByteArray *array, QIODevice::OpenMode openMode)
    : m_textStream(array, openMode),
      m_wasWrittenTo(false)
{
}

Logger::BufferStream::~BufferStream()
{
}

AbstractLoggerStream &Logger::BufferStream::operator<<(QChar ch)
{
    m_wasWrittenTo = true;
    m_textStream << ch;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(char ch)
{
    m_wasWrittenTo = true;
    m_textStream << ch;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(signed short i)
{
    m_wasWrittenTo = true;
    m_textStream << i;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(unsigned short i)
{
    m_wasWrittenTo = true;
    m_textStream << i;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(signed int i)
{
    m_wasWrittenTo = true;
    m_textStream << i;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(unsigned int i)
{
    m_wasWrittenTo = true;
    m_textStream << i;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(signed long i)
{
    m_wasWrittenTo = true;
    m_textStream << i;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(unsigned long i)
{
    m_wasWrittenTo = true;
    m_textStream << i;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(qlonglong i)
{
    m_wasWrittenTo = true;
    m_textStream << i;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(qulonglong i)
{
    m_wasWrittenTo = true;
    m_textStream << i;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(float f)
{
    m_wasWrittenTo = true;
    m_textStream << f;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(double f)
{
    m_wasWrittenTo = true;
    m_textStream << f;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(const QString &s)
{
    m_wasWrittenTo = true;
    m_textStream << s;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(QLatin1String s)
{
    m_wasWrittenTo = true;
    m_textStream << s;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(const QByteArray &array)
{
    m_wasWrittenTo = true;
    m_textStream << array;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(const char *c)
{
    m_wasWrittenTo = true;
    m_textStream << c;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(const void *ptr)
{
    m_wasWrittenTo = true;
    m_textStream << ptr;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(QTextStreamFunction f)
{
    m_wasWrittenTo = true;
    m_textStream << f;

    return *this;
}

AbstractLoggerStream &Logger::BufferStream::operator<<(QTextStreamManipulator m)
{
    m_wasWrittenTo = true;
    m_textStream << m;

    return *this;
}

bool Logger::BufferStream::wasWrittenTo() const
{
    return m_wasWrittenTo;
}
