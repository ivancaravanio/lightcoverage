/****************************************************************************
**
** Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
** Contact: http://www.qt-project.org/legal
**
** This file is part of the tools applications of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Digia.  For licensing terms and
** conditions see http://qt.digia.com/licensing.  For further information
** use the contact form at http://qt.digia.com/contact-us.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Digia gives you certain additional
** rights.  These rights are described in the Digia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
**
** $QT_END_LICENSE$
**
****************************************************************************/
#include "tst_deployment_mac.h"

#include "deployer.h"

#include <QtCore/QDir>
#include <QtCore/QList>
#include <QtCore/QPair>
#include <QtCore/QDebug>
#include <QtCore/QString>
#include <QtCore/QFileInfo>
#include <QtCore/QStringList>

#include <QtTest/QTest>

Q_DECLARE_METATYPE(MacDeployQt::Shared::ModuleInfo)
Q_DECLARE_METATYPE(QList<MacDeployQt::Shared::ModuleInfo>)

using namespace MacDeployQt::Shared;

void tst_deployment_mac::testParseOtoolLibraryLine()
{
    const ModuleInfo moduleInfo("/System/Library/CoreServices/Finder.app");

    typedef QPair<QString, FrameworkInfo> OtoolLineFrameworkInfoPair;
    QList<OtoolLineFrameworkInfoPair> otoolLineExpectedFrameworkInfoList;
    const FrameworkInfo expectedQtGuiFrameworkInfo = {
        /* frameworkDirectory   */ QLatin1String("/Users/foo/build/qt-4.4/lib/"),
        /* frameworkName        */ QLatin1String("QtGui.framework"),
        /* frameworkPath        */ QLatin1String("/Users/foo/build/qt-4.4/lib/QtGui.framework"),
        /* binaryDirectory      */ QLatin1String("Versions/4"),
        /* binaryName           */ QLatin1String("QtGui%1"),
        /* binaryPath           */ QLatin1String("/Versions/4/QtGui%1"),
        /* version              */ QLatin1String("4"),
        /* installName          */ QLatin1String("/Users/foo/build/qt-4.4/lib/QtGui.framework/Versions/4/QtGui"),
        /* deployedInstallName  */ QLatin1String("@executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui%1"),
        /* sourceFilePath       */ QLatin1String("/Users/foo/build/qt-4.4/lib/QtGui.framework/Versions/4/QtGui%1"),
        /* destinationDirectory */ QLatin1String("Contents/Frameworks/QtGui.framework/Versions/4")
    };
    otoolLineExpectedFrameworkInfoList.append(
                OtoolLineFrameworkInfoPair(
                    "   /Users/foo/build/qt-4.4/lib/QtGui.framework/Versions/4/QtGui (compatibility version 4.4.0, current version 4.4.0)",
                    expectedQtGuiFrameworkInfo));

    const FrameworkInfo expectedPhononFrameworkInfo = {
        /* frameworkDirectory   */ QLatin1String("/Users/foo/build/qt-4.4/lib/"),
        /* frameworkName        */ QLatin1String("phonon.framework"),
        /* frameworkPath        */ QLatin1String("/Users/foo/build/qt-4.4/lib/phonon.framework"),
        /* binaryDirectory      */ QLatin1String("Versions/4"),
        /* binaryName           */ QLatin1String("phonon%1"),
        /* binaryPath           */ QLatin1String("/Versions/4/phonon%1"),
        /* version              */ QLatin1String("4"),
        /* installName          */ QLatin1String("/Users/foo/build/qt-4.4/lib/phonon.framework/Versions/4/phonon"),
        /* deployedInstallName  */ QLatin1String("@executable_path/../Frameworks/phonon.framework/Versions/4/phonon%1"),
        /* sourceFilePath       */ QLatin1String("/Users/foo/build/qt-4.4/lib/phonon.framework/Versions/4/phonon%1"),
        /* destinationDirectory */ QLatin1String("Contents/Frameworks/phonon.framework/Versions/4")
    };
    otoolLineExpectedFrameworkInfoList.append(
                OtoolLineFrameworkInfoPair(
                    "   /Users/foo/build/qt-4.4/lib/phonon.framework/Versions/4/phonon (compatibility version 4.1.0, current version 4.1.0)",
                    expectedPhononFrameworkInfo));

    const FrameworkInfo expectedPhonon2FrameworkInfo = {
        /* frameworkDirectory   */ QLatin1String("/usr/local/Qt-4.4.0/lib/"),
        /* frameworkName        */ QLatin1String("phonon.framework"),
        /* frameworkPath        */ QLatin1String("/usr/local/Qt-4.4.0/lib/phonon.framework"),
        /* binaryDirectory      */ QLatin1String("Versions/4"),
        /* binaryName           */ QLatin1String("phonon%1"),
        /* binaryPath           */ QLatin1String("/Versions/4/phonon%1"),
        /* version              */ QLatin1String("4"),
        /* installName          */ QLatin1String("/usr/local/Qt-4.4.0/lib/phonon.framework/Versions/4/phonon"),
        /* deployedInstallName  */ QLatin1String("@executable_path/../Frameworks/phonon.framework/Versions/4/phonon%1"),
        /* sourceFilePath       */ QLatin1String("/usr/local/Qt-4.4.0/lib/phonon.framework/Versions/4/phonon%1"),
        /* destinationDirectory */ QLatin1String("Contents/Frameworks/phonon.framework/Versions/4")
    };
    otoolLineExpectedFrameworkInfoList.append(
                OtoolLineFrameworkInfoPair(
                    "   /usr/local/Qt-4.4.0/lib/phonon.framework/Versions/4/phonon (compatibility version 4.1.0, current version 4.1.0)",
                    expectedPhonon2FrameworkInfo));

    const FrameworkInfo expectedQtGui2FrameworkInfo = {
        /* frameworkDirectory   */ QLatin1String("/Library/Frameworks/"),
        /* frameworkName        */ QLatin1String("QtGui.framework"),
        /* frameworkPath        */ QLatin1String("/Library/Frameworks/QtGui.framework"),
        /* binaryDirectory      */ QLatin1String("Versions/4"),
        /* binaryName           */ QLatin1String("QtGui%1"),
        /* binaryPath           */ QLatin1String("/Versions/4/QtGui%1"),
        /* version              */ QLatin1String("4"),
        /* installName          */ QLatin1String("QtGui.framework/Versions/4/QtGui"),
        /* deployedInstallName  */ QLatin1String("@executable_path/../Frameworks/QtGui.framework/Versions/4/QtGui%1"),
        /* sourceFilePath       */ QLatin1String("/Library/Frameworks/QtGui.framework/Versions/4/QtGui%1"),
        /* destinationDirectory */ QLatin1String("Contents/Frameworks/QtGui.framework/Versions/4")
    };
    otoolLineExpectedFrameworkInfoList.append(
                OtoolLineFrameworkInfoPair(
                    "   QtGui.framework/Versions/4/QtGui (compatibility version 4.1.0, current version 4.1.0)",
                    expectedQtGui2FrameworkInfo));

    const FrameworkInfo expectedQtGuiPhononFrameworkInfo = {
        /* frameworkDirectory   */ QLatin1String("/Library/Frameworks/"),
        /* frameworkName        */ QLatin1String("phonon.framework"),
        /* frameworkPath        */ QLatin1String("/Library/Frameworks/phonon.framework"),
        /* binaryDirectory      */ QLatin1String("Versions/4"),
        /* binaryName           */ QLatin1String("phonon%1"),
        /* binaryPath           */ QLatin1String("/Versions/4/phonon%1"),
        /* version              */ QLatin1String("4"),
        /* installName          */ QLatin1String("phonon.framework/Versions/4/phonon"),
        /* deployedInstallName  */ QLatin1String("@executable_path/../Frameworks/phonon.framework/Versions/4/phonon%1"),
        /* sourceFilePath       */ QLatin1String("/Library/Frameworks/phonon.framework/Versions/4/phonon%1"),
        /* destinationDirectory */ QLatin1String("Contents/Frameworks/phonon.framework/Versions/4")
    };
    otoolLineExpectedFrameworkInfoList.append(
                OtoolLineFrameworkInfoPair(
                    "   phonon.framework/Versions/4/QtGui (compatibility version 4.1.0, current version 4.1.0)",
                    expectedQtGuiPhononFrameworkInfo));

    const FrameworkInfo expectedQtCLuceneFrameworkInfo = {
        /* frameworkDirectory   */ QLatin1String("/Users/foo/build/qt-4.4/lib/"),
        /* frameworkName        */ QLatin1String("libQtCLucene.4.dylib"),
        /* frameworkPath        */ QLatin1String("/Users/foo/build/qt-4.4/lib/libQtCLucene%1.4.dylib"),
        /* binaryDirectory      */ QLatin1String("/Users/foo/build/qt-4.4/lib/"),
        /* binaryName           */ QLatin1String("libQtCLucene%1.4.dylib"),
        /* binaryPath           */ QString("/Users/foo/build/qt-4.4/lib/libQtCLucene%1.4.dylib"),
        /* version              */ QString(),
        /* installName          */ QLatin1String("/Users/foo/build/qt-4.4/lib/libQtCLucene.4.dylib"),
        /* deployedInstallName  */ QLatin1String("@executable_path/../Frameworks/libQtCLucene%1.4.dylib"),
        /* sourceFilePath       */ QLatin1String("/Users/foo/build/qt-4.4/lib/libQtCLucene%1.4.dylib"),
        /* destinationDirectory */ QLatin1String("Contents/Frameworks/") };
    otoolLineExpectedFrameworkInfoList.append(
                OtoolLineFrameworkInfoPair(
                    "   /Users/foo/build/qt-4.4/lib/libQtCLucene.4.dylib (compatibility version 4.4.0, current version 4.4.0)",
                    expectedQtCLuceneFrameworkInfo));

    const FrameworkInfo expectedQtCLucene2FrameworkInfo = {
        /* frameworkDirectory   */ QLatin1String("/usr/lib/"),
        /* frameworkName        */ QLatin1String("libQtCLucene.4.dylib"),
        /* frameworkPath        */ QLatin1String("/usr/lib/libQtCLucene%1.4.dylib"),
        /* binaryDirectory      */ QLatin1String("/usr/lib/"),
        /* binaryName           */ QLatin1String("libQtCLucene%1.4.dylib"),
        /* binaryPath           */ QString("/usr/lib/libQtCLucene%1.4.dylib"),
        /* version              */ QString(),
        /* installName          */ QLatin1String("libQtCLucene.4.dylib"),
        /* deployedInstallName  */ QLatin1String("@executable_path/../Frameworks/libQtCLucene%1.4.dylib"),
        /* sourceFilePath       */ QLatin1String("/usr/lib/libQtCLucene%1.4.dylib"),
        /* destinationDirectory */ QLatin1String("Contents/Frameworks/") };
    otoolLineExpectedFrameworkInfoList.append(
                OtoolLineFrameworkInfoPair(
                    "libQtCLucene.4.dylib (compatibility version 4.4.0, current version 4.4.0)",
                    expectedQtCLucene2FrameworkInfo));

    const FrameworkInfo expectedFooFrameworkInfo = {
        /* frameworkDirectory   */ QString(),
        /* frameworkName        */ QString(),
        /* frameworkPath        */ QString(),
        /* binaryDirectory      */ QString(),
        /* binaryName           */ QString(),
        /* binaryPath           */ QString(),
        /* version              */ QString(),
        /* installName          */ QString(),
        /* deployedInstallName  */ QString(),
        /* sourceFilePath       */ QString(),
        /* destinationDirectory */ QString() };
    otoolLineExpectedFrameworkInfoList.append(
                OtoolLineFrameworkInfoPair(
                    "/foo",
                    expectedFooFrameworkInfo));

    const QList<bool> useDebugLibsValues = QList<bool>() << true << false;
    static const QLatin1String firstArgumentSpecifier("%1");
    foreach (const OtoolLineFrameworkInfoPair &otoolLineFrameworkInfo, otoolLineExpectedFrameworkInfoList) {
        foreach (const bool useDebugLibsValue, useDebugLibsValues) {
            const QString libraryBinaryBaseFileNameSuffix = useDebugLibsValue ? "_debug" : "";
            const FrameworkInfo actualFrameworkInfo = Deployer::parseOtoolLibraryLine(moduleInfo, otoolLineFrameworkInfo.first, useDebugLibsValue);
            // qDebug() << actualFrameworkInfo;
            QCOMPARE(actualFrameworkInfo.frameworkDirectory,   otoolLineFrameworkInfo.second.frameworkDirectory.contains(firstArgumentSpecifier)
                                                               ? otoolLineFrameworkInfo.second.frameworkDirectory.arg(libraryBinaryBaseFileNameSuffix)
                                                               : otoolLineFrameworkInfo.second.frameworkDirectory);
            QCOMPARE(actualFrameworkInfo.frameworkName,        otoolLineFrameworkInfo.second.frameworkName.contains(firstArgumentSpecifier)
                                                               ? otoolLineFrameworkInfo.second.frameworkName.arg(libraryBinaryBaseFileNameSuffix)
                                                               : otoolLineFrameworkInfo.second.frameworkName);
            QCOMPARE(actualFrameworkInfo.frameworkPath,        otoolLineFrameworkInfo.second.frameworkPath.contains(firstArgumentSpecifier)
                                                               ? otoolLineFrameworkInfo.second.frameworkPath.arg(libraryBinaryBaseFileNameSuffix)
                                                               : otoolLineFrameworkInfo.second.frameworkPath);
            QCOMPARE(actualFrameworkInfo.binaryDirectory,      otoolLineFrameworkInfo.second.binaryDirectory.contains(firstArgumentSpecifier)
                                                               ? otoolLineFrameworkInfo.second.binaryDirectory.arg(libraryBinaryBaseFileNameSuffix)
                                                               : otoolLineFrameworkInfo.second.binaryDirectory);
            QCOMPARE(actualFrameworkInfo.binaryName,           otoolLineFrameworkInfo.second.binaryName.contains(firstArgumentSpecifier)
                                                               ? otoolLineFrameworkInfo.second.binaryName.arg(libraryBinaryBaseFileNameSuffix)
                                                               : otoolLineFrameworkInfo.second.binaryName);
            QCOMPARE(actualFrameworkInfo.binaryPath,           otoolLineFrameworkInfo.second.binaryPath.contains(firstArgumentSpecifier)
                                                               ? otoolLineFrameworkInfo.second.binaryPath.arg(libraryBinaryBaseFileNameSuffix)
                                                               : otoolLineFrameworkInfo.second.binaryPath);
            QCOMPARE(actualFrameworkInfo.version,              otoolLineFrameworkInfo.second.version.contains(firstArgumentSpecifier)
                                                               ? otoolLineFrameworkInfo.second.version.arg(libraryBinaryBaseFileNameSuffix)
                                                               : otoolLineFrameworkInfo.second.version);
            QCOMPARE(actualFrameworkInfo.installName,          otoolLineFrameworkInfo.second.installName.contains(firstArgumentSpecifier)
                                                               ? otoolLineFrameworkInfo.second.installName.arg(libraryBinaryBaseFileNameSuffix)
                                                               : otoolLineFrameworkInfo.second.installName);
            QCOMPARE(actualFrameworkInfo.deployedInstallName,  otoolLineFrameworkInfo.second.deployedInstallName.contains(firstArgumentSpecifier)
                                                               ? otoolLineFrameworkInfo.second.deployedInstallName.arg(libraryBinaryBaseFileNameSuffix)
                                                               : otoolLineFrameworkInfo.second.deployedInstallName);
            QCOMPARE(actualFrameworkInfo.sourceFilePath,       otoolLineFrameworkInfo.second.sourceFilePath.contains(firstArgumentSpecifier)
                                                               ? otoolLineFrameworkInfo.second.sourceFilePath.arg(libraryBinaryBaseFileNameSuffix)
                                                               : otoolLineFrameworkInfo.second.sourceFilePath);
            QCOMPARE(actualFrameworkInfo.destinationDirectory, otoolLineFrameworkInfo.second.destinationDirectory.contains(firstArgumentSpecifier)
                                                               ? otoolLineFrameworkInfo.second.destinationDirectory.arg(libraryBinaryBaseFileNameSuffix)
                                                               : otoolLineFrameworkInfo.second.destinationDirectory);
        }
    }
}

void tst_deployment_mac::testgetQtFrameworks()
{
    const ModuleInfo moduleInfo("/System/Library/CoreServices/Finder.app");

    const QList<bool> useDebugLibsValues = QList<bool>() << true << false;
    foreach (const bool useDebugLibsValue, useDebugLibsValues) {
        const QString libraryBinaryBaseFileNameSuffix = useDebugLibsValue ? "_debug" : "";
        {
            QStringList otool = QStringList()
            << "/Users/foo/build/qt-4.4/lib/phonon.framework/Versions/4/phonon (compatibility version 4.1.0, current version 4.1.0)"
            << "/Users/foo/build/qt-4.4/lib/QtGui.framework/Versions/4/QtGui (compatibility version 4.4.0, current version 4.4.0)"
            << "/System/Library/Frameworks/Carbon.framework/Versions/A/Carbon (compatibility version 2.0.0, current version 136.0.0)"
            << "/System/Library/Frameworks/AppKit.framework/Versions/C/AppKit (compatibility version 45.0.0, current version 949.27.0)"
            << "/Users/foo/build/qt-4.4/lib/QtCore.framework/Versions/4/QtCore (compatibility version 4.4.0, current version 4.4.0)"
            << "/usr/lib/libz.1.dylib (compatibility version 1.0.0, current version 1.2.3)"
            << "/usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 111.0.0)"
            << "/usr/lib/libstdc++.6.dylib (compatibility version 7.0.0, current version 7.4.0)"
            << "/usr/lib/libgcc_s.1.dylib (compatibility version 1.0.0, current version 1.0.0)"
            << " "
            ;

            QList<FrameworkInfo> frameworks = Deployer::getQtFrameworks(moduleInfo, otool, useDebugLibsValue);
            QCOMPARE(frameworks.count(), 3);
            QCOMPARE(frameworks.at(0).binaryName, QString("phonon%1").arg(libraryBinaryBaseFileNameSuffix));
            QCOMPARE(frameworks.at(1).binaryName, QString("QtGui%1").arg(libraryBinaryBaseFileNameSuffix));
            QCOMPARE(frameworks.at(2).binaryName, QString("QtCore%1").arg(libraryBinaryBaseFileNameSuffix));
        }
        {
            QStringList otool = QStringList()
            << "QtHelp.framework/Versions/4/QtHelp (compatibility version 4.4.0, current version 4.4.0)"
            << "libQtCLucene.4.dylib (compatibility version 4.4.0, current version 4.4.0)"
            << "QtSql.framework/Versions/4/QtSql (compatibility version 4.4.0, current version 4.4.0)"
            << "QtXml.framework/Versions/4/QtXml (compatibility version 4.4.0, current version 4.4.0)"
            << "QtGui.framework/Versions/4/QtGui (compatibility version 4.4.0, current version 4.4.0)"
            << "/System/Library/Frameworks/Carbon.framework/Versions/A/Carbon (compatibility version 2.0.0, current version 128.0.0)"
            << "/System/Library/Frameworks/AppKit.framework/Versions/C/AppKit (compatibility version 45.0.0, current version 824.42.0)"
            << "QtNetwork.framework/Versions/4/QtNetwork (compatibility version 4.4.0, current version 4.4.0)"
            << "QtCore.framework/Versions/4/QtCore (compatibility version 4.4.0, current version 4.4.0)"
            << "/usr/lib/libz.1.dylib (compatibility version 1.0.0, current version 1.2.3)"
            << "/usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 88.3.6)"
            << "/usr/lib/libstdc++.6.dylib (compatibility version 7.0.0, current version 7.4.0)"
            << "/usr/lib/libgcc_s.1.dylib (compatibility version 1.0.0, current version 1.0.0)"
            ;

            QList<FrameworkInfo> frameworks = Deployer::getQtFrameworks(moduleInfo, otool, useDebugLibsValue);
            QCOMPARE(frameworks.count(), 7);
            QCOMPARE(frameworks.at(0).binaryName, QString("QtHelp%1").arg(libraryBinaryBaseFileNameSuffix));
            QCOMPARE(frameworks.at(1).binaryName, QString("libQtCLucene%1.4.dylib").arg(libraryBinaryBaseFileNameSuffix));
            QCOMPARE(frameworks.at(2).binaryName, QString("QtSql%1").arg(libraryBinaryBaseFileNameSuffix));
            QCOMPARE(frameworks.at(3).binaryName, QString("QtXml%1").arg(libraryBinaryBaseFileNameSuffix));
            QCOMPARE(frameworks.at(4).binaryName, QString("QtGui%1").arg(libraryBinaryBaseFileNameSuffix));
            QCOMPARE(frameworks.at(5).binaryName, QString("QtNetwork%1").arg(libraryBinaryBaseFileNameSuffix));
            QCOMPARE(frameworks.at(6).binaryName, QString("QtCore%1").arg(libraryBinaryBaseFileNameSuffix));
        }
    }
}

void tst_deployment_mac::testModuleInfoApplicationBinary()
{
    const ModuleInfo moduleInfo("/usr/libexec/PlistBuddy");

    QCOMPARE(moduleInfo.isValid(), true);

    QCOMPARE(moduleInfo.type(), ModuleTypeApplicationExecutable);
    QCOMPARE(moduleInfo.isBundle(), false);
    QCOMPARE(moduleInfo.isBinary(), true);
    QCOMPARE(moduleInfo.isApplication(), true);
    QCOMPARE(moduleInfo.isLibrary(), false);

    QCOMPARE(moduleInfo.path(), QLatin1String("/usr/libexec/PlistBuddy"));
    QCOMPARE(moduleInfo.binaryFilePath(), QLatin1String("/usr/libexec/PlistBuddy"));

    QCOMPARE(moduleInfo.workingDirPath(), QLatin1String("/usr/libexec"));

    QCOMPARE(moduleInfo.bundleRelativeBinaryDirPath(), QLatin1String("."));
    QCOMPARE(moduleInfo.bundleRelativeBinaryFilePath(), QLatin1String("./PlistBuddy"));

    QCOMPARE(moduleInfo.dependenciesRelativeDirPath(), QLatin1String("."));
    QCOMPARE(moduleInfo.dependenciesAbsoluteDirPath(), QLatin1String("/usr/libexec/."));

    QCOMPARE(moduleInfo.librariesInstallNameDirPath(), QLatin1String("@executable_path/Frameworks"));

    QCOMPARE(moduleInfo.librariesRelativeDirPath(), QLatin1String("./Frameworks"));
    QCOMPARE(moduleInfo.librariesAbsoluteDirPath(), QLatin1String("/usr/libexec/./Frameworks"));

    QCOMPARE(moduleInfo.plugInsRelativeDirPath(), QLatin1String("./PlugIns"));
    QCOMPARE(moduleInfo.plugInsAbsoluteDirPath(), QLatin1String("/usr/libexec/./PlugIns"));

    QCOMPARE(moduleInfo.resourcesRelativeDirPath(), QLatin1String("./Resources"));
    QCOMPARE(moduleInfo.resourcesAbsoluteDirPath(), QLatin1String("/usr/libexec/./Resources"));

    QCOMPARE(moduleInfo.bundleLibraryModules(), QList<ModuleInfo>());

    QCOMPARE(ModuleInfo::moduleExtension(ModuleTypeApplicationExecutable), QLatin1String(""));
}

void tst_deployment_mac::testModuleInfoApplicationBundle()
{
    const ModuleInfo moduleInfo("/System/Library/CoreServices/Finder.app");

    QCOMPARE(moduleInfo.isValid(), true);

    QCOMPARE(moduleInfo.type(), ModuleTypeApplicationBundle);
    QCOMPARE(moduleInfo.isBundle(), true);
    QCOMPARE(moduleInfo.isBinary(), false);
    QCOMPARE(moduleInfo.isApplication(), true);
    QCOMPARE(moduleInfo.isLibrary(), false);

    QCOMPARE(moduleInfo.path(), QLatin1String("/System/Library/CoreServices/Finder.app"));
    QCOMPARE(moduleInfo.binaryFilePath(), QLatin1String("/System/Library/CoreServices/Finder.app/Contents/MacOS/Finder"));

    QCOMPARE(moduleInfo.workingDirPath(), QLatin1String("/System/Library/CoreServices/Finder.app"));

    QCOMPARE(moduleInfo.bundleRelativeBinaryDirPath(), QLatin1String("Contents/MacOS"));
    QCOMPARE(moduleInfo.bundleRelativeBinaryFilePath(), QLatin1String("Contents/MacOS/Finder"));

    QCOMPARE(moduleInfo.dependenciesRelativeDirPath(), QLatin1String("Contents"));
    QCOMPARE(moduleInfo.dependenciesAbsoluteDirPath(), QLatin1String("/System/Library/CoreServices/Finder.app/Contents"));

    QCOMPARE(moduleInfo.librariesInstallNameDirPath(), QLatin1String("@executable_path/../Frameworks"));

    QCOMPARE(moduleInfo.librariesRelativeDirPath(), QLatin1String("Contents/Frameworks"));
    QCOMPARE(moduleInfo.librariesAbsoluteDirPath(), QLatin1String("/System/Library/CoreServices/Finder.app/Contents/Frameworks"));

    QCOMPARE(moduleInfo.plugInsRelativeDirPath(), QLatin1String("Contents/PlugIns"));
    QCOMPARE(moduleInfo.plugInsAbsoluteDirPath(), QLatin1String("/System/Library/CoreServices/Finder.app/Contents/PlugIns"));

    QCOMPARE(moduleInfo.resourcesRelativeDirPath(), QLatin1String("Contents/Resources"));
    QCOMPARE(moduleInfo.resourcesAbsoluteDirPath(), QLatin1String("/System/Library/CoreServices/Finder.app/Contents/Resources"));

    QCOMPARE(moduleInfo.bundleLibraryModules(), QList<ModuleInfo>());

    QCOMPARE(ModuleInfo::moduleExtension(ModuleTypeApplicationBundle), QLatin1String("app"));
}

void tst_deployment_mac::testModuleInfoLibraryBinary()
{
    QFileInfo systemLibraryInfo("/usr/lib/libSystem.dylib");
    if (systemLibraryInfo.isSymLink())
        systemLibraryInfo.setFile(systemLibraryInfo.symLinkTarget());

    const QString moduleName = systemLibraryInfo.fileName();
    const QString moduleDirPath = systemLibraryInfo.canonicalPath();
    const QString modulePath = systemLibraryInfo.canonicalFilePath();

    const ModuleInfo moduleInfo(modulePath);

    QCOMPARE(moduleInfo.isValid(), true);

    QCOMPARE(moduleInfo.type(), ModuleTypeLibraryDynamicBinary);
    QCOMPARE(moduleInfo.isBundle(), false);
    QCOMPARE(moduleInfo.isBinary(), true);
    QCOMPARE(moduleInfo.isApplication(), false);
    QCOMPARE(moduleInfo.isLibrary(), true);

    QCOMPARE(moduleInfo.path(), modulePath);
    QCOMPARE(moduleInfo.binaryFilePath(), modulePath);

    QCOMPARE(moduleInfo.workingDirPath(), moduleDirPath);

    QCOMPARE(moduleInfo.bundleRelativeBinaryDirPath(), QLatin1String("."));
    QCOMPARE(moduleInfo.bundleRelativeBinaryFilePath(), QLatin1String("./") + moduleName);

    QCOMPARE(moduleInfo.dependenciesRelativeDirPath(), QLatin1String("."));
    QCOMPARE(moduleInfo.dependenciesAbsoluteDirPath(), moduleDirPath + QLatin1String("/."));

    QCOMPARE(moduleInfo.librariesInstallNameDirPath(), QLatin1String("@loader_path/Libraries"));

    QCOMPARE(moduleInfo.librariesRelativeDirPath(), QLatin1String("./Libraries"));
    QCOMPARE(moduleInfo.librariesAbsoluteDirPath(), moduleDirPath + QLatin1String("/./Libraries"));

    QCOMPARE(moduleInfo.plugInsRelativeDirPath(), QLatin1String("./PlugIns"));
    QCOMPARE(moduleInfo.plugInsAbsoluteDirPath(), moduleDirPath + QLatin1String("/./PlugIns"));

    QCOMPARE(moduleInfo.resourcesRelativeDirPath(), QLatin1String("./Resources"));
    QCOMPARE(moduleInfo.resourcesAbsoluteDirPath(), moduleDirPath + QLatin1String("/./Resources"));

    QCOMPARE(moduleInfo.bundleLibraryModules(), QList<ModuleInfo>());

    QCOMPARE(ModuleInfo::moduleExtension(ModuleTypeLibraryDynamicBinary), QLatin1String("dylib"));
}

void tst_deployment_mac::testModuleInfoLibraryBundle()
{
    const QString modulePath = "/System/Library/Frameworks/System.framework";
    const ModuleInfo moduleInfo(modulePath);

    QCOMPARE(moduleInfo.isValid(), true);

    QCOMPARE(moduleInfo.type(), ModuleTypeLibraryBundle);
    QCOMPARE(moduleInfo.isBundle(), true);
    QCOMPARE(moduleInfo.isBinary(), false);
    QCOMPARE(moduleInfo.isApplication(), false);
    QCOMPARE(moduleInfo.isLibrary(), true);

    QFileInfo libraryBinaryFileInfo(modulePath + "/Versions/Current/System");
    QCOMPARE(libraryBinaryFileInfo.exists(), true);

    if (libraryBinaryFileInfo.isSymLink())
        libraryBinaryFileInfo.setFile(libraryBinaryFileInfo.symLinkTarget());

    QCOMPARE(moduleInfo.path(), modulePath);
    QCOMPARE(moduleInfo.binaryFilePath(), libraryBinaryFileInfo.canonicalFilePath());

    QCOMPARE(moduleInfo.workingDirPath(), modulePath);

    const QString bundleRelativeBinaryDirPath = QDir(modulePath).relativeFilePath(libraryBinaryFileInfo.canonicalPath());
    const QString bundleAbsoluteBinaryDirPath = modulePath
                                                + QDir::separator()
                                                + bundleRelativeBinaryDirPath;
    QCOMPARE(moduleInfo.bundleRelativeBinaryDirPath(), bundleRelativeBinaryDirPath);
    QCOMPARE(moduleInfo.bundleRelativeBinaryFilePath(), bundleRelativeBinaryDirPath + QDir::separator() + libraryBinaryFileInfo.fileName());

    QCOMPARE(moduleInfo.dependenciesRelativeDirPath(), bundleRelativeBinaryDirPath);
    QCOMPARE(moduleInfo.dependenciesAbsoluteDirPath(), bundleAbsoluteBinaryDirPath);

    QCOMPARE(moduleInfo.librariesInstallNameDirPath(), QLatin1String("@loader_path/Libraries"));

    QCOMPARE(moduleInfo.librariesRelativeDirPath(), bundleRelativeBinaryDirPath + "/Libraries");
    QCOMPARE(moduleInfo.librariesAbsoluteDirPath(), bundleAbsoluteBinaryDirPath + "/Libraries");

    QCOMPARE(moduleInfo.plugInsRelativeDirPath(), bundleRelativeBinaryDirPath + "/PlugIns");
    QCOMPARE(moduleInfo.plugInsAbsoluteDirPath(), bundleAbsoluteBinaryDirPath + "/PlugIns");

    QCOMPARE(moduleInfo.resourcesRelativeDirPath(), bundleRelativeBinaryDirPath + "/Resources");
    QCOMPARE(moduleInfo.resourcesAbsoluteDirPath(), bundleAbsoluteBinaryDirPath + "/Resources");

    QCOMPARE(moduleInfo.bundleLibraryModules(), QList<ModuleInfo>());

    QCOMPARE(ModuleInfo::moduleExtension(ModuleTypeLibraryBundle), QLatin1String("framework"));
}

QTEST_MAIN(tst_deployment_mac)
