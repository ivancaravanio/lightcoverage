#include "MainController.h"

#include "Data/Room.h"
#include "MainWindow.h"
#include "Utilities/GeneralUtilities.h"
#include "Utilities/RoomSerializer.h"
#include "Utilities/WidgetUtilities.h"

#include <QApplication>
#include <QDir>
#include <QFileDialog>
#include <QMessageBox>

using namespace LightCoverage;

MainController::MainController( QObject* parent )
    : QObject( parent )
{
}

MainController::~MainController()
{
    // the order is important; first delete the view then the model
    m_mainWindow.reset();
    m_room.reset();
}

void MainController::init( const QString& roomFilePath )
{
    MainWindow* const mainWindow = new MainWindow;
    connect( mainWindow, SIGNAL(loadRoomRequested(QString)), this, SLOT(loadRoomInteractively(QString)) );
    connect( mainWindow, SIGNAL(saveRoomRequested()), this, SLOT(saveRoom()) );
    connect( mainWindow, SIGNAL(clearRoomRequested()), this, SLOT(clearRoom()) );
    m_mainWindow.reset( mainWindow );

    this->setRoom( roomFilePath );
    mainWindow->setGeometry( WidgetUtilities::scaledConcentricCurrentDesktopGeometry( 3.0 / 4.0, true, mainWindow ) );
    mainWindow->show();
}

bool MainController::setRoom( const QString& roomFilePath )
{
    QString existingRoomFilePath;
    const bool isNewRoomFilePathFilled = ! roomFilePath.isEmpty();
    if ( isNewRoomFilePathFilled )
    {
        existingRoomFilePath = GeneralUtilities::toExistingCanonicalFilePath( roomFilePath );
        if ( existingRoomFilePath.isEmpty() )
        {
            return false;
        }
    }

    Room* newRoom = nullptr;
    if ( isNewRoomFilePathFilled )
    {
        Room* const roomCandidate = MainController::room( existingRoomFilePath );
        if ( roomCandidate == nullptr )
        {
            return false;
        }

        if ( ! m_room.isNull() )
        {
            if ( *roomCandidate == *m_room )
            {
                return true;
            }
        }

        newRoom = roomCandidate;
    }

    this->setRoom( newRoom );
    m_roomFilePath = existingRoomFilePath;
    m_mainWindow->setRoomFilePath( m_roomFilePath );

    return true;
}

void MainController::setRoom( Room* const room )
{
    m_mainWindow->setRoom( room );
    m_room.reset( room );
}

void MainController::clearRoom()
{
    this->setRoom( QString() );
}

Room* MainController::room( const QString& roomFilePath )
{
    if ( roomFilePath.isEmpty() )
    {
        return nullptr;
    }

    const Room newRoomCandidate = RoomSerializer::deserialize( roomFilePath );
    return newRoomCandidate.isValid()
           ? new Room( newRoomCandidate )
           : nullptr;
}

void MainController::loadRoomInteractively( const QString& roomFilePath )
{
    QString finalRoomFilePath;
    if ( roomFilePath.isEmpty() )
    {
        finalRoomFilePath = QFileDialog::getOpenFileName( qApp->activeWindow(),
                                                          tr( "Select Room..." ),
                                                          m_roomFilePath.isEmpty()
                                                          ? QString()
                                                          : QFileInfo( m_roomFilePath ).canonicalPath() );
        if ( finalRoomFilePath.isEmpty() )
        {
            return;
        }
    }
    else
    {
        finalRoomFilePath = roomFilePath;
    }

    if ( ! this->setRoom( finalRoomFilePath ) )
    {
        QMessageBox::critical( qApp->activeWindow(),
                               qAppName(),
                               tr( "%1 does not contain valid room configuration." )
                               .arg( QDir::toNativeSeparators( finalRoomFilePath ) ) );
    }
}

void MainController::saveRoom()
{
    if ( m_roomFilePath.isEmpty()
         || m_room.isNull() )
    {
        return;
    }

    RoomSerializer::serialize( m_roomFilePath, *m_room );
}
