#include "RoomDisplayWidget.h"

#include "Data/Room.h"

#include <QPainter>
#include <QRectF>
#include <QSizeF>

using namespace LightCoverage;

using namespace LightCoverage::Data;
using namespace LightCoverage::Utilities;

RoomDisplayWidget::RoomDisplayWidget( QWidget* const parent, Qt::WindowFlags f )
    : QWidget( parent, f )
{
}

RoomDisplayWidget::~RoomDisplayWidget()
{
}

void RoomDisplayWidget::refresh( const quint64& traits )
{
    const bool shouldBuildColumnsCache = traits == ChangeNotifier::s_invalidTrait
                                         || ( traits & Room::TraitColumns );
    const bool shouldBuildIlluminatedRegions = shouldBuildColumnsCache
                                               || ( traits & Room::TraitLightSourcePosition );
    if ( shouldBuildColumnsCache )
    {
        this->buildColumnsCache();
    }

    if ( shouldBuildIlluminatedRegions )
    {
        this->buildIlluminatedRegionsCache();
    }

    this->buildCache();
    this->update();
}

Rectangle2D RoomDisplayWidget::drawableRect() const
{
    return Rectangle2D( this->rect() );
}

void RoomDisplayWidget::paintEvent( QPaintEvent* event )
{
    Q_UNUSED( event )

    Room* const room = this->room();
    if ( room == nullptr )
    {
        this->QWidget::paintEvent( event );
        return;
    }

    if ( ! room->isValid() )
    {
        this->QWidget::paintEvent( event );
        return;
    }

    const QPair< QRectF, QMatrix > roomScreenRectMatrix = this->roomScreenRectMatrix();
    const QRectF& roomScreenRect = roomScreenRectMatrix.first;
    const QMatrix& roomScreenMatrix = roomScreenRectMatrix.second;

    QPainter painter( this );
    painter.setRenderHint( QPainter::Antialiasing );

    const QPen borderPen = RoomDisplayWidget::borderPen();

    painter.setPen( borderPen );
    painter.setBrush( QBrush( s_shadowColor ) );
    painter.drawRect( roomScreenRect );

    const int illuminatedRegionsCount = m_illuminatedRegions.size();
    if ( illuminatedRegionsCount > 0 )
    {
        painter.setPen( QPen( s_illuminationColor ) );
        painter.setBrush( QBrush( s_illuminationColor ) );

        for ( int i = 0; i < illuminatedRegionsCount; ++ i )
        {
            painter.drawConvexPolygon( roomScreenMatrix.map( m_illuminatedRegions[ i ] ) );
        }
    }

    painter.setPen( borderPen );

    const int columnsCount = m_columns.size();
    if ( columnsCount > 0 )
    {
        painter.setBrush( QBrush( s_columnColor ) );

        for ( int i = 0; i < columnsCount; ++ i )
        {
            painter.drawConvexPolygon( roomScreenMatrix.map( m_columns[ i ] ) );
        }
    }

    painter.setBrush( Qt::transparent );
    painter.drawRect( roomScreenRect );

    const QRectF lightSourceRect = roomScreenMatrix.mapRect( room->lightSourceBoundingRect().toQRectF() );

    painter.setPen( borderPen );
    painter.setBrush( s_lightSourceColor );

    static const int degreeDiscretesCount = 16;
    painter.drawPie( lightSourceRect, 0 * degreeDiscretesCount, 360 * degreeDiscretesCount );

    const QPointF lightSourceNewPos = lightSourceRect.center();
    const QRectF circleCorrectionRect( lightSourceNewPos + QPointF( 0.0, -s_borderThickness ),
                                       QPointF( lightSourceRect.right() -s_borderThickness,
                                                lightSourceNewPos.y() + s_borderThickness ) );
    painter.setPen( s_lightSourceColor );
    painter.drawRect( circleCorrectionRect );

#ifdef DRAW_ILLUMINATED_POINTS
    painter.setBrush( QBrush( QColor( 255, 0, 0, 127 ) ) );

    const QFontMetrics fm = this->fontMetrics();
    const QTextOption textOption( Qt::AlignCenter );

    static const QString vertexDescriptionTemplate = RoomDisplayWidget::vertexDescriptionTemplate();

    const QList< Room::VertexSortData >& farthestIlluminatedPoints = room->farthestIlluminatedPoints();
    const int farthestIlluminatedPointsCount = farthestIlluminatedPoints.size();
    for ( int i = 0;
          i < farthestIlluminatedPointsCount;
          ++ i )
    {
        const Room::VertexSortData& illuminatedPtData = farthestIlluminatedPoints[ i ];

        const Vector2D& roomLocalIlluminatedPt = illuminatedPtData.vertex();
        const QString illuminatedPtDescription = vertexDescriptionTemplate
                                                 .arg( i ).arg( illuminatedPtData.polygonId() )
                                                 .arg( roomLocalIlluminatedPt.x(), 0, 'f', 2 ).arg( roomLocalIlluminatedPt.y(), 0, 'f', 2 );

        const QPointF illuminatedPt = roomScreenMatrix.map( QPointF( roomLocalIlluminatedPt ) );
        const QRectF illuminatedPtDescriptionRect = Rectangle2D( Size( fm.size( Qt::TextExpandTabs,
                                                                                illuminatedPtDescription ) ) * 1.2 )
                                                    .movedCenter( Vector2D( illuminatedPt ) ).toQRectF();
        painter.setPen( s_borderColor );
        painter.drawRect( illuminatedPtDescriptionRect );

        painter.setPen( Qt::black );
        painter.drawText( illuminatedPtDescriptionRect, illuminatedPtDescription, textOption );
    }
#endif
}

void RoomDisplayWidget::mousePressEvent( QMouseEvent* event )
{
    QWidget::mousePressEvent( event );
    this->processMouseEvent( event );
}

void RoomDisplayWidget::mouseMoveEvent( QMouseEvent* event )
{
    QWidget::mouseMoveEvent( event );
    this->processMouseEvent( event );
}

void RoomDisplayWidget::mouseReleaseEvent( QMouseEvent* event )
{
    QWidget::mouseReleaseEvent( event );
    this->processMouseEvent( event );
}

void RoomDisplayWidget::buildIlluminatedRegionsCache()
{
    m_illuminatedRegions.clear();
    Room* const room = this->room();
    if ( room == nullptr )
    {
        return;
    }

    const QList< Triangle2D > illuminatedRegions = room->illuminatedRegions();
    const int illuminatedRegionsCount = illuminatedRegions.size();
    m_illuminatedRegions.reserve( illuminatedRegionsCount );
    for ( int i = 0; i < illuminatedRegionsCount; ++ i )
    {
        m_illuminatedRegions.append( illuminatedRegions[ i ].toQPolygonF() );
    }
}

void RoomDisplayWidget::buildColumnsCache()
{
    m_columns.clear();
    Room* const room = this->room();
    if ( room == nullptr )
    {
        return;
    }

    const QList< Polygon2D >& columns = room->columns();
    const int columnsCount = columns.size();
    m_columns.reserve( columnsCount );
    for ( int i = 0; i < columnsCount; ++ i )
    {
        m_columns.append( columns[ i ].toQPolygonF() );
    }
}

void RoomDisplayWidget::buildCache()
{
    this->buildIlluminatedRegionsCache();
    this->buildColumnsCache();
}

QPen RoomDisplayWidget::borderPen()
{
    return QPen( QBrush( s_borderColor ), s_borderThickness, Qt::SolidLine, Qt::SquareCap, Qt::MiterJoin );
}

#ifdef DRAW_ILLUMINATED_POINTS
QString RoomDisplayWidget::vertexDescriptionTemplate()
{
    QString descriptionTemplate;
    QTextStream ts( & descriptionTemplate, QIODevice::WriteOnly | QIODevice::Text );
    ts << "i( %1 ), p( %2 )" << endl
       << "x( %3 )" << endl
       << "y( %4 )";

    return descriptionTemplate;
}
#endif
