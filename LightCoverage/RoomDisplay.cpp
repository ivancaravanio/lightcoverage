#include "RoomDisplay.h"

#include "Utilities/ClearColorAlternator.h"
#include "Utilities/GeneralUtilities.h"
#include "Utilities/LineWidthAlternator.h"
#include "Utilities/MathUtilities.h"
#include "Utilities/OpenGlErrorLogger.h"
#include "Utilities/OpenGlUtilities.h"

#include <QDebug>
#include <qmath.h>

using namespace LightCoverage;

const QList< GLuint > RoomDisplay::s_allOrderedGenericVertexAttributeIndices =
        QList< GLuint >()
        << RoomDisplay::GenericVertexAttributeIndexVertexPos;

const QList< RoomDisplay::GraphicEntity > RoomDisplay::s_orderedEntities =
        QList< RoomDisplay::GraphicEntity >()
        << RoomDisplay::GraphicEntityRoom
        << RoomDisplay::GraphicEntityIlluminatedArea
        << RoomDisplay::GraphicEntityColumn
        << RoomDisplay::GraphicEntityLightSource;

RoomDisplay::RoomDisplay( const QGLFormat& aOpenGlFormat, QWidget* const parent )
    : QGLWidget( aOpenGlFormat, parent )
    , m_shaderProgram( aOpenGlFormat )
    , m_vao( OpenGlUtilities::s_invalidUnsignedGlValue )
    , m_vbo( OpenGlUtilities::s_invalidUnsignedGlValue )
    , m_isGlInitialized( false )
{
}

RoomDisplay::~RoomDisplay()
{
    this->disposeOpenGlResources();
}

void RoomDisplay::initializeGL()
{
    this->init();
}

void RoomDisplay::resizeGL( int w, int h )
{
    Q_UNUSED( w )
    Q_UNUSED( h )

    ViewportAlternator viewportAlternator = this->viewportAlternator( false );
    viewportAlternator.change();
}

void RoomDisplay::paintGL()
{
    if ( ! OpenGlUtilities::isUnsignedGlValueValid( m_vao )
         || ! OpenGlUtilities::isUnsignedGlValueValid( m_vbo ) )
    {
        return;
    }

    glClear( GL_COLOR_BUFFER_BIT );
    LOG_OPENGL_ERROR();

    OpenGlCapabilityEnabler cullFaceEnabler( GL_CULL_FACE, true, false );
    cullFaceEnabler.change();

    OpenGlObjectBinder cullFaceModeAlternator(
        OpenGlObjectBinder::ObjectTypeCullFaceMode,
        GL_BACK,
        OpenGlUtilities::invalidEnumGlValue(),
        false );
    cullFaceModeAlternator.bind();

    m_shaderProgram.activate();

    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    this->enableGenericVertexAttributeArrays( true );

    GLint vertexStartIndex = 0;
    const GLint colorUniformVarLoc = this->uniformVariableCachedLocation( UniformVariableIndexColor );

    GraphicEntity lastDrawnEntity = GraphicEntityInvalid;
    const int entitiesCount = m_entitiesDrawData.size();
    for ( int i = 0; i < entitiesCount; ++ i )
    {
        const GraphicEntityDrawData& graphicEntityDrawData = m_entitiesDrawData[ i ];
        const int verticesCount = graphicEntityDrawData.vertices().size();

        const GraphicEntity entity = graphicEntityDrawData.entity();
        if ( entity != lastDrawnEntity )
        {
            const QColor color = RoomDisplay::color( entity );
            m_shaderProgram.setUniformColor( colorUniformVarLoc, color );
            lastDrawnEntity = entity;
        }

        glDrawArrays( graphicEntityDrawData.openGLPrimitive(),
                      vertexStartIndex,
                      verticesCount );
        LOG_OPENGL_ERROR();

        vertexStartIndex += verticesCount;
    }

    m_shaderProgram.setUniformColor( colorUniformVarLoc, s_borderColor );

    LineWidthAlternator lineWidthAlternator( s_borderThickness );
    lineWidthAlternator.change();

    vertexStartIndex = 0;
    for ( int i = 0; i < entitiesCount; ++ i )
    {
        const GraphicEntityDrawData& graphicEntityDrawData = m_entitiesDrawData[ i ];
        const int verticesCount = graphicEntityDrawData.vertices().size();
        if ( graphicEntityDrawData.hasBorder() )
        {
            const bool isLightSourceEntity = graphicEntityDrawData.entity() == GraphicEntityLightSource;
            glDrawArrays( GL_LINE_LOOP,
                          isLightSourceEntity
                          ? vertexStartIndex + 1
                          : vertexStartIndex,
                          isLightSourceEntity
                          ? verticesCount - 1
                          : verticesCount );
            LOG_OPENGL_ERROR();
        }

        vertexStartIndex += verticesCount;
    }

    vaoBinder.unbind();

    cullFaceModeAlternator.unbind();
    cullFaceEnabler.rollback();

    this->enableGenericVertexAttributeArrays( false );

    m_shaderProgram.deactivate();
}

void RoomDisplay::disposeOpenGlResources()
{
    if ( OpenGlUtilities::isUnsignedGlValueValid( m_vao ) )
    {
        glDeleteVertexArrays( 1, & m_vao );
        LOG_OPENGL_ERROR();

        m_vao = OpenGlUtilities::s_invalidUnsignedGlValue;
    }

    QVector< GLuint > vbosToDispose;
    if ( OpenGlUtilities::isUnsignedGlValueValid( m_vbo ) )
    {
        vbosToDispose.append( m_vbo );
        m_vao = OpenGlUtilities::s_invalidUnsignedGlValue;
    }

    const int vbosToDisposeCount = vbosToDispose.size();
    if ( vbosToDisposeCount > 0 )
    {
        glDeleteBuffers( vbosToDisposeCount, vbosToDispose.constData() );
        LOG_OPENGL_ERROR();
    }
}

QRect RoomDisplay::viewport( const int w, const int h ) const
{
    Q_UNUSED( w )
    Q_UNUSED( h )

    return this->roomScreenRectMatrix().first.toRect();
}

QRect RoomDisplay::viewport() const
{
    return this->viewport( this->width(), this->height() );
}

ViewportAlternator RoomDisplay::viewportAlternator( const bool autoRollback ) const
{
    return this->viewportAlternator(
                this->viewport(),
                autoRollback );
}

OpenGlObjectBinder RoomDisplay::vaoBinder( const bool autoUnbind ) const
{
    return this->vaoBinder( m_vao, autoUnbind );
}

OpenGlObjectBinder RoomDisplay::vboBinder( const bool autoUnbind ) const
{
    return this->vboBinder( m_vbo, autoUnbind );
}

void RoomDisplay::applyRoom( const bool shouldUpdateUi )
{
    this->resetEntitiesDrawData();
    this->resetProjectionMatrix();

    if ( shouldUpdateUi )
    {
        this->updateGL();
    }
}

void RoomDisplay::applyRoomOpenGlData( const bool shouldUpdateUi )
{
    this->applyEntitiesDrawData();
    this->applyProjectionMatrix();

    if ( shouldUpdateUi )
    {
        this->updateGL();
    }
}

QVector< Vector3Df > RoomDisplay::vertices( const Triangle2D& triangle )
{
    const Triangle2D counterClockwiseWindingOrderTriangle = triangle.havingWindingOrder( false );
    return GeneralUtilities::convertedCollection< QVector< Vector2D >, QVector< Vector3Df > >( counterClockwiseWindingOrderTriangle.vertices() );
}

QVector< Vector3Df > RoomDisplay::vertices( const QList< Triangle2D >& triangles )
{
    QVector< Vector3Df > vertices;

    const int trianglesCount = triangles.size();
    if ( trianglesCount == 0 )
    {
        return vertices;
    }

    vertices.reserve( trianglesCount * Triangle2D::s_verticesCount );
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        const Triangle2D& triangle = triangles[ i ];
        vertices << RoomDisplay::vertices( triangle );
    }

    return vertices;
}

QVector< Vector3Df > RoomDisplay::vertices( const QList< Triangle2D* >& triangles )
{
    QVector< Vector3Df > vertices;

    const int trianglesCount = triangles.size();
    if ( trianglesCount == 0 )
    {
        return vertices;
    }

    vertices.reserve( trianglesCount * Triangle2D::s_verticesCount );
    for ( int i = 0; i < trianglesCount; ++ i )
    {
        const Triangle2D* const triangle = triangles[ i ];
        vertices << RoomDisplay::vertices( *triangle );
    }

    return vertices;
}

QVector< Vector3Df > RoomDisplay::vertices( const QList< Polygon2D >& polygons )
{
    QVector< Vector3Df > vertices;
    foreach ( const Polygon2D& polygon, polygons )
    {
        const QList< Triangle2D* > triangles = polygon.toTriangles();
        vertices << RoomDisplay::vertices( triangles );

        qDeleteAll( triangles );
    }

    return vertices;
}

QList< RoomDisplay::GraphicEntityDrawData > RoomDisplay::graphicEntityDrawDataCalculated( const RoomDisplay::GraphicEntity graphicEntity ) const
{
    Room* const room = this->room();
    if ( room == nullptr )
    {
        return QList< RoomDisplay::GraphicEntityDrawData >();
    }

    switch ( graphicEntity )
    {
        case GraphicEntityRoom:
        {
            const QScopedPointer< Polygon2D > roomPoly( this->roomRect().toPolygon2D() );
            const QPair< OpenGlUtilities::GeometricPrimitiveType, QVector< Vector2D > > primitiveTypeAndVertices = roomPoly->toTriangleFanStrip();
            return QList< RoomDisplay::GraphicEntityDrawData >()
                   << GraphicEntityDrawData( graphicEntity,
                                             primitiveTypeAndVertices.first,
                                             GeneralUtilities::convertedCollection< QVector< Vector2D >, QVector< Vector3Df > >( primitiveTypeAndVertices.second ) );
        }

        case GraphicEntityColumn:
        {
            QList< RoomDisplay::GraphicEntityDrawData > primitivesVertices;

            const QList< Polygon2D >& columns = room->columns();
            const int columnsCount = columns.size();

            primitivesVertices.reserve( columnsCount );
            for ( int i = 0; i < columnsCount; ++ i )
            {
                const QPair< OpenGlUtilities::GeometricPrimitiveType, QVector< Vector2D > > primitiveTypeAndVertices = columns[ i ].toTriangleFanStrip();
                const GraphicEntityDrawData entityDrawData(
                            graphicEntity,
                            primitiveTypeAndVertices.first,
                            GeneralUtilities::convertedCollection< QVector< Vector2D >, QVector< Vector3Df > >( primitiveTypeAndVertices.second ) );
                primitivesVertices.append( entityDrawData );
            }

            return primitivesVertices;
        }

        case GraphicEntityIlluminatedArea:
            return QList< RoomDisplay::GraphicEntityDrawData >()
                   << GraphicEntityDrawData( graphicEntity,
                                             OpenGlUtilities::GeometricPrimitiveTypeTriangle,
                                             RoomDisplay::vertices( room->illuminatedRegions() ) );

        case GraphicEntityLightSource:
            return QList< RoomDisplay::GraphicEntityDrawData >()
                   << GraphicEntityDrawData( graphicEntity,
                                             OpenGlUtilities::GeometricPrimitiveTypeTriangleFan,
                                             RoomDisplay::discreteCircleTriangleFanVertices( room->lightSourcePosition(), room->lightSourceRadius() ) );
    }

    return QList< RoomDisplay::GraphicEntityDrawData >();
}

QColor RoomDisplay::color( const RoomDisplay::GraphicEntity graphicEntity )
{
    switch ( graphicEntity )
    {
        case GraphicEntityRoom:            return s_shadowColor;
        case GraphicEntityIlluminatedArea: return s_illuminationColor;
        case GraphicEntityColumn:          return s_columnColor;
        case GraphicEntityLightSource:     return s_lightSourceColor;
    }

    return QColor();
}

void RoomDisplay::setProjectionMatrix( const QMatrix4x4& aProjectionMatrix )
{
    if ( aProjectionMatrix == m_projectionMatrix )
    {
        return;
    }

    m_projectionMatrix = aProjectionMatrix;
    this->applyProjectionMatrix();
}

QMatrix4x4 RoomDisplay::projectionMatrixCalculated() const
{
    Room* const room = this->room();
    if ( room == nullptr )
    {
        return QMatrix4x4();
    }

    const Rectangle2D roomRect = this->roomRect();

    QMatrix4x4 projectionMatrix;
    // projectionMatrix.ortho( QRectF( drawingArea ) );
    projectionMatrix.ortho( roomRect.left(), // x-left
                            roomRect.right(), // x-right
                            roomRect.bottom(), // y-bottom
                            roomRect.top(), // y-top
                            -1.0f, // z-near
                            1.0f // z-far
                            );

    return projectionMatrix;
}

void RoomDisplay::resetProjectionMatrix()
{
    this->setProjectionMatrix( this->projectionMatrixCalculated() );
}

void RoomDisplay::applyProjectionMatrix()
{
    if ( ! m_isGlInitialized )
    {
        return;
    }

    const QVector< float > rawProjectionMatrix = MathUtilities::rawMatrix( m_projectionMatrix );

    m_shaderProgram.setUniformMatrix4fv(
                this->uniformVariableCachedLocation( UniformVariableIndexMatrix ),
                1,
                GL_FALSE,
                rawProjectionMatrix.constData() );
}

void RoomDisplay::setEntitiesDrawData( const QList< RoomDisplay::GraphicEntityDrawData >& aEntitiesDrawData )
{
    m_entitiesDrawData = aEntitiesDrawData;
    this->applyEntitiesDrawData();
}

QList< RoomDisplay::GraphicEntityDrawData > RoomDisplay::entitiesDrawDataCalculated() const
{
    QList< GraphicEntityDrawData > entitiesDrawData;

    const int entityTypesCount = s_orderedEntities.size();
    for ( int i = 0; i < entityTypesCount; ++ i )
    {
        entitiesDrawData.append( this->graphicEntityDrawDataCalculated( s_orderedEntities[ i ] ) );
    }

    return entitiesDrawData;
}

void RoomDisplay::resetEntitiesDrawData()
{
    this->setEntitiesDrawData( this->entitiesDrawDataCalculated() );
}

void RoomDisplay::applyEntitiesDrawData()
{
    if ( ! m_isGlInitialized )
    {
        return;
    }

    if ( ! OpenGlUtilities::isUnsignedGlValueValid( m_vbo ) )
    {
        glGenBuffers( 1, & m_vbo );
        LOG_OPENGL_ERROR();
    }

    OpenGlObjectBinder vboBinder = this->vboBinder();
    vboBinder.bind();

    QVector< Vector3Df > allVertices;
    const int entitiesCount = m_entitiesDrawData.size();
    for ( int i = 0; i < entitiesCount; ++i )
    {
        allVertices << m_entitiesDrawData[ i ].vertices();
    }

    glBufferData( GL_ARRAY_BUFFER,
                  allVertices.size() * sizeof( Vector3Df ),
                  allVertices.constData(),
                  GL_STATIC_DRAW );
    LOG_OPENGL_ERROR();
}

QVector< Vector3Df > RoomDisplay::discreteCircleTriangleFanVertices(
        const Vector2D& centerPt,
        const float radius,
        const int peripheralPointsCount )
{
    static const int peripheralPointsCountMin = 3;
    if ( peripheralPointsCount < peripheralPointsCountMin
         || ! MathUtilities::isLengthValid( radius, false ) )
    {
        return QVector< Vector3Df >();
    }

    QVector< Vector3Df > vertices;
    vertices.reserve( peripheralPointsCount + 2 );
    vertices.append( Vector3Df( centerPt ) );

    const float deltaAngle = 360.0f / peripheralPointsCount;
    for ( int i = 0; i <= peripheralPointsCount; ++ i )
    {
        vertices.append( Vector3Df( centerPt + Vector2D::fromAngleLength( deltaAngle * i, radius ) ) );
    }

    return vertices;
}

bool RoomDisplay::isGraphicEntityValid( const RoomDisplay::GraphicEntity graphicEntity )
{
    return graphicEntity == GraphicEntityRoom
           || graphicEntity == GraphicEntityIlluminatedArea
           || graphicEntity == GraphicEntityColumn
           || graphicEntity == GraphicEntityLightSource;
}

void RoomDisplay::enableGenericVertexAttributeArrays( const bool enable )
{
    foreach ( const GLuint genericVertexAttributeIndex, s_allOrderedGenericVertexAttributeIndices )
    {
        if ( enable )
        {
            glEnableVertexAttribArray( genericVertexAttributeIndex );
            LOG_OPENGL_ERROR();
        }
        else
        {
            glDisableVertexAttribArray( genericVertexAttributeIndex );
            LOG_OPENGL_ERROR();
        }
    }
}

QByteArray RoomDisplay::uniformVariableName( const int index )
{
    switch ( index )
    {
        case UniformVariableIndexColor:
            return "color";
        case UniformVariableIndexMatrix:
            return "matrix";
    }

    return QByteArray();
}

bool RoomDisplay::isUniformVariableIndexValid( const int index )
{
    return 0 <= index && index < UniformVariablesCount;
}

GLint RoomDisplay::uniformVariableLocation( const int index ) const
{
    if ( ! RoomDisplay::isUniformVariableIndexValid( index )
         || ! m_shaderProgram.isValid() )
    {
        return OpenGlUtilities::s_invalidSignedGlValue;
    }

    const QByteArray uniformVariableName = RoomDisplay::uniformVariableName( index );
    return uniformVariableName.isEmpty()
           ? OpenGlUtilities::s_invalidSignedGlValue
           : m_shaderProgram.uniformVarLocation( uniformVariableName );
}

void RoomDisplay::cacheUniformVariableLocations()
{
    m_uniformVariableCachedLocations.clear();
    m_uniformVariableCachedLocations.reserve( UniformVariablesCount );
    for ( int i = 0; i < UniformVariablesCount; ++ i )
    {
        m_uniformVariableCachedLocations.insert( i, this->uniformVariableLocation( i ) );
    }
}

GLint RoomDisplay::uniformVariableCachedLocation( const int index ) const
{
    return m_uniformVariableCachedLocations.value( index, OpenGlUtilities::s_invalidSignedGlValue );
}

void RoomDisplay::mousePressEvent( QMouseEvent* event )
{
    QGLWidget::mousePressEvent( event );
    this->processMouseEvent( event );
}

void RoomDisplay::mouseMoveEvent( QMouseEvent* event )
{
    QGLWidget::mouseMoveEvent( event );
    this->processMouseEvent( event );
}

void RoomDisplay::mouseReleaseEvent( QMouseEvent* event )
{
    QGLWidget::mouseReleaseEvent( event );
    this->processMouseEvent( event );

    if ( ! this->hasFocus() )
    {
        this->setFocus( Qt::MouseFocusReason );
    }
}

void RoomDisplay::init()
{
    if ( m_isGlInitialized )
    {
        return;
    }

    m_isGlInitialized = true;

    if ( OpenGlUtilities::isUnsignedGlValueValid( m_vao )
         && OpenGlUtilities::isUnsignedGlValueValid( m_vbo )
         && ! m_uniformVariableCachedLocations.isEmpty() )
    {
        return;
    }

    // problem with nVidia chips:
    // http://stackoverflow.com/questions/8302625/segmentation-fault-at-glgenvertexarrays-1-vao
    glewExperimental = GL_TRUE;

    GLenum err = glewInit();
    LOG_OPENGL_ERROR();
    if ( GLEW_OK != err )
    {
        qDebug().nospace() << "Error initializing GLEW: " << QString::fromLatin1( reinterpret_cast< const char* >( glewGetErrorString( err ) ) );
        return;
    }

    const bool isBuiltOk = m_shaderProgram.build(
                               ":/shaders/RoomDisplay.vert",
                               ":/shaders/RoomDisplay.frag" );
    if ( ! isBuiltOk )
    {
        return;
    }

    glEnable( GL_LINE_SMOOTH );
    LOG_OPENGL_ERROR();

    glHint( GL_LINE_SMOOTH_HINT, GL_NICEST );
    LOG_OPENGL_ERROR();

    glEnable( GL_POLYGON_SMOOTH );
    LOG_OPENGL_ERROR();

    glHint( GL_POLYGON_SMOOTH_HINT, GL_NICEST );
    LOG_OPENGL_ERROR();

    // glEnable( GL_DEPTH_TEST );
    // LOG_OPENGL_ERROR();

    ClearColorAlternator clearColorAlternator( Qt::white, false );
    clearColorAlternator.change();

    this->cacheUniformVariableLocations();
    this->applyRoom( false );
    this->applyRoomOpenGlData( false );

    OpenGlObjectBinder vboBinder = this->vboBinder( false );
    vboBinder.bind();

    glGenVertexArrays( 1, & m_vao );
    LOG_OPENGL_ERROR();

    OpenGlObjectBinder vaoBinder = this->vaoBinder( false );
    vaoBinder.bind();

    this->enableGenericVertexAttributeArrays( true );

    glVertexAttribPointer( GenericVertexAttributeIndexVertexPos,
                           MathUtilities::s_axesCount3D,
                           GL_FLOAT,
                           GL_FALSE,
                           sizeof( Vector3Df ),
                           nullptr );
    LOG_OPENGL_ERROR();

    vaoBinder.unbind();
    vboBinder.unbind();

    this->enableGenericVertexAttributeArrays( false );
}

void RoomDisplay::refresh( const quint64& traits )
{
    Q_UNUSED( traits )

    this->applyRoom( m_isGlInitialized && this->isVisible() );
}

Rectangle2D RoomDisplay::drawableRect() const
{
    return Rectangle2D( this->rect() );
}

void RoomDisplay::printGLInfo()
{
#ifdef THOROUGH_LOGGING_ENABLED
    const GLubyte* renderer = glGetString( GL_RENDERER );
    LOG_OPENGL_ERROR();

    const GLubyte* vendor = glGetString( GL_VENDOR );
    LOG_OPENGL_ERROR();

    const GLubyte* version = glGetString( GL_VERSION );
    LOG_OPENGL_ERROR();

    const GLubyte* glslVersion = glGetString( GL_SHADING_LANGUAGE_VERSION );
    LOG_OPENGL_ERROR();

    GLint major = -1;
    GLint minor = -1;
    glGetIntegerv( GL_MAJOR_VERSION, &major );
    LOG_OPENGL_ERROR();

    glGetIntegerv( GL_MINOR_VERSION, &minor );
    LOG_OPENGL_ERROR();

    GLint maxVertexTextureUnitsCount = 0;
    glGetIntegerv( GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, & maxVertexTextureUnitsCount );
    LOG_OPENGL_ERROR();

    GLint maxFragmentTextureUnitsCount = 0;
    glGetIntegerv( GL_MAX_TEXTURE_IMAGE_UNITS, & maxFragmentTextureUnitsCount );
    LOG_OPENGL_ERROR();

    GLint maxCombinedTextureUnitsCount = 0;
    glGetIntegerv( GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS, & maxCombinedTextureUnitsCount );
    LOG_OPENGL_ERROR();

    qDebug() << "GL Vendor:" << QLatin1String( reinterpret_cast< const char* > ( vendor ) ) << endl
             << "GL Renderer:" << QLatin1String( reinterpret_cast< const char* > ( renderer ) ) << endl
             << "GL Version (string):" << QLatin1String( reinterpret_cast< const char* > ( version ) ) << endl
             << "GL Version (integer):" << QString( "%1.%2" ).arg( major ).arg( minor ) << endl
             << "GLSL Version:" << QLatin1String( reinterpret_cast< const char* > ( glslVersion ) ) << endl
             << "Vertex texture units count:" << maxVertexTextureUnitsCount << endl
             << "Fragment texture units count:" << maxFragmentTextureUnitsCount << endl
             << "Combined texture units count:" << maxCombinedTextureUnitsCount;

    GLint extensionsCount = 0;
    glGetIntegerv( GL_NUM_EXTENSIONS, &extensionsCount );
    LOG_OPENGL_ERROR();

    qDebug() << "extensions:";

    const int extensionsCountDigitsCount = qFloor( log10( static_cast< float >( extensionsCount ) ) ) + 1;
    for ( int i = 0; i < extensionsCount; ++i )
    {
        qDebug().nospace() << '\t' << QString( "%1" ).arg( i, extensionsCountDigitsCount, 10, QChar( QLatin1Char( ' ' ) ) ) << ": " << QLatin1String( reinterpret_cast< const char* > ( glGetStringi( GL_EXTENSIONS, i ) ) );
        LOG_OPENGL_ERROR();
    }

    GLint frontFace = -1;
    glGetIntegerv( GL_FRONT_FACE, & frontFace );

    qDebug() << "GL_FRONT_FACE:" << ( frontFace == GL_CCW
                                      ? "CCW"
                                      : ( frontFace == GL_CW
                                          ? "CW"
                                          : "unknown" ) );
#endif
}


RoomDisplay::GraphicEntityDrawData::GraphicEntityDrawData(
        const RoomDisplay::GraphicEntity aEntity,
        const OpenGlUtilities::GeometricPrimitiveType aPrimitive,
        const QVector< Vector3Df >& aVertices )
    : m_entity( RoomDisplay::GraphicEntityInvalid )
    , m_primitive( OpenGlUtilities::GeometricPrimitiveTypeInvalid )
    , m_vertices( aVertices )
{
    this->setEntity( aEntity );
    this->setPrimitive( aPrimitive );
}

GLenum RoomDisplay::GraphicEntityDrawData::openGLPrimitive() const
{
    return OpenGlUtilities::openGLGeometricPrimitiveType( m_primitive );
}

bool RoomDisplay::GraphicEntityDrawData::hasBorder() const
{
    return    m_primitive == RoomDisplay::GraphicEntityRoom
           || m_primitive == RoomDisplay::GraphicEntityColumn
           || m_primitive == RoomDisplay::GraphicEntityLightSource;
}

bool RoomDisplay::GraphicEntityDrawData::isValid() const
{
    return RoomDisplay::isGraphicEntityValid( m_entity )
           && OpenGlUtilities::isGeometricPrimitiveTypeValid( m_primitive )
           && ! m_vertices.isEmpty();
}

void RoomDisplay::GraphicEntityDrawData::setEntity( const RoomDisplay::GraphicEntity aEntity )
{
    if ( RoomDisplay::isGraphicEntityValid( aEntity ) )
    {
        m_entity = aEntity;
    }
}

void RoomDisplay::GraphicEntityDrawData::setPrimitive( const OpenGlUtilities::GeometricPrimitiveType aPrimitive )
{
    if ( OpenGlUtilities::isGeometricPrimitiveTypeValid( aPrimitive ) )
    {
        m_primitive = aPrimitive;
    }
}
