#ifndef LIGHTCOVERAGE_ABSTRACTROOMDISPLAY_H
#define LIGHTCOVERAGE_ABSTRACTROOMDISPLAY_H

#include "Data/Rectangle2D.h"
#include "Data/Vector2D.h"
#include "Utilities/ChangeListener.h"

#include <QColor>
#include <QMatrix>
#include <QPair>
#include <QPoint>
#include <QRectF>

QT_FORWARD_DECLARE_CLASS(QMouseEvent)

namespace LightCoverage {
namespace Data {

class Room;

}

using namespace LightCoverage::Data;
using namespace LightCoverage::Utilities;

class AbstractRoomDisplay : public ChangeListener
{
public:
    AbstractRoomDisplay();
    virtual ~AbstractRoomDisplay();

    void setRoom( Room* const aRoom );
    inline Room* room() const
    {
        return m_room;
    }

    virtual void aboutToRefresh( const quint64& traits = 0ULL );
    virtual void refresh( const quint64& traits = 0ULL ) = 0;
    virtual Rectangle2D drawableRect() const;
    Rectangle2D roomRect() const;

protected:
    static const QColor s_columnColor;
    static const QColor s_lightSourceColor;
    static const QColor s_illuminationColor;
    static const QColor s_shadowColor;
    static const QColor s_borderColor;
    static const float s_borderThickness;
    static const float s_normalizedRoomMargin;

protected:
    void onChanged( ChangeNotifier* const notifier, const quint64& traits = 0ULL ) /* override */;
    QPair< QRectF, QMatrix > roomScreenRectMatrix() const;

    void processMouseEvent( QMouseEvent* const mouseEvent );
    void processMousePressed( const QPoint& mousePos );
    void processMouseMoved( const QPoint& mousePos );
    void processMouseReleased( const QPoint& mousePos );

private:
    static const Qt::MouseButton s_allowedMouseButton;

private:
    Room*    m_room;
    bool     m_isMouseBeingDragged;
    Vector2D m_initialLightSourceOffset;
};

}

#endif // LIGHTCOVERAGE_ABSTRACTROOMDISPLAY_H
