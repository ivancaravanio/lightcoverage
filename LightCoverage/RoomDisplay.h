#ifndef LIGHTCOVERAGE_ROOMDISPLAY_H
#define LIGHTCOVERAGE_ROOMDISPLAY_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include "AbstractRoomDisplay.h"
#include "Data/Room.h"
#include "Data/Vector3Df.h"
#include "Utilities/AbstractOpenGlClient.h"
#include "Utilities/OpenGlUtilities.h"
#include "Utilities/ShaderProgram.h"

#include <QByteArray>
#include <QColor>
#include <QGLWidget>
#include <QHash>
#include <QList>
#include <QMap>
#include <QMatrix4x4>
#include <QRect>
#include <QString>
#include <QVector>

QT_FORWARD_DECLARE_CLASS(QKeyEvent)
QT_FORWARD_DECLARE_CLASS(QMouseEvent)

#ifndef QT_NO_WHEELEVENT
    QT_FORWARD_DECLARE_CLASS(QWheelEvent)
#endif

namespace LightCoverage {

using namespace LightCoverage::Data;
using namespace LightCoverage::Utilities;

class RoomDisplay : public QGLWidget, public AbstractOpenGlClient, public AbstractRoomDisplay
{
    Q_OBJECT

public:
    explicit RoomDisplay( const QGLFormat& aOpenGlFormat, QWidget* const parent = nullptr );
    ~RoomDisplay() /* override */;

    void init() /* override */;
    void refresh( const quint64& traits = 0ULL ) /* override */;

    Rectangle2D drawableRect() const /* override */;

protected:
    void initializeGL() /* override */;
    void resizeGL( int w, int h ) /* override */;
    void paintGL() /* override */;

    void mousePressEvent( QMouseEvent* event ) /* override */;
    void mouseMoveEvent( QMouseEvent* event ) /* override */;
    void mouseReleaseEvent( QMouseEvent* event ) /* override */;

    static void printGLInfo();

private:
    enum GenericVertexAttributeIndex
    {
        GenericVertexAttributeIndexInvalid   = -1,
        GenericVertexAttributeIndexVertexPos = 0,
        GenericVertexAttributesCount         = 1
    };

    enum FragmentOutputVariableIndex
    {
        FragmentOutputVariableIndexInvalid = -1,
        FragmentOutputVariableIndexColor   = 0,
        FragmentOutputVariablesCount       = 1
    };

    enum UniformVariableIndex
    {
        UniformVariableIndexInvalid = -1,
        UniformVariableIndexColor   = 0,
        UniformVariableIndexMatrix  = 1,
        UniformVariablesCount       = 2
    };

    enum GraphicEntity
    {
        GraphicEntityInvalid         = -1,
        GraphicEntityRoom            = 0,
        GraphicEntityIlluminatedArea = 1,
        GraphicEntityColumn          = 2,
        GraphicEntityLightSource     = 3,
        GraphicEntitiesCount         = 4
    };

private:
    static const QList< GLuint >        s_allOrderedGenericVertexAttributeIndices;
    static const QList< GraphicEntity > s_orderedEntities;

private:
    struct GraphicEntityDrawData
    {
    public:
        explicit GraphicEntityDrawData(
                const GraphicEntity aEntity,
                const OpenGlUtilities::GeometricPrimitiveType aGeometricPrimitiveType,
                const QVector< Vector3Df >& aVertices );

        inline GraphicEntity entity() const
        {
            return m_entity;
        }

        OpenGlUtilities::GeometricPrimitiveType primitive() const
        {
            return m_primitive;
        }

        GLenum openGLPrimitive() const;

        inline const QVector< Vector3Df >& vertices() const
        {
            return m_vertices;
        }

        bool hasBorder() const;

        bool isValid() const;

    private:
        void setEntity( const GraphicEntity aEntity );
        void setPrimitive( const OpenGlUtilities::GeometricPrimitiveType aPrimitive );

    private:
        GraphicEntity                           m_entity;
        OpenGlUtilities::GeometricPrimitiveType m_primitive;
        QVector< Vector3Df >                    m_vertices;
    };

private:
    static QByteArray uniformVariableName( const int index );
    static bool isUniformVariableIndexValid( const int index );
    GLint uniformVariableLocation( const int index ) const;
    void cacheUniformVariableLocations();
    GLint uniformVariableCachedLocation( const int index ) const;

    void enableGenericVertexAttributeArrays( const bool enable );

    void disposeOpenGlResources();

    QRect viewport( const int w, const int h ) const;
    QRect viewport() const;

    using AbstractOpenGlClient::viewportAlternator;
    ViewportAlternator viewportAlternator( const bool autoRollback = true ) const;

    using AbstractOpenGlClient::vaoBinder;
    OpenGlObjectBinder vaoBinder( const bool autoUnbind = true ) const;

    using AbstractOpenGlClient::vboBinder;
    OpenGlObjectBinder vboBinder( const bool autoUnbind = true ) const;

    void applyRoom( const bool shouldUpdateUi );
    void applyRoomOpenGlData( const bool shouldUpdateUi );

    static QVector< Vector3Df > vertices( const Triangle2D& triangle );
    static QVector< Vector3Df > vertices( const QList< Triangle2D >& triangles );
    static QVector< Vector3Df > vertices( const QList< Triangle2D* >& triangles );
    static QVector< Vector3Df > vertices( const QList< Polygon2D >& polygons );
    QList< GraphicEntityDrawData > graphicEntityDrawDataCalculated( const GraphicEntity graphicEntity ) const;
    static QColor color( const GraphicEntity graphicEntity );

    void setProjectionMatrix( const QMatrix4x4& aProjectionMatrix );
    QMatrix4x4 projectionMatrixCalculated() const;
    void resetProjectionMatrix();
    void applyProjectionMatrix();
    void setEntitiesDrawData( const QList< GraphicEntityDrawData >& aEntitiesDrawData );
    QList< GraphicEntityDrawData > entitiesDrawDataCalculated() const;
    void resetEntitiesDrawData();
    void applyEntitiesDrawData();

    static QVector< Vector3Df > discreteCircleTriangleFanVertices( const Vector2D& centerPt, const float radius, const int peripheralPointsCount = 60 );
    static bool isGraphicEntityValid( const GraphicEntity graphicEntity );

private:
    ShaderProgram m_shaderProgram;
    GLuint m_vao;
    GLuint m_vbo;

    // 2: all shaders common variable - per vertex and per fragment invariant
    QHash< int, GLint > m_uniformVariableCachedLocations;

    QList< GraphicEntityDrawData > m_entitiesDrawData;
    QMatrix4x4 m_projectionMatrix;

    bool m_isGlInitialized;
};

}

#endif // LIGHTCOVERAGE_ROOMDISPLAY_H
