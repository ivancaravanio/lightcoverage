#ifndef LIGHTCOVERAGE_MAINCONTROLLER_H
#define LIGHTCOVERAGE_MAINCONTROLLER_H

#include "Utilities/Retranslatable.h"

#include <QObject>
#include <QScopedPointer>
#include <QString>

namespace LightCoverage {
namespace Data {

class Room;

}

using namespace LightCoverage::Data;

class MainWindow;

class MainController : public QObject
{
    Q_OBJECT

public:
    explicit MainController( QObject* const parent = nullptr );
    ~MainController() /* override */;

    void init( const QString& roomFilePath = QString() );

private slots:
    void loadRoomInteractively( const QString& roomFilePath );
    void saveRoom();
    void clearRoom();

private:
    bool setRoom( const QString& roomFilePath );
    void setRoom( Room* const room );
    static Room* room( const QString& roomFilePath );

private:
    QScopedPointer< MainWindow > m_mainWindow;
    QScopedPointer< Room >       m_room;
    QString                      m_roomFilePath;
};

}

#endif // LIGHTCOVERAGE_MAINCONTROLLER_H
