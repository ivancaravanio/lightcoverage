#include "MainWindow.h"

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include "Data/Room.h"
#include "RoomCompositeDisplay.h"
#include "RoomFilePathDisplay.h"
#include "Utilities/GeneralUtilities.h"

#include <QApplication>
#include <QHBoxLayout>
#include <QLabel>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QStatusBar>
#include <QTextStream>

#ifndef QT_NO_DRAGANDDROP
    #include <QDragEnterEvent>
    #include <QDropEvent>
    #include <QMimeData>
#endif

using namespace LightCoverage;

const int MainWindow::s_maxAllowedRecentlyUsedRoomFilePaths = 5;

MainWindow::MainWindow( QWidget* parent )
    : QMainWindow( parent )
    , m_fileMenu( nullptr )
    , m_openRoomAction( nullptr )
    , m_recentlyUsedRoomsMenu( nullptr )
    , m_saveRoomAction( nullptr )
    , m_closeRoomAction( nullptr )
    , m_helpMenu( nullptr )
    , m_aboutAction( nullptr )
    , m_roomCompositeDisplay( nullptr )
    , m_roomFilePathDisplay( nullptr )
{
#ifndef QT_NO_DRAGANDDROP
    this->setAcceptDrops( true );
#endif

    QMenuBar* const menuBar = this->menuBar();

    m_fileMenu = new QMenu;
    m_openRoomAction             = m_fileMenu->addAction( QString(), this, SIGNAL(loadRoomRequested()), QKeySequence( Qt::CTRL + Qt::Key_O ) );

    m_recentlyUsedRoomsMenu = new QMenu;
    m_fileMenu->addMenu( m_recentlyUsedRoomsMenu );

    m_saveRoomAction             = m_fileMenu->addAction( QString(), this, SIGNAL(saveRoomRequested()), QKeySequence( Qt::CTRL + Qt::Key_S ) );
    m_closeRoomAction            = m_fileMenu->addAction( QString(), this, SIGNAL(clearRoomRequested()), QKeySequence( Qt::CTRL + Qt::Key_W ) );
    menuBar->addMenu( m_fileMenu );

    m_helpMenu = new QMenu;
    m_aboutAction = m_helpMenu->addAction( QString(), this, SLOT(about()) );
    menuBar->addMenu( m_helpMenu );

    m_roomCompositeDisplay = new RoomCompositeDisplay;
    this->setCentralWidget( m_roomCompositeDisplay );

    m_roomFilePathDisplay = new RoomFilePathDisplay;

    QStatusBar* const statusBar = this->statusBar();
    statusBar->setContentsMargins( QMargins() );
    statusBar->addWidget( m_roomFilePathDisplay );

    this->retranslateInternal();
    this->updateMenusAvailability();
}

MainWindow::~MainWindow()
{
}

void MainWindow::setRoomFilePath( const QString& aRoomFilePath )
{
    m_roomFilePathDisplay->setFilePath( aRoomFilePath );
    this->stackMostRecentlyUsedRoomFilePath( aRoomFilePath );
}

QString MainWindow::roomFilePath() const
{
    return m_roomFilePathDisplay->filePath();
}

void MainWindow::setRoom( Room* const aRoom )
{
    Room* const previousRoom = this->room();
    m_roomCompositeDisplay->setRoom( aRoom );

    if ( ( this->room() == nullptr ) ^ ( previousRoom == nullptr ) )
    {
        this->updateMenusAvailability();
    }
}

Room* MainWindow::room() const
{
    return m_roomCompositeDisplay->room();
}

void MainWindow::retranslate()
{
    this->retranslateInternal();
}

void MainWindow::setRecentlyUsedRoomFilePaths( const QStringList& aRecentlyUsedRoomFilePaths )
{
    const int newActionsCount = aRecentlyUsedRoomFilePaths.size();
    const QList< QAction* > previousActions = m_recentlyUsedRoomsMenu->actions();
    const int previousActionsCount = previousActions.size();
    if ( newActionsCount == previousActionsCount )
    {
        bool areRoomFilePathsEqual = true;
        for ( int i = 0; i < newActionsCount; ++ i )
        {
            if ( aRecentlyUsedRoomFilePaths[ i ] != previousActions[ i ]->text() )
            {
                areRoomFilePathsEqual = false;
                break;
            }
        }

        if ( areRoomFilePathsEqual )
        {
            return;
        }
    }

    for ( int i = previousActionsCount - 1; i >= 0; -- i )
    {
        m_recentlyUsedRoomsMenu->removeAction( previousActions[ i ] );
    }

    for ( int i = 0; i < newActionsCount || i < s_maxAllowedRecentlyUsedRoomFilePaths; ++ i )
    {
        m_recentlyUsedRoomsMenu->addAction( aRecentlyUsedRoomFilePaths[ i ],
                                            this, SLOT(requestOpenRecentlyUsedFilePath()),
                                            MainWindow::recentlyUsedRoomFilePathShortcut( i ) );
    }
}

QStringList MainWindow::recentlyUsedRoomFilePaths() const
{
    const QList< QAction* > roomFilePathActions = m_recentlyUsedRoomsMenu->actions();
    const int roomFilePathActionsCount = roomFilePathActions.size();
    if ( roomFilePathActionsCount == 0 )
    {
        return QStringList();
    }

    QStringList recentlyUsedRoomFilePaths;
    recentlyUsedRoomFilePaths.reserve( roomFilePathActionsCount );
    for ( int i = 0; i < roomFilePathActionsCount; ++i )
    {
        recentlyUsedRoomFilePaths.append( roomFilePathActions[ i ]->text() );
    }

    return recentlyUsedRoomFilePaths;
}

QKeySequence MainWindow::recentlyUsedRoomFilePathShortcut( const int index )
{
    return 0 <= index && index < s_maxAllowedRecentlyUsedRoomFilePaths
           ? QKeySequence( Qt::ALT + Qt::Key_1 + index )
           : QKeySequence();
}

#ifndef QT_NO_DRAGANDDROP
void MainWindow::dragEnterEvent( QDragEnterEvent* event )
{
    QMainWindow::dragEnterEvent( event );

    const QString filePath = GeneralUtilities::filePathFromMimeData( event->mimeData() );
    if ( ! filePath.isEmpty() )
    {
        event->acceptProposedAction();
    }
}

void MainWindow::dropEvent( QDropEvent* event )
{
    QMainWindow::dropEvent( event );

    const QString filePath = GeneralUtilities::filePathFromMimeData( event->mimeData() );
    if ( ! filePath.isEmpty() )
    {
        emit loadRoomRequested( filePath );
    }
}
#endif

void MainWindow::about()
{
    QMessageBox* const aboutMessageBox = new QMessageBox( this );
    connect( aboutMessageBox, SIGNAL(finished(int)), aboutMessageBox, SLOT(deleteLater()) );
    aboutMessageBox->setText( MainWindow::aboutText() );
    aboutMessageBox->setIconPixmap( QPixmap( "://Icons/Icon_64.png" ) );
    aboutMessageBox->show();
}

void MainWindow::requestOpenRecentlyUsedFilePath()
{
    QAction* const action = dynamic_cast< QAction* >( this->sender() );
    if ( action == nullptr )
    {
        return;
    }

    emit loadRoomRequested( action->text() );
}

void MainWindow::retranslateInternal()
{
    m_fileMenu->setTitle( tr( "&File" ) );
    m_openRoomAction->setText( tr( "&Open Room..." ) );
    m_recentlyUsedRoomsMenu->setTitle( tr( "R&ecent Rooms" ) );
    m_saveRoomAction->setText( tr( "&Save Room" ) );
    m_closeRoomAction->setText( tr( "&Close Room" ) );
    m_helpMenu->setTitle( tr( "&Help" ) );
    m_aboutAction->setText( tr( "&About..." ) );
}

void MainWindow::updateMenusAvailability()
{
    const bool hasRoom = this->room() != nullptr;
    m_saveRoomAction->setEnabled( hasRoom );
    m_closeRoomAction->setEnabled( hasRoom );
}

QString MainWindow::aboutText()
{
    // TODO: find a way to detect processor architecture for Linux
#if defined(Q_OS_WIN32) || defined(Q_OS_MAC32)
    static const QString bitness = "32-bit";
#elif defined(Q_OS_WIN64) || defined(Q_OS_MAC64)
    static const QString bitness = "64-bit";
#endif

    QString aboutText;
    QTextStream ts( & aboutText );
    ts << QString( "%1 %2 %3" ).arg( qAppName() ).arg( QApplication::applicationVersion() ).arg( bitness ) << endl
       << tr( "Copyright %1 %2" ).arg( QChar( 0x00A9 ) ).arg( QLatin1String( COMPANY ) ) << endl
       << QLatin1String( COMPANY_DOMAIN ) << endl
       << tr( "Used libraries:" ) << endl
       << tr( "\tQt %1" ).arg( QT_VERSION_STR ) << endl
       << tr( "\tOpenGL Extension Wrangler Library (GLEW) %1" )
          .arg( QLatin1String( reinterpret_cast< const char* >( glewGetString( GLEW_VERSION ) ) ) );

    return aboutText;
}

void MainWindow::stackMostRecentlyUsedRoomFilePath( const QString& aRecentlyUsedRoomFilePath )
{
    if ( aRecentlyUsedRoomFilePath.isEmpty() )
    {
        return;
    }

    QList< QAction* > recentlyUsedRoomFilePathActions = m_recentlyUsedRoomsMenu->actions();
    QMutableListIterator< QAction* > recentlyUsedRoomFilePathActionsIter( recentlyUsedRoomFilePathActions );
    int i = 1;
    while ( recentlyUsedRoomFilePathActionsIter.hasNext() )
    {
        recentlyUsedRoomFilePathActionsIter.next();
        QAction* const recentlyUsedRoomFilePathAction = recentlyUsedRoomFilePathActionsIter.value();
        if ( recentlyUsedRoomFilePathAction->text() == aRecentlyUsedRoomFilePath
             || i >= s_maxAllowedRecentlyUsedRoomFilePaths )
        {
            if ( i == 1 )
            {
                return;
            }

            m_recentlyUsedRoomsMenu->removeAction( recentlyUsedRoomFilePathAction );
            recentlyUsedRoomFilePathActionsIter.remove();
            continue;
        }

        recentlyUsedRoomFilePathAction->setShortcut( MainWindow::recentlyUsedRoomFilePathShortcut( i ) );
        ++ i;
    }

    QAction* const mostRecentlyUsedRoomFilePathAction = new QAction( aRecentlyUsedRoomFilePath, nullptr );
    mostRecentlyUsedRoomFilePathAction->setShortcut( MainWindow::recentlyUsedRoomFilePathShortcut( 0 ) );
    connect( mostRecentlyUsedRoomFilePathAction, SIGNAL(triggered(bool)), this, SLOT(requestOpenRecentlyUsedFilePath()) );
    m_recentlyUsedRoomsMenu->insertAction( recentlyUsedRoomFilePathActions.isEmpty()
                                           ? nullptr
                                           : recentlyUsedRoomFilePathActions.first(),
                                           mostRecentlyUsedRoomFilePathAction );
}
