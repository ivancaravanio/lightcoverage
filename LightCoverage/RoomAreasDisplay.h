#ifndef LIGHTCOVERAGE_ROOMAREASDISPLAY_H
#define LIGHTCOVERAGE_ROOMAREASDISPLAY_H

#include "AbstractRoomDisplay.h"
#include "Utilities/Retranslatable.h"

#include <QString>
#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QLabel)

namespace LightCoverage {

using namespace LightCoverage::Utilities;

class RoomAreasDisplay : public QWidget, public AbstractRoomDisplay, public Retranslatable
{
    Q_OBJECT

public:
    explicit RoomAreasDisplay( QWidget* const parent = nullptr, Qt::WindowFlags f = 0 );
    ~RoomAreasDisplay() /* override */;

    void refresh( const quint64& traits = 0ULL ) /* override */;
    void retranslate() /* override */;

private:
    void retranslateInternal();
    void updateAreas();
    static QString areaPercentageToString( const float area );

private:
    QLabel* m_illuminatedAreaLabel;
    QLabel* m_illuminatedAreaDisplay;
    QLabel* m_shadowedAreaLabel;
    QLabel* m_shadowedAreaDisplay;
    QLabel* m_columsAreaLabel;
    QLabel* m_columsAreaDisplay;
};

}

#endif // LIGHTCOVERAGE_ROOMAREASDISPLAY_H
