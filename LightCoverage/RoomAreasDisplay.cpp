#include "RoomAreasDisplay.h"

#include "Data/Room.h"

#include <QGridLayout>
#include <QLabel>

using namespace LightCoverage;

RoomAreasDisplay::RoomAreasDisplay( QWidget* const parent, Qt::WindowFlags f )
    : QWidget( parent, f )
    , m_illuminatedAreaLabel( nullptr )
    , m_illuminatedAreaDisplay( nullptr )
    , m_shadowedAreaLabel( nullptr )
    , m_shadowedAreaDisplay( nullptr )
    , m_columsAreaLabel( nullptr )
    , m_columsAreaDisplay( nullptr )
{
    m_illuminatedAreaLabel = new QLabel;
    m_illuminatedAreaDisplay = new QLabel;
    m_shadowedAreaLabel = new QLabel;
    m_shadowedAreaDisplay = new QLabel;
    m_columsAreaLabel = new QLabel;
    m_columsAreaDisplay = new QLabel;

    QGridLayout* const mainLayout = new QGridLayout( this );
    mainLayout->addWidget( m_illuminatedAreaLabel,   0, 0 );
    mainLayout->addWidget( m_illuminatedAreaDisplay, 0, 1, Qt::AlignRight );
    mainLayout->addWidget( m_shadowedAreaLabel,      1, 0 );
    mainLayout->addWidget( m_shadowedAreaDisplay,    1, 1, Qt::AlignRight );
    mainLayout->addWidget( m_columsAreaLabel,        2, 0 );
    mainLayout->addWidget( m_columsAreaDisplay,      2, 1, Qt::AlignRight );
    mainLayout->setColumnStretch( 0, 0 );
    mainLayout->setColumnStretch( 1, 1 );

    this->retranslateInternal();
    this->updateAreas();
}

RoomAreasDisplay::~RoomAreasDisplay()
{
}

void RoomAreasDisplay::refresh( const quint64& traits )
{
    Q_UNUSED( traits )

    this->updateAreas();
}

void RoomAreasDisplay::retranslate()
{
    this->retranslateInternal();
}

void RoomAreasDisplay::retranslateInternal()
{
    m_illuminatedAreaLabel->setText( tr( "Illuminated Area:" ) );
    m_shadowedAreaLabel->setText( tr( "Shadowed Area:" ) );
    m_columsAreaLabel->setText( tr( "Columns Area:" ) );
}

void RoomAreasDisplay::updateAreas()
{
    Room* const room = this->room();

    float illuminatedArea = 0.0f;
    float columnsArea     = 0.0f;
    float shadowedArea    = 0.0f;
    if ( room != nullptr )
    {
        illuminatedArea = room->illuminatedArea();
        columnsArea     = room->columnsArea();
        shadowedArea    = room->shadowedArea();

        static const float maxPercentage = 100.0f;
        const float        coeff         = maxPercentage / room->area();

        illuminatedArea *= coeff;
        columnsArea     *= coeff;
        shadowedArea    *= coeff;
    }

    m_illuminatedAreaDisplay->setText( RoomAreasDisplay::areaPercentageToString( illuminatedArea ) );
    m_shadowedAreaDisplay->setText(    RoomAreasDisplay::areaPercentageToString( shadowedArea    ) );
    m_columsAreaDisplay->setText(      RoomAreasDisplay::areaPercentageToString( columnsArea     ) );
}

QString RoomAreasDisplay::areaPercentageToString( const float area )
{
    static const QString areaStringTemplate = "%1 %";
    return areaStringTemplate.arg( area, 0, 'f', 1 );
}
