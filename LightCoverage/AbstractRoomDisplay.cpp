#include "AbstractRoomDisplay.h"

#include "Data/Room.h"
#include "Utilities/MathUtilities.h"

#include <QMouseEvent>

using namespace LightCoverage;

const QColor AbstractRoomDisplay::s_columnColor( 217, 152, 108 );
const QColor AbstractRoomDisplay::s_illuminationColor( 255, 255, 240 );
const QColor AbstractRoomDisplay::s_lightSourceColor( Qt::yellow );
const QColor AbstractRoomDisplay::s_shadowColor( 193, 193, 193 );
const QColor AbstractRoomDisplay::s_borderColor( Qt::black );
const float AbstractRoomDisplay::s_borderThickness = 1.0f;
const float AbstractRoomDisplay::s_normalizedRoomMargin = 0.05f;

const Qt::MouseButton AbstractRoomDisplay::s_allowedMouseButton = Qt::LeftButton;

AbstractRoomDisplay::AbstractRoomDisplay()
    : m_room( nullptr )
    , m_isMouseBeingDragged( false )
{
}

AbstractRoomDisplay::~AbstractRoomDisplay()
{
    if ( m_room != nullptr )
    {
        m_room->removeListener( this );
    }
}

void AbstractRoomDisplay::setRoom( Room* const aRoom )
{
    if ( aRoom == m_room )
    {
        return;
    }

    const bool isOldRoomValid = m_room != nullptr;
    const bool isNewRoomValid = aRoom != nullptr;
    if ( isNewRoomValid )
    {
        if ( ! aRoom->isValid() )
        {
            return;
        }

        if ( isOldRoomValid )
        {
            if ( *aRoom == *m_room )
            {
                return;
            }
        }
    }

    this->aboutToRefresh();

    if ( isOldRoomValid )
    {
        m_room->removeListener( this );
    }

    m_room = aRoom;
    if ( isNewRoomValid )
    {
        aRoom->addListener( this );
    }

    this->refresh();
}

void AbstractRoomDisplay::aboutToRefresh( const quint64& traits )
{
    Q_UNUSED( traits )
}

Rectangle2D AbstractRoomDisplay::drawableRect() const
{
    return Rectangle2D();
}

Rectangle2D AbstractRoomDisplay::roomRect() const
{
    return m_room != nullptr
           ? Rectangle2D( m_room->size() )
           : Rectangle2D();
}

void AbstractRoomDisplay::onChanged( ChangeNotifier* const notifier, const quint64& traits )
{
    if ( notifier != m_room )
    {
        return;
    }

    this->refresh( traits );
}

QPair< QRectF, QMatrix > AbstractRoomDisplay::roomScreenRectMatrix() const
{
    if ( m_room == nullptr )
    {
        return QPair< QRectF, QMatrix >();
    }

    const Rectangle2D drawableRect = this->drawableRect();
    if ( ! drawableRect.isValid() )
    {
        return QPair< QRectF, QMatrix >();
    }

    const Size& roomSize = m_room->size();

    const double scale = roomSize.inscriptionScale( drawableRect.size() ) * ( 1.0 - s_normalizedRoomMargin );
    const Size roomDrawingAreaSize = roomSize * scale;

    Rectangle2D roomDrawingArea( roomDrawingAreaSize );

    const Vector2D roomOffset = drawableRect.center() - roomDrawingArea.center();
    roomDrawingArea.translate( roomOffset );

    const QRectF roomScreenRect = roomDrawingArea.toQRectF();

    QMatrix offsetScaleMatrix;
    offsetScaleMatrix.translate( roomOffset.x(), roomOffset.y() );
    offsetScaleMatrix.scale( scale, scale );

    const QMatrix roomToScreenMatrix = offsetScaleMatrix * MathUtilities::yFlipAboutCenterMatrix( roomDrawingArea.center() );

    return QPair< QRectF, QMatrix >( roomScreenRect, roomToScreenMatrix );
}

void AbstractRoomDisplay::processMouseEvent(
        QMouseEvent* const mouseEvent )
{
    if ( mouseEvent == nullptr )
    {
        return;
    }

    const QEvent::Type eventType = mouseEvent->type();
    if ( eventType != QEvent::MouseButtonRelease
         && mouseEvent->buttons() != s_allowedMouseButton )
    {
        return;
    }

    const QPoint mousePos = mouseEvent->pos();
    switch ( eventType )
    {
        case QEvent::MouseButtonPress:   this->processMousePressed( mousePos ); break;
        case QEvent::MouseMove:          this->processMouseMoved( mousePos ); break;
        case QEvent::MouseButtonRelease: this->processMouseReleased( mousePos ); break;
    }
}

void AbstractRoomDisplay::processMousePressed( const QPoint& mousePos )
{
    if ( m_isMouseBeingDragged )
    {
        return;
    }

    Room* const room = this->room();
    if ( room == nullptr )
    {
        return;
    }

    const QPair< QRectF, QMatrix > roomScreenRectMatrix = this->roomScreenRectMatrix();
    const Vector2D roomLocalPt( roomScreenRectMatrix.second.inverted().map( QPointF( mousePos ) ) );
    if ( ! room->lightSourceBoundingRect().isContainedInInscribedCenteredCircle( roomLocalPt ) )
    {
        return;
    }

    m_isMouseBeingDragged = true;
    m_initialLightSourceOffset = roomLocalPt - room->lightSourcePosition();
}

void AbstractRoomDisplay::processMouseMoved( const QPoint& mousePos )
{
    if ( ! m_isMouseBeingDragged )
    {
        return;
    }

    Room* const room = this->room();
    if ( room == nullptr )
    {
        return;
    }

    const QPair< QRectF, QMatrix > roomScreenRectMatrix = this->roomScreenRectMatrix();
    const QMatrix& roomScreenMatrix = roomScreenRectMatrix.second;

    const QPointF roomLocalPt = roomScreenMatrix.inverted().map( QPointF( mousePos ) );
    const Vector2D roomLocalBoundedPt = Rectangle2D( room->size() ).expanded( - room->lightSourceRadius() ).boundedPt( Vector2D( roomLocalPt ) );
    room->setLightSourcePosition( roomLocalBoundedPt - m_initialLightSourceOffset );
}

void AbstractRoomDisplay::processMouseReleased( const QPoint& mousePos )
{
    Q_UNUSED( mousePos )

    m_isMouseBeingDragged = false;
    m_initialLightSourceOffset.zero();
}
