#ifndef LIGHTCOVERAGE_ROOMFILEPATHDISPLAY_H
#define LIGHTCOVERAGE_ROOMFILEPATHDISPLAY_H

#include <QString>
#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QPushButton)

namespace LightCoverage {

class RoomFilePathDisplay : public QWidget
{
    Q_OBJECT

public:
    explicit RoomFilePathDisplay( QWidget* const parent = nullptr );

    void setFilePath( const QString& aFilePath );
    inline const QString& filePath() const
    {
        return m_filePath;
    }

private slots:
    void openFileFolder();
    void openFile();

private:
    void applyFilePath();

private:
    QString m_filePath;

    QPushButton* m_dirPathDisplay;
    QPushButton* m_fileNameDisplay;
};

}

#endif // LIGHTCOVERAGE_ROOMFILEPATHDISPLAY_H
