#include "RoomCompositeDisplay.h"

#include "Data/Room.h"
#include "RoomAreasDisplay.h"
#include "RoomDisplay.h"
#include "RoomDisplayWidget.h"

#include <QAction>
#include <QGLFormat>
#include <QHBoxLayout>
#include <QTabWidget>

using namespace LightCoverage;

RoomCompositeDisplay::RoomCompositeDisplay( QWidget* const parent, Qt::WindowFlags f )
    : QWidget( parent, f )
    , m_roomDisplaySwitch( nullptr )
    , m_roomGPUDisplayTabIndex( -1 )
    , m_roomCPUDisplayTabIndex( -1 )
{
    m_roomDisplaySwitch = new QTabWidget;

    QAction* const navigateToGpuDisplayAction = new QAction( this );
    navigateToGpuDisplayAction->setShortcut( QKeySequence( Qt::CTRL + Qt::Key_1 ) );
    connect( navigateToGpuDisplayAction, SIGNAL(triggered()), this, SLOT(navigateToGpuDisplay()) );
    m_roomDisplaySwitch->addAction( navigateToGpuDisplayAction );

    QAction* const navigateToCpuDisplayAction = new QAction( this );
    navigateToCpuDisplayAction->setShortcut( QKeySequence( Qt::CTRL + Qt::Key_2 ) );
    connect( navigateToCpuDisplayAction, SIGNAL(triggered()), this, SLOT(navigateToCpuDisplay()) );
    m_roomDisplaySwitch->addAction( navigateToCpuDisplayAction );

    QGLFormat glFormat;
    glFormat.setVersion( 3, 3 );
    glFormat.setProfile( QGLFormat::CoreProfile );

    RoomDisplay* const roomDisplayGpu = new RoomDisplay( glFormat );
    m_roomGPUDisplayTabIndex = m_roomDisplaySwitch->addTab( roomDisplayGpu, QString() );
    m_roomDisplays.append( roomDisplayGpu );

    RoomDisplayWidget* const roomDisplayCpu = new RoomDisplayWidget;
    m_roomCPUDisplayTabIndex = m_roomDisplaySwitch->addTab( roomDisplayCpu, QString() );
    m_roomDisplays.append( roomDisplayCpu );

    RoomAreasDisplay* const roomAreasDisplay = new RoomAreasDisplay;
    roomAreasDisplay->setContentsMargins( QMargins() );
    m_roomDisplays.append( roomAreasDisplay );

    QHBoxLayout* const mainLayout = new QHBoxLayout( this );
    mainLayout->addWidget( m_roomDisplaySwitch, 1 );
    mainLayout->addWidget( roomAreasDisplay, 0, Qt::AlignTop | Qt::AlignRight );

    this->retranslateInternal();
}

RoomCompositeDisplay::~RoomCompositeDisplay()
{
}

void RoomCompositeDisplay::setRoom( Room* const aRoom )
{
    foreach ( AbstractRoomDisplay* const roomDisplay, m_roomDisplays )
    {
        roomDisplay->setRoom( aRoom );
    }
}

Room* RoomCompositeDisplay::room() const
{
    return m_roomDisplays.isEmpty()
           ? nullptr
           : m_roomDisplays.first()->room();
}

void RoomCompositeDisplay::retranslate()
{
    this->retranslateInternal();
}

void RoomCompositeDisplay::navigateToGpuDisplay()
{
    m_roomDisplaySwitch->setCurrentIndex( m_roomGPUDisplayTabIndex );
}

void RoomCompositeDisplay::navigateToCpuDisplay()
{
    m_roomDisplaySwitch->setCurrentIndex( m_roomCPUDisplayTabIndex );
}

void RoomCompositeDisplay::retranslateInternal()
{
    m_roomDisplaySwitch->setTabText( m_roomGPUDisplayTabIndex, tr( "GPU" ) );
    m_roomDisplaySwitch->setTabText( m_roomCPUDisplayTabIndex, tr( "CPU" ) );
}
