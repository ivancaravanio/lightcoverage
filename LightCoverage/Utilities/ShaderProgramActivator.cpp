#include "ShaderProgramActivator.h"

#include "OpenGlErrorLogger.h"
#include "OpenGlUtilities.h"
#include "ShaderProgram.h"

using namespace LightCoverage::Utilities;

ShaderProgramActivator::ShaderProgramActivator()
    : m_shaderProgramId( OpenGlUtilities::s_invalidUnsignedGlValue )
    , m_previousShaderProgramId( OpenGlUtilities::s_invalidUnsignedGlValue )
{
}

ShaderProgramActivator::ShaderProgramActivator(
        const QGLFormat& aOpenGlFormat,
        const GLuint aShaderProgramId )
    : m_openGlFormat( aOpenGlFormat )
    , m_shaderProgramId( aShaderProgramId )
    , m_previousShaderProgramId( OpenGlUtilities::s_invalidUnsignedGlValue )
{
}

ShaderProgramActivator::~ShaderProgramActivator()
{
}

bool ShaderProgramActivator::isValid() const
{
    return OpenGlUtilities::isUnsignedGlValueValid( m_shaderProgramId );
}

GLuint ShaderProgramActivator::activeShaderProgramId() const
{
    return ShaderProgram::id( m_openGlFormat );
}

void ShaderProgramActivator::activate()
{
    if ( ! this->isValid() )
    {
        return;
    }

    const GLuint activeShaderProgramId = this->activeShaderProgramId();
    if ( activeShaderProgramId != m_shaderProgramId )
    {
        m_previousShaderProgramId = activeShaderProgramId;
        glUseProgram( m_shaderProgramId );
        LOG_OPENGL_ERROR();
    }
}

void ShaderProgramActivator::deactivate()
{
    if ( ! OpenGlUtilities::isUnsignedGlValueValid( m_previousShaderProgramId ) )
    {
        return;
    }

    glUseProgram( m_previousShaderProgramId );
    LOG_OPENGL_ERROR();

    m_previousShaderProgramId = OpenGlUtilities::s_invalidUnsignedGlValue;
}
