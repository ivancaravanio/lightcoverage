#ifndef LIGHTCOVERAGE_UTILITIES_OPENGLOBJECTBINDER_H
#define LIGHTCOVERAGE_UTILITIES_OPENGLOBJECTBINDER_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include "OpenGlUtilities.h"

#include <QtGlobal>

namespace LightCoverage {
namespace Utilities {

class OpenGlObjectBinder
{
public:
    enum ObjectType
    {
        ObjectTypeInvalid,
        ObjectTypeTextureUnit,  // binding target: 1 , object name: 0
        ObjectTypeTexture,      // binding target: 1 , object name: 1
        ObjectTypeBuffer,       // binding target: 1 , object name: 1
        ObjectTypeVertexArray,  // binding target: 0 , object name: 1
        ObjectTypeFramebuffer,  // binding target: 1 , object name: 1
        ObjectTypeRenderbuffer, // binding target: 1 , object name: 1
        ObjectTypePolygonMode,  // binding target: 1 , object name: 1
        ObjectTypeCullFaceMode  // binding target: 1 , object name: 0
    };

public:
    OpenGlObjectBinder();
    OpenGlObjectBinder(
            const ObjectType aObjectType,
            const GLenum aBindingTarget,
            const GLuint aObjectName,
            const bool aAutoUnbind = true );
    ~OpenGlObjectBinder();

    bool isValid() const;

    ObjectType objectType() const;
    GLenum bindingTarget() const;
    GLuint objectName() const;
    GLuint requestedData() const;

    void bind();
    void unbind();

    // can be binding target or object name
    GLuint boundData( bool* const ok = nullptr ) const;

private:
    static GLenum openGlBindingTargetCheck(
            const ObjectType objectType,
            const GLenum bindingTarget = OpenGlUtilities::invalidEnumGlValue() );
    GLenum openGlBindingTargetCheck() const;

    bool isObjectNameUsedAsBoundData() const;

    void bind( const GLenum aBindingTarget,
               const GLuint aObjectName );

private:
    ObjectType m_objectType;

    GLenum     m_bindingTarget;
    GLuint     m_objectName;

    bool       m_autoUnbind;

    bool       m_isRequestedDataBound;
    GLuint     m_previouslyBoundData;
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_OPENGLOBJECTBINDER_H
