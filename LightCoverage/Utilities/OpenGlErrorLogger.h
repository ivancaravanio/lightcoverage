#ifndef LIGHTCOVERAGE_UTILITIES_OPENGLERRORLOGGER_H
#define LIGHTCOVERAGE_UTILITIES_OPENGLERRORLOGGER_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include <QString>
#include <QtGlobal>

namespace LightCoverage {
namespace Utilities {

class OpenGlErrorLogger
{
public:
    ~OpenGlErrorLogger();

    static OpenGlErrorLogger& instance();

    void logOpenGlError(
            const QString& filePath,
            const int lineNumber ) const;
    static void logOpenGlError(
            const GLenum openGlErrorCode,
            const QString& filePath = QString(),
            const int lineNumber = -1 );

    static bool isOpenGlErrorCodeValid( const GLenum openGlErrorCode );
    static QString errorDescription( const GLenum openGlErrorCode );

private:
    OpenGlErrorLogger();
};

}
}

#ifndef LOG_OPENGL_ERRORS
    #define LOG_OPENGL_ERRORS
#endif

#ifdef LOG_OPENGL_ERRORS
    #define LOG_OPENGL_ERROR() LightCoverage::Utilities::OpenGlErrorLogger::instance().logOpenGlError( __FILE__, __LINE__ )
#else
    #define LOG_OPENGL_ERROR()
#endif

#endif // LIGHTCOVERAGE_UTILITIES_OPENGLERRORLOGGER_H
