#include "FramebufferBinder.h"

#include "OpenGlErrorLogger.h"
#include "OpenGlUtilities.h"

using namespace LightCoverage::Utilities;

FramebufferBinder::FramebufferBinder()
    : m_requestedFboId( OpenGlUtilities::s_invalidUnsignedGlValue )
    , m_bindingTarget( BindingTargetBoth )
    , m_previouslyBoundFboId( OpenGlUtilities::s_invalidUnsignedGlValue )
    , m_isRequestedFboBound( false )
    , m_autoUnbind( false )
{
}

FramebufferBinder::FramebufferBinder(
        const GLuint aRequestedFboId,
        const BindingTarget aBindingTarget,
        const bool aAutoUnbind )
    : m_requestedFboId( aRequestedFboId )
    , m_bindingTarget( aBindingTarget )
    , m_previouslyBoundFboId( OpenGlUtilities::s_invalidUnsignedGlValue )
    , m_isRequestedFboBound( false )
    , m_autoUnbind( aAutoUnbind )
{
}

FramebufferBinder::~FramebufferBinder()
{
    if ( m_autoUnbind )
    {
        this->unbind();
    }
}

GLuint FramebufferBinder::requestedFboId() const
{
    return m_requestedFboId;
}

FramebufferBinder::BindingTarget FramebufferBinder::bindingTarget() const
{
    return m_bindingTarget;
}

GLuint FramebufferBinder::boundFboId() const
{
    // 0 is the default FBO -> screen
    // CAUTION: Qt's QOpenGL(Widget|Window) use a proxy framebuffer that differs from the screen's one:
    //
    // http://doc-snapshot.qt-project.org/qt5-5.4/qopenglwindow.html :
    //     QOpenGLWindow supports multiple update behaviors.
    //     The default, NoPartialUpdate is equivalent to a regular, OpenGL-based QWindow or the legacy QGLWidget.
    //     In contrast, PartialUpdateBlit and PartialUpdateBlend are more in line with QOpenGLWidget's way of working, where there is always an extra, dedicated framebuffer object present.
    //     These modes allow, by sacrificing some performance, redrawing only a smaller area on each paint and having the rest of the content preserved from of the previous frame.
    //     This is useful for applications than render incrementally using QPainter, because this way they do not have to redraw the entire window content on each paintGL() call.
    //
    // http://doc-snapshot.qt-project.org/qt5-5.4/qopenglwidget.html :
    //     All rendering happens into an OpenGL framebuffer object.
    //     makeCurrent() ensure that it is bound in the context.
    //     Keep this in mind when creating and binding additional framebuffer objects in the rendering code in paintGL().
    //     Never re-bind the framebuffer with ID 0.
    //     Instead, call defaultFramebufferObject() to get the ID that should be bound.

    GLint currentlyBoundFbo = OpenGlUtilities::s_invalidUnsignedGlValue;
    glGetIntegerv( this->openGlFboBindingTargetCheck(), & currentlyBoundFbo );
    LOG_OPENGL_ERROR();

    return currentlyBoundFbo;
}

GLenum FramebufferBinder::openGlFboBindingTarget( const FramebufferBinder::BindingTarget bindingTarget )
{
    switch ( bindingTarget )
    {
        case BindingTargetDraw: return GL_DRAW_FRAMEBUFFER;
        case BindingTargetRead: return GL_READ_FRAMEBUFFER;
        case BindingTargetBoth: return GL_FRAMEBUFFER;
    }

    return GL_FRAMEBUFFER;
}

GLenum FramebufferBinder::openGlFboBindingTarget() const
{
    return FramebufferBinder::openGlFboBindingTarget( m_bindingTarget );
}

GLenum FramebufferBinder::openGlFboBindingTargetCheck( const FramebufferBinder::BindingTarget bindingTarget )
{
    switch ( bindingTarget )
    {
        case BindingTargetDraw: return GL_DRAW_FRAMEBUFFER_BINDING;
        case BindingTargetRead: return GL_READ_FRAMEBUFFER_BINDING;
        case BindingTargetBoth: return GL_FRAMEBUFFER_BINDING;
    }

    return GL_FRAMEBUFFER_BINDING;
}

GLenum FramebufferBinder::openGlFboBindingTargetCheck() const
{
    return FramebufferBinder::openGlFboBindingTargetCheck( m_bindingTarget );
}

void FramebufferBinder::bind( const GLuint fboId )
{
    glBindFramebuffer( this->openGlFboBindingTarget(), fboId );
    LOG_OPENGL_ERROR();
}

void FramebufferBinder::bind()
{
    const GLuint boundFboId = this->boundFboId();
    if ( boundFboId == m_requestedFboId )
    {
        return;
    }

    m_previouslyBoundFboId = boundFboId;
    m_isRequestedFboBound = true;
    this->bind( m_requestedFboId );
}

void FramebufferBinder::unbind()
{
    if ( ! m_isRequestedFboBound )
    {
        return;
    }

    this->bind( m_previouslyBoundFboId );

    m_previouslyBoundFboId = OpenGlUtilities::s_invalidUnsignedGlValue;
    m_isRequestedFboBound = false;
}
