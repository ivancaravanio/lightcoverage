#ifndef LIGHTCOVERAGE_UTILITIES_GENERALUTILITIES_H
#define LIGHTCOVERAGE_UTILITIES_GENERALUTILITIES_H

#include <QByteArray>

#ifndef QT_NO_DRAGANDDROP
    QT_FORWARD_DECLARE_CLASS(QMimeData)
#endif

namespace LightCoverage {
namespace Utilities {

class GeneralUtilities
{
public:
    template < typename T, typename P >
    static P convertedCollection( const T& collection );

    static QByteArray fileContents( const QString& fileName );
    static QString toExistingCanonicalFilePath( const QString& filePath );
    static bool areFilePathsEqual( const QString& leftFilePath,
                                   const QString& rightFilePath );

#ifndef QT_NO_DRAGANDDROP
    static QString filePathFromMimeData( const QMimeData* const mimeData );
#endif

private:
    // do not define:
    GeneralUtilities();
};

template < typename T, typename P >
static P GeneralUtilities::convertedCollection( const T& collection )
{
    P result;

    const int itemsCount = collection.size();
    if ( itemsCount == 0 )
    {
        return result;
    }

    result.reserve( itemsCount );
    for ( int i = 0; i < itemsCount; ++ i )
    {
        result.append( P::value_type( collection[ i ] ) );
    }

    return result;
}

}
}

#endif // LIGHTCOVERAGE_UTILITIES_GENERALUTILITIES_H
