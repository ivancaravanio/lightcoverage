#ifndef LIGHTCOVERAGE_UTILITIES_OPENGLUTILITIES_H
#define LIGHTCOVERAGE_UTILITIES_OPENGLUTILITIES_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

namespace LightCoverage {
namespace Utilities {

class OpenGlUtilities
{
public:
    enum GeometricPrimitiveType
    {
        GeometricPrimitiveTypeInvalid,
        GeometricPrimitiveTypeTriangle,
        GeometricPrimitiveTypeTriangleFan,
        GeometricPrimitiveTypeTriangleStrip
    };

public:
    static const GLuint s_invalidUnsignedGlValue;
    static const GLint  s_invalidSignedGlValue;

public:
    static GLenum invalidEnumGlValue();

    static bool isUnsignedGlValueValid( const GLuint unsignedGlValue );
    static bool isSignedGlValueValid( const GLint signedGlValue );
    static bool isEnumGlValueValid( const GLenum enumGlValue );

    static bool isGeometricPrimitiveTypeValid( const GeometricPrimitiveType geometricPrimitiveType );
    static GLenum openGLGeometricPrimitiveType( const GeometricPrimitiveType geometricPrimitiveType );

private:
    // do not define
    OpenGlUtilities();
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_OPENGLUTILITIES_H
