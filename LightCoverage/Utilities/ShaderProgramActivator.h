#ifndef LIGHTCOVERAGE_UTILITIES_SHADERPROGRAMACTIVATOR_H
#define LIGHTCOVERAGE_UTILITIES_SHADERPROGRAMACTIVATOR_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include <QGLFormat>
#include <QtGlobal>

namespace LightCoverage {
namespace Utilities {

class ShaderProgramActivator
{
public:
    ShaderProgramActivator();
    ShaderProgramActivator(
            const QGLFormat& aOpenGlFormat,
            const GLuint aShaderProgramId );
    ~ShaderProgramActivator();

    bool isValid() const;

    GLuint activeShaderProgramId() const;

    void activate();
    void deactivate();

private:
    QGLFormat m_openGlFormat;
    GLuint    m_shaderProgramId;
    GLuint    m_previousShaderProgramId;
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_SHADERPROGRAMACTIVATOR_H
