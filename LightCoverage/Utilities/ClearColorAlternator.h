#ifndef LIGHTCOVERAGE_UTILITIES_CLEARCOLORALTERNATOR_H
#define LIGHTCOVERAGE_UTILITIES_CLEARCOLORALTERNATOR_H

#include <QColor>

namespace LightCoverage {
namespace Utilities {

class ClearColorAlternator
{
public:
    ClearColorAlternator();
    ClearColorAlternator(
            const QColor& aRequestedClearColor,
            const bool aAutoRollback = true );
    ~ClearColorAlternator();

    bool isValid() const;

    const QColor& requestedClearColor() const;

    QColor currentClearColor() const;

    void change();
    void rollback();

private:
    void change( const QColor& clearColor );

private:
    QColor m_requestedClearColor;
    QColor m_previousClearColor;
    bool m_autoRollback;
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_CLEARCOLORALTERNATOR_H
