#ifndef LIGHTCOVERAGE_UTILITIES_VIEWPORTALTERNATOR_H
#define LIGHTCOVERAGE_UTILITIES_VIEWPORTALTERNATOR_H

#include <QRect>

namespace LightCoverage {
namespace Utilities {

// http://www.gamedev.net/page/resources/_/technical/opengl/opengl-frame-buffer-object-101-r2331 :
//     glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
//     glPushAttrib(GL_VIEWPORT_BIT);
//     glViewport(0,0,width, height); // Render as normal here
//                                    // output goes to the FBO and it’s attached buffers glPopAttrib();
//     glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
//     Three lines which probably jumped out at you right away are the glPushAttrib/glPopAttrib pair and the glViewport call.
//     The glViewport call is needed so that we don’t try to render into an area bigger than, or even smaller than, the FBO is setup for.
//     The glPushAtrrib and glPopAttrib are used as a quick way to save the old viewport information,
//     this is needed because the FBO shares all of its states with the main context
//     and as such any changes made affect both the FBO and the main context you would be normally rendering to.
//
// https://www.opengl.org/sdk/docs/man2/xhtml/glPushAttrib.xml
//
// https://www.opengl.org/discussion_boards/showthread.php/173957-glPushAttrib-depreciated-replace-by
//

class ViewportAlternator
{
public:
    ViewportAlternator();
    ViewportAlternator(
            const QRect& aRequestedViewport,
            const bool aAutoRollback = true );
    ~ViewportAlternator();

    bool isValid() const;

    const QRect& requestedViewport() const;

    // Qt's 2D plane Y axis direction is pointing downwards whereas OpenGL's one is pointing upwards
    // so this rectangle's top-left is actually bottom-left
    QRect currentViewport() const;

    void change();
    void rollback();

private:
    void change( const QRect& viewport );

private:
    QRect m_requestedViewport;
    QRect m_previousViewport;
    bool m_autoRollback;
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_VIEWPORTALTERNATOR_H
