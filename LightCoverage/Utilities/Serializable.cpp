#include "Serializable.h"

using namespace LightCoverage::Utilities;

Serializable::~Serializable()
{
}

QString Serializable::serialize() const
{
    QString s;
    QTextStream ts( & s );
    this->serialize( ts );

    return s;
}
