#include "OpenGlErrorLogger.h"

#include "../LightCoverage_namespace.h"

#include <QDebug>

using namespace LightCoverage::Utilities;

OpenGlErrorLogger::OpenGlErrorLogger()
{
}

OpenGlErrorLogger::~OpenGlErrorLogger()
{
}

OpenGlErrorLogger& OpenGlErrorLogger::instance()
{
    static OpenGlErrorLogger logger;
    return logger;
}

void OpenGlErrorLogger::logOpenGlError( const QString& filePath, const int lineNumber ) const
{
    const GLenum openGlErrorCode = glGetError();
    OpenGlErrorLogger::logOpenGlError( openGlErrorCode, filePath, lineNumber );
}

void OpenGlErrorLogger::logOpenGlError(
        const GLenum openGlErrorCode,
        const QString& filePath,
        const int lineNumber )
{
    if ( ! OpenGlErrorLogger::isOpenGlErrorCodeValid( openGlErrorCode )
         || openGlErrorCode == GL_NO_ERROR )
    {
        return;
    }

    qDebug() << "OpenGL error:";
    if ( ! filePath.isEmpty() )
    {
        qDebug() << "\tfile:" << filePath;
    }

    if ( lineNumber >= 0 )
    {
        qDebug() << "\tline:" << lineNumber;
    }

    qDebug() << "\tcode:" << QString::number( openGlErrorCode, 16 ) << endl
                       << "\tdescription:" << OpenGlErrorLogger::errorDescription( openGlErrorCode );
}

bool OpenGlErrorLogger::isOpenGlErrorCodeValid( const GLenum openGlErrorCode )
{
    return openGlErrorCode == GL_INVALID_ENUM
           || openGlErrorCode == GL_INVALID_VALUE
           || openGlErrorCode == GL_INVALID_OPERATION
           || openGlErrorCode == GL_STACK_OVERFLOW
           || openGlErrorCode == GL_STACK_UNDERFLOW
           || openGlErrorCode == GL_OUT_OF_MEMORY;
}

QString OpenGlErrorLogger::errorDescription( const GLenum openGlErrorCode )
{
    switch ( openGlErrorCode )
    {
        case GL_INVALID_ENUM:
            return STRINGIFY( GL_INVALID_ENUM );
        case GL_INVALID_VALUE:
            return STRINGIFY( GL_INVALID_VALUE );
        case GL_INVALID_OPERATION:
            return STRINGIFY( GL_INVALID_OPERATION );
        case GL_STACK_OVERFLOW:
            return STRINGIFY( GL_STACK_OVERFLOW );
        case GL_STACK_UNDERFLOW:
            return STRINGIFY( GL_STACK_UNDERFLOW );
        case GL_OUT_OF_MEMORY:
            return STRINGIFY( GL_OUT_OF_MEMORY );
        default:
            break;
    }

    return QString();
}
