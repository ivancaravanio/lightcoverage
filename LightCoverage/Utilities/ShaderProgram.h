#ifndef LIGHTCOVERAGE_UTILITIES_SHADERPROGRAM_H
#define LIGHTCOVERAGE_UTILITIES_SHADERPROGRAM_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include "../Data/ShaderInfo.h"
#include "OpenGlUtilities.h"
#include "ShaderProgramActivator.h"

#include <QColor>
#include <QGLFormat>
#include <QList>
#include <QtGlobal>

namespace LightCoverage {
namespace Utilities {

using namespace LightCoverage::Data;

class ShaderProgram
{
public:
    ShaderProgram();
    ShaderProgram(
            const QGLFormat& aOpenGlFormat,
            const QList< ShaderInfo >& aShaders = QList< ShaderInfo >() );
    ~ShaderProgram();

    void setOpenGLFormat( const QGLFormat& aOpenGlFormat );
    void appendShader( const ShaderInfo& shaderInfo );
    void setShaders( const QList< ShaderInfo >& aShaders );

    bool build();
    bool build( const QString& vertexShaderFilePath,
                const QString& fragmentShaderFilePath );

    static GLuint id( const QGLFormat& aOpenGlFormat );
    GLuint id() const;
    bool isValid() const;

    void activate();
    void deactivate();
    bool isActive() const;

    GLuint activeShaderProgramId() const;

    void clear();

    // Shader compilers are generally pretty aggressive about optimizing away compile-time constants in any form,
    // and a good mature GLSL compiler will have no trouble with this.
    // The variable should be an active one = existing and not optimized-out.
    GLint genericVertexAttributeVarLocation( const QByteArray& name ) const;
    GLint fragmentOutputVarLocation( const QByteArray& name ) const;
    GLint uniformVarLocation( const QByteArray& name ) const;

    void setUniformVariable1f( const GLint location, const GLfloat v0 );
    void setUniformVariable2f( const GLint location, const GLfloat v0, const GLfloat v1 );
    void setUniformVariable3f( const GLint location, const GLfloat v0, const GLfloat v1, const GLfloat v2 );
    void setUniformVariable4f( const GLint location, const GLfloat v0, const GLfloat v1, const GLfloat v2, const GLfloat v3 );

    // https://www.opengl.org/wiki/GLSL_:_common_mistakes#How_to_use_glUniform
    void setUniformVariable1fv( const GLint location, const GLsizei count, const GLfloat* const value );
    void setUniformVariable2fv( const GLint location, const GLsizei count, const GLfloat* const value );
    void setUniformVariable3fv( const GLint location, const GLsizei count, const GLfloat* const value );
    void setUniformVariable4fv( const GLint location, const GLsizei count, const GLfloat* const value );

    void setUniformVariable1i( const GLint location, const GLint v0 );
    void setUniformVariable2i( const GLint location, const GLint v0, const GLint v1 );
    void setUniformVariable3i( const GLint location, const GLint v0, const GLint v1, const GLint v2 );
    void setUniformVariable4i( const GLint location, const GLint v0, const GLint v1, const GLint v2, const GLint v3 );

    void setUniformVariable1iv( const GLint location, const GLsizei count, const GLint* const value );
    void setUniformVariable2iv( const GLint location, const GLsizei count, const GLint* const value );
    void setUniformVariable3iv( const GLint location, const GLsizei count, const GLint* const value );
    void setUniformVariable4iv( const GLint location, const GLsizei count, const GLint* const value );

    void setUniformMatrix2fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix3fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix4fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );

    void setUniformMatrix2x3fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix3x2fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix2x4fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix4x2fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix3x4fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );
    void setUniformMatrix4x3fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value );

    void setUniformColor( const GLint location, const QColor& aColor );

    void dumpPostLinkInfo();

private:
    static void cleanShaders(
            const QList< GLuint >& shaderIds,
            const GLuint shaderProgramId = OpenGlUtilities::s_invalidUnsignedGlValue );
    static QString shaderVariableTypeDescription( const GLenum shaderVariableType );

private:
    QGLFormat m_openGlFormat;
    QList< ShaderInfo > m_shaders;

    GLuint m_id;
    ShaderProgramActivator m_activator;
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_SHADERPROGRAM_H
