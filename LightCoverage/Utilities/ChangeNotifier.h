#ifndef LIGHTCOVERAGE_UTILITIES_CHANGENOTIFIER_H
#define LIGHTCOVERAGE_UTILITIES_CHANGENOTIFIER_H

#include <QList>
#include <QtGlobal>

namespace LightCoverage {
namespace Utilities {

class ChangeListener;

class ChangeNotifier
{
public:
    static const quint64 s_invalidTrait;

public:
    ChangeNotifier();
    virtual ~ChangeNotifier();

    void addListener( ChangeListener* const listener );
    void removeListener( ChangeListener* const listener );
    void clearListeners();
    bool hasListener( ChangeListener* const listener ) const;
    int listenersCount() const;

    virtual void setNotificationsBlocked( const bool aAreNotificationsBlocked );
    bool areNotificationsBlocked() const;

    void changed( const quint64& aTraits );
    virtual quint64 allTraits() const;
    virtual void notifyAllRolesChanged();

private:
    QList< ChangeListener* > m_listeners;
    bool                     m_areNotificationsBlocked;
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_CHANGENOTIFIER_H
