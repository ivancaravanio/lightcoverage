#include "GeneralUtilities.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>

#ifndef QT_NO_DRAGANDDROP
    #include <QMimeData>
    #include <QUrl>
#endif

using namespace LightCoverage::Utilities;

QByteArray GeneralUtilities::fileContents( const QString& fileName )
{
    QFile file( fileName );
    if ( ! file.open( QIODevice::ReadOnly ) )
    {
        return QByteArray();
    }

    return file.readAll();
}

QString GeneralUtilities::toExistingCanonicalFilePath( const QString& filePath )
{
    if ( filePath.isEmpty() )
    {
        return QString();
    }

    QFileInfo fileInfo( filePath );
    if ( ! fileInfo.exists() )
    {
        return QString();
    }

    if ( fileInfo.isSymLink() )
    {
        fileInfo.setFile( fileInfo.symLinkTarget() );
    }

    return fileInfo.exists()
           ? QDir::toNativeSeparators( fileInfo.canonicalFilePath() )
           : QString();
}

bool GeneralUtilities::areFilePathsEqual( const QString& leftFilePath, const QString& rightFilePath )
{
    return leftFilePath.compare( rightFilePath,
                             #ifdef Q_OS_WIN
                                 Qt::CaseInsensitive
                             #else
                                 Qt::CaseSensitive
                             #endif
                                 ) == 0;
}

#ifndef QT_NO_DRAGANDDROP
QString GeneralUtilities::filePathFromMimeData( const QMimeData* const mimeData )
{
    if ( mimeData == nullptr )
    {
        return QString();
    }

    if ( ! mimeData->hasUrls() )
    {
        return QString();
    }

    const QList< QUrl > urls = mimeData->urls();
    if ( urls.isEmpty() )
    {
        return QString();
    }

    const QUrl& firstUrl = urls.first();
    if ( ! firstUrl.isLocalFile() )
    {
        return QString();
    }

    return GeneralUtilities::toExistingCanonicalFilePath( firstUrl.toLocalFile() );
}
#endif
