#ifndef LIGHTCOVERAGE_UTILITIES_RETRANSLATABLE_H
#define LIGHTCOVERAGE_UTILITIES_RETRANSLATABLE_H

#include <QSet>

namespace LightCoverage {
namespace Utilities {

class Retranslatable
{
public:
    Retranslatable();
    virtual ~Retranslatable();

    virtual void retranslate() = 0;
    static void retranslateAll();

private:
    static QSet< Retranslatable* > s_allRetranslatables;
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_RETRANSLATABLE_H
