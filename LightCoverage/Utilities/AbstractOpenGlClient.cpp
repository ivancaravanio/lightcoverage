#include "AbstractOpenGlClient.h"

#include "OpenGlUtilities.h"

using namespace LightCoverage::Utilities;

AbstractOpenGlClient::~AbstractOpenGlClient()
{
}

OpenGlObjectBinder AbstractOpenGlClient::textureUnitBinder(
        const GLenum textureUnit,
        const bool autoUnbind ) const
{
    return OpenGlObjectBinder( OpenGlObjectBinder::ObjectTypeTextureUnit,
                               textureUnit,
                               OpenGlUtilities::s_invalidUnsignedGlValue,
                               autoUnbind );
}

OpenGlObjectBinder AbstractOpenGlClient::textureBinder(
        const GLuint textureId,
        const bool autoUnbind ) const
{
    return OpenGlUtilities::isUnsignedGlValueValid( textureId )
           ? OpenGlObjectBinder( OpenGlObjectBinder::ObjectTypeTexture,
                                 GL_TEXTURE_2D,
                                 textureId,
                                 autoUnbind )
           : OpenGlObjectBinder();
}

OpenGlObjectBinder AbstractOpenGlClient::vaoBinder( const GLuint vaoId, const bool autoUnbind ) const
{
    return OpenGlUtilities::isUnsignedGlValueValid( vaoId )
           ? OpenGlObjectBinder( OpenGlObjectBinder::ObjectTypeVertexArray,
                                 OpenGlUtilities::invalidEnumGlValue(),
                                 vaoId,
                                 autoUnbind )
           : OpenGlObjectBinder();
}

OpenGlObjectBinder AbstractOpenGlClient::vboBinder( const GLuint vboId, const bool autoUnbind ) const
{
    return OpenGlUtilities::isUnsignedGlValueValid( vboId )
           ? OpenGlObjectBinder( OpenGlObjectBinder::ObjectTypeBuffer,
                                 GL_ARRAY_BUFFER,
                                 vboId,
                                 autoUnbind )
           : OpenGlObjectBinder();
}

OpenGlObjectBinder AbstractOpenGlClient::veaBinder( const GLuint veaId, const bool autoUnbind ) const
{
    return OpenGlUtilities::isUnsignedGlValueValid( veaId )
           ? OpenGlObjectBinder( OpenGlObjectBinder::ObjectTypeBuffer,
                                 GL_ELEMENT_ARRAY_BUFFER,
                                 veaId,
                                 autoUnbind )
           : OpenGlObjectBinder();
}

OpenGlObjectBinder AbstractOpenGlClient::polygonModeAlternator(
        const GLenum face,
        const GLenum mode,
        const bool autoUnbind )
{
    return OpenGlObjectBinder(
                OpenGlObjectBinder::ObjectTypePolygonMode,
                face,
                static_cast< GLuint >( mode ),
                autoUnbind );
}

FramebufferBinder AbstractOpenGlClient::fboBinder(
        const GLuint aRequestedFboId,
        const FramebufferBinder::BindingTarget aBindingTarget,
        const bool aAutoUnbind ) const
{
    return FramebufferBinder(
                aRequestedFboId,
                aBindingTarget,
                aAutoUnbind );
}

ViewportAlternator AbstractOpenGlClient::viewportAlternator( const QRect& aRequestedViewport, const bool autoRollback ) const
{
    return ViewportAlternator(
                aRequestedViewport,
                autoRollback );
}

OpenGlCapabilityEnabler AbstractOpenGlClient::capabilityEnabler(
        const GLenum aCapability,
        const bool aShouldEnable,
        const bool aAutoRollback ) const
{
    return OpenGlCapabilityEnabler(
                aCapability,
                aShouldEnable,
                aAutoRollback );
}
