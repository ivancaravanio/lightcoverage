#include "OpenGlTimeCounter.h"

#include "OpenGlErrorLogger.h"
#include "OpenGlUtilities.h"

#include <QDebug>
#include <QString>

using namespace LightCoverage::Utilities;

OpenGlTimeCounter::OpenGlTimeCounter()
    : m_timeNanosecs( 0ULL )
    , m_isRunning( false )
{
    // do not activate + deactivate shader program,
    // since GPU calls time consumption is measured during the time it is active because of other calls
}

OpenGlTimeCounter::~OpenGlTimeCounter()
{
    const int timerQueriesCount = m_timerQueries.size();
    if ( timerQueriesCount > 0 )
    {
        glDeleteQueries( timerQueriesCount, m_timerQueries.constData() );
        m_timerQueries.clear();
    }
}

void OpenGlTimeCounter::start()
{
    if ( m_timerQueries.isEmpty() )
    {
        m_timerQueries.fill( OpenGlUtilities::s_invalidUnsignedGlValue, TimerQueriesCount );
        glGenQueries( m_timerQueries.size(), m_timerQueries.data() );
        LOG_OPENGL_ERROR();
    }

    m_isRunning = true;
    m_timeNanosecs = 0ULL;

    const GLint startTimerQuery = m_timerQueries[ TimerQueryIndexStart ];
    glQueryCounter( startTimerQuery, GL_TIMESTAMP );
    LOG_OPENGL_ERROR();
}

void OpenGlTimeCounter::stop()
{
    if ( ! m_isRunning )
    {
        return;
    }

    const GLint endTimerQuery = m_timerQueries[ TimerQueryIndexEnd ];
    glQueryCounter( endTimerQuery, GL_TIMESTAMP );
    LOG_OPENGL_ERROR();

    GLint isEndTimeAvailable = GL_FALSE;
    do {
        glGetQueryObjectiv( endTimerQuery, GL_QUERY_RESULT_AVAILABLE, &isEndTimeAvailable );
        LOG_OPENGL_ERROR();
    } while ( GL_FALSE == isEndTimeAvailable );

    const GLint startTimerQuery = m_timerQueries[ TimerQueryIndexStart ];

    GLuint64 startTime = 0ULL;
    glGetQueryObjectui64v( startTimerQuery, GL_QUERY_RESULT, &startTime );
    LOG_OPENGL_ERROR();

    GLuint64 endTime = 0ULL;
    glGetQueryObjectui64v( endTimerQuery, GL_QUERY_RESULT, &endTime );
    LOG_OPENGL_ERROR();

    m_isRunning = false;
    m_timeNanosecs = endTime - startTime;
}

bool OpenGlTimeCounter::isRunning() const
{
    return m_isRunning;
}

GLuint64 OpenGlTimeCounter::timeNanosecs() const
{
    return m_timeNanosecs;
}

qreal OpenGlTimeCounter::timeMillisecs() const
{
    return qreal( m_timeNanosecs ) / 1E6;
}

void OpenGlTimeCounter::dumpTime() const
{
    qDebug() << QString( "time (ms): %1" ).arg( this->timeMillisecs(), 0, 'f', 2 );
}
