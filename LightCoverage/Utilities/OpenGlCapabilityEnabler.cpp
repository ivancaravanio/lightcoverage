#include "OpenGlCapabilityEnabler.h"

#include "OpenGlErrorLogger.h"
#include "OpenGlUtilities.h"

using namespace LightCoverage::Utilities;

OpenGlCapabilityEnabler::OpenGlCapabilityEnabler()
    : m_capability( OpenGlUtilities::invalidEnumGlValue() )
    , m_shouldEnable( false )
    , m_wasChanged( false )
    , m_autoRollback( false )
{
}

OpenGlCapabilityEnabler::OpenGlCapabilityEnabler(
        const GLenum aCapability,
        const bool aShouldEnable,
        const bool aAutoRollback )
    : m_capability( OpenGlUtilities::isEnumGlValueValid( aCapability )
                    ? aCapability
                    : OpenGlUtilities::invalidEnumGlValue() )
    , m_shouldEnable( aShouldEnable )
    , m_wasChanged( false )
    , m_autoRollback( aAutoRollback )
{
}

OpenGlCapabilityEnabler::~OpenGlCapabilityEnabler()
{
    if ( m_autoRollback )
    {
        this->rollback();
    }
}

bool OpenGlCapabilityEnabler::isValid() const
{
    return OpenGlUtilities::isEnumGlValueValid( m_capability );
}

void OpenGlCapabilityEnabler::change( const bool shouldEnable )
{
    if ( ! this->isValid() )
    {
        return;
    }

    if ( shouldEnable )
    {
        glEnable( m_capability );
        LOG_OPENGL_ERROR();
    }
    else
    {
        glDisable( m_capability );
        LOG_OPENGL_ERROR();
    }
}

void OpenGlCapabilityEnabler::change()
{
    if ( ! this->isValid() )
    {
        return;
    }

    const bool isEnabled = this->isEnabled();
    if ( isEnabled == m_shouldEnable )
    {
        return;
    }

    m_wasChanged = true;
    this->change( m_shouldEnable );
}

void OpenGlCapabilityEnabler::rollback()
{
    if ( ! m_wasChanged
         || ! this->isValid() )
    {
        return;
    }

    this->change( ! m_shouldEnable );
}

GLenum OpenGlCapabilityEnabler::capability() const
{
    return m_capability;
}

bool OpenGlCapabilityEnabler::shouldEnable() const
{
    return m_shouldEnable;
}

bool OpenGlCapabilityEnabler::isEnabled() const
{
    if ( ! this->isValid() )
    {
        return false;
    }

    const GLboolean isEnabled = glIsEnabled( m_capability );
    LOG_OPENGL_ERROR();

    return isEnabled == GL_TRUE;
}
