#ifndef LIGHTCOVERAGE_UTILITIES_ROOMSERIALIZER_H
#define LIGHTCOVERAGE_UTILITIES_ROOMSERIALIZER_H

#include "../Data/Polygon2D.h"
#include "../Data/Room.h"
#include "../Data/Size.h"
#include "../Data/Vector2D.h"

#include <QFile>
#include <QString>
#include <QStringList>
#include <QVector>

namespace LightCoverage {
namespace Utilities {

using namespace LightCoverage::Data;

class RoomSerializer
{
public:
    static Room deserialize( const QString& aFilePath );
    static void serialize( const QString& aFilePath, const Room& config );

private:
    static const char s_separator;

private:
    // do not define
    RoomSerializer();

    static QString readCleanLine( QFile& file );

    static QStringList strings( QFile& file );
    static QStringList strings( const QString& s );

    static QVector< float > floats( QFile& file, bool* const converted = nullptr );
    static QVector< float > floats( const QStringList& strings, bool* const converted = nullptr );

    static QVector< Vector2D > points( QFile& file, bool* const converted = nullptr );
    static QVector< Vector2D > points( const QStringList& strings, bool* const converted = nullptr );

    static QVector< Size > sizes( QFile& file, bool* const converted = nullptr );
    static QVector< Size > sizes( const QStringList& strings, bool* const converted = nullptr );

    static Polygon2D poly( QFile& file, bool* const converted = nullptr );
    static Polygon2D poly( const QString& s, bool* const converted = nullptr );

    static bool columns( QList< Polygon2D >& aColumns, QFile& file );
    static bool hasMoreContent( QFile& file );
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_ROOMSERIALIZER_H
