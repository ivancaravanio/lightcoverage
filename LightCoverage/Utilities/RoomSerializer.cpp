#include "RoomSerializer.h"

#include "MathUtilities.h"

#include <QTextStream>

using namespace LightCoverage::Utilities;

const char RoomSerializer::s_separator = ' ';

Room RoomSerializer::deserialize( const QString& aFilePath )
{
    QFile file( aFilePath );
    if ( ! file.open( QIODevice::ReadOnly | QIODevice::Text ) )
    {
        return Room();
    }

    if ( file.bytesAvailable() == 0 )
    {
        return Room();
    }

    bool converted = false;
    const QVector< Size > roomSizes = RoomSerializer::sizes( file, & converted );
    if ( ! converted || roomSizes.size() != 1 )
    {
        return Room();
    }

    if ( file.bytesAvailable() == 0 )
    {
        return Room();
    }

    const QVector< Vector2D > lightPositions = RoomSerializer::points( file, & converted );
    if ( ! converted || lightPositions.size() != 1 )
    {
        return Room();
    }

    QList< Polygon2D > columns;
    if ( ! RoomSerializer::columns( columns, file ) )
    {
        return Room();
    }

    const Room config( roomSizes.first(),
                       lightPositions.first(),
                       columns );

    return config.isValid()
           ? config
           : Room();
}

void RoomSerializer::serialize( const QString& aFilePath, const Room& config )
{
    if ( ! config.isValid() )
    {
        return;
    }

    QFile file( aFilePath );
    if ( ! file.open( QIODevice::WriteOnly | QIODevice::Text ) )
    {
        return;
    }

    QTextStream ts( & file );
    ts << config.size().width() << s_separator << config.size().height() << endl
       << config.lightSourcePosition().x() << s_separator << config.lightSourcePosition().y() << endl;

    const QList< Polygon2D >& columns = config.columns();
    const int columnsCount = columns.size();
    if ( columnsCount == 0 )
    {
        return;
    }

    ts << columnsCount << endl;

    for ( int i = 0; i < columnsCount; ++ i )
    {
        const Polygon2D& column = columns[ i ];
        const int verticesCount = column.verticesCount();
        ts << verticesCount;
        for ( int j = 0; j < verticesCount; ++ j )
        {
            const Vector2D& v = column.vertexAt( j );
            ts << s_separator << v.x() << s_separator << v.y();
        }

        ts << endl;
    }
}

QString RoomSerializer::readCleanLine( QFile& file )
{
    return QString( file.readLine() ).trimmed();
}

QStringList RoomSerializer::strings( QFile& file )
{
    return RoomSerializer::strings( RoomSerializer::readCleanLine( file ) );
}

QStringList RoomSerializer::strings( const QString& s )
{
    return s.split( s_separator, QString::SkipEmptyParts );
}

QVector< float > RoomSerializer::floats( QFile& file, bool* const converted )
{
    return RoomSerializer::floats( RoomSerializer::strings( file ), converted );
}

QVector< float > RoomSerializer::floats( const QStringList& strings, bool* const converted )
{
    const int stringsCount = strings.size();
    if ( stringsCount == 0 )
    {
        return QVector< float >();
    }

    QVector< float > floats;

    bool ok = false;
    for ( int i = 0; i < stringsCount; ++ i )
    {
        const float num = strings[ i ].toFloat( & ok );
        if ( ! ok )
        {
            if ( converted != nullptr )
            {
                *converted = false;
            }

            return QVector< float >();
        }

        floats.append( num );
    }

    if ( converted != nullptr )
    {
        *converted = true;
    }

    return floats;
}

QVector< Vector2D > RoomSerializer::points( QFile& file, bool* const converted )
{
    return RoomSerializer::points( RoomSerializer::strings( file ), converted );
}

QVector< Vector2D > RoomSerializer::points( const QStringList& strings, bool* const converted )
{
    const int stringsCount = strings.size();
    if ( stringsCount % 2 != 0 )
    {
        if ( converted != nullptr )
        {
            *converted = false;
        }

        return QVector< Vector2D >();
    }

    bool ok = false;

    QVector< Vector2D > points;
    for ( int i = 0; i < stringsCount; i += 2 )
    {
        const float x = strings[ i ].toFloat( & ok );
        if ( ! converted )
        {
            if ( converted != nullptr )
            {
                *converted = false;
            }

            return QVector< Vector2D >();
        }

        const float y = strings[ i + 1 ].toFloat( & ok );
        if ( ! converted )
        {
            if ( converted != nullptr )
            {
                *converted = false;
            }

            return QVector< Vector2D >();
        }

        points.append( Vector2D( x, y ) );
    }

    if ( converted != nullptr )
    {
        *converted = true;
    }

    return points;
}

QVector< Size > RoomSerializer::sizes( QFile& file, bool* const converted )
{
    return RoomSerializer::sizes( RoomSerializer::strings( file ), converted );
}

QVector< Size > RoomSerializer::sizes( const QStringList& strings, bool* const converted )
{
    const int stringsCount = strings.size();
    if ( stringsCount % 2 != 0 )
    {
        if ( converted != nullptr )
        {
            *converted = false;
        }

        return QVector< Size >();
    }

    bool ok = false;

    QVector< Size > points;
    for ( int i = 0; i < stringsCount; i += 2 )
    {
        const float w = strings[ i ].toFloat( & ok );
        if ( ! converted || ! MathUtilities::isLengthValid( w ) )
        {
            if ( converted != nullptr )
            {
                *converted = false;
            }

            return QVector< Size >();
        }

        const float h = strings[ i + 1 ].toFloat( & ok );
        if ( ! converted || ! MathUtilities::isLengthValid( w ) )
        {
            if ( converted != nullptr )
            {
                *converted = false;
            }

            return QVector< Size >();
        }

        points.append( Size( w, h ) );
    }

    if ( converted != nullptr )
    {
        *converted = true;
    }

    return points;
}

Polygon2D RoomSerializer::poly( QFile& file, bool* const converted )
{
    return RoomSerializer::poly( RoomSerializer::readCleanLine( file ), converted );
}

Polygon2D RoomSerializer::poly( const QString& s, bool* const converted )
{
    QStringList strings = RoomSerializer::strings( s );
    const int stringsCount = strings.size();
    if ( stringsCount % 2 != 1 )
    {
        if ( converted != nullptr )
        {
            *converted = false;
        }

        return Polygon2D();
    }

    bool ok = false;
    const QString vecticesCountRaw = strings.takeFirst();
    const int verticesCount = vecticesCountRaw.toInt( & ok );
    if ( ! ok
         || verticesCount * MathUtilities::s_axesCount2D != strings.size() )
    {
        if ( converted != nullptr )
        {
            *converted = false;
        }

        return Polygon2D();
    }

    const QVector< Vector2D > vertices = RoomSerializer::points( strings, & ok );
    if ( ! ok || verticesCount != vertices.size() )
    {
        if ( converted != nullptr )
        {
            *converted = ok;
        }

        return Polygon2D();
    }

    if ( converted != nullptr )
    {
        *converted = true;
    }

    return Polygon2D( vertices );
}

bool RoomSerializer::columns( QList< Polygon2D >& aColumns, QFile& file )
{
    aColumns.clear();

    if ( file.bytesAvailable() == 0 )
    {
        return true;
    }

    bool converted = false;
    const QString declaredColumnsCountRaw = RoomSerializer::readCleanLine( file );
    if ( declaredColumnsCountRaw.isEmpty() )
    {
        return ! RoomSerializer::hasMoreContent( file );
    }

    const int declaredColumnsCount = declaredColumnsCountRaw.toInt( & converted );
    if ( ! converted || declaredColumnsCount < 0 )
    {
        return false;
    }

    QList< Polygon2D > columnsCandidate;
    while ( columnsCandidate.size() < declaredColumnsCount
            && file.bytesAvailable() > 0 )
    {
        const Polygon2D column = RoomSerializer::poly( file, & converted );
        if ( ! converted )
        {
            return false;
        }

        columnsCandidate.append( column );
        if ( columnsCandidate.size() > declaredColumnsCount )
        {
            return false;
        }
    }

    if ( columnsCandidate.size() != declaredColumnsCount
         || RoomSerializer::hasMoreContent( file ) )
    {
        return false;
    }

    aColumns = columnsCandidate;
    return true;
}

bool RoomSerializer::hasMoreContent( QFile& file )
{
    while ( file.bytesAvailable() > 0 )
    {
        const QString line = RoomSerializer::readCleanLine( file );
        if ( ! line.isEmpty() )
        {
            return true;
        }
    }

    return false;
}
