#include "WidgetUtilities.h"

#include <QApplication>
#include <QDesktopWidget>
#include <QMatrix>

using namespace LightCoverage::Utilities;

QWidget* WidgetUtilities::activeOrTopLevelWidget()
{
    QWidget* const topLevelWindow = QApplication::activeWindow();
    if ( topLevelWindow != nullptr )
    {
        return topLevelWindow;
    }

    const QWidgetList topLevelWidgets = QApplication::topLevelWidgets();
    return topLevelWidgets.isEmpty()
           ? nullptr
           : topLevelWidgets.first();
}

QRect WidgetUtilities::scaledConcentricCurrentDesktopGeometry(
        const qreal& scale,
        const bool available,
        QWidget* const currentWindow )
{
    QDesktopWidget* const desktopWidget = QApplication::desktop();

    const QRect desktopGeometry = currentWindow == nullptr
                                  ? ( available
                                      ? desktopWidget->availableGeometry()
                                      : desktopWidget->screenGeometry() )
                                  : ( available
                                      ? desktopWidget->availableGeometry( currentWindow )
                                      : desktopWidget->screenGeometry( currentWindow ) );
    const QPoint center = desktopGeometry.center();

    QMatrix mat;
    mat.translate( center.x(), center.y() )
       .scale( scale, scale )
       .translate( - center.x(), - center.y() );
    return mat.mapRect( desktopGeometry );
}
