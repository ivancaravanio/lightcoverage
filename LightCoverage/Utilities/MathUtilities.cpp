#include "MathUtilities.h"

#include <qmath.h>

#include <math.h>

using namespace LightCoverage::Utilities;

const int    MathUtilities::s_axesCount2D   = 2;
const int    MathUtilities::s_axesCount3D   = 3;
const int    MathUtilities::s_axesCount4D   = 4;
const double MathUtilities::s_invalidLength = -1.0;

const double MathUtilities::s_piDegrees       = 180.0;
const double MathUtilities::s_doublePiDegrees = 2.0 * MathUtilities::s_piDegrees;

double MathUtilities::degrees( const double& radians )
{
    return radians / M_PI * s_piDegrees;
}

double MathUtilities::radians( const double& degrees )
{
    return degrees / s_piDegrees * M_PI;
}

double MathUtilities::toPositive360DegreesAngle( const double& angleDegrees )
{
    double angle360Degrees = angleDegrees;
    const double angleDegreesAbs = qAbs( angleDegrees );
    if ( MathUtilities::isFuzzyGreaterThanOrEqual( angleDegreesAbs, s_doublePiDegrees ) )
    {
        angle360Degrees = static_cast< int >( angleDegreesAbs ) % static_cast< int >( s_doublePiDegrees ) * angleDegrees / angleDegreesAbs;
    }

    return MathUtilities::isFuzzyLessThan( angle360Degrees, 0.0 )
           ? MathUtilities::s_doublePiDegrees + angle360Degrees
           : angle360Degrees;
}

double MathUtilities::toPositive360DegreesAngleFromRadians( const double& angleRadians )
{
    return MathUtilities::toPositive360DegreesAngle( MathUtilities::degrees( angleRadians ) );
}

double MathUtilities::isFuzzyGreaterOrLessThan( const double& value, const double& referenceValue, const bool greater, const bool equal )
{
    return MathUtilities::isFuzzyEqual( value, referenceValue )
           ? equal
           : ( greater
               ? value > referenceValue
               : value < referenceValue );
}

double MathUtilities::isFuzzyContainedInRegion( const double& minValue, const double& value, const double& maxValue, const bool fully )
{
    return    MathUtilities::isFuzzyGreaterOrLessThan( value, minValue, true, fully )
           && MathUtilities::isFuzzyGreaterOrLessThan( value, maxValue, false, fully );
}

double MathUtilities::isFuzzyGreaterThan( const double& value, const double& referenceValue )
{
    return MathUtilities::isFuzzyGreaterOrLessThan( value, referenceValue, true, false );
}

double MathUtilities::isFuzzyGreaterThanOrEqual( const double& value, const double& referenceValue )
{
    return MathUtilities::isFuzzyGreaterOrLessThan( value, referenceValue, true, true );
}

double MathUtilities::isFuzzyLessThan( const double& value, const double& referenceValue )
{
    return MathUtilities::isFuzzyGreaterOrLessThan( value, referenceValue, false, false );
}

double MathUtilities::isFuzzyLessThanOrEqual( const double& value, const double& referenceValue )
{
    return MathUtilities::isFuzzyGreaterOrLessThan( value, referenceValue, false, true );
}

double MathUtilities::sign( const double& num )
{
    return num > 0.0
           ? 1.0
           : -1.0;
}

QVector< float > MathUtilities::rawMatrix( const QMatrix4x4& matrix )
{
    static const int componentsCount = 4 * 4;
    QVector< float > result;
    result.reserve( componentsCount );

#if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
    const float* const matrixRawSource = matrix.constData();
#else
    const qreal* const matrixRawSource = matrix.constData();
#endif
    for ( int i = 0; i < componentsCount; ++ i )
    {
        result.append( static_cast< float >( matrixRawSource[ i ] ) );
    }

    return result;
}

bool MathUtilities::isLengthValid( const double& length, const bool allowZeroLength )
{
    return allowZeroLength
           ? MathUtilities::isFuzzyGreaterThanOrEqual( length, 0.0 )
           : MathUtilities::isFuzzyGreaterThan( length, 0.0 );
}

QMatrix MathUtilities::yFlipAboutCenterMatrix( const QPointF& center )
{
    QMatrix m;
    m.translate( center.x(), center.y() );
    m.setMatrix( m.m11(), m.m12(),
                 m.m21(), -m.m22(),
                 m.dx(), m.dy() );
    m.translate( - center.x(), - center.y() );

    return m;
}

int MathUtilities::cyclicNumber(
        const int minNumber,
        const int number,
        const int maxNumber )
{
    const int rangeSpan = maxNumber - minNumber + 1;
    if ( rangeSpan <= 0 )
    {
        return number;
    }

    const int minLocalNumber       = number - minNumber;
    const int minLocalCyclicNumber = minLocalNumber >= 0
                                     ? minLocalNumber % rangeSpan
                                     : ( rangeSpan - qAbs( minLocalNumber ) % rangeSpan );
    return minLocalCyclicNumber + minNumber;
}
