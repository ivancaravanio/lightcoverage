#ifndef LIGHTCOVERAGE_UTILITIES_WIDGETUTILITIES_H
#define LIGHTCOVERAGE_UTILITIES_WIDGETUTILITIES_H

#include <QRect>
#include <QtGlobal>

QT_FORWARD_DECLARE_CLASS(QDesktopWidget)
QT_FORWARD_DECLARE_CLASS(QWidget)

namespace LightCoverage {
namespace Utilities {

class WidgetUtilities
{
public:
    static QWidget* activeOrTopLevelWidget();
    static QRect scaledConcentricCurrentDesktopGeometry(
            const qreal& scale,
            const bool available,
            QWidget* const currentWindow = nullptr );

private:
    WidgetUtilities();
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_WIDGETUTILITIES_H
