#ifndef LIGHTCOVERAGE_UTILITIES_CHANGELISTENER_H
#define LIGHTCOVERAGE_UTILITIES_CHANGELISTENER_H

#include <QVector>

namespace LightCoverage {
namespace Utilities {

class ChangeNotifier;

class ChangeListener
{
    friend class ChangeNotifier;

public:
    ChangeListener();
    virtual ~ChangeListener();

protected:
    virtual void onChanged( ChangeNotifier* const notifier, const quint64& traits = 0ULL ) = 0;
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_CHANGELISTENER_H
