#ifndef LIGHTCOVERAGE_UTILITIES_SERIALIZABLE_H
#define LIGHTCOVERAGE_UTILITIES_SERIALIZABLE_H

#include <QString>
#include <QTextStream>

namespace LightCoverage {
namespace Utilities {

class Serializable
{
public:
    virtual ~Serializable();

    virtual void serialize( QTextStream& ts ) const = 0;
    QString serialize() const;
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_SERIALIZABLE_H
