#include "ShaderProgram.h"

#include "GeneralUtilities.h"
#include "OpenGlErrorLogger.h"

#include <QDebug>
#include <QScopedArrayPointer>

using namespace LightCoverage::Utilities;

ShaderProgram::ShaderProgram()
    : m_id( OpenGlUtilities::s_invalidUnsignedGlValue )
{
}

ShaderProgram::ShaderProgram(
        const QGLFormat& aOpenGlFormat,
        const QList< ShaderInfo >& aShaders )
    : m_openGlFormat( aOpenGlFormat )
    , m_shaders( aShaders )
    , m_id( OpenGlUtilities::s_invalidUnsignedGlValue )
{
}

ShaderProgram::~ShaderProgram()
{
    this->clear();
}

void ShaderProgram::setOpenGLFormat( const QGLFormat& aOpenGlFormat )
{
    m_openGlFormat = aOpenGlFormat;
}

void ShaderProgram::appendShader( const ShaderInfo& shaderInfo )
{
    m_shaders.append( shaderInfo );
}

void ShaderProgram::setShaders( const QList< ShaderInfo >& aShaders )
{
    m_shaders = aShaders;
}

bool ShaderProgram::isValid() const
{
    return OpenGlUtilities::isUnsignedGlValueValid( m_id );
}

bool ShaderProgram::build()
{
    this->clear();

    bool allShadersCompiledSuccessfully = true;

    QList< GLuint > shaderIds;
    foreach ( const ShaderInfo& shader, m_shaders )
    {
        const GLuint shaderId = glCreateShader( shader.openGlShaderType() );
        LOG_OPENGL_ERROR();
        if ( ! OpenGlUtilities::isUnsignedGlValueValid( shaderId ) )
        {
            return false;
        }

        const GLchar* shaderSourceCode = shader.sourceCode.constData();
        glShaderSource( shaderId, 1, & shaderSourceCode, NULL );
        LOG_OPENGL_ERROR();

        glCompileShader( shaderId );
        LOG_OPENGL_ERROR();

        GLint shaderCompilationStatus = GL_FALSE;
        glGetShaderiv( shaderId, GL_COMPILE_STATUS, & shaderCompilationStatus );
        LOG_OPENGL_ERROR();

        if ( GL_TRUE == shaderCompilationStatus )
        {
            shaderIds.append( shaderId );
            continue;
        }

        allShadersCompiledSuccessfully = false;

        GLint logLength = 0;
        glGetShaderiv( shaderId, GL_INFO_LOG_LENGTH, & logLength );
        LOG_OPENGL_ERROR();

        if ( logLength == 0 )
        {
            qDebug() << "Shader:" << endl
                     << shader << endl
                     << endl
                     << "could not obtain compilation log length";
            continue;
        }

        const QScopedArrayPointer< char > log( new char[ logLength ] );
        glGetShaderInfoLog( shaderId, logLength, NULL, log.data() );
        LOG_OPENGL_ERROR();

        qDebug() << "Shader:" << endl
                 << shader << endl
                 << endl
                 << "compilation log:" << endl
                 << "-> ====================" << endl
                 << log.data() << endl
                 << "<- --------------------" << endl;
    }

    if ( ! allShadersCompiledSuccessfully )
    {
        qDebug() << "One or more shaders weren\'t setup correctly." << endl
                 << "Skipping program creation and attributes assignment.";

        ShaderProgram::cleanShaders( shaderIds );

        return false;
    }

    m_id = glCreateProgram();
    LOG_OPENGL_ERROR();

    if ( ! OpenGlUtilities::isUnsignedGlValueValid( m_id ) )
    {
        qDebug() << "Error creating program object.";

        ShaderProgram::cleanShaders( shaderIds );

        return false;
    }

    foreach ( const GLuint shaderId, shaderIds )
    {
        glAttachShader( m_id, shaderId );
        LOG_OPENGL_ERROR();
    }

    // https://www.opengl.org/wiki/GLSL_Object
    // =======================================
    // Pre-link setup
    // There are a number of operations that may need to be performed on programs before linking.
    //
    // Vertex Attributes for a Vertex Shader (if present in the program object) can be manually assigned an attribute index.
    // Obviously, if no vertex shader is in the program, you do not need to assign attributes manually.
    // Note that it is still best to assign them explicitly in the shader, where possible.

    foreach ( const ShaderInfo& shader, m_shaders )
    {
        const ShaderInfo::ShaderType shaderType = shader.type();

        const QMap< GLuint, QByteArray >::const_iterator shaderVariablesIterEnd = shader.variableLocationNames.constEnd();
        for ( QMap< GLuint, QByteArray >::const_iterator shaderVariablesIter = shader.variableLocationNames.constBegin();
              shaderVariablesIter != shaderVariablesIterEnd;
              ++ shaderVariablesIter )
        {
            switch ( shaderType )
            {
                case ShaderInfo::ShaderTypeVertex:
                    glBindAttribLocation(
                                m_id,
                                shaderVariablesIter.key(),
                                shaderVariablesIter.value().constData() );
                    LOG_OPENGL_ERROR();
                    break;
                case ShaderInfo::ShaderTypeFragment:
                    // Fragment shader bindings specified in the shader source will be used if specified,
                    // regardless of whether a location was specified using one of these functions.
                    glBindFragDataLocation(
                                m_id,
                                shaderVariablesIter.key(),
                                shaderVariablesIter.value().constData() );
                    LOG_OPENGL_ERROR();
                    break;
                default:
                    break;
            }
        }
    }

    glLinkProgram( m_id );
    LOG_OPENGL_ERROR();

    ShaderProgram::cleanShaders( shaderIds, m_id );
    shaderIds.clear();

    GLint programLinkStatus = GL_FALSE;
    glGetProgramiv( m_id, GL_LINK_STATUS, & programLinkStatus );
    LOG_OPENGL_ERROR();

    if ( GL_FALSE == programLinkStatus )
    {
        qDebug() << "Failed to link shader program.";

        GLint logLength = 0;
        glGetProgramiv( m_id, GL_INFO_LOG_LENGTH, & logLength );
        LOG_OPENGL_ERROR();

        if ( logLength > 0 )
        {
            const QScopedArrayPointer< char > log( new char[ logLength ] );
            glGetProgramInfoLog( m_id, logLength, NULL, log.data() );
            LOG_OPENGL_ERROR();

            qDebug() << "Program log:" << endl
                     << log;
        }
        else
        {
            qDebug() << "Failed to obtain program linking status message length.";
        }

        this->clear();

        return false;
    }

    return true;
}

bool ShaderProgram::build(
        const QString& vertexShaderFilePath,
        const QString& fragmentShaderFilePath )
{
    const ShaderInfo vertexShaderInfo(
                ShaderInfo::ShaderTypeVertex,
                GeneralUtilities::fileContents( vertexShaderFilePath ) );

    const ShaderInfo fragmentShaderInfo(
                ShaderInfo::ShaderTypeFragment,
                GeneralUtilities::fileContents( fragmentShaderFilePath ) );

    this->setShaders(
                QList< ShaderInfo >()
                << vertexShaderInfo
                << fragmentShaderInfo );

    return this->build();
}

GLuint ShaderProgram::id( const QGLFormat& aOpenGlFormat )
{
    // http://cu-droplet.googlecode.com/git-history/5dd30c9749a595441098aa1d448445075b6f363d/DropletSimDemos/DropletRenderer/src/RenderableDroplet.cpp
    // OpenGL 3.3:  GL_CURRENT_PROGRAM
    // OpenGL 4.2+: GL_ACTIVE_PROGRAM

    GLint activeShaderProgramId = static_cast< GLint >( OpenGlUtilities::s_invalidUnsignedGlValue );
    glGetIntegerv(
                aOpenGlFormat.majorVersion() >= 4
                && aOpenGlFormat.minorVersion() >= 2
                ? GL_ACTIVE_PROGRAM
                : GL_CURRENT_PROGRAM,
                & activeShaderProgramId );
    LOG_OPENGL_ERROR();

    return static_cast< GLuint >( activeShaderProgramId );
}

GLuint ShaderProgram::id() const
{
    return m_id;
}

void ShaderProgram::activate()
{
    if ( ! this->isValid() )
    {
        return;
    }

    m_activator = ShaderProgramActivator( m_openGlFormat, m_id );
    m_activator.activate();
}

void ShaderProgram::deactivate()
{
    if ( ! this->isValid() )
    {
        return;
    }

    m_activator.deactivate();
}

bool ShaderProgram::isActive() const
{
    return this->isValid()
           && this->activeShaderProgramId() == m_id;
}

GLuint ShaderProgram::activeShaderProgramId() const
{
    return ShaderProgram::id( m_openGlFormat );
}

void ShaderProgram::clear()
{
    if ( OpenGlUtilities::isUnsignedGlValueValid( m_id ) )
    {
        glDeleteProgram( m_id );
        LOG_OPENGL_ERROR();

        m_id = OpenGlUtilities::s_invalidUnsignedGlValue;
    }
}

GLint ShaderProgram::genericVertexAttributeVarLocation( const QByteArray& name ) const
{
    // shader program needs only to be linked correctly, not used/activated
    if ( ! this->isValid() )
    {
        return OpenGlUtilities::s_invalidSignedGlValue;
    }

    const GLint location = glGetAttribLocation( m_id, name.constData() );
    LOG_OPENGL_ERROR();

    if ( ! OpenGlUtilities::isSignedGlValueValid( location ) )
    {
        qDebug().nospace() << "shader program -> get generic vertex attribute location -> generic vertex attribute \"" << name << "\" not found.";
    }

    return location;
}

GLint ShaderProgram::fragmentOutputVarLocation( const QByteArray& name ) const
{
    // shader program needs only to be linked correctly, not used/activated
    if ( ! this->isValid() )
    {
        return OpenGlUtilities::s_invalidSignedGlValue;
    }

    const GLint location = glGetFragDataLocation( m_id, name.constData() );
    LOG_OPENGL_ERROR();

    if ( ! OpenGlUtilities::isSignedGlValueValid( location ) )
    {
        qDebug().nospace() << "shader program -> get fragment output variable location -> fragment output variable \"" << name << "\" not found.";
    }

    return location;
}

GLint ShaderProgram::uniformVarLocation( const QByteArray& name ) const
{
    // shader program needs only to be linked correctly, not used/activated
    if ( ! this->isValid() )
    {
        return OpenGlUtilities::s_invalidSignedGlValue;
    }

    const GLint location = glGetUniformLocation( m_id, name.constData() );
    LOG_OPENGL_ERROR();

    if ( ! OpenGlUtilities::isSignedGlValueValid( location ) )
    {
        qDebug().nospace() << "shader program -> get uniform variable location -> uniform variable \"" << name << "\" not found.";
    }

    return location;
}

void ShaderProgram::setUniformVariable1f( const GLint location, const GLfloat v0 )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform1f( location, v0 );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable2f( const GLint location, const GLfloat v0, const GLfloat v1 )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform2f( location, v0, v1 );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable3f( const GLint location, const GLfloat v0, const GLfloat v1, const GLfloat v2 )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform3f( location, v0, v1, v2 );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable4f( const GLint location, const GLfloat v0, const GLfloat v1, const GLfloat v2, const GLfloat v3 )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform4f( location, v0, v1, v2, v3 );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable1fv( const GLint location, const GLsizei count, const GLfloat* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform1fv( location, count, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable2fv( const GLint location, const GLsizei count, const GLfloat* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform2fv( location, count, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable3fv( const GLint location, const GLsizei count, const GLfloat* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform3fv( location, count, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable4fv( const GLint location, const GLsizei count, const GLfloat* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform4fv( location, count, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable1i( const GLint location, const GLint v0 )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform1i( location, v0 );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable2i( const GLint location, const GLint v0, const GLint v1 )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform2i( location, v0, v1 );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable3i( const GLint location, const GLint v0, const GLint v1, const GLint v2 )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform3i( location, v0, v1, v2 );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable4i( const GLint location, const GLint v0, const GLint v1, const GLint v2, const GLint v3 )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform4i( location, v0, v1, v2, v3 );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable1iv( const GLint location, const GLsizei count, const GLint* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform1iv( location, count, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable2iv( const GLint location, const GLsizei count, const GLint* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform2iv( location, count, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable3iv( const GLint location, const GLsizei count, const GLint* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform3iv( location, count, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformVariable4iv( const GLint location, const GLsizei count, const GLint* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniform4iv( location, count, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformMatrix2fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniformMatrix2fv( location, count, transpose, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformMatrix3fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniformMatrix3fv( location, count, transpose, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformMatrix4fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniformMatrix4fv( location, count, transpose, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformMatrix2x3fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniformMatrix2x3fv( location, count, transpose, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformMatrix3x2fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniformMatrix3x2fv( location, count, transpose, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformMatrix2x4fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniformMatrix2x4fv( location, count, transpose, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformMatrix4x2fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniformMatrix4x2fv( location, count, transpose, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformMatrix3x4fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniformMatrix3x4fv( location, count, transpose, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformMatrix4x3fv( const GLint location, const GLsizei count, const GLboolean transpose, const GLfloat* const value )
{
    if ( ! this->isValid() )
    {
        return;
    }

    this->activate();
    glUniformMatrix4x3fv( location, count, transpose, value );
    LOG_OPENGL_ERROR();
    this->deactivate();
}

void ShaderProgram::setUniformColor( const GLint location, const QColor& aColor )
{
    this->setUniformVariable4f( location,
                                aColor.redF(),
                                aColor.greenF(),
                                aColor.blueF(),
                                aColor.alphaF() );
}

void ShaderProgram::dumpPostLinkInfo()
{
    if ( ! this->isValid() )
    {
        return;
    }

    GLint uniformVarsCount = 0;
    // NOTE: this is a program variable
    glGetProgramiv( m_id, GL_ACTIVE_UNIFORMS, &uniformVarsCount );
    LOG_OPENGL_ERROR();

    GLint activeUniformVarNameMaxLength = 0;
    glGetProgramiv( m_id, GL_ACTIVE_UNIFORM_MAX_LENGTH, &activeUniformVarNameMaxLength );
    LOG_OPENGL_ERROR();

    qDebug() << "Uniform variables count:" << uniformVarsCount;

    qDebug() << "Uniform variables:";

    const QScopedArrayPointer< GLchar > uniformVarName( new GLchar[ activeUniformVarNameMaxLength ] );
    GLsizei uniformVarNameLength = 0;
    GLint uniformVarSize = 0;
    GLenum uniformVarType = 0;
    for ( int uniformVarIndex = 0;
          uniformVarIndex < uniformVarsCount;
          ++ uniformVarIndex )
    {
        uniformVarNameLength = 0;
        uniformVarSize = 0;
        uniformVarType = 0;

        glGetActiveUniform( m_id,
                            uniformVarIndex,
                            activeUniformVarNameMaxLength,
                            & uniformVarNameLength,
                            & uniformVarSize,
                            & uniformVarType,
                            uniformVarName.data() );

        if ( glGetError() != GL_NO_ERROR )
        {
            qDebug() << QString( "\tindex: %1: no uniform variable" )
                        .arg( uniformVarIndex );
            continue;
        }

        qDebug() << QString( "\tindex: %1, name: %2, type: %3 (%4), size: %5" )
                              .arg( uniformVarIndex )
                              .arg( uniformVarName.data() )
                              .arg( ShaderProgram::shaderVariableTypeDescription( uniformVarType ) )
                              .arg( "0x" + QString::number( uniformVarType, 16 ).toUpper() )
                              .arg( uniformVarSize );
    }

    GLint maxVertexInputAttributesCount = 0;
    // The maximum number of 4-component generic vertex attributes accessible to a vertex shader.
    // The value must be at least 16.
    // NOTE: this is a OpenGL driver variable
    glGetIntegerv( GL_MAX_VERTEX_ATTRIBS, &maxVertexInputAttributesCount );
    LOG_OPENGL_ERROR();

    qDebug() << "The maximum allowed count of 4-component generic vertex attributes accessible to a vertex shader:" << maxVertexInputAttributesCount;

    GLint vertexInputAttributesCount = 0;
    // NOTE: this is a program variable
    glGetProgramiv( m_id, GL_ACTIVE_ATTRIBUTES, &vertexInputAttributesCount );
    LOG_OPENGL_ERROR();

    qDebug() << "The number of registered 4-component generic vertex attributes accessible to a vertex shader:" << vertexInputAttributesCount;

    GLint maxVertexInputAttributeNameLength = 0;
    // NOTE: this is a program variable
    glGetProgramiv( m_id, GL_ACTIVE_ATTRIBUTE_MAX_LENGTH, &maxVertexInputAttributeNameLength );
    LOG_OPENGL_ERROR();

    qDebug() << "The maximum allowed character length of the name of a "
                "4-component generic vertex attributes accessible to a vertex shader:" << maxVertexInputAttributeNameLength;

    const QScopedArrayPointer< GLchar > vertexInputAttributeName( new GLchar[ maxVertexInputAttributeNameLength ] );
    GLsizei vertexInputAttributeNameLength = -1;
    GLint vertexInputAttributeByteSize = 0;
    GLenum vertexInputAttributeType = 0;
    for ( int i = 0; i < vertexInputAttributesCount; ++ i )
    {
        vertexInputAttributeNameLength = 0;
        vertexInputAttributeByteSize = 0;
        vertexInputAttributeType = 0;

        glGetActiveAttrib( m_id,
                           i,
                           maxVertexInputAttributeNameLength,
                           &vertexInputAttributeNameLength,
                           &vertexInputAttributeByteSize,
                           &vertexInputAttributeType,
                           vertexInputAttributeName.data() );
        LOG_OPENGL_ERROR();

        if ( vertexInputAttributeNameLength == -1 )
        {
            qDebug() << "vertex input attribute:" << endl
                     << "index:" << i << endl
                     << "An error occurred.";
            continue;
        }

        if ( vertexInputAttributeNameLength == 0 )
        {
            qDebug() << "vertex input attribute:" << endl
                     << "index:" << i << endl
                     << "No information available.";
            continue;
        }

        qDebug() << "vertex input attribute:" << endl
                 << "index:" << i << endl
                 << "name:" << vertexInputAttributeName.data() << endl
                 << "type:" << ( "0x" + QString::number( vertexInputAttributeType, 16 ).toUpper() ) << endl
                 << "byte-size:" << vertexInputAttributeByteSize;
    }
}

void ShaderProgram::cleanShaders(
        const QList< GLuint >& shaderIds,
        const GLuint shaderProgramId )
{
    const bool shouldDetach = OpenGlUtilities::isUnsignedGlValueValid( shaderProgramId );
    foreach ( const GLuint shaderId, shaderIds )
    {
        if ( shouldDetach )
        {
            glDetachShader( shaderProgramId, shaderId );
            LOG_OPENGL_ERROR();
        }

        glDeleteShader( shaderId );
        LOG_OPENGL_ERROR();
    }
}

QString ShaderProgram::shaderVariableTypeDescription( const GLenum shaderVariableType )
{
    // https://www.opengl.org/sdk/docs/man4/html/glGetActiveUniform.xhtml
    switch ( shaderVariableType )
    {
        case GL_FLOAT: return "float";
        case GL_FLOAT_VEC2: return "vec2";
        case GL_FLOAT_VEC3: return "vec3";
        case GL_FLOAT_VEC4: return "vec4";
        case GL_DOUBLE: return "double";
        case GL_DOUBLE_VEC2: return "dvec2";
        case GL_DOUBLE_VEC3: return "dvec3";
        case GL_DOUBLE_VEC4: return "dvec4";
        case GL_INT: return "int";
        case GL_INT_VEC2: return "ivec2";
        case GL_INT_VEC3: return "ivec3";
        case GL_INT_VEC4: return "ivec4";
        case GL_UNSIGNED_INT: return "unsigned int";
        case GL_UNSIGNED_INT_VEC2: return "uvec2";
        case GL_UNSIGNED_INT_VEC3: return "uvec3";
        case GL_UNSIGNED_INT_VEC4: return "uvec4";
        case GL_BOOL: return "bool";
        case GL_BOOL_VEC2: return "bvec2";
        case GL_BOOL_VEC3: return "bvec3";
        case GL_BOOL_VEC4: return "bvec4";
        case GL_FLOAT_MAT2: return "mat2";
        case GL_FLOAT_MAT3: return "mat3";
        case GL_FLOAT_MAT4: return "mat4";
        case GL_FLOAT_MAT2x3: return "mat2x3";
        case GL_FLOAT_MAT2x4: return "mat2x4";
        case GL_FLOAT_MAT3x2: return "mat3x2";
        case GL_FLOAT_MAT3x4: return "mat3x4";
        case GL_FLOAT_MAT4x2: return "mat4x2";
        case GL_FLOAT_MAT4x3: return "mat4x3";
        case GL_DOUBLE_MAT2: return "dmat2";
        case GL_DOUBLE_MAT3: return "dmat3";
        case GL_DOUBLE_MAT4: return "dmat4";
        case GL_DOUBLE_MAT2x3: return "dmat2x3";
        case GL_DOUBLE_MAT2x4: return "dmat2x4";
        case GL_DOUBLE_MAT3x2: return "dmat3x2";
        case GL_DOUBLE_MAT3x4: return "dmat3x4";
        case GL_DOUBLE_MAT4x2: return "dmat4x2";
        case GL_DOUBLE_MAT4x3: return "dmat4x3";
        case GL_SAMPLER_1D: return "sampler1D";
        case GL_SAMPLER_2D: return "sampler2D";
        case GL_SAMPLER_3D: return "sampler3D";
        case GL_SAMPLER_CUBE: return "samplerCube";
        case GL_SAMPLER_1D_SHADOW: return "sampler1DShadow";
        case GL_SAMPLER_2D_SHADOW: return "sampler2DShadow";
        case GL_SAMPLER_1D_ARRAY: return "sampler1DArray";
        case GL_SAMPLER_2D_ARRAY: return "sampler2DArray";
        case GL_SAMPLER_1D_ARRAY_SHADOW: return "sampler1DArrayShadow";
        case GL_SAMPLER_2D_ARRAY_SHADOW: return "sampler2DArrayShadow";
        case GL_SAMPLER_2D_MULTISAMPLE: return "sampler2DMS";
        case GL_SAMPLER_2D_MULTISAMPLE_ARRAY: return "sampler2DMSArray";
        case GL_SAMPLER_CUBE_SHADOW: return "samplerCubeShadow";
        case GL_SAMPLER_BUFFER: return "samplerBuffer";
        case GL_SAMPLER_2D_RECT: return "sampler2DRect";
        case GL_SAMPLER_2D_RECT_SHADOW: return "sampler2DRectShadow";
        case GL_INT_SAMPLER_1D: return "isampler1D";
        case GL_INT_SAMPLER_2D: return "isampler2D";
        case GL_INT_SAMPLER_3D: return "isampler3D";
        case GL_INT_SAMPLER_CUBE: return "isamplerCube";
        case GL_INT_SAMPLER_1D_ARRAY: return "isampler1DArray";
        case GL_INT_SAMPLER_2D_ARRAY: return "isampler2DArray";
        case GL_INT_SAMPLER_2D_MULTISAMPLE: return "isampler2DMS";
        case GL_INT_SAMPLER_2D_MULTISAMPLE_ARRAY: return "isampler2DMSArray";
        case GL_INT_SAMPLER_BUFFER: return "isamplerBuffer";
        case GL_INT_SAMPLER_2D_RECT: return "isampler2DRect";
        case GL_UNSIGNED_INT_SAMPLER_1D: return "usampler1D";
        case GL_UNSIGNED_INT_SAMPLER_2D: return "usampler2D";
        case GL_UNSIGNED_INT_SAMPLER_3D: return "usampler3D";
        case GL_UNSIGNED_INT_SAMPLER_CUBE: return "usamplerCube";
        case GL_UNSIGNED_INT_SAMPLER_1D_ARRAY: return "usampler2DArray";
        case GL_UNSIGNED_INT_SAMPLER_2D_ARRAY: return "usampler2DArray";
        case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE: return "usampler2DMS";
        case GL_UNSIGNED_INT_SAMPLER_2D_MULTISAMPLE_ARRAY: return "usampler2DMSArray";
        case GL_UNSIGNED_INT_SAMPLER_BUFFER: return "usamplerBuffer";
        case GL_UNSIGNED_INT_SAMPLER_2D_RECT: return "usampler2DRect";
        case GL_IMAGE_1D: return "image1D";
        case GL_IMAGE_2D: return "image2D";
        case GL_IMAGE_3D: return "image3D";
        case GL_IMAGE_2D_RECT: return "image2DRect";
        case GL_IMAGE_CUBE: return "imageCube";
        case GL_IMAGE_BUFFER: return "imageBuffer";
        case GL_IMAGE_1D_ARRAY: return "image1DArray";
        case GL_IMAGE_2D_ARRAY: return "image2DArray";
        case GL_IMAGE_2D_MULTISAMPLE: return "image2DMS";
        case GL_IMAGE_2D_MULTISAMPLE_ARRAY: return "image2DMSArray";
        case GL_INT_IMAGE_1D: return "iimage1D";
        case GL_INT_IMAGE_2D: return "iimage2D";
        case GL_INT_IMAGE_3D: return "iimage3D";
        case GL_INT_IMAGE_2D_RECT: return "iimage2DRect";
        case GL_INT_IMAGE_CUBE: return "iimageCube";
        case GL_INT_IMAGE_BUFFER: return "iimageBuffer";
        case GL_INT_IMAGE_1D_ARRAY: return "iimage1DArray";
        case GL_INT_IMAGE_2D_ARRAY: return "iimage2DArray";
        case GL_INT_IMAGE_2D_MULTISAMPLE: return "iimage2DMS";
        case GL_INT_IMAGE_2D_MULTISAMPLE_ARRAY: return "iimage2DMSArray";
        case GL_UNSIGNED_INT_IMAGE_1D: return "uimage1D";
        case GL_UNSIGNED_INT_IMAGE_2D: return "uimage2D";
        case GL_UNSIGNED_INT_IMAGE_3D: return "uimage3D";
        case GL_UNSIGNED_INT_IMAGE_2D_RECT: return "uimage2DRect";
        case GL_UNSIGNED_INT_IMAGE_CUBE: return "uimageCube";
        case GL_UNSIGNED_INT_IMAGE_BUFFER: return "uimageBuffer";
        case GL_UNSIGNED_INT_IMAGE_1D_ARRAY: return "uimage1DArray";
        case GL_UNSIGNED_INT_IMAGE_2D_ARRAY: return "uimage2DArray";
        case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE: return "uimage2DMS";
        case GL_UNSIGNED_INT_IMAGE_2D_MULTISAMPLE_ARRAY: return "uimage2DMSArray";
        case GL_UNSIGNED_INT_ATOMIC_COUNTER: return "atomic_uint";
        default:
            break;
    }

    return QString();
}
