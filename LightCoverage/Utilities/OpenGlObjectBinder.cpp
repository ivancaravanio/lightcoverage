#include "OpenGlObjectBinder.h"

#include "OpenGlErrorLogger.h"

using namespace LightCoverage::Utilities;

OpenGlObjectBinder::OpenGlObjectBinder()
    : m_objectType( ObjectTypeInvalid )
    , m_bindingTarget( OpenGlUtilities::invalidEnumGlValue() )
    , m_objectName( OpenGlUtilities::s_invalidUnsignedGlValue )
    , m_autoUnbind( false )
    , m_isRequestedDataBound( false )
    , m_previouslyBoundData( OpenGlUtilities::s_invalidUnsignedGlValue )
{
}

OpenGlObjectBinder::OpenGlObjectBinder(
        const ObjectType aObjectType,
        const GLenum aBindingTarget,
        const GLuint aObjectName,
        const bool aAutoUnbind )
    : m_objectType( aObjectType )
    , m_bindingTarget( aBindingTarget )
    , m_objectName( aObjectName )
    , m_autoUnbind( aAutoUnbind )
    , m_isRequestedDataBound( false )
    , m_previouslyBoundData( OpenGlUtilities::s_invalidUnsignedGlValue )
{
}

OpenGlObjectBinder::~OpenGlObjectBinder()
{
    if ( m_autoUnbind )
    {
        this->unbind();
    }
}

bool OpenGlObjectBinder::isValid() const
{
    return m_objectType != ObjectTypeInvalid;
}

OpenGlObjectBinder::ObjectType OpenGlObjectBinder::objectType() const
{
    return m_objectType;
}

GLenum OpenGlObjectBinder::bindingTarget() const
{
    return m_bindingTarget;
}

GLuint OpenGlObjectBinder::objectName() const
{
    return m_objectName;
}

GLuint OpenGlObjectBinder::requestedData() const
{
    return this->isObjectNameUsedAsBoundData()
           ? m_objectName
           : m_bindingTarget;
}

GLenum OpenGlObjectBinder::openGlBindingTargetCheck(
        const OpenGlObjectBinder::ObjectType objectType,
        const GLenum bindingTarget )
{
    switch ( objectType )
    {
        case ObjectTypeTextureUnit:
            return GL_ACTIVE_TEXTURE;
        case ObjectTypeTexture:
            switch ( bindingTarget )
            {
                case GL_TEXTURE_1D:
                    return GL_TEXTURE_BINDING_1D;
                case GL_TEXTURE_2D:
                    return GL_TEXTURE_BINDING_2D;
                case GL_TEXTURE_3D:
                    return GL_TEXTURE_BINDING_3D;
                case GL_TEXTURE_1D_ARRAY:
                    return GL_TEXTURE_BINDING_1D_ARRAY;
                case GL_TEXTURE_2D_ARRAY:
                    return GL_TEXTURE_BINDING_2D_ARRAY;
                case GL_TEXTURE_RECTANGLE:
                    return GL_TEXTURE_BINDING_RECTANGLE;
                case GL_TEXTURE_CUBE_MAP:
                    return GL_TEXTURE_BINDING_CUBE_MAP;
                case GL_TEXTURE_2D_MULTISAMPLE:
                    return GL_TEXTURE_BINDING_2D_MULTISAMPLE;
                case GL_TEXTURE_2D_MULTISAMPLE_ARRAY:
                    return GL_TEXTURE_BINDING_2D_MULTISAMPLE_ARRAY;
                case GL_TEXTURE_BUFFER:
                    return GL_TEXTURE_BINDING_BUFFER;
                case GL_TEXTURE_CUBE_MAP_ARRAY:
                    return GL_TEXTURE_BINDING_CUBE_MAP_ARRAY;
            }

            break;
        case ObjectTypeBuffer:
            switch ( bindingTarget )
            {
                case GL_ARRAY_BUFFER:
                    return GL_ARRAY_BUFFER_BINDING;
                case GL_ATOMIC_COUNTER_BUFFER:
                    return GL_ATOMIC_COUNTER_BUFFER_BINDING;
                case GL_COPY_READ_BUFFER:
                    return GL_COPY_READ_BUFFER_BINDING;
                case GL_COPY_WRITE_BUFFER:
                    return GL_COPY_WRITE_BUFFER_BINDING;
                case GL_DRAW_INDIRECT_BUFFER:
                    return GL_DRAW_INDIRECT_BUFFER_BINDING;
                case GL_DISPATCH_INDIRECT_BUFFER:
                    return GL_DISPATCH_INDIRECT_BUFFER_BINDING;
                case GL_ELEMENT_ARRAY_BUFFER:
                    return GL_ELEMENT_ARRAY_BUFFER_BINDING;
                case GL_PIXEL_PACK_BUFFER:
                    return GL_PIXEL_PACK_BUFFER_BINDING;
                case GL_PIXEL_UNPACK_BUFFER:
                    return GL_PIXEL_UNPACK_BUFFER_BINDING;
                case GL_QUERY_BUFFER:
                    return GL_QUERY_BUFFER_BINDING;
                case GL_SHADER_STORAGE_BUFFER:
                    return GL_SHADER_STORAGE_BUFFER_BINDING;
                case GL_TEXTURE_BUFFER:
                    return GL_TEXTURE_BUFFER_BINDING;
                case GL_TRANSFORM_FEEDBACK_BUFFER:
                    return GL_TRANSFORM_FEEDBACK_BUFFER_BINDING;
                case GL_UNIFORM_BUFFER:
                    return GL_UNIFORM_BUFFER_BINDING;
            }

            break;
        case ObjectTypeVertexArray:
            return GL_VERTEX_ARRAY_BINDING;
        case ObjectTypeFramebuffer:
            switch ( bindingTarget )
            {
                case GL_FRAMEBUFFER:
                    return GL_FRAMEBUFFER_BINDING;
                case GL_DRAW_FRAMEBUFFER:
                    return GL_DRAW_FRAMEBUFFER_BINDING;
                case GL_READ_FRAMEBUFFER:
                    return GL_READ_FRAMEBUFFER_BINDING;
            }

            break;
        case ObjectTypeRenderbuffer:
            return GL_RENDERBUFFER_BINDING;
        case ObjectTypePolygonMode:
            return GL_POLYGON_MODE;
        case ObjectTypeCullFaceMode:
            return GL_CULL_FACE_MODE;
        default:
            break;
    }

    return OpenGlUtilities::invalidEnumGlValue();
}

GLenum OpenGlObjectBinder::openGlBindingTargetCheck() const
{
    return OpenGlObjectBinder::openGlBindingTargetCheck( m_objectType, m_bindingTarget );
}

bool OpenGlObjectBinder::isObjectNameUsedAsBoundData() const
{
    return    m_objectType == ObjectTypeTexture
           || m_objectType == ObjectTypeBuffer
           || m_objectType == ObjectTypeVertexArray
           || m_objectType == ObjectTypeRenderbuffer
           || m_objectType == ObjectTypePolygonMode;
}

void OpenGlObjectBinder::bind( const GLenum aBindingTarget,
                               const GLuint aObjectName )
{
    switch ( m_objectType )
    {
        case ObjectTypeTextureUnit:
            glActiveTexture( aBindingTarget );
            LOG_OPENGL_ERROR();
            break;
        case ObjectTypeTexture:
            glBindTexture( aBindingTarget, aObjectName );
            LOG_OPENGL_ERROR();
            break;
        case ObjectTypeBuffer:
            glBindBuffer( aBindingTarget, aObjectName );
            LOG_OPENGL_ERROR();
            break;
        case ObjectTypeVertexArray:
            glBindVertexArray( aObjectName );
            LOG_OPENGL_ERROR();
            break;
        case ObjectTypeFramebuffer:
            glBindFramebuffer( aBindingTarget, aObjectName );
            LOG_OPENGL_ERROR();
            break;
        case ObjectTypeRenderbuffer:
            glBindRenderbuffer( aBindingTarget, aObjectName );
            LOG_OPENGL_ERROR();
            break;
        case ObjectTypePolygonMode:
            glPolygonMode( aBindingTarget, aObjectName );
            LOG_OPENGL_ERROR();
            break;
        case ObjectTypeCullFaceMode:
            glCullFace( aBindingTarget );
            LOG_OPENGL_ERROR();
            break;
        default:
            break;
    }
}

void OpenGlObjectBinder::bind()
{
    if ( ! this->isValid() )
    {
        return;
    }

    bool ok = false;
    const GLuint boundData = this->boundData( & ok );
    const GLuint reqData = this->requestedData();
    if ( ! ok || boundData == reqData )
    {
        return;
    }

    m_isRequestedDataBound = true;
    m_previouslyBoundData = boundData;
    this->bind( m_bindingTarget,
                this->isObjectNameUsedAsBoundData()
                ? m_objectName
                : OpenGlUtilities::s_invalidUnsignedGlValue );
}

void OpenGlObjectBinder::unbind()
{
    if ( ! m_isRequestedDataBound )
    {
        return;
    }

    const bool isObjectNameUsedAsBoundData = this->isObjectNameUsedAsBoundData();
    this->bind( isObjectNameUsedAsBoundData
                ? m_bindingTarget
                : m_previouslyBoundData,
                isObjectNameUsedAsBoundData
                ? m_previouslyBoundData
                : OpenGlUtilities::s_invalidUnsignedGlValue );

    m_previouslyBoundData = OpenGlUtilities::s_invalidUnsignedGlValue;
    m_isRequestedDataBound = false;
}

GLuint OpenGlObjectBinder::boundData( bool* const ok ) const
{
    if ( ! this->isValid() )
    {
        if ( ok != nullptr )
        {
            *ok = false;
        }

        return OpenGlUtilities::s_invalidUnsignedGlValue;
    }

    GLint data[ 2 ] = { static_cast< GLint >( OpenGlUtilities::s_invalidUnsignedGlValue ),
                        static_cast< GLint >( OpenGlUtilities::s_invalidUnsignedGlValue ) };

    // GL_POLYGON_MODE fails on AMD/ATi chips:
    // http://devgurus.amd.com/thread/169728
    glGetIntegerv( this->openGlBindingTargetCheck(), data );
    const GLenum openGlErrorCode = glGetError();
    if ( openGlErrorCode != GL_NO_ERROR )
    {
        // OpenGlErrorLogger::logOpenGlError( openGlErrorCode, __FILE__, __LINE__ );
        if ( ok != nullptr )
        {
            *ok = false;
        }

        return OpenGlUtilities::s_invalidUnsignedGlValue;
    }

    if ( ok != nullptr )
    {
        *ok = true;
    }

    return static_cast< GLuint >(
                data[ m_objectType == ObjectTypePolygonMode
                      && m_bindingTarget == GL_BACK
                      ? 1
                      : 0 ] );
}
