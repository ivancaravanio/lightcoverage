#ifndef LIGHTCOVERAGE_UTILITIES_FRAMEBUFFERBINDER_H
#define LIGHTCOVERAGE_UTILITIES_FRAMEBUFFERBINDER_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include <QtGlobal>

namespace LightCoverage {
namespace Utilities {

class FramebufferBinder
{
public:
    enum BindingTarget
    {
        BindingTargetDraw,
        BindingTargetRead,
        BindingTargetBoth,
        /*
         * https://www.opengl.org/wiki/Framebuffer :
         *     glBindFramebuffer( GLenum target, GLuint framebuffer ) :
         *         Binding to the GL_FRAMEBUFFER​ target is equivalent to binding that framebuffer to *both* GL_DRAW_FRAMEBUFFER​ and GL_READ_FRAMEBUFFER​.
         *         Note that most other uses of GL_FRAMEBUFFER​ mean the draw framebuffer; this is the case when it means both.
         * https://www.opengl.org/wiki/Framebuffer_Object :
         *     Framebuffer Object Structure :
         *         glBindFramebuffer( GLenum target, GLuint framebuffer ) :
         *             The target​ parameter for this object can take one of 3 values: GL_FRAMEBUFFER​, GL_READ_FRAMEBUFFER​, or GL_DRAW_FRAMEBUFFER​.
         *             The last two allow you to bind an FBO so that reading commands (glReadPixels​, etc) and
         *             writing commands (all rendering commands) can happen to two different framebuffers.
         *             The GL_FRAMEBUFFER​ binding target simply sets both the read and the write to the same FBO.
         *     Attaching Images :
         *         void glFramebufferTexture1D​(GLenum target​, GLenum attachment​, GLenum textarget​, GLuint texture​, GLint level​);
         *         void glFramebufferTexture2D​(GLenum target​, GLenum attachment​, GLenum textarget​, GLuint texture​, GLint level​);
         *         void glFramebufferTextureLayer​(GLenum target​, GLenum attachment​, GLuint texture​, GLint level​, GLint layer​);
         *         ---
         *             The target​ parameter here is the same as the one for bind.
         *             However, GL_FRAMEBUFFER doesn't mean both read and draw (as that would make no sense);
         *             instead, it is the same as GL_DRAW_FRAMEBUFFER.
         *             The attachment​ parameter is one of the above attachment points.
         *
         *     operation                                   | GL_FRAMEBUFFER meaning
         *     ============================================|=================================
         *     glBindFramebuffer( GLenum target, ... )     | GL_DRAW_BUFFER && GL_READ_BUFFER
         *     --------------------------------------------|---------------------------------
         *     glFramebufferTexture1D(GLenum target​, ... ) | GL_DRAW_BUFFER
         *     glFramebufferTexture2D(GLenum target​, ... ) | GL_DRAW_BUFFER
         *     glFramebufferTexture2D(GLenum target​, ... ) | GL_DRAW_BUFFER
         */
    };

public:
    FramebufferBinder();
    FramebufferBinder(
            const GLuint aRequestedFboId,
            const BindingTarget aBindingTarget,
            const bool aAutoUnbind = true );
    ~FramebufferBinder();

    GLuint requestedFboId() const;
    BindingTarget bindingTarget() const;

    void bind();
    void unbind();

    // 0 - the index of the default framebuffer - the screen
    // https://www.opengl.org/wiki/Default_Framebuffer
    // default framebuffer can differ from the OpenGL's default one.
    // Qt 5.4's QOpenGLWindow and QOpenGLWidget classes use a dedicated one.
    // && CommonEntities::isUnsignedGlValueValid( m_requestedFboId )
    GLuint boundFboId() const;

private:
    static GLenum openGlFboBindingTarget( const BindingTarget bindingTarget );
    GLenum openGlFboBindingTarget() const;

    static GLenum openGlFboBindingTargetCheck( const BindingTarget bindingTarget );
    GLenum openGlFboBindingTargetCheck() const;

    void bind( const GLuint fboId );

private:
    GLuint        m_requestedFboId;
    BindingTarget m_bindingTarget;

    GLuint        m_previouslyBoundFboId;
    bool          m_isRequestedFboBound;
    bool          m_autoUnbind;
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_FRAMEBUFFERBINDER_H
