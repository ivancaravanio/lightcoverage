#ifndef LIGHTCOVERAGE_UTILITIES_NULLABLE_H
#define LIGHTCOVERAGE_UTILITIES_NULLABLE_H

namespace LightCoverage {
namespace Utilities {

template < class T >
class Nullable
{
public:
    Nullable();
    explicit Nullable( const T& initialValue, const bool aIsNull = true );

    void setValue( const T& aValue );
    const T& value() const;
    T& rvalue();

    bool isNull() const;
    void unset();
    void unset( const T& aValue );

    operator T() const;

    bool operator ==( const Nullable< T >& other ) const;
    bool operator !=( const Nullable< T >& other ) const;

    Nullable< T >& operator =( const T& other );

private:
    bool m_isNull;
    T    m_value;
};

template < class T > Nullable< T >::Nullable()
    : m_isNull( true )
{
}

template < class T > Nullable< T >::Nullable( const T& initialValue, const bool aIsNull )
    : m_isNull( aIsNull )
    , m_value( initialValue )
{
}

template < class T > void Nullable< T >::setValue( const T& aValue )
{
    m_value = aValue;
    m_isNull = false;
}

template < class T > const T& Nullable< T >::value() const
{
    return m_value;
}

template < class T > T& Nullable< T >::rvalue()
{
    m_isNull = false;
    return m_value;
}

template < class T > bool Nullable< T >::isNull() const
{
    return m_isNull;
}

template < class T > void Nullable< T >::unset()
{
    m_isNull = true;
}

template < class T > void Nullable< T >::unset( const T& aValue )
{
    m_value = aValue;
    this->unset();
}

template < class T > Nullable< T >::operator T() const
{
    return m_value;
}

template < class T > bool Nullable< T >::operator ==( const Nullable< T >& other ) const
{
    if ( m_isNull != other.m_isNull )
    {
        return false;
    }

    return m_isNull
           ? true
           : m_value == other.m_value;
}

template < class T > bool Nullable< T >::operator !=( const Nullable< T >& value ) const
{
    return !( *this == value );
}

template < class T > Nullable< T >& Nullable< T >::operator =( const T& value )
{
    this->setValue( value );

    return *this;
}

}
}

#endif // LIGHTCOVERAGE_UTILITIES_NULLABLE_H
