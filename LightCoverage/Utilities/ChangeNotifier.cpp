#include "ChangeNotifier.h"

#include "ChangeListener.h"

using namespace LightCoverage::Utilities;

const quint64 ChangeNotifier::s_invalidTrait = 0ULL;

ChangeNotifier::ChangeNotifier()
    : m_areNotificationsBlocked( false )
{
}

ChangeNotifier::~ChangeNotifier()
{
}

void ChangeNotifier::addListener( ChangeListener* const listener )
{
    if ( ! this->hasListener( listener ) )
    {
        m_listeners.append( listener );
    }
}

void ChangeNotifier::removeListener( ChangeListener* const listener )
{
    m_listeners.removeAll( listener );
}

void ChangeNotifier::clearListeners()
{
    m_listeners.clear();
}

bool ChangeNotifier::hasListener( ChangeListener* const listener ) const
{
    return m_listeners.contains( listener );
}

int ChangeNotifier::listenersCount() const
{
    return m_listeners.size();
}

void ChangeNotifier::setNotificationsBlocked( const bool aAreNotificationsBlocked )
{
    if ( m_areNotificationsBlocked != aAreNotificationsBlocked )
    {
        m_areNotificationsBlocked = aAreNotificationsBlocked;
    }
}

bool ChangeNotifier::areNotificationsBlocked() const
{
    return m_areNotificationsBlocked;
}

void ChangeNotifier::changed( const quint64& traits )
{
    if ( m_areNotificationsBlocked || traits == s_invalidTrait )
    {
        return;
    }

    foreach ( ChangeListener* const listener, m_listeners )
    {
        listener->onChanged( this, traits );
    }
}

quint64 ChangeNotifier::allTraits() const
{
    return s_invalidTrait;
}

void ChangeNotifier::notifyAllRolesChanged()
{
    this->changed( this->allTraits() );
}
