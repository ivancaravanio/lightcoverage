#ifndef LIGHTCOVERAGE_UTILITIES_ABSTRACTOPENGLCLIENT_H
#define LIGHTCOVERAGE_UTILITIES_ABSTRACTOPENGLCLIENT_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include "FramebufferBinder.h"
#include "OpenGlCapabilityEnabler.h"
#include "OpenGlObjectBinder.h"
#include "ViewportAlternator.h"

#include <QtGlobal>

namespace LightCoverage {
namespace Utilities {

class AbstractOpenGlClient
{
public:
    virtual ~AbstractOpenGlClient();

    virtual void init() = 0;

protected:
    OpenGlObjectBinder textureUnitBinder( const GLenum textureUnit, const bool autoUnbind = true ) const;
    OpenGlObjectBinder textureBinder( const GLuint textureId, const bool autoUnbind = true ) const;
    OpenGlObjectBinder vaoBinder( const GLuint vaoId, const bool autoUnbind = true ) const;
    OpenGlObjectBinder vboBinder( const GLuint vboId, const bool autoUnbind = true ) const;
    OpenGlObjectBinder veaBinder( const GLuint veaId, const bool autoUnbind = true ) const;
    OpenGlObjectBinder polygonModeAlternator( const GLenum face, const GLenum mode, const bool autoUnbind = true );
    FramebufferBinder fboBinder( const GLuint aRequestedFboId,
                                 const FramebufferBinder::BindingTarget aBindingTarget,
                                 const bool aAutoUnbind = true ) const;
    ViewportAlternator viewportAlternator( const QRect& aRequestedViewport, const bool autoRollback = true ) const;
    OpenGlCapabilityEnabler capabilityEnabler(
            const GLenum aCapability,
            const bool aShouldEnable,
            const bool aAutoRollback = true ) const;
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_ABSTRACTOPENGLCLIENT_H
