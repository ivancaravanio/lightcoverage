#include "LineWidthAlternator.h"

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

#include "MathUtilities.h"
#include "OpenGlErrorLogger.h"

#include <QtGlobal>

using namespace LightCoverage::Utilities;

LineWidthAlternator::LineWidthAlternator()
    : m_requestedLineWidth( MathUtilities::s_invalidLength )
    , m_previousLineWidth( MathUtilities::s_invalidLength )
    , m_autoRollback( false )
{
}

LineWidthAlternator::LineWidthAlternator(
        const float aRequestedLineWidth,
        const bool aAutoRollback )
    : m_requestedLineWidth( MathUtilities::s_invalidLength )
    , m_previousLineWidth( MathUtilities::s_invalidLength )
    , m_autoRollback( aAutoRollback )
{
    this->setRequestedLineWidth( aRequestedLineWidth );
}

LineWidthAlternator::~LineWidthAlternator()
{
    if ( m_autoRollback )
    {
        this->rollback();
    }
}

bool LineWidthAlternator::isValid() const
{
    return MathUtilities::isLengthValid( m_requestedLineWidth );
}

float LineWidthAlternator::requestedLineWidth() const
{
    return m_requestedLineWidth;
}

float LineWidthAlternator::currentLineWidth() const
{
    float lineWidth = 0.0f;
    glGetFloatv( GL_LINE_WIDTH, & lineWidth );
    LOG_OPENGL_ERROR();

    return lineWidth;
}

void LineWidthAlternator::change( const float aLineWidth )
{
    glLineWidth( aLineWidth );
    LOG_OPENGL_ERROR();
}

void LineWidthAlternator::setRequestedLineWidth( const float aRequestedLineWidth )
{
    if ( MathUtilities::isLengthValid( aRequestedLineWidth ) )
    {
        m_requestedLineWidth = aRequestedLineWidth;
    }
}

void LineWidthAlternator::change()
{
    if ( ! this->isValid() )
    {
        return;
    }

    const float currentLineWidth = this->currentLineWidth();
    if ( MathUtilities::isFuzzyEqual( currentLineWidth, m_requestedLineWidth ) )
    {
        return;
    }

    m_previousLineWidth = currentLineWidth;
    this->change( m_requestedLineWidth );
}

void LineWidthAlternator::rollback()
{
    if ( ! MathUtilities::isLengthValid( m_previousLineWidth ) )
    {
        return;
    }

    this->change( m_previousLineWidth );
    m_previousLineWidth = MathUtilities::s_invalidLength;
}
