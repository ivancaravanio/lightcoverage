#include "OpenGlUtilities.h"

#include <limits>

using namespace LightCoverage::Utilities;

const GLuint OpenGlUtilities::s_invalidUnsignedGlValue = 0U;
const GLint  OpenGlUtilities::s_invalidSignedGlValue   = -1;

GLenum OpenGlUtilities::invalidEnumGlValue()
{
    return std::numeric_limits< GLenum >::is_signed
           ? OpenGlUtilities::s_invalidSignedGlValue
           : OpenGlUtilities::s_invalidUnsignedGlValue;
}

bool OpenGlUtilities::isUnsignedGlValueValid( const GLuint unsignedGlValue )
{
    return unsignedGlValue > 0U;
}

bool OpenGlUtilities::isSignedGlValueValid( const GLint signedGlValue )
{
    return signedGlValue >= 0;
}

bool OpenGlUtilities::isEnumGlValueValid( const GLenum enumGlValue )
{
    return enumGlValue > OpenGlUtilities::invalidEnumGlValue();
}

bool OpenGlUtilities::isGeometricPrimitiveTypeValid( const OpenGlUtilities::GeometricPrimitiveType geometricPrimitiveType )
{
    return geometricPrimitiveType == GeometricPrimitiveTypeTriangle
           || geometricPrimitiveType == GeometricPrimitiveTypeTriangleFan
           || geometricPrimitiveType == GeometricPrimitiveTypeTriangleStrip;
}

GLenum OpenGlUtilities::openGLGeometricPrimitiveType( const OpenGlUtilities::GeometricPrimitiveType geometricPrimitiveType )
{
    switch ( geometricPrimitiveType )
    {
        case GeometricPrimitiveTypeTriangle:      return GL_TRIANGLES;
        case GeometricPrimitiveTypeTriangleFan:   return GL_TRIANGLE_FAN;
        case GeometricPrimitiveTypeTriangleStrip: return GL_TRIANGLE_STRIP;
        default:                                  break;
    }

    return OpenGlUtilities::invalidEnumGlValue();
}
