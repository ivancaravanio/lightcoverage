#ifndef LIGHTCOVERAGE_UTILITIES_OPENGLCAPABILITYENABLER_H
#define LIGHTCOVERAGE_UTILITIES_OPENGLCAPABILITYENABLER_H

// glew.h should be included prior to including gl.h
#include <GL/glew.h>

namespace LightCoverage {
namespace Utilities {

class OpenGlCapabilityEnabler
{
public:
    OpenGlCapabilityEnabler();
    OpenGlCapabilityEnabler(
            const GLenum aCapability,
            const bool aShouldEnable,
            const bool aAutoRollback = true );
    ~OpenGlCapabilityEnabler();

    bool isValid() const;

    GLenum capability() const;
    bool shouldEnable() const;
    bool isEnabled() const;

    void change();
    void rollback();

private:
    void change( const bool shouldEnable );

private:
    GLenum m_capability;
    bool   m_shouldEnable;
    bool   m_wasChanged;
    bool   m_autoRollback;
};

}
}

#endif // LIGHTCOVERAGE_UTILITIES_OPENGLCAPABILITYENABLER_H
