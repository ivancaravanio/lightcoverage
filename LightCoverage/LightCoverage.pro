QT += core gui opengl

CONFIG += c++11

macx {
    # Objective-C++ does not recognize C++11 syntax despite the CONFIG += c++11 option
    # https://bugreports.qt.io/browse/QTBUG-39057
    # https://bugreports.qt.io/browse/QTBUG-36575
    # https://codereview.qt-project.org/#/c/122199/

    QMAKE_OBJCXXFLAGS_PRECOMPILE += -std=c++11 -stdlib=libc++
}

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG *= precompile_header

# DEFINES += DRAW_ILLUMINATED_POINTS

PRECOMPILED_HEADER = LightCoverage_precompiled.h

include( ../Build/Global.pri )
include( ../Build/Versions.pri )

TARGET = $$PRODUCT_NAME
TEMPLATE = app

SOURCES += \
    AbstractRoomDisplay.cpp \
    Data/LineSegment2D.cpp \
    Data/Polygon2D.cpp \
    Data/Rectangle2D.cpp \
    Data/Room.cpp \
    Data/ShaderInfo.cpp \
    Data/Size.cpp \
    Data/Triangle2D.cpp \
    Data/Vector2D.cpp \
    Data/Vector3D.cpp \
    Data/Vector3Df.cpp \
    LightCoverage_namespace.cpp \
    main.cpp \
    MainController.cpp \
    MainWindow.cpp \
    RoomAreasDisplay.cpp \
    RoomCompositeDisplay.cpp \
    RoomFilePathDisplay.cpp \
    RoomDisplay.cpp \
    RoomDisplayWidget.cpp \
    Utilities/AbstractOpenGlClient.cpp \
    Utilities/ChangeListener.cpp \
    Utilities/ChangeNotifier.cpp \
    Utilities/ClearColorAlternator.cpp \
    Utilities/FramebufferBinder.cpp \
    Utilities/GeneralUtilities.cpp \
    Utilities/LineWidthAlternator.cpp \
    Utilities/MathUtilities.cpp \
    Utilities/OpenGlCapabilityEnabler.cpp \
    Utilities/OpenGlErrorLogger.cpp \
    Utilities/OpenGlObjectBinder.cpp \
    Utilities/OpenGlTimeCounter.cpp \
    Utilities/OpenGlUtilities.cpp \
    Utilities/Retranslatable.cpp \
    Utilities/RoomSerializer.cpp \
    Utilities/Serializable.cpp \
    Utilities/ShaderProgram.cpp \
    Utilities/ShaderProgramActivator.cpp \
    Utilities/ViewportAlternator.cpp \
    Utilities/WidgetUtilities.cpp

HEADERS += \
    AbstractRoomDisplay.h \
    Data/LineSegment2D.h \
    Data/Polygon2D.h \
    Data/Rectangle2D.h \
    Data/Room.h \
    Data/ShaderInfo.h \
    Data/Size.h \
    Data/Triangle2D.h \
    Data/Vector2D.h \
    Data/Vector3D.h \
    Data/Vector3Df.h \
    LightCoverage_namespace.h \
    LightCoverage_precompiled.h \
    MainController.h \
    MainWindow.h \
    RoomAreasDisplay.h \
    RoomCompositeDisplay.h \
    RoomFilePathDisplay.h \
    RoomDisplay.h \
    RoomDisplayWidget.h \
    Utilities/AbstractOpenGlClient.h \
    Utilities/ChangeListener.h \
    Utilities/ChangeNotifier.h \
    Utilities/ClearColorAlternator.h \
    Utilities/FramebufferBinder.h \
    Utilities/GeneralUtilities.h \
    Utilities/LineWidthAlternator.h \
    Utilities/MathUtilities.h \
    Utilities/Nullable.h \
    Utilities/OpenGlCapabilityEnabler.h \
    Utilities/OpenGlErrorLogger.h \
    Utilities/OpenGlObjectBinder.h \
    Utilities/OpenGlTimeCounter.h \
    Utilities/OpenGlUtilities.h \
    Utilities/Retranslatable.h \
    Utilities/RoomSerializer.h \
    Utilities/Serializable.h \
    Utilities/ShaderProgram.h \
    Utilities/ShaderProgramActivator.h \
    Utilities/ViewportAlternator.h \
    Utilities/WidgetUtilities.h

RESOURCES += \
    Resources/Resources.qrc

# glew
GLEW_BASE_DIR_PATH = $${PWD}/../ThirdParty/glew
GLEW_HEADERS_DIR_PATH = $${GLEW_BASE_DIR_PATH}/include

INCLUDEPATH += $$GLEW_HEADERS_DIR_PATH

# https://forum.qt.io/topic/5487/building-projects-for-32-and-64-bit-windows-on-64-bit-machine
defineTest(is64BitBuild) {
    equals( QMAKE_TARGET.arch, x64 ) {
        return (true)
    }

    return (false)
}

BITNESS_DIR_NAME =
is64BitBuild() {
    BITNESS_DIR_NAME = x64
} else {
    BITNESS_DIR_NAME = Win32
}

GLEW_BASE_FILE_NAME = glew32

GLEW_STATIC_LIBS_DIR_PATH = $${GLEW_BASE_DIR_PATH}/lib/Release/$${BITNESS_DIR_NAME}
LIBS += -L$$GLEW_STATIC_LIBS_DIR_PATH -l$${GLEW_BASE_FILE_NAME}

VERSION_MAJ = $$LIGHTCOVERAGE_VER_MAJ
VERSION_MIN = $$LIGHTCOVERAGE_VER_MIN
VERSION_PAT = $$LIGHTCOVERAGE_VER_PAT

USED_LIBRARIES =

ICON_COMPLETE_BASE_FILE_PATH = $${PWD}/Resources/Icons/Icon

# SHOULD_DEPLOY = 1

# isDebugBuild() {
#     SHOULD_DEPLOY = 0
# } else {
#     SHOULD_DEPLOY = 1
# }

include( ../Build/Build.pri )

GLEW_DYNAMIC_LIBS_DIR_PATH = $${GLEW_BASE_DIR_PATH}/bin/Release/$${BITNESS_DIR_NAME}
GLEW_DYNAMIC_LIB_FILE_NAME = $$libraryFullBinaryFileName( $$GLEW_BASE_FILE_NAME )

for( DEPLOY_DIR_PATH, DEPLOY_DIR_PATHS ) {
    QMAKE_POST_LINK += $$copyFile( $${GLEW_DYNAMIC_LIBS_DIR_PATH}/$${GLEW_DYNAMIC_LIB_FILE_NAME}, $$DEPLOY_DIR_PATH ) $$CRLF
    QMAKE_CLEAN += $${DEPLOY_DIR_PATH}/$${GLEW_DYNAMIC_LIB_FILE_NAME}
}
