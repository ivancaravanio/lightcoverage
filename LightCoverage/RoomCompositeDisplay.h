#ifndef LIGHTCOVERAGE_ROOMCOMPOSITEDISPLAY_H
#define LIGHTCOVERAGE_ROOMCOMPOSITEDISPLAY_H

#include "Utilities/Retranslatable.h"

#include <QList>
#include <QWidget>

QT_FORWARD_DECLARE_CLASS(QTabWidget)

namespace LightCoverage {

class AbstractRoomDisplay;

namespace Data {

class Room;

}

using namespace LightCoverage::Data;
using namespace LightCoverage::Utilities;

class RoomCompositeDisplay : public QWidget, public Retranslatable
{
    Q_OBJECT

public:
    explicit RoomCompositeDisplay( QWidget* const parent = nullptr, Qt::WindowFlags f = 0 );
    ~RoomCompositeDisplay() /* override */;

    void setRoom( Room* const aRoom );
    Room* room() const;

    void refresh() /* override */;
    void retranslate() /* override */;

private slots:
    void navigateToGpuDisplay();
    void navigateToCpuDisplay();

private:
    void retranslateInternal();

private:
    QTabWidget* m_roomDisplaySwitch;
    QList< AbstractRoomDisplay* > m_roomDisplays;

    int m_roomGPUDisplayTabIndex;
    int m_roomCPUDisplayTabIndex;
};

}

#endif // LIGHTCOVERAGE_ROOMCOMPOSITEDISPLAY_H
