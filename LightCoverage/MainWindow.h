#ifndef LIGHTCOVERAGE_MAINWINDOW_H
#define LIGHTCOVERAGE_MAINWINDOW_H

#include "Utilities/Retranslatable.h"

#include <QKeySequence>
#include <QMainWindow>
#include <QStringList>

QT_FORWARD_DECLARE_CLASS(QAction)
QT_FORWARD_DECLARE_CLASS(QMenu)

namespace LightCoverage {
namespace Data {

class Room;

}

class RoomCompositeDisplay;
class RoomFilePathDisplay;

using namespace LightCoverage::Data;
using namespace LightCoverage::Utilities;

class MainWindow : public QMainWindow, public Retranslatable
{
    Q_OBJECT

public:
    explicit MainWindow( QWidget* const parent = nullptr );
    ~MainWindow() /* override */;

    void setRoomFilePath( const QString& aRoomFilePath );
    QString roomFilePath() const;

    void setRoom( Room* const aRoom );
    Room* room() const;

    void retranslate() /* override */;

signals:
    void loadRoomRequested( const QString& filePath = QString() );
    void saveRoomRequested();
    void clearRoomRequested();

protected:
#ifndef QT_NO_DRAGANDDROP
    void dragEnterEvent( QDragEnterEvent* event ) /* override */;
    void dropEvent( QDropEvent* event ) /* override */;
#endif

private:
    static const int s_maxAllowedRecentlyUsedRoomFilePaths;

private slots:
    void about();
    void requestOpenRecentlyUsedFilePath();

private:
    void retranslateInternal();
    void updateMenusAvailability();
    static QString aboutText();
    void stackMostRecentlyUsedRoomFilePath( const QString& aRecentlyUsedRoomFilePath );
    void setRecentlyUsedRoomFilePaths( const QStringList& aRecentlyUsedRoomFilePaths );
    QStringList recentlyUsedRoomFilePaths() const;
    static QKeySequence recentlyUsedRoomFilePathShortcut( const int index );

private:
    // file menu
    QMenu*   m_fileMenu;
    QAction* m_openRoomAction;

    // file->recently used rooms menu
    QMenu* m_recentlyUsedRoomsMenu;

    QAction* m_saveRoomAction;
    QAction* m_closeRoomAction;

    // help menu
    QMenu*   m_helpMenu;
    QAction* m_aboutAction;

    RoomCompositeDisplay* m_roomCompositeDisplay;
    RoomFilePathDisplay* m_roomFilePathDisplay;
};

}

#endif // LIGHTCOVERAGE_MAINWINDOW_H
