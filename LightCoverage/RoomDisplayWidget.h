#ifndef LIGHTCOVERAGE_ROOMDISPLAYWIDGET_H
#define LIGHTCOVERAGE_ROOMDISPLAYWIDGET_H

#include "AbstractRoomDisplay.h"
#include "Data/Rectangle2D.h"

#include <QList>
#include <QPen>
#include <QPolygonF>
#include <QWidget>
#ifdef DRAW_ILLUMINATED_POINTS
    #include <QString>
#endif

namespace LightCoverage {

using namespace LightCoverage::Data;

class RoomDisplayWidget : public QWidget, public AbstractRoomDisplay
{
    Q_OBJECT

public:
    explicit RoomDisplayWidget( QWidget* const parent = nullptr, Qt::WindowFlags f = 0 );
    ~RoomDisplayWidget() /* override */;

    void refresh( const quint64& traits = 0ULL ) /* override */;

    Rectangle2D drawableRect() const /* override */;

protected:
    void paintEvent( QPaintEvent* event ) /* override */;

    void mousePressEvent( QMouseEvent* event ) /* override */;
    void mouseMoveEvent( QMouseEvent* event ) /* override */;
    void mouseReleaseEvent( QMouseEvent* event ) /* override */;

private:
    void buildIlluminatedRegionsCache();
    void buildColumnsCache();
    void buildCache();
    static QPen borderPen();

#ifdef DRAW_ILLUMINATED_POINTS
    static QString vertexDescriptionTemplate();
#endif

private:
    QList< QPolygonF > m_illuminatedRegions;
    QList< QPolygonF > m_columns;
};

}

#endif // LIGHTCOVERAGE_ROOMDISPLAYWIDGET_H
