#include "MainController.h"

#include <QApplication>
#include <QStringList>

using namespace LightCoverage;

int main( int argc, char* argv[] )
{
    QApplication a( argc, argv );
    QApplication::setApplicationVersion( VERSION_STRING );

    const QStringList arguments = a.arguments();

    MainController controller;
    controller.init( arguments.size() >= 2
                     ? arguments[ 1 ]
                     : QString() );

    return a.exec();
}
