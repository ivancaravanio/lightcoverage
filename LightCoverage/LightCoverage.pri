include( ../Build/QmakeHelper/QmakeHelper.pri )

LIGHTCOVERAGE_VER_MAJ       = 1
LIGHTCOVERAGE_VER_MIN       = 0
LIGHTCOVERAGE_VER_PAT       = 0
LIGHTCOVERAGE_VERSION_PARTS = $$LIGHTCOVERAGE_VER_MAJ \
                              $$LIGHTCOVERAGE_VER_MIN \
                              $$LIGHTCOVERAGE_VER_PAT
LIGHTCOVERAGE_VERSION       = $$versionString( LIGHTCOVERAGE_VERSION_PARTS )
