#ifndef LIGHTCOVERAGE_DATA_VECTOR2D_H
#define LIGHTCOVERAGE_DATA_VECTOR2D_H

#include <QDebug>
#include <QPoint>
#include <QPointF>

namespace LightCoverage {
namespace Data {

class Vector2D
{
public:
    explicit Vector2D( const double& aX, const double& aY );
    explicit Vector2D( const QPoint& pt );
    explicit Vector2D( const QPointF& pt );
    Vector2D();

    static Vector2D fromAngleLength( const double& aAngle, const double& aLength );

    void setX( const double& aX );
    inline double x() const
    {
        return m_x;
    }

    void setY( const double& aY );
    inline double y() const
    {
        return m_y;
    }

    void set( const double& aX, const double& aY );
    void zero();

    double length() const;
    bool isUnit() const;
    double distanceTo( const Vector2D& other ) const;
    double dot( const Vector2D& other ) const;

    bool operator==( const Vector2D& other ) const;
    bool operator!=( const Vector2D& other ) const;

    Vector2D operator+( const Vector2D& other ) const;
    Vector2D& operator+=( const Vector2D& other );
    Vector2D operator-( const Vector2D& other ) const;
    Vector2D& operator-=( const Vector2D& other );
    Vector2D operator*( const double& factor ) const;
    Vector2D& operator*=( const double& factor );
    Vector2D operator/( const double& factor ) const;
    Vector2D& operator/=( const double& factor );

    void normalize();
    Vector2D normalized() const;
    Vector2D normal() const;
    double angle() const;
    double angleTo( const Vector2D& other ) const;

    QPoint toQPoint() const;
    operator QPoint() const;

    QPointF toQPointF() const;
    operator QPointF() const;

private:
    double m_x;
    double m_y;
};

}
}

LightCoverage::Data::Vector2D operator*( const double& factor, const LightCoverage::Data::Vector2D& pt );

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const LightCoverage::Data::Vector2D& pt );
#endif

#endif // LIGHTCOVERAGE_DATA_VECTOR2D_H
