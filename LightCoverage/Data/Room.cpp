#include "Room.h"

#include "../Utilities/MathUtilities.h"

using namespace LightCoverage::Data;

Room::Room(
        const Size& aSize,
        const Vector2D& aLightSourcePosition,
        const QList< Polygon2D >& aColumns )
    : m_size( aSize )
    , m_lightSourcePosition( aLightSourcePosition )
    , m_columns( aColumns )
    , m_isValid( false )
    , m_columnsArea( MathUtilities::s_invalidLength )
    , m_illuminatedArea( MathUtilities::s_invalidLength )
{
}

Room::Room()
    : m_isValid( false )
    , m_columnsArea( MathUtilities::s_invalidLength )
    , m_illuminatedArea( MathUtilities::s_invalidLength )
{
}

void Room::setSize( const Size& aSize )
{
    if ( aSize == m_size )
    {
        return;
    }

    this->clearCache();
    m_size = aSize;
    this->changed( TraitSize );
}

void Room::setLightSourcePosition( const Vector2D& aLightSourcePosition )
{
    if ( aLightSourcePosition == m_lightSourcePosition )
    {
        return;
    }

    this->clearCache();
    m_lightSourcePosition = aLightSourcePosition;
    this->changed( TraitLightSourcePosition );
}

double Room::lightSourceRadius() const
{
    return m_size.minLength() * 0.01;
}

Rectangle2D Room::lightSourceBoundingRect() const
{
    const double lightSourceRadius = this->lightSourceRadius();
    const Vector2D radiusVector( lightSourceRadius, lightSourceRadius );

    return Rectangle2D( m_lightSourcePosition - radiusVector,
                        m_lightSourcePosition + radiusVector );
}

void Room::setColumns( const QList< Polygon2D >& aColumns )
{
    if ( aColumns == m_columns )
    {
        return;
    }

    this->clearCache( true );
    m_columns = aColumns;
    this->changed( TraitColumns );
}

bool Room::isValid() const
{
    if ( m_isValid.isNull() )
    {
        m_isValid = this->isValidCalculated();
    }

    return m_isValid;
}

double Room::area() const
{
    return m_size.area();
}

double Room::columnsArea() const
{
    if ( ! MathUtilities::isLengthValid( m_columnsArea ) )
    {
        m_columnsArea = this->columnsAreaCalculated();
    }

    return m_columnsArea;
}

// #include <QTime>

QList< Triangle2D > Room::illuminatedRegions() const
{
    if ( m_illuminatedRegions.isNull() )
    {
        // QTime t;
        // t.start();
        m_illuminatedRegions = this->illuminatedRegionsCalculated();
        // qDebug() << "illuminated regions calc time:" << QTime().addMSecs( t.elapsed() ).toString( "hh:mm:ss.zzz" );
    }

    return m_illuminatedRegions;
}

double Room::illuminatedArea() const
{
    if ( ! MathUtilities::isLengthValid( m_illuminatedArea ) )
    {
        m_illuminatedArea = this->illuminatedAreaCalculated();
    }

    return m_illuminatedArea;
}

double Room::shadowedArea() const
{
    return this->area() - this->columnsArea() - this->illuminatedArea();
}

void Room::clear()
{
    m_size.clear();
    m_lightSourcePosition.zero();
    m_columns.clear();

    this->clearCache( true );
}

bool Room::operator==( const Room& other ) const
{
    return    m_size                == other.m_size
           && m_lightSourcePosition == other.m_lightSourcePosition
           && m_columns             == other.m_columns;
}

bool Room::operator!=( const Room& other ) const
{
    return ! ( *this == other );
}

quint64 Room::allTraits() const
{
    return TraitsAll;
}

void Room::angularSort( QList< VertexSortData >& vertices )
{
    qSort( vertices.begin(), vertices.end(), & VertexSortData::hasLessAngleCcw );
}

QList< Triangle2D > Room::triangleFan(
        const QList< VertexSortData >& radiusPts,
        const Vector2D& centerPt,
        const bool opened )
{
    const int radiusPointsCount = radiusPts.size();
    if ( radiusPointsCount + 1 < Triangle2D::s_verticesCount )
    {
        return QList< Triangle2D >();
    }

    QList< Triangle2D > triangles;
    const int trianglesCount = opened
                               ? radiusPointsCount - 1
                               : radiusPointsCount;
    triangles.reserve( trianglesCount );

    for ( int i = 0; i < trianglesCount; ++ i )
    {
        const Triangle2D tri( centerPt,
                              radiusPts[ i ].vertex(),
                              radiusPts[ ( i + 1 ) % radiusPointsCount ].vertex() );
        if ( ! tri.isValid() )
        {
            continue;
        }

        triangles.append( tri );
    }

    return triangles;
}

QList< Triangle2D > Room::triangleFan(
        const QList< VertexSortData >& radiusPts,
        const bool opened ) const
{
    return Room::triangleFan( radiusPts, m_lightSourcePosition, opened );
}

void Room::clearColumnsAreaCache()
{
    m_columnsArea = MathUtilities::s_invalidLength;
}

void Room::clearCache( const bool clearColumnsAreaCacheAsWell )
{
    m_isValid.unset( false );
    m_illuminatedRegions.unset( QList< Triangle2D >() );
    m_illuminatedArea = MathUtilities::s_invalidLength;

#ifdef DRAW_ILLUMINATED_POINTS
    m_farthestIlluminatedPoints.clear();
#endif

    if ( clearColumnsAreaCacheAsWell )
    {
        this->clearColumnsAreaCache();
    }
}

bool Room::isValidCalculated() const
{
    if ( ! m_size.isValid() )
    {
        return false;
    }

    const Rectangle2D roomRect( m_size );
    if ( ! roomRect.contains( m_lightSourcePosition ) )
    {
        return false;
    }

    const int columnsCount = m_columns.size();
    for ( int i = 0; i < columnsCount; ++ i )
    {
        const Polygon2D& column = m_columns[ i ];
        if ( ! column.isConvex()
             || ! roomRect.contains( column.boundingRect() ) )
        {
            return false;
        }
    }

    for ( int i = 0; i < columnsCount; ++ i )
    {
        const Polygon2D& column = m_columns[ i ];
        for ( int j = i + 1; j < columnsCount; ++ j )
        {
            const Polygon2D& otherColumn = m_columns[ j ];
            if ( column.interactionType( otherColumn ) != Polygon2D::InteractionTypeNone )
            {
                return false;
            }
        }
    }

    return true;
}

double Room::columnsAreaCalculated() const
{
    double area = 0.0;

    const int columnsCount = m_columns.size();
    for ( int i = 0; i < columnsCount; ++i )
    {
        area += m_columns[ i ].area();
    }

    return area;
}

QList< Triangle2D > Room::illuminatedRegionsCalculated() const
{
    if ( ! this->isValid() )
    {
        return QList< Triangle2D >();
    }

    const int columnsCount = m_columns.size();
    for ( int columnIndex = 0; columnIndex < columnsCount; ++ columnIndex )
    {
        const Polygon2D& column = m_columns[ columnIndex ];
        if ( column.contains( m_lightSourcePosition ) )
        {
            return QList< Triangle2D >();
        }
    }

    QList< VertexSortData > farthestIlluminatedPoints;

    const Rectangle2D roomRect( m_size );
    const QList< LineSegment2D > roomEdges = roomRect.edges();
    const int roomEdgesCount = roomEdges.size();

    const QVector< Vector2D > roomVertices = roomRect.vertices();

    static const int s_roomBoundsPolygonIndex = -1;

    for ( int columnIndex = 0; columnIndex <= columnsCount; ++columnIndex )
    {
        const bool isRoomBoundsIntersectionTest = columnIndex == columnsCount;
        const QVector< Vector2D >& vertices = isRoomBoundsIntersectionTest
                                              ? roomVertices
                                              : m_columns[ columnIndex ].vertices();
        const int verticesCount = vertices.size();
        for ( int columnVertexIndex = 0; columnVertexIndex < verticesCount; ++columnVertexIndex )
        {
            bool skipCurrentPoint = false;

            const Vector2D& vertex = vertices[ columnVertexIndex ];

            const LineSegment2D initialLightBeam( m_lightSourcePosition, vertex );
            const double initialLightBeamLength = initialLightBeam.length();

            bool isRoomBoundsIntersectionPtInit = false;
            Vector2D roomBoundsIntersectionPt;
            if ( isRoomBoundsIntersectionTest )
            {
                roomBoundsIntersectionPt = vertex;
                isRoomBoundsIntersectionPtInit = true;
            }
            else
            {
                for ( int roomEdgeIndex = 0; roomEdgeIndex < roomEdgesCount; ++ roomEdgeIndex )
                {
                    const LineSegment2D roomEdge = roomEdges[ roomEdgeIndex ];
                    if ( initialLightBeam.rayIntersects( roomEdge, true, & roomBoundsIntersectionPt ) )
                    {
                        isRoomBoundsIntersectionPtInit = true;
                        break;
                    }
                }
            }

            if ( ! isRoomBoundsIntersectionPtInit )
            {
                // impossible; could happen only because of rounding errors and
                // thus two double numbers that are actually equal could happen to be different
                qDebug() << "WARNING: light ray starting from light position and passing through "
                            "column/room\'s vertex does not cross the room bounds. "
                            "The most probable cause is the chosen epsilon - it is very small and should be bigger.";
                continue;
            }

            Vector2D farthestIlluminatedPt = roomBoundsIntersectionPt;
            int intersectedOtherColumnIndex = s_roomBoundsPolygonIndex;

            LineSegment2D activeLightBeam( m_lightSourcePosition, farthestIlluminatedPt );
            double activeLightBeamLength = activeLightBeam.length();
            for ( int otherColumnIndex = isRoomBoundsIntersectionTest
                                         ? 0
                                         : -1;
                  otherColumnIndex < columnsCount;
                  ++ otherColumnIndex )
            {
                if ( skipCurrentPoint )
                {
                    break;
                }

                if ( otherColumnIndex == columnIndex )
                {
                    continue;
                }

                const bool isSameColumnIntersectionTest = otherColumnIndex == -1;
                const int otherColumnActualIndex = isSameColumnIntersectionTest
                                                   ? columnIndex
                                                   : otherColumnIndex;
                const Polygon2D& otherColumn = m_columns[ otherColumnActualIndex ];
                const int otherVerticesCount = otherColumn.verticesCount();
                for ( int otherColumnVertexIndex = 0;
                      otherColumnVertexIndex < otherVerticesCount;
                      ++ otherColumnVertexIndex )
                {
                    const LineSegment2D otherEdge = otherColumn.edgeStartingFromVertex( otherColumnVertexIndex );
                    Vector2D intersectionPt;
                    if ( ! activeLightBeam.rayIntersects( otherEdge, true, & intersectionPt ) )
                    {
                        continue;
                    }

                    const LineSegment2D activeLightBeamCandidate( m_lightSourcePosition, intersectionPt );
                    const double activeLightBeamCandidateLength = activeLightBeamCandidate.length();
                    if ( isSameColumnIntersectionTest
                         && intersectionPt == vertex )
                    {
                        continue;
                    }

                    if ( MathUtilities::isFuzzyLessThan( activeLightBeamCandidateLength, initialLightBeamLength ) )
                    {
                        skipCurrentPoint = true;
                        break;
                    }

                    if ( ! MathUtilities::isFuzzyLessThan( activeLightBeamCandidateLength, activeLightBeamLength ) )
                    {
                        continue;
                    }

                    const Vector2D& nearestIntersectionPt = isSameColumnIntersectionTest
                                                            ? vertex
                                                            : intersectionPt;

                    farthestIlluminatedPt = nearestIntersectionPt;
                    activeLightBeam.setEndPt( nearestIntersectionPt );
                    activeLightBeamLength = activeLightBeam.length();
                    if ( isSameColumnIntersectionTest )
                    {
                        break;
                    }

                    if ( ! isRoomBoundsIntersectionTest )
                    {
                        intersectedOtherColumnIndex = otherColumnActualIndex;
                    }
                }
            }

            if ( skipCurrentPoint )
            {
                continue;
            }

            const double angle = initialLightBeam.angle();
            farthestIlluminatedPoints.append( VertexSortData( vertex, angle, isRoomBoundsIntersectionTest
                                                                             ? s_roomBoundsPolygonIndex
                                                                             : columnIndex ) );

            if ( farthestIlluminatedPt != vertex )
            {
                farthestIlluminatedPoints.append( VertexSortData( farthestIlluminatedPt, angle, intersectedOtherColumnIndex ) );
            }
        }
    }

    Room::angularSort( farthestIlluminatedPoints );

    int lastPolyProcessedPtsCount = 1;

    const int lastFarthestIlluminatedPointIndex = farthestIlluminatedPoints.size() - 1;
    for ( int i = 1; i <= lastFarthestIlluminatedPointIndex; ++ i )
    {
        VertexSortData& prevVertex       = farthestIlluminatedPoints[ i - 1 ];
        const int       prevVertexPolyId = prevVertex.polygonId();

        VertexSortData& vertex           = farthestIlluminatedPoints[ i ];
        const int       vertexPolyId     = vertex.polygonId();

        if ( vertexPolyId == prevVertexPolyId )
        {
            ++ lastPolyProcessedPtsCount;
            continue;
        }

        if ( lastPolyProcessedPtsCount > 1 )
        {
            lastPolyProcessedPtsCount = 1;
            continue;
        }

        if ( ! MathUtilities::isFuzzyEqual( vertex.angle(), prevVertex.angle() ) )
        {
            continue;
        }

        // NOTE: polygon id caching is done not only because of reusability
        // but because the initial polygon indices should be known,
        // since point swaps could occur that could alter them (see below)
        const int       prevPrevPrevVertexIndex  = MathUtilities::cyclicNumber( 0, i - 3, lastFarthestIlluminatedPointIndex );
        VertexSortData& prevPrevPrevVertex       = farthestIlluminatedPoints[ prevPrevPrevVertexIndex ];
        const int       prevPrevPrevVertexPolyId = prevPrevPrevVertex.polygonId();

        const int       prevPrevVertexIndex      = MathUtilities::cyclicNumber( 0, i - 2, lastFarthestIlluminatedPointIndex );
        VertexSortData& prevPrevVertex           = farthestIlluminatedPoints[ prevPrevVertexIndex ];
        const int       prevPrevVertexPolyId     = prevPrevVertex.polygonId();
        if ( ! MathUtilities::isFuzzyEqual( prevPrevVertex.angle(), prevPrevPrevVertex.angle() ) )
        {
            if ( vertexPolyId == prevPrevVertexPolyId )
            {
                qSwap( vertex, prevVertex );
            }

            continue;
        }

        if (    ( vertexPolyId == prevPrevVertexPolyId
                  || vertexPolyId == prevPrevPrevVertexPolyId )
             && ( prevVertexPolyId == prevPrevVertexPolyId
                  || prevVertexPolyId == prevPrevPrevVertexPolyId ) )
        {
            const int lightSourceClosestPolyId = MathUtilities::isFuzzyLessThan( ( vertex.vertex() - m_lightSourcePosition ).length(),
                                                                                 ( prevVertex.vertex() - m_lightSourcePosition ).length() )
                                                 ? vertexPolyId
                                                 : prevVertexPolyId;
            if ( prevVertexPolyId != lightSourceClosestPolyId )
            {
                qSwap( vertex, prevVertex );
            }

            if ( prevPrevVertexPolyId != lightSourceClosestPolyId )
            {
                qSwap( prevPrevVertex, prevPrevPrevVertex );
            }

            continue;
        }

        if ( prevVertexPolyId == prevPrevVertexPolyId )
        {
            continue;
        }

        // IMPORTANT: cache the comparisons prior to swaping the points
        // this is an additional security measure if the polygon ids are not cached
        const bool prevPointsAreEqual    = prevVertexPolyId == prevPrevPrevVertexPolyId;
        const bool currentPointsAreEqual = vertexPolyId == prevPrevVertexPolyId;

        if ( prevPointsAreEqual )
        {
            qSwap( prevPrevVertex, prevPrevPrevVertex );
        }

        if ( currentPointsAreEqual )
        {
            qSwap( vertex, prevVertex );
        }

        if ( ! prevPointsAreEqual
             && ! currentPointsAreEqual )
        {
            qSwap( prevPrevVertex, prevPrevPrevVertex );
            qSwap( vertex, prevVertex );
        }
    }

#ifdef DRAW_ILLUMINATED_POINTS
    m_farthestIlluminatedPoints = farthestIlluminatedPoints;
#endif

    return this->triangleFan( farthestIlluminatedPoints, false );
}

double Room::illuminatedAreaCalculated() const
{
    double area = 0.0;
    const QList< Triangle2D > illuminatedRegions = this->illuminatedRegions();
    const int illuminatedRegionsCount = illuminatedRegions.size();
    for ( int i = 0; i < illuminatedRegionsCount; ++ i )
    {
        area += illuminatedRegions[ i ].area();
    }

    return area;
}


Room::VertexSortData::VertexSortData(
        const Vector2D& aVertex,
        const double& aAngle,
        const int aPolygonId )
    : m_vertex( aVertex )
    , m_angle( aAngle )
    , m_polygonId( aPolygonId )
{
}

Room::VertexSortData::VertexSortData()
    : m_angle( 0.0 )
    , m_polygonId( -1 )
{
}

bool Room::VertexSortData::hasLessAngleCcw(
        const Room::VertexSortData& leftVertex,
        const Room::VertexSortData& rightVertex )
{
    return MathUtilities::isFuzzyLessThan( leftVertex.angle(), rightVertex.angle() );
}
