#include "Vector2D.h"

#include "../Utilities/MathUtilities.h"

#include <qmath.h>

#include <math.h>

using namespace LightCoverage::Data;

using namespace LightCoverage::Utilities;

Vector2D::Vector2D( const double& aX, const double& aY )
    : m_x( aX )
    , m_y( aY )
{
}

Vector2D::Vector2D( const QPoint& pt )
    : m_x( pt.x() )
    , m_y( pt.y() )
{
}

Vector2D::Vector2D( const QPointF& pt )
    : m_x( pt.x() )
    , m_y( pt.y() )
{
}

Vector2D::Vector2D()
    : m_x( 0.0 )
    , m_y( 0.0 )
{
}

Vector2D Vector2D::fromAngleLength( const double& aAngle, const double& aLength )
{
    return Vector2D( cos( MathUtilities::radians( aAngle ) ), sin( MathUtilities::radians( aAngle ) ) ) * aLength;
}

void Vector2D::setX( const double& aX )
{
    m_x = aX;
}

void Vector2D::setY( const double& aY )
{
    m_y = aY;
}

void Vector2D::set( const double& aX, const double& aY )
{
    this->setX( aX );
    this->setY( aY );
}

void Vector2D::zero()
{
    this->set( 0.0, 0.0 );
}

double Vector2D::length() const
{
    return sqrt( this->dot( *this ) );
}

bool Vector2D::isUnit() const
{
    return MathUtilities::isFuzzyEqual( this->length(), 1.0 );
}

double Vector2D::distanceTo( const Vector2D& other ) const
{
    return Vector2D( other - *this ).length();
}

double Vector2D::dot( const Vector2D& other ) const
{
    return m_x * other.m_x + m_y * other.m_y;
}

bool Vector2D::operator==( const Vector2D& other ) const
{
    return MathUtilities::isFuzzyEqual( m_x, other.m_x )
           && MathUtilities::isFuzzyEqual( m_y, other.m_y );
}

bool Vector2D::operator!=( const Vector2D& other ) const
{
    return ! ( *this == other );
}

Vector2D Vector2D::operator+( const Vector2D& other ) const
{
    Vector2D vec = *this;
    vec += other;

    return vec;
}

Vector2D& Vector2D::operator+=( const Vector2D& other )
{
    m_x += other.m_x;
    m_y += other.m_y;

    return *this;
}

Vector2D Vector2D::operator-( const Vector2D& other ) const
{
    Vector2D vec = *this;
    vec -= other;

    return vec;
}

Vector2D& Vector2D::operator-=( const Vector2D& other )
{
    m_x -= other.m_x;
    m_y -= other.m_y;

    return *this;
}

Vector2D Vector2D::operator*( const double& factor ) const
{
    Vector2D vec = *this;
    vec *= factor;

    return vec;
}

Vector2D& Vector2D::operator*=( const double& factor )
{
    m_x *= factor;
    m_y *= factor;

    return *this;
}

Vector2D Vector2D::operator/( const double& factor ) const
{
    Vector2D vec = *this;
    vec /= factor;

    return vec;
}

Vector2D& Vector2D::operator/=( const double& factor )
{
    if ( MathUtilities::isFuzzyZero( factor ) )
    {
        const double inf = std::numeric_limits< double >::infinity();
        m_x = MathUtilities::sign( m_x ) * inf;
        m_y = MathUtilities::sign( m_y ) * inf;
    }
    else
    {
        m_x /= factor;
        m_y /= factor;
    }

    return *this;
}

void Vector2D::normalize()
{
    *this /= this->length();
}

Vector2D Vector2D::normalized() const
{
    Vector2D vec = *this;
    vec.normalize();

    return vec;
}

Vector2D Vector2D::normal() const
{
    return Vector2D( -m_y, m_x );
}

double Vector2D::angle() const
{
    return MathUtilities::toPositive360DegreesAngleFromRadians( atan2( m_y, m_x ) );
}

double Vector2D::angleTo( const Vector2D& other ) const
{
    // angles diff: slower due to 2 atan2 operations, compared to sqrts + acos for the dot product one:
    // http://stackoverflow.com/questions/9316995/which-is-more-efficient-atan2-or-sqrt
    // return other.angle() - this->angle();

    // dot product implementation
    return MathUtilities::toPositive360DegreesAngleFromRadians( acos( this->normalized().dot( other.normalized() ) ) );
}

QPoint Vector2D::toQPoint() const
{
    return QPoint( static_cast< int >( m_x ),
                   static_cast< int >( m_y ) );
}

Vector2D::operator QPoint() const
{
    return this->toQPoint();
}

QPointF Vector2D::toQPointF() const
{
    return QPointF( m_x, m_y );
}

Vector2D::operator QPointF() const
{
    return this->toQPointF();
}

Vector2D operator*( const double& factor, const Vector2D& vec )
{
    return vec * factor;
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Vector2D& vec )
{
    return d.nospace() << "Vector2D( " << vec.x() << ", " << vec.y() << " )";
}
#endif
