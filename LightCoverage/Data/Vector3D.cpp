#include "Vector3D.h"

#include "../Utilities/MathUtilities.h"

#include <qmath.h>

#include <math.h>

using namespace LightCoverage::Data;

using namespace LightCoverage::Utilities;

Vector3D::Vector3D( const double& aX, const double& aY, const double& aZ )
    : m_x( aX )
    , m_y( aY )
    , m_z( aZ )
{
}

Vector3D::Vector3D( const Vector2D& vector2D, const double& aZ )
    : m_x( vector2D.x() )
    , m_y( vector2D.y() )
    , m_z( aZ )
{
}

Vector3D::Vector3D()
    : m_x( 0.0 )
    , m_y( 0.0 )
    , m_z( 0.0 )
{
}

void Vector3D::setX( const double& aX )
{
    m_x = aX;
}

void Vector3D::setY( const double& aY )
{
    m_y = aY;
}

void Vector3D::setZ( const double& aZ )
{
    m_z = aZ;
}

void Vector3D::set( const double& aX, const double& aY, const double& aZ )
{
    this->setX( aX );
    this->setY( aY );
    this->setZ( aZ );
}

double Vector3D::length() const
{
    return sqrt( this->dot( *this ) );
}

bool Vector3D::isUnit() const
{
    return MathUtilities::isFuzzyEqual( this->length(), 1.0 );
}

double Vector3D::distanceTo( const Vector3D& other ) const
{
    return Vector3D( other - *this ).length();
}

double Vector3D::dot( const Vector3D& other ) const
{
    return m_x * other.m_x + m_y * other.m_y + m_z * other.m_z;
}

Vector3D Vector3D::cross( const Vector3D& other ) const
{
    return Vector3D( m_y * other.m_z - m_z * other.m_y,
                     m_z * other.m_x - m_x * other.m_z,
                     m_x * other.m_y - m_y * other.m_x );
}

bool Vector3D::operator==( const Vector3D& other ) const
{
    return MathUtilities::isFuzzyEqual( m_x, other.m_x )
           && MathUtilities::isFuzzyEqual( m_y, other.m_y )
           && MathUtilities::isFuzzyEqual( m_z, other.m_z );
}

bool Vector3D::operator!=( const Vector3D& other ) const
{
    return ! ( *this == other );
}

Vector3D Vector3D::operator+( const Vector3D& other ) const
{
    Vector3D pt = *this;
    pt += other;

    return pt;
}

Vector3D& Vector3D::operator+=( const Vector3D& other )
{
    m_x += other.m_x;
    m_y += other.m_y;
    m_z += other.m_z;

    return *this;
}

Vector3D Vector3D::operator-( const Vector3D& other ) const
{
    Vector3D pt = *this;
    pt -= other;

    return pt;
}

Vector3D& Vector3D::operator-=( const Vector3D& other )
{
    m_x -= other.m_x;
    m_y -= other.m_y;
    m_z -= other.m_z;

    return *this;
}

Vector3D Vector3D::operator*( const double& factor ) const
{
    Vector3D pt = *this;
    pt *= factor;

    return pt;
}

Vector3D& Vector3D::operator*=( const double& factor )
{
    m_x *= factor;
    m_y *= factor;
    m_z *= factor;

    return *this;
}

Vector3D Vector3D::operator/( const double& factor ) const
{
    Vector3D pt = *this;
    pt /= factor;

    return pt;
}

Vector3D& Vector3D::operator/=( const double& factor )
{
    if ( MathUtilities::isFuzzyZero( factor ) )
    {
        const double inf = std::numeric_limits< double >::infinity();
        m_x = MathUtilities::sign( m_x ) * inf;
        m_y = MathUtilities::sign( m_y ) * inf;
        m_z = MathUtilities::sign( m_z ) * inf;
    }
    else
    {
        m_x /= factor;
        m_y /= factor;
        m_z /= factor;
    }

    return *this;
}

void Vector3D::normalize()
{
    *this /= this->length();
}

Vector3D Vector3D::normalized() const
{
    Vector3D pt = *this;
    pt.normalize();

    return pt;
}

Vector3D Vector3D::normal( const Vector3D& other ) const
{
    return this->cross( other ).normalized();
}

double Vector3D::angle() const
{
    return MathUtilities::toPositive360DegreesAngleFromRadians( atan2( m_y, m_x ) );
}

double Vector3D::angleTo( const Vector3D& other ) const
{
    // angles diff: slower due to 2 atan2 operations, compared to sqrts + acos for the dot product one:
    // http://stackoverflow.com/questions/9316995/which-is-more-efficient-atan2-or-sqrt
    // return other.angle() - this->angle();

    // dot product implementation
    return MathUtilities::degrees( acos( this->normalized().dot( other.normalized() ) ) );
}

Vector3D operator*( const double& factor, const Vector3D& pt )
{
    return pt * factor;
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Vector3D& pt )
{
    return d.nospace() << "Vector3D( " << pt.x() << ", " << pt.y() << ", " << pt.z() << " )";
}
#endif
