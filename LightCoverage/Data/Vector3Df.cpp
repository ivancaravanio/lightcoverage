#include "Vector3Df.h"

#include "../Utilities/MathUtilities.h"

#include <qmath.h>

#include <math.h>

using namespace LightCoverage::Data;

using namespace LightCoverage::Utilities;

Vector3Df::Vector3Df( const float aX, const float aY, const float aZ )
    : m_x( aX )
    , m_y( aY )
    , m_z( aZ )
{
}

Vector3Df::Vector3Df( const Vector2D& vector2D, const float aZ )
    : m_x( vector2D.x() )
    , m_y( vector2D.y() )
    , m_z( aZ )
{
}

Vector3Df::Vector3Df()
    : m_x( 0.0f )
    , m_y( 0.0f )
    , m_z( 0.0f )
{
}

void Vector3Df::setX( const float aX )
{
    m_x = aX;
}

void Vector3Df::setY( const float aY )
{
    m_y = aY;
}

void Vector3Df::setZ( const float aZ )
{
    m_z = aZ;
}

void Vector3Df::set( const float aX, const float aY, const float aZ )
{
    this->setX( aX );
    this->setY( aY );
    this->setZ( aZ );
}

float Vector3Df::length() const
{
    return sqrt( this->dot( *this ) );
}

bool Vector3Df::isUnit() const
{
    return MathUtilities::isFuzzyEqual( this->length(), 1.0f );
}

float Vector3Df::distanceTo( const Vector3Df& other ) const
{
    return Vector3Df( other - *this ).length();
}

float Vector3Df::dot( const Vector3Df& other ) const
{
    return m_x * other.m_x + m_y * other.m_y + m_z * other.m_z;
}

Vector3Df Vector3Df::cross( const Vector3Df& other ) const
{
    return Vector3Df( m_y * other.m_z - m_z * other.m_y,
                     m_z * other.m_x - m_x * other.m_z,
                     m_x * other.m_y - m_y * other.m_x );
}

bool Vector3Df::operator==( const Vector3Df& other ) const
{
    return MathUtilities::isFuzzyEqual( m_x, other.m_x )
           && MathUtilities::isFuzzyEqual( m_y, other.m_y )
           && MathUtilities::isFuzzyEqual( m_z, other.m_z );
}

bool Vector3Df::operator!=( const Vector3Df& other ) const
{
    return ! ( *this == other );
}

Vector3Df Vector3Df::operator+( const Vector3Df& other ) const
{
    Vector3Df pt = *this;
    pt += other;

    return pt;
}

Vector3Df& Vector3Df::operator+=( const Vector3Df& other )
{
    m_x += other.m_x;
    m_y += other.m_y;
    m_z += other.m_z;

    return *this;
}

Vector3Df Vector3Df::operator-( const Vector3Df& other ) const
{
    Vector3Df pt = *this;
    pt -= other;

    return pt;
}

Vector3Df& Vector3Df::operator-=( const Vector3Df& other )
{
    m_x -= other.m_x;
    m_y -= other.m_y;
    m_z -= other.m_z;

    return *this;
}

Vector3Df Vector3Df::operator*( const float factor ) const
{
    Vector3Df pt = *this;
    pt *= factor;

    return pt;
}

Vector3Df& Vector3Df::operator*=( const float factor )
{
    m_x *= factor;
    m_y *= factor;
    m_z *= factor;

    return *this;
}

Vector3Df Vector3Df::operator/( const float factor ) const
{
    Vector3Df pt = *this;
    pt /= factor;

    return pt;
}

Vector3Df& Vector3Df::operator/=( const float factor )
{
    if ( qFuzzyIsNull( factor ) )
    {
        const float inf = std::numeric_limits< float >::infinity();
        m_x = MathUtilities::sign( m_x ) * inf;
        m_y = MathUtilities::sign( m_y ) * inf;
        m_z = MathUtilities::sign( m_z ) * inf;
    }
    else
    {
        m_x /= factor;
        m_y /= factor;
        m_z /= factor;
    }

    return *this;
}

void Vector3Df::normalize()
{
    *this /= this->length();
}

Vector3Df Vector3Df::normalized() const
{
    Vector3Df pt = *this;
    pt.normalize();

    return pt;
}

Vector3Df Vector3Df::normal( const Vector3Df& other ) const
{
    return this->cross( other ).normalized();
}

float Vector3Df::angle() const
{
    return MathUtilities::toPositive360DegreesAngleFromRadians( atan2f( m_y, m_x ) );
}

float Vector3Df::angleTo( const Vector3Df& other ) const
{
    // angles diff: slower due to 2 atan2 operations, compared to sqrts + acos for the dot product one:
    // http://stackoverflow.com/questions/9316995/which-is-more-efficient-atan2-or-sqrt
    // return other.angle() - this->angle();

    // dot product implementation
    return MathUtilities::degrees( acos( this->normalized().dot( other.normalized() ) ) );
}

Vector3Df operator*( const float factor, const Vector3Df& pt )
{
    return pt * factor;
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Vector3Df& pt )
{
    return d.nospace() << "Vector3Df( " << pt.x() << ", " << pt.y() << ", " << pt.z() << " )";
}
#endif
