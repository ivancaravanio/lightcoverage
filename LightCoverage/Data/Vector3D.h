#ifndef LIGHTCOVERAGE_DATA_VECTOR3D_H
#define LIGHTCOVERAGE_DATA_VECTOR3D_H

#include "Vector2D.h"

#include <QDebug>

namespace LightCoverage {
namespace Data {

class Vector3D
{
public:
    explicit Vector3D( const double& aX, const double& aY, const double& aZ );
    explicit Vector3D( const Vector2D& vector2D, const double& aZ = 0.0 );
    Vector3D();

    void setX( const double& aX );
    inline double x() const
    {
        return m_x;
    }

    void setY( const double& aY );
    inline double y() const
    {
        return m_y;
    }

    void setZ( const double& aZ );
    inline double z() const
    {
        return m_z;
    }

    void set( const double& aX, const double& aY, const double& aZ );

    double length() const;
    bool isUnit() const;
    double distanceTo( const Vector3D& other ) const;
    double dot( const Vector3D& other ) const;
    Vector3D cross( const Vector3D& other ) const;

    bool operator==( const Vector3D& other ) const;
    bool operator!=( const Vector3D& other ) const;

    Vector3D operator+( const Vector3D& other ) const;
    Vector3D& operator+=( const Vector3D& other );
    Vector3D operator-( const Vector3D& other ) const;
    Vector3D& operator-=( const Vector3D& other );
    Vector3D operator*( const double& factor ) const;
    Vector3D& operator*=( const double& factor );
    Vector3D operator/( const double& factor ) const;
    Vector3D& operator/=( const double& factor );

    void normalize();
    Vector3D normalized() const;
    Vector3D normal( const Vector3D& other ) const;
    double angle() const;
    double angleTo( const Vector3D& other ) const;

private:
    double m_x;
    double m_y;
    double m_z;
};

}
}

LightCoverage::Data::Vector3D operator*( const double& factor, const LightCoverage::Data::Vector3D& pt );

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const LightCoverage::Data::Vector3D& pt );
#endif

#endif // LIGHTCOVERAGE_DATA_VECTOR3D_H
