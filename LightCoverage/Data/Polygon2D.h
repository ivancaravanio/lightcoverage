#ifndef LIGHTCOVERAGE_DATA_POLYGON2D_H
#define LIGHTCOVERAGE_DATA_POLYGON2D_H

#include "../Utilities/OpenGlUtilities.h"

#include "LineSegment2D.h"
#include "Vector2D.h"
#include "Rectangle2D.h"

#include <QList>
#include <QPair>
#include <QPolygon>
#include <QPolygonF>
#include <QScopedPointer>
#include <QVector>

namespace LightCoverage {
namespace Data {

class Triangle2D;

using namespace LightCoverage::Utilities;

class Polygon2D
{
public:
    enum InteractionType
    {
        InteractionTypeNone,
        InteractionTypeIntersection,
        InteractionTypeContainment
    };

public:
    explicit Polygon2D( const QVector< Vector2D >& aVertices );
    explicit Polygon2D( const QPolygon& polygon );
    explicit Polygon2D( const QPolygonF& polygon );
    Polygon2D();
    virtual ~Polygon2D();

    virtual bool isValid() const;

    // it is advised to clean the poly prior to checking whether the polygon is convex
    bool isConvex() const;
    bool isConcave() const;
    bool isSelfIntersecting() const;
    virtual double area() const;
    void reverse();
    Polygon2D reversed() const;

    QList< Triangle2D* > toTriangles() const;
    QPair< OpenGlUtilities::GeometricPrimitiveType, QVector< Vector2D > > toTriangleFanStrip() const;

    int verticesCount() const;
    int verticesCount( const Vector2D& vertex ) const;
    bool hasVertices() const;
    bool hasVertex( const Vector2D& vertex ) const;
    bool contains( const Vector2D& pt ) const;
    InteractionType interactionType( const Polygon2D& other ) const;
    bool contains( const Polygon2D& other ) const;
    bool intersects( const Polygon2D& other ) const;

    void setVertices( const QVector< Vector2D >& aVertices );
    void setVertexAt( const int index, const Vector2D& v );
    const QVector< Vector2D >& vertices() const;
    // clean removes vertices lying on a line formed by their two neighbouring points
    void cleanVertices();
    void clearVertices();

    Vector2D& vertexAt( const int index );
    const Vector2D& vertexAt( const int index ) const;
    Vector2D& operator[]( const int index );
    const Vector2D& operator[]( const int index ) const;
    void append( const Vector2D& vertex );
    void append( const QVector< Vector2D >& aVertices );
    void prepend( const Vector2D& vertex );
    void prepend( const QVector< Vector2D >& aVertices );
    void insert( const int index, const Vector2D& vertex );
    void insert( const int index, const int numberVertexCopies, const Vector2D& vertex );
    void replace( const int index, const Vector2D& vertex );
    void remove( const Vector2D& vertex );
    void remove( const QVector< Vector2D >& vertices );
    void remove( const int index );
    void remove( const int index, const int count );

    Polygon2D& operator<<( const Vector2D& aVertex );
    Polygon2D& operator<<( const QVector< Vector2D >& aVertices );

    Polygon2D& operator+=( const Vector2D& aVertex );
    Polygon2D& operator+=( const QVector< Vector2D >& aVertices );
    Polygon2D operator+( const Vector2D& aVertex ) const;
    Polygon2D operator+( const QVector< Vector2D >& aVertices ) const;
    Polygon2D& operator-=( const Vector2D& aVertex );
    Polygon2D& operator-=( const QVector< Vector2D >& aVertices );
    Polygon2D operator-( const Vector2D& aVertex ) const;
    Polygon2D operator-( const QVector< Vector2D >& aVertices ) const;

    Polygon2D& operator=( const QPolygon& polygon );
    Polygon2D& operator=( const QPolygonF& polygon );

    void fillVertices( const Vector2D& vertex, const int size = -1 );

    int vertexIndex( const Vector2D& vertex, const int startingIndex = 0 ) const;
    int lastVertexIndex( const Vector2D& vertex, const int startingIndex = -1 ) const;

    Vector2D& firstVertex();
    const Vector2D& firstVertex() const;
    Vector2D& lastVertex();
    const Vector2D& lastVertex() const;

    int cyclicVertexIndex( const int index ) const;
    Vector2D& cyclicVertexAt( const int index );
    const Vector2D& cyclicVertexAt( const int index ) const;

    double angleAtVertex( const int index ) const;
    double angleAtVertex( const Vector2D& vertex ) const;

    LineSegment2D edgeStartingFromVertex( const int index ) const;
    bool isValidVertexIndex( const int index ) const;

    Rectangle2D boundingRect() const;

    bool operator==( const Polygon2D& other ) const;
    bool operator!=( const Polygon2D& other ) const;

    // provide a functor or a function taking Triangle* and returning bool which
    // denotes whether the provided triangle should be automatically disposed
    template < typename T >
    void foreachTriangle( T& f ) const;

    QPolygon toQPolygon() const;
    operator QPolygon() const;

    QPolygonF toQPolygonF() const;
    operator QPolygonF() const;

private:
    class TrianglesCollector
    {
    public:
        inline const QList< Triangle2D* >& triangles() const
        {
            return m_triangles;
        }

        bool operator()( Triangle2D* const t );

    private:
        QList< Triangle2D* > m_triangles;
    };

    class AreaCalculator
    {
    public:
        AreaCalculator();

        inline const double& area() const
        {
            return m_area;
        }

        bool operator()( Triangle2D* const t );

    private:
        double m_area;
    };

private:
    QVector< Vector2D > m_vertices;
};

template < typename T >
void Polygon2D::foreachTriangle( T& f ) const
{
    if ( ! this->isValid()
         || this->isSelfIntersecting() )
    {
        return;
    }

    if ( this->isConvex() )
    {
        const Vector2D& firstVertex = this->firstVertex();
        const int verticesCount = this->verticesCount();
        const int trianglesCount = verticesCount - 2;

        const QScopedPointer< Triangle2D > firstTriangle( new Triangle2D( firstVertex, this->vertexAt( 1 ), this->vertexAt( 2 ) ) );
        const bool isWindingOrderClockwise = firstTriangle->isWindingOrderClockwise();
        for ( int i = 1; i <= trianglesCount; ++ i )
        {
            int v1Index = i;
            int v2Index = i + 1;
            if ( isWindingOrderClockwise )
            {
                v1Index = verticesCount - v1Index;
                v2Index = verticesCount - v2Index;
            }

            Triangle2D* const t = new Triangle2D( firstVertex,
                                                  this->vertexAt( v1Index ),
                                                  this->vertexAt( v2Index ) );
            if ( f( t ) )
            {
                delete t;
            }
        }

        return;
    }

    // TODO: implement triangulation for concave polygon
}

}
}

/* not valid operations
 *
 * LightCoverage::Data::Polygon2D operator<<( const LightCoverage::Data::Point2D& aVertex, const LightCoverage::Data::Polygon2D& polygon ) const;
 * LightCoverage::Data::Polygon2D operator<<( const QVector< LightCoverage::Data::Point2D >& aVertices, const LightCoverage::Data::Polygon2D& polygon ) const;
 *
 * LightCoverage::Data::Polygon2D operator+( const LightCoverage::Data::Point2D& aVertex, const LightCoverage::Data::Polygon2D& polygon ) const;
 * LightCoverage::Data::Polygon2D operator+( const QVector< LightCoverage::Data::Point2D >& aVertices, const LightCoverage::Data::Polygon2D& polygon ) const;
 *
 * LightCoverage::Data::Polygon2D operator-( const LightCoverage::Data::Point2D& aVertex, const LightCoverage::Data::Polygon2D& polygon ) const;
 * LightCoverage::Data::Polygon2D operator-( const QVector< LightCoverage::Data::Point2D >& aVertices, const LightCoverage::Data::Polygon2D& polygon ) const;
 */

#endif // LIGHTCOVERAGE_DATA_POLYGON2D_H
