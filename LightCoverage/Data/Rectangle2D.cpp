#include "Rectangle2D.h"

#include "../Utilities/MathUtilities.h"
#include "Polygon2D.h"
#include "Triangle2D.h"

#include <qmath.h>

//#include <math.h>

using namespace LightCoverage::Data;

using namespace LightCoverage::Utilities;

Rectangle2D::Rectangle2D( const Vector2D& aBottomLeft, const Size& aSize )
{
    this->set( aBottomLeft, aSize );
}

Rectangle2D::Rectangle2D( const Vector2D& aBottomLeft, const Vector2D& aTopRight )
{
    this->set( aBottomLeft, aTopRight );
}

Rectangle2D::Rectangle2D(
        const double& aLeft, const double& aBottom,
        const double& aRight, const double& aTop )
{
    this->set( Vector2D( aLeft, aBottom ), Vector2D( aRight, aTop ) );
}

Rectangle2D::Rectangle2D( const QRectF& other )
{
    this->setBottomLeft( Vector2D( other.topLeft() ) );
    this->setSize( Size( other.size() ) );
}

Rectangle2D::Rectangle2D( const QRect& other )
{
    this->setBottomLeft( Vector2D( other.topLeft() ) );
    this->setSize( Size( other.size() ) );
}

Rectangle2D::Rectangle2D( const Size& aSize )
{
    this->setSize( aSize );
}

Rectangle2D::Rectangle2D()
{
}

void Rectangle2D::setBottomLeft( const Vector2D& aBottomLeft )
{
    m_bottomLeft = aBottomLeft;
}

void Rectangle2D::setX( const double& aX )
{
    m_bottomLeft.setX( aX );
}

void Rectangle2D::setY( const double& aY )
{
    m_bottomLeft.setY( aY );
}

void Rectangle2D::setSize( const Size& aSize )
{
    if ( aSize.isValid() )
    {
        m_size = aSize;
    }
}

void Rectangle2D::setWidth( const double& aWidth )
{
    m_size.setWidth( aWidth );
}

void Rectangle2D::setHeight( const double& aHeight )
{
    m_size.setHeight( aHeight );
}

void Rectangle2D::set( const Vector2D& aBottomLeft, const Size& aSize )
{
    this->setBottomLeft( aBottomLeft );
    this->setSize( aSize );
}

void Rectangle2D::set( const Vector2D& aBottomLeft, const Vector2D& aTopRight )
{
    this->setBottomLeft( aBottomLeft );
    this->setSize( Size( aTopRight - aBottomLeft ) );
}

Vector2D Rectangle2D::bottomRight() const
{
    return m_bottomLeft + Vector2D( m_size.width(), 0.0 );
}

Vector2D Rectangle2D::topLeft() const
{
    return m_bottomLeft + Vector2D( 0.0, m_size.height() );
}

Vector2D Rectangle2D::topRight() const
{
    return m_bottomLeft + Vector2D( m_size.width(), m_size.height() );
}

Vector2D Rectangle2D::center() const
{
    return m_bottomLeft + m_size.toVector2D() / 2.0;
}

QVector< Vector2D > Rectangle2D::vertices() const
{
    QVector< Vector2D > vertices;
    vertices.reserve( 4 );
    vertices << this->bottomLeft()
             << this->bottomRight()
             << this->topRight()
             << this->topLeft();

    return vertices;
}

void Rectangle2D::translate( const Vector2D& distance )
{
    m_bottomLeft += distance;
}

void Rectangle2D::moveCenter( const Vector2D& newPos )
{
    this->translate( newPos - this->center() );
}

void Rectangle2D::moveBottomLeft( const Vector2D& newPos )
{
    this->translate( newPos - this->bottomLeft() );
}

void Rectangle2D::moveBottomRight( const Vector2D& newPos )
{
    this->translate( newPos - this->bottomRight() );
}

void Rectangle2D::moveTopRight( const Vector2D& newPos )
{
    this->translate( newPos - this->topRight() );
}

void Rectangle2D::moveTopLeft( const Vector2D& newPos )
{
    this->translate( newPos - this->topLeft() );
}

Rectangle2D Rectangle2D::translated( const Vector2D& distance ) const
{
    Rectangle2D result = *this;
    result.translate( distance );

    return result;
}

Rectangle2D Rectangle2D::movedCenter( const Vector2D& newPos ) const
{
    Rectangle2D result = *this;
    result.moveCenter( newPos );

    return result;
}

Rectangle2D Rectangle2D::movedBottomLeft( const Vector2D& newPos ) const
{
    Rectangle2D result = *this;
    result.moveBottomLeft( newPos );

    return result;
}

Rectangle2D Rectangle2D::movedBottomRight( const Vector2D& newPos ) const
{
    Rectangle2D result = *this;
    result.moveBottomRight( newPos );

    return result;
}

Rectangle2D Rectangle2D::movedTopRight( const Vector2D& newPos ) const
{
    Rectangle2D result = *this;
    result.moveTopRight( newPos );

    return result;
}

Rectangle2D Rectangle2D::movedTopLeft( const Vector2D& newPos ) const
{
    Rectangle2D result = *this;
    result.moveTopLeft( newPos );

    return result;
}

void Rectangle2D::expand(
        const double& leftMargin,
        const double& bottomMargin,
        const double& rightMargin,
        const double& topMargin )
{
    if ( ! this->isValid() )
    {
        return;
    }

    const double newLeft   = this->left() - leftMargin;
    const double newBottom = this->bottom() - bottomMargin;
    const double newRight  = this->right() + rightMargin;
    const double newTop    = this->top() + topMargin;
    const Size newSize( newRight - newLeft, newTop - newBottom );
    if ( ! newSize.isValid() )
    {
        return;
    }

    this->setBottomLeft( Vector2D( newLeft, newBottom ) );
    this->setSize( newSize );
}

void Rectangle2D::expand( const double& margin )
{
    this->expand( margin, margin, margin, margin );
}

Rectangle2D Rectangle2D::expanded(
        const double& leftMargin,
        const double& bottomMargin,
        const double& rightMargin,
        const double& topMargin ) const
{
    Rectangle2D result = *this;
    result.expand( leftMargin, bottomMargin, rightMargin, topMargin );

    return result;
}

Rectangle2D Rectangle2D::expanded( const double& margin ) const
{
    Rectangle2D result = *this;
    result.expand( margin );

    return result;
}

double Rectangle2D::top() const
{
    return this->bottom() + m_size.height();
}

double Rectangle2D::left() const
{
    return m_bottomLeft.x();
}

double Rectangle2D::bottom() const
{
    return m_bottomLeft.y();
}

double Rectangle2D::right() const
{
    return this->left() + m_size.width();
}

LineSegment2D Rectangle2D::topEdge() const
{
    return LineSegment2D( this->topRight(), this->topLeft() );
}

LineSegment2D Rectangle2D::leftEdge() const
{
    return LineSegment2D( this->topLeft(), this->bottomLeft() );
}

LineSegment2D Rectangle2D::bottomEdge() const
{
    return LineSegment2D( this->bottomLeft(), this->bottomRight() );
}

LineSegment2D Rectangle2D::rightEdge() const
{
    return LineSegment2D( this->bottomRight(), this->topRight() );
}

QList< LineSegment2D > Rectangle2D::edges() const
{
    QList< LineSegment2D > edges;
    edges.reserve( 4 );
    edges << this->bottomEdge()
          << this->rightEdge()
          << this->topEdge()
          << this->leftEdge();

    return edges;
}

bool Rectangle2D::isEmpty() const
{
    return m_size.isEmpty();
}

bool Rectangle2D::isValid() const
{
    return m_size.isValid();
}

double Rectangle2D::area() const
{
    return m_size.area();
}

bool Rectangle2D::contains( const Vector2D& pt, const bool accountBorders ) const
{
    return MathUtilities::isFuzzyContainedInRegion( this->left(), pt.x(), this->right(), accountBorders )
           && MathUtilities::isFuzzyContainedInRegion( this->bottom(), pt.y(), this->top(), accountBorders );
}

bool Rectangle2D::contains( const Rectangle2D& rect, const bool accountBorders ) const
{
    return this->contains( rect.bottomLeft(), accountBorders )
           && this->contains( rect.topRight(), accountBorders );
}

bool Rectangle2D::intersects(
        const Rectangle2D& other,
        const bool accountBorders,
        Rectangle2D* const intersectionRect ) const
{
    const Rectangle2D commonRect( qMax( this->left(), other.left() ),
                                  qMax( this->bottom(), other.bottom() ),
                                  qMin( this->right(), other.right() ),
                                  qMin( this->top(), other.top() ) );

    if ( ! commonRect.isValid() )
    {
        return false;
    }

    if ( accountBorders
         && ( MathUtilities::isFuzzyZero( commonRect.size().width() )
              || MathUtilities::isFuzzyZero( commonRect.size().height() ) ) )
    {
        return false;
    }

    if ( intersectionRect != nullptr )
    {
        *intersectionRect = commonRect;
    }

    return true;
}

Vector2D Rectangle2D::boundedPt(
        const Vector2D& pt,
        const bool allowBorders ) const
{
    const Rectangle2D r = allowBorders
                          ? *this
                          : this->expanded( - MathUtilities::epsilon< double >() );

    return Vector2D( qBound( r.left(),   pt.x(), r.right() ),
                     qBound( r.bottom(), pt.y(), r.top() ) );
}

bool Rectangle2D::isContainedInInscribedCenteredCircle( const Vector2D& pt, const bool accountBorders ) const
{
    if ( ! this->isValid() )
    {
        return false;
    }

    const double circleRadius = m_size.minLength() / 2.0;
    const Vector2D centerPt = this->center();

    return MathUtilities::isFuzzyGreaterOrLessThan( ( pt - centerPt ).length(), circleRadius, false, accountBorders );
}

bool Rectangle2D::operator==( const Rectangle2D& other ) const
{
    return    m_bottomLeft == other.m_bottomLeft
           && m_size       == other.m_size;
}

bool Rectangle2D::operator!=( const Rectangle2D& other ) const
{
    return ! ( *this == other );
}

Polygon2D* Rectangle2D::toPolygon2D() const
{
    return new Polygon2D( this->vertices() );
}

QList< Triangle2D* > Rectangle2D::toTriangles() const
{
    const Vector2D bottomLeft  = this->bottomLeft();
    const Vector2D bottomRight = this->bottomRight();
    const Vector2D topRight    = this->topRight();
    const Vector2D topLeft     = this->topLeft();

    if ( ! this->isValid() )
    {
        return QList< Triangle2D* >();
    }

    QList< Triangle2D* > triangles;
    triangles.reserve( 2 );
    triangles << new Triangle2D( bottomLeft, bottomRight, topRight )
              << new Triangle2D( bottomLeft, topRight, topLeft );

    return triangles;
}

QRectF Rectangle2D::toQRectF() const
{
    return QRectF( m_bottomLeft, this->size() );
}

Rectangle2D::operator QRectF() const
{
    return this->toQRectF();
}

QRect Rectangle2D::toQRect() const
{
    return this->toQRectF().toRect();
}

Rectangle2D::operator QRect() const
{
    return this->toQRect();
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Rectangle2D& rect )
{
    return d.nospace() << "Rectangle( x: " << rect.x() << ", y: " << rect.y() << ", w: " << rect.width() << ", h: " << rect.height() << " )";
}
#endif
