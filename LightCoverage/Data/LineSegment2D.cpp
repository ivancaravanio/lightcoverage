#include "LineSegment2D.h"

#include "../Utilities/MathUtilities.h"

#include <qmath.h>

#include <limits>
#include <math.h>

using namespace LightCoverage::Data;

using namespace LightCoverage::Utilities;

LineSegment2D::LineSegment2D( const Vector2D& aStartPt, const Vector2D& aEndPt )
    : m_startPt( aStartPt )
    , m_endPt( aEndPt )
{
}

LineSegment2D::LineSegment2D( const QLineF& other )
    : m_startPt( other.p1() )
    , m_endPt( other.p2() )
{
}

LineSegment2D::LineSegment2D( const QLine& other )
    : m_startPt( other.p1() )
    , m_endPt( other.p2() )
{
}

LineSegment2D::LineSegment2D()
{
}

void LineSegment2D::setStartPt( const Vector2D& aStartPt )
{
    m_startPt = aStartPt;
}

void LineSegment2D::setEndPt( const Vector2D& aEndPt )
{
    m_endPt = aEndPt;
}

void LineSegment2D::set( const Vector2D& aStartPt, const Vector2D& aEndPt )
{
    this->setStartPt( aStartPt );
    this->setEndPt( aEndPt );
}

bool LineSegment2D::isValid() const
{
    return ! this->isPoint();
}

bool LineSegment2D::isPoint() const
{
    return m_startPt == m_endPt;
}

double LineSegment2D::length() const
{
    return m_startPt.distanceTo( m_endPt );
}

bool LineSegment2D::operator==( const LineSegment2D& other ) const
{
    return m_startPt == other.startPt()
           && m_endPt == other.endPt();
}

bool LineSegment2D::operator!=( const LineSegment2D& other ) const
{
    return ! ( *this == other );
}

double LineSegment2D::angle() const
{
    return this->vector().angle();
}

bool LineSegment2D::lineIntersects(
        const LineSegment2D& other,
        const bool accountEnds,
        Vector2D* const intersectionPt ) const
{
    if ( this->isParallelTo( other ) )
    {
        return false;
    }

    const double x0 = m_startPt.x();
    const double y0 = m_startPt.y();
    const double x1 = m_endPt.x();
    const double y1 = m_endPt.y();

    const double x2 = other.startPt().x();
    const double y2 = other.startPt().y();
    const double x3 = other.endPt().x();
    const double y3 = other.endPt().y();

    const double denominator = ( x0 - x1 ) * ( y2 - y3 ) - ( y0 - y1 ) * ( x2 - x3 );
    const double a = x0 * y1 - y0 * x1;
    const double b = x2 * y3 - y2 * x3;

    const Vector2D pt( ( a * ( x2 - x3 ) - ( x0 - x1 ) * b ) / denominator,
                      ( a * ( y2 - y3 ) - ( y0 - y1 ) * b ) / denominator );
    if ( ! other.contains( pt, accountEnds ) )
    {
        return false;
    }

    if ( intersectionPt != nullptr )
    {
        *intersectionPt = pt;
    }

    return true;
}

bool LineSegment2D::rayIntersects(
        const LineSegment2D& other,
        const bool accountEnds,
        Vector2D* const intersectionPt ) const
{
    Vector2D pt;
    if ( ! this->lineIntersects( other, accountEnds, & pt ) )
    {
        return false;
    }

    if ( pt == m_startPt )
    {
        if ( ! accountEnds )
        {
            return false;
        }
    }
    else if ( this->vector().dot( LineSegment2D( m_startPt, pt ).vector() ) < 0 )
    {
        return false;
    }

    if ( intersectionPt != nullptr )
    {
        *intersectionPt = pt;
    }

    return true;
}

bool LineSegment2D::intersects(
        const LineSegment2D& other,
        const bool accountEnds,
        Vector2D* const intersectionPt ) const
{
    Vector2D pt;
    if ( ! this->lineIntersects( other, accountEnds, & pt ) )
    {
        return false;
    }

    if ( ! this->contains( pt, accountEnds ) )
    {
        return false;
    }

    if ( intersectionPt != nullptr )
    {
        *intersectionPt = pt;
    }

    return true;
}

bool LineSegment2D::isParallelTo( const LineSegment2D& other ) const
{
    return MathUtilities::isFuzzyEqual( ( m_startPt.x() - m_endPt.x() ) * ( other.startPt().y() - other.endPt().y() ),
                                        ( ( m_startPt.y() - m_endPt.y() ) * ( other.startPt().x() - other.endPt().x() ) ) );
}

bool LineSegment2D::contains( const Vector2D& pt, const bool accountEnds ) const
{
    const double x0 = m_startPt.x();
    const double y0 = m_startPt.y();
    const double x1 = m_endPt.x();
    const double y1 = m_endPt.y();
    const double x2 = pt.x();
    const double y2 = pt.y();

    if ( ! MathUtilities::isFuzzyEqual(
             ( y1 - y0 ) * ( x2 - x0 ),
             ( y2 - y0 ) * ( x1 - x0 ) ) )
    {
        return false;
    }

    return this->boundingBoxContains( pt, accountEnds );
}

bool LineSegment2D::boundingBoxContainsX( const double& xValue, const bool accountEnds ) const
{
    return MathUtilities::isFuzzyContainedInRegion( this->minX(), xValue, this->maxX(), accountEnds );
}

bool LineSegment2D::boundingBoxContainsY( const double& yValue, const bool accountEnds ) const
{
    return MathUtilities::isFuzzyContainedInRegion( this->minY(), yValue, this->maxY(), accountEnds );
}

bool LineSegment2D::boundingBoxContains( const Vector2D& pt, const bool accountEnds ) const
{
    return this->boundingBoxContainsX( pt.x(), accountEnds )
           && this->boundingBoxContainsY( pt.y(), accountEnds );
}

bool LineSegment2D::contains( const LineSegment2D& other, const bool accountEnds ) const
{
    return this->contains( other.startPt(), accountEnds )
           && this->contains( other.endPt(), accountEnds );
}

double LineSegment2D::minX() const
{
    return qMin( m_startPt.x(), m_endPt.x() );
}

double LineSegment2D::maxX() const
{
    return qMax( m_startPt.x(), m_endPt.x() );
}

double LineSegment2D::minY() const
{
    return qMin( m_startPt.y(), m_endPt.y() );
}

double LineSegment2D::maxY() const
{
    return qMax( m_startPt.y(), m_endPt.y() );
}

double LineSegment2D::slope() const
{
    if ( this->isPoint() )
    {
        return 0.0f;
    }

    const double yDiff = m_endPt.y() - m_startPt.y();
    if ( this->isVertical() )
    {
        return MathUtilities::sign( yDiff ) * std::numeric_limits< double >::infinity();
    }

    return yDiff / ( m_endPt.x() - m_startPt.x() );
}

double LineSegment2D::xIntercept() const
{
    return this->isHorizontal()
           ? std::numeric_limits< double >::quiet_NaN()
           : ( - m_startPt.y() / this->slope() + m_startPt.x() );
}

double LineSegment2D::yIntercept() const
{
    return this->isVertical()
           ? std::numeric_limits< double >::quiet_NaN()
           : ( - m_startPt.x() * this->slope() + m_startPt.y() );
}

Vector2D LineSegment2D::vector() const
{
    return m_endPt - m_startPt;
}

Vector2D LineSegment2D::unitVector() const
{
    return this->vector().normalized();
}

bool LineSegment2D::isHorizontal() const
{
    return MathUtilities::isFuzzyEqual( m_endPt.y(), m_startPt.y() );
}

bool LineSegment2D::isVertical() const
{
    return MathUtilities::isFuzzyEqual( m_endPt.x(), m_startPt.x() );
}

QLineF LineSegment2D::toQLineF() const
{
    return QLineF( m_startPt.toQPointF(), m_endPt.toQPointF() );
}

LineSegment2D::operator QLineF() const
{
    return this->toQLineF();
}

QLine LineSegment2D::toQLine() const
{
    return QLine( m_startPt.toQPoint(), m_endPt.toQPoint() );
}

LineSegment2D::operator QLine() const
{
    return this->toQLine();
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const LineSegment2D& lineSegment )
{
    return d.nospace() << "LineSegment2D( startPt: " << lineSegment.startPt() << ", endPt: " << lineSegment.endPt() << " )";
}
#endif
