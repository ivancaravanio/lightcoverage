#include "Polygon2D.h"

#include "../Utilities/GeneralUtilities.h"
#include "../Utilities/MathUtilities.h"
#include "Triangle2D.h"

#include <limits>
#include <utility>

using namespace LightCoverage::Data;

Polygon2D::Polygon2D()
{
}

Polygon2D::~Polygon2D()
{
}

Polygon2D::Polygon2D( const QVector< Vector2D >& aVertices )
{
    this->setVertices( aVertices );
}

Polygon2D::Polygon2D( const QPolygon& polygon )
{
    *this = polygon;
}

Polygon2D::Polygon2D( const QPolygonF& polygon )
{
    *this = polygon;
}

bool Polygon2D::isValid() const
{
    return this->verticesCount() >= Triangle2D::s_verticesCount;
}

bool Polygon2D::isConvex() const
{
    // if angles between all neighbor edges have the same sign, no matter positive or negative
    // and no matter whether the direction of the polygon, then it is convex

    if ( ! this->isValid() )
    {
        return false;
    }

    double angle = 0.0;

    const int verticesCount = this->verticesCount();
    for ( int i = 0; i < verticesCount; ++ i )
    {
        const double vertexAngle = this->angleAtVertex( i );
        // if the polygon was not cleaned up prior to calling this function
        // there could be parallel edges - do not take into account the angle between them
        if ( MathUtilities::isFuzzyZero( vertexAngle ) )
        {
            continue;
        }

        if ( MathUtilities::isFuzzyZero( angle ) )
        {
            angle = vertexAngle;
            continue;
        }

        if ( ( angle > 0.0 ) ^ ( vertexAngle > 0.0 ) )
        {
            return false;
        }
    }

    return true;
}

bool Polygon2D::isConcave() const
{
    return ! this->isConvex();
}

bool Polygon2D::isSelfIntersecting() const
{
    const int verticesCount = this->verticesCount();
    for ( int i = 0; i < verticesCount; ++ i )
    {
        for ( int j = i + 1; j < verticesCount; ++ j )
        {
            const LineSegment2D edge0 = this->edgeStartingFromVertex( i );
            const LineSegment2D edge1 = this->edgeStartingFromVertex( j );
            if ( edge0.intersects( edge1, false ) )
            {
                return true;
            }
        }
    }

    return false;
}

double Polygon2D::area() const
{
    AreaCalculator areaCalculator;
    this->foreachTriangle( areaCalculator );

    return areaCalculator.area();
}

void Polygon2D::reverse()
{
    const int verticesCount = this->verticesCount();

    // intentionally remove fractional part
    const int halfVerticesCount = verticesCount / 2;

    for ( int i = 0; i < halfVerticesCount; ++ i )
    {
        std::swap( m_vertices[ i ], m_vertices[ verticesCount - 1 - i ] );
    }
}

Polygon2D Polygon2D::reversed() const
{
    Polygon2D result = *this;
    result.reverse();

    return result;
}

QList< Triangle2D* > Polygon2D::toTriangles() const
{
    TrianglesCollector trianglesCollector;
    this->foreachTriangle( trianglesCollector );

    return trianglesCollector.triangles();
}

QPair< OpenGlUtilities::GeometricPrimitiveType, QVector< Vector2D > > Polygon2D::toTriangleFanStrip() const
{
    if ( ! this->isValid()
         || this->isSelfIntersecting() )
    {
        return qMakePair( OpenGlUtilities::GeometricPrimitiveTypeInvalid, QVector< Vector2D >() );
    }

    if ( this->isConvex() )
    {
        const Triangle2D firstTriangle( this->firstVertex(), this->vertexAt( 1 ), this->vertexAt( 2 ) );
        return qMakePair( OpenGlUtilities::GeometricPrimitiveTypeTriangleFan,
                          firstTriangle.isWindingOrderClockwise()
                          ? this->reversed().vertices()
                          : this->vertices() );
    }

    // TODO: implement for concave polygon using triangle strip
    return qMakePair( OpenGlUtilities::GeometricPrimitiveTypeTriangleStrip, QVector< Vector2D >() );
}

int Polygon2D::verticesCount() const
{
    return m_vertices.size();
}

int Polygon2D::verticesCount( const Vector2D& vertex ) const
{
    return m_vertices.count( vertex );
}

bool Polygon2D::hasVertices() const
{
    return ! m_vertices.isEmpty();
}

void Polygon2D::setVertices( const QVector< Vector2D >& aVertices )
{
    m_vertices = aVertices;
}

void Polygon2D::setVertexAt( const int index, const Vector2D& v )
{
    if ( this->isValidVertexIndex( index ) )
    {
        m_vertices[ index ] = v;
    }
}

const QVector< Vector2D >& Polygon2D::vertices() const
{
    return m_vertices;
}

void Polygon2D::cleanVertices()
{
    for ( int i = this->verticesCount() - 1; i >= 1; -- i )
    {
        const LineSegment2D lineSegment( this->vertexAt( i ), this->cyclicVertexAt( i - 2 ) );
        if ( lineSegment.contains( this->vertexAt( i - 1 ) ) )
        {
            this->remove( i - 1 );
        }
    }
}

void Polygon2D::clearVertices()
{
    m_vertices.clear();
}

Vector2D& Polygon2D::vertexAt( const int index )
{
    return m_vertices[ index ];
}

const Vector2D& Polygon2D::vertexAt( const int index ) const
{
    return m_vertices.at( index );
}

Vector2D& Polygon2D::operator[]( const int index )
{
    return m_vertices[ index ];
}

const Vector2D& Polygon2D::operator[]( const int index ) const
{
    return m_vertices[ index ];
}

void Polygon2D::append( const Vector2D& vertex )
{
    m_vertices.append( vertex );
}

void Polygon2D::append( const QVector< Vector2D >& vertices )
{
    m_vertices += vertices;
}

void Polygon2D::prepend( const Vector2D& vertex )
{
    m_vertices.prepend( vertex );
}

void Polygon2D::prepend( const QVector< Vector2D >& aVertices )
{
    const int additionalVerticesCount = aVertices.size();
    for ( int i = 0; i < additionalVerticesCount; ++ i )
    {
        m_vertices.prepend( aVertices[ i ] );
    }
}

void Polygon2D::insert( const int index, const Vector2D& vertex )
{
    m_vertices.insert( index, vertex );
}

void Polygon2D::insert( const int index, const int numberVertexCopies, const Vector2D& vertex )
{
    m_vertices.insert( index, numberVertexCopies, vertex );
}

void Polygon2D::replace( const int index, const Vector2D& vertex )
{
    m_vertices.replace( index, vertex );
}

void Polygon2D::remove( const Vector2D& vertex )
{
    this->remove( this->vertexIndex( vertex ) );
}

void Polygon2D::remove( const QVector< Vector2D >& aVertices )
{
    const int additionalVerticesCount = aVertices.size();
    for ( int i = 0; i < additionalVerticesCount; ++ i )
    {
        this->remove( aVertices[ i ] );
    }
}

void Polygon2D::remove( const int index )
{
    m_vertices.remove( index );
}

void Polygon2D::remove( const int index, const int count )
{
    m_vertices.remove( index, count );
}

Polygon2D& Polygon2D::operator<<( const Vector2D& aVertex )
{
    this->append( aVertex );

    return *this;
}

Polygon2D& Polygon2D::operator<<( const QVector< Vector2D >& aVertices )
{
    this->append( aVertices );

    return *this;
}

Polygon2D& Polygon2D::operator+=( const Vector2D& aVertex )
{
    this->append( aVertex );

    return *this;
}

Polygon2D& Polygon2D::operator+=( const QVector< Vector2D >& aVertices )
{
    this->append( aVertices );

    return *this;
}

Polygon2D Polygon2D::operator+( const Vector2D& aVertex ) const
{
    Polygon2D result = *this;
    result += aVertex;

    return result;
}

Polygon2D Polygon2D::operator+( const QVector< Vector2D >& aVertices ) const
{
    Polygon2D result = *this;
    result += aVertices;

    return result;
}

Polygon2D& Polygon2D::operator-=( const Vector2D& aVertex )
{
    this->remove( aVertex );

    return *this;
}

Polygon2D& Polygon2D::operator-=( const QVector< Vector2D >& aVertices )
{
    this->remove( aVertices );

    return *this;
}

Polygon2D Polygon2D::operator-( const Vector2D& aVertex ) const
{
    Polygon2D result = *this;
    result -= aVertex;

    return result;
}

Polygon2D Polygon2D::operator-( const QVector< Vector2D >& aVertices ) const
{
    Polygon2D result = *this;
    result -= aVertices;

    return result;
}

Polygon2D& Polygon2D::operator=( const QPolygon& polygon )
{
    this->setVertices( GeneralUtilities::convertedCollection< QVector< QPoint >, QVector< Vector2D > >( polygon ) );

    return *this;
}

Polygon2D& Polygon2D::operator=( const QPolygonF& polygon )
{
    this->setVertices( GeneralUtilities::convertedCollection< QVector< QPointF >, QVector< Vector2D > >( polygon ) );

    return *this;
}

void Polygon2D::fillVertices( const Vector2D& vertex, const int size )
{
    m_vertices.fill( vertex, size );
}

int Polygon2D::vertexIndex( const Vector2D& vertex, const int startingIndex ) const
{
    return m_vertices.indexOf( vertex, startingIndex );
}

int Polygon2D::lastVertexIndex( const Vector2D& vertex, const int startingIndex ) const
{
    return m_vertices.lastIndexOf( vertex, startingIndex );
}

bool Polygon2D::hasVertex( const Vector2D& vertex ) const
{
    return m_vertices.contains( vertex );
}

bool Polygon2D::contains( const Vector2D& pt ) const
{
    const int verticesCount = this->verticesCount();
    if ( verticesCount == 0 )
    {
        return false;
    }

    if ( verticesCount == 1 )
    {
        return this->firstVertex() == pt;
    }

    const Rectangle2D boundingRect = this->boundingRect();
    if ( ! boundingRect.contains( pt ) )
    {
        return false;
    }

    int intersectionsCount = 0;
    const LineSegment2D ray( pt, Vector2D( boundingRect.right() + 1.0f, pt.y() ) );
    for ( int i = 0; i < verticesCount; ++ i )
    {
        const LineSegment2D lineSegment = this->edgeStartingFromVertex( i );
        if ( ray.intersects( lineSegment ) )
        {
            ++ intersectionsCount;
        }
    }

    return ( intersectionsCount % 2 ) == 1;
}

Polygon2D::InteractionType Polygon2D::interactionType( const Polygon2D& other ) const
{
    if ( ! this->isValid() || ! other.isValid() )
    {
        return InteractionTypeNone;
    }

    if ( ! this->boundingRect().intersects( other.boundingRect() ) )
    {
        return InteractionTypeNone;
    }

    if ( this->intersects( other ) )
    {
        return InteractionTypeIntersection;
    }

    const QVector< Vector2D >& otherVertices = other.vertices();
    const int otherVerticesCount = otherVertices.size();
    for ( int i = 0; i < otherVerticesCount; ++ i )
    {
        const Vector2D& otherVertex = otherVertices[ i ];
        if ( ! this->contains( otherVertex ) )
        {
            return Polygon2D::InteractionTypeNone;
        }
    }

    return InteractionTypeContainment;
}

bool Polygon2D::contains( const Polygon2D& other ) const
{
    return this->interactionType( other ) == InteractionTypeContainment;
}

bool Polygon2D::intersects( const Polygon2D& other ) const
{
    if ( ! this->isValid() || ! other.isValid() )
    {
        return false;
    }

    const int otherLastVertexIndex = other.verticesCount() - 1;
    const int lastVertexIndex = this->verticesCount() - 1;
    for ( int i = 0; i <= lastVertexIndex; ++ i )
    {
        for ( int j = 0; j <= otherLastVertexIndex; ++ j )
        {
            if ( this->edgeStartingFromVertex( i ).intersects( other.edgeStartingFromVertex( j ) ) )
            {
                return true;
            }
        }
    }

    return false;
}

Vector2D& Polygon2D::firstVertex()
{
    return m_vertices.first();
}

const Vector2D& Polygon2D::firstVertex() const
{
    return m_vertices.first();
}

Vector2D& Polygon2D::lastVertex()
{
    return m_vertices.last();
}

const Vector2D& Polygon2D::lastVertex() const
{
    return m_vertices.last();
}

int Polygon2D::cyclicVertexIndex( const int vertexIndex ) const
{
    return MathUtilities::cyclicNumber( 0, vertexIndex, this->verticesCount() - 1 );
}

Vector2D& Polygon2D::cyclicVertexAt( const int index )
{
    return this->vertexAt( this->cyclicVertexIndex( index ) );
}

const Vector2D& Polygon2D::cyclicVertexAt( const int index ) const
{
    return this->vertexAt( this->cyclicVertexIndex( index ) );
}

double Polygon2D::angleAtVertex( const int index ) const
{
    if ( ! this->isValidVertexIndex( index ) )
    {
        return std::numeric_limits< double >::quiet_NaN();
    }

    const Vector2D& v       = this->vertexAt( index );
    const Vector2D  vecPrev = this->cyclicVertexAt( index - 1 ) - v;
    const Vector2D  vecNext = this->cyclicVertexAt( index + 1 ) - v;

    return vecPrev.angleTo( vecNext );
}

double Polygon2D::angleAtVertex( const Vector2D& vertex ) const
{
    return this->angleAtVertex( this->vertexIndex( vertex ) );
}

LineSegment2D Polygon2D::edgeStartingFromVertex( const int index ) const
{
    return this->isValidVertexIndex( index )
           ? LineSegment2D( this->vertexAt( index ), this->cyclicVertexAt( index + 1 ) )
           : LineSegment2D();
}

bool Polygon2D::isValidVertexIndex( const int index ) const
{
    return 0 <= index && index < this->verticesCount();
}

Rectangle2D Polygon2D::boundingRect() const
{
    // not possible because of macro and function name clash:
    // const double maxFloat = std::numeric_limits< double >::max();
    const double maxFloat = FLT_MAX;
    double minX = maxFloat;
    double minY = maxFloat;
    double maxX = - maxFloat;
    double maxY = - maxFloat;

    const int verticesCount = this->verticesCount();
    for ( int i = 0; i < verticesCount; ++ i )
    {
        const Vector2D& v = this->vertexAt( i );

        const double x = v.x();
        if ( x < minX )
        {
            minX = x;
        }

        if ( x > maxX )
        {
            maxX = x;
        }

        const double y = v.y();
        if ( y < minY )
        {
            minY = y;
        }

        if ( y > maxY )
        {
            maxY = y;
        }
    }

    return Rectangle2D( minX, minY, maxX, maxY );
}

bool Polygon2D::operator==( const Polygon2D& other ) const
{
    return m_vertices == other.m_vertices;
}

bool Polygon2D::operator!=( const Polygon2D& other ) const
{
    return ! ( *this == other );
}

QPolygon Polygon2D::toQPolygon() const
{
    return QPolygon( GeneralUtilities::convertedCollection< QVector< Vector2D >, QVector< QPoint > >( m_vertices ) );
}

QPolygonF Polygon2D::toQPolygonF() const
{
    return QPolygonF( GeneralUtilities::convertedCollection< QVector< Vector2D >, QVector< QPointF > >( m_vertices ) );
}

Polygon2D::operator QPolygon() const
{
    return this->toQPolygon();
}

Polygon2D::operator QPolygonF() const
{
    return this->toQPolygonF();
}


bool Polygon2D::TrianglesCollector::operator()( Triangle2D* const t )
{
    if ( t == nullptr )
    {
        return true;
    }

    m_triangles.append( t );

    return false;
}


Polygon2D::AreaCalculator::AreaCalculator()
    : m_area( 0.0 )
{
}

bool Polygon2D::AreaCalculator::operator()( Triangle2D* const t )
{
    if ( t != nullptr )
    {
        m_area += t->area();
    }

    return true;
}
