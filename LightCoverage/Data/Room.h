#ifndef LIGHTCOVERAGE_DATA_ROOM_H
#define LIGHTCOVERAGE_DATA_ROOM_H

#include "../Utilities/ChangeNotifier.h"
#include "../Utilities/Nullable.h"
#include "Polygon2D.h"
#include "Size.h"
#include "Triangle2D.h"
#include "Vector2D.h"

#include <QList>
#include <QtGlobal>

namespace LightCoverage {
namespace Data {

using namespace LightCoverage::Utilities;

class Room : public ChangeNotifier
{
public:
    enum Trait : quint64
    {
        TraitSize                = 0x1 << 0,
        TraitLightSourcePosition = 0x1 << 1,
        TraitColumns             = 0x1 << 2,
        TraitsAll                = TraitSize
                                   | TraitLightSourcePosition
                                   | TraitColumns
    };

#ifdef DRAW_ILLUMINATED_POINTS
    struct VertexSortData
    {
    public:
        VertexSortData(
                const Vector2D& aVertex,
                const double&   aAngle,
                const int       aPolygonId );
        VertexSortData();

        inline const Vector2D& vertex() const
        {
            return m_vertex;
        }

        inline const double& angle() const
        {
            return m_angle;
        }

        inline int polygonId() const
        {
            return m_polygonId;
        }

        static bool hasLessAngleCcw( const VertexSortData& leftVertex, const VertexSortData& rightVertex );

    private:
        Vector2D m_vertex;
        double   m_angle;
        int      m_polygonId;
    };
#endif

public:
    Room( const Size& aSize,
          const Vector2D& aLightSourcePosition,
          const QList< Polygon2D >& aColumns );
    Room();

    void setSize( const Size& aSize );
    inline const Size& size() const
    {
        return m_size;
    }

    void setLightSourcePosition( const Vector2D& aLightSourcePosition );
    inline const Vector2D& lightSourcePosition() const
    {
        return m_lightSourcePosition;
    }

    double lightSourceRadius() const;
    Rectangle2D lightSourceBoundingRect() const;

    void setColumns( const QList< Polygon2D >& aColumns );
    inline const QList< Polygon2D >& columns() const
    {
        return m_columns;
    }

    bool isValid() const;

    double area() const;
    double columnsArea() const;
    QList< Triangle2D > illuminatedRegions() const;
    double illuminatedArea() const;
    double shadowedArea() const;

    void clear();

    bool operator==( const Room& other ) const;
    bool operator!=( const Room& other ) const;

    quint64 allTraits() const /* override */;

#ifdef DRAW_ILLUMINATED_POINTS
    inline const QList< VertexSortData >& farthestIlluminatedPoints() const
    {
        return m_farthestIlluminatedPoints;
    }
#endif

#ifndef DRAW_ILLUMINATED_POINTS
private:
    struct VertexSortData
    {
    public:
        VertexSortData(
                const Vector2D& aVertex,
                const double&   aAngle,
                const int       aPolygonId );
        VertexSortData();

        inline const Vector2D& vertex() const
        {
            return m_vertex;
        }

        inline const double& angle() const
        {
            return m_angle;
        }

        inline int polygonId() const
        {
            return m_polygonId;
        }

        static bool hasLessAngleCcw( const VertexSortData& leftVertex, const VertexSortData& rightVertex );

    private:
        Vector2D m_vertex;
        double   m_angle;
        int      m_polygonId;
    };
#endif

private:
    static void angularSort( QList< VertexSortData >& vertices );

    static QList< Triangle2D > triangleFan(
            const QList< VertexSortData >& radiusPts,
            const Vector2D& centerPt,
            const bool opened = false );
    QList< Triangle2D > triangleFan(
            const QList< VertexSortData >& radiusPts,
            const bool opened = false ) const;

    void clearColumnsAreaCache();
    void clearCache( const bool clearColumnsAreaCacheAsWell = false );

    bool isValidCalculated() const;
    double columnsAreaCalculated() const;
    QList< Triangle2D > illuminatedRegionsCalculated() const;
    double illuminatedAreaCalculated() const;

private:
    Size               m_size;
    Vector2D           m_lightSourcePosition;
    QList< Polygon2D > m_columns;

    mutable Nullable< bool >                m_isValid;
    mutable double                          m_columnsArea;
#ifdef DRAW_ILLUMINATED_POINTS
    mutable QList< VertexSortData > m_farthestIlluminatedPoints;
#endif
    mutable Nullable< QList< Triangle2D > > m_illuminatedRegions;
    mutable double                          m_illuminatedArea;
};

}
}

#endif // LIGHTCOVERAGE_DATA_ROOM_H
