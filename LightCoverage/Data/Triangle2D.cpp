#include "Triangle2D.h"

#include "../Utilities/MathUtilities.h"

#include <QTextStream>
#include <QtGlobal>

using namespace LightCoverage::Data;

using namespace LightCoverage::Utilities;

const int Triangle2D::s_verticesCount = 3;

Triangle2D::Triangle2D( const Vector2D& aV0,
                        const Vector2D& aV1,
                        const Vector2D& aV2 )
    : Polygon2D( QVector< Vector2D >()
                 << aV0
                 << aV1
                 << aV2 )
{
}

Triangle2D::Triangle2D()
{
}

Triangle2D::~Triangle2D()
{
}

void Triangle2D::setVertices(
        const Vector2D& aV0,
        const Vector2D& aV1,
        const Vector2D& aV2 )
{
    if ( this->hasVertices() )
    {
        this->setV0( aV0 );
        this->setV1( aV1 );
        this->setV2( aV2 );
    }
    else
    {
        this->Polygon2D::setVertices( QVector< Vector2D >()
                                      << aV0
                                      << aV1
                                      << aV2 );
    }
}

void Triangle2D::setV0( const Vector2D& aV0 )
{
    this->setVertexAt( 0, aV0 );
}

const Vector2D& Triangle2D::v0() const
{
    return this->cyclicVertexAt( 0 );
}

void Triangle2D::setV1( const Vector2D& aV1 )
{
    this->setVertexAt( 1, aV1 );
}

const Vector2D& Triangle2D::v1() const
{
    return this->cyclicVertexAt( 1 );
}

void Triangle2D::setV2( const Vector2D& aV2 )
{
    this->setVertexAt( 2, aV2 );
}

const Vector2D& Triangle2D::v2() const
{
    return this->cyclicVertexAt( 2 );
}

Vector2D Triangle2D::vec10() const
{
    return this->v1() - this->v0();
}

Vector2D Triangle2D::vec20() const
{
    return this->v2() - this->v0();
}

Vector2D Triangle2D::vec21() const
{
    return this->v2() - this->v1();
}

LineSegment2D Triangle2D::edge10() const
{
    return LineSegment2D( this->v0(), this->v1() );
}

LineSegment2D Triangle2D::edge20() const
{
    return LineSegment2D( this->v0(), this->v2() );
}

LineSegment2D Triangle2D::edge21() const
{
    return LineSegment2D( this->v1(), this->v2() );
}

bool Triangle2D::isValid() const
{
    if ( this->verticesCount() != s_verticesCount )
    {
        return false;
    }

    const Vector2D& v0 = this->v0();
    const Vector2D& v1 = this->v1();
    const Vector2D& v2 = this->v2();
    if ( v0 == v1
         || v1 == v2
         || v2 == v0 )
    {
        return false;
    }

    const double angles[] =
    {
        this->angleAtVertex( 0 ),
        this->angleAtVertex( 1 ),
        this->angleAtVertex( 2 )
    };

    for ( int i = 0; i < s_verticesCount; ++ i )
    {
        const double angle = angles[ i ];
        if ( ! MathUtilities::isFuzzyFinite( angle ) )
        {
            return false;
        }

        const double absAngle = qAbs( angle );
        if ( MathUtilities::isFuzzyEqual( absAngle, 0.0 )
             || MathUtilities::isFuzzyEqual( absAngle, MathUtilities::s_piDegrees )
             || MathUtilities::isFuzzyEqual( absAngle, MathUtilities::s_doublePiDegrees ) )
        {
            return false;
        }
    }

    return true;
}

// bool Triangle2D::contains( const Point2D& v ) const
// {
//     const Point2D v0 = this->v0();
//     const Point2D v1 = this->v1();
//     const Point2D v2 = this->v2();

//     const bool b0 = Triangle2D::sign( v, v0, v1 ) < 0.0f;
//     const bool b1 = Triangle2D::sign( v, v1, v2 ) < 0.0f;
//     if ( b0 != b1 )
//     {
//         return false;
//     }

//     const bool b2 = Triangle2D::sign( v, v2, v0 ) < 0.0f;
//     return b1 == b2;
// }

double Triangle2D::area() const
{
    const Vector2D v0 = this->v0();
    const Vector2D v1 = this->v1();
    const Vector2D v2 = this->v2();

    const Vector2D vec10 = v1 - v0;
    const Vector2D vec20 = v2 - v0;
    const double edgesLengthProduct = vec10.length() * vec20.length();
    const double sinAngle = sqrt( 1.0 - pow( vec10.dot( vec20 ) / edgesLengthProduct, 2 ) );

    return sinAngle * edgesLengthProduct / 2.0;
}

double Triangle2D::sign() const
{
    return Triangle2D::sign( this->v0(), this->v1(), this->v2() );
}

void Triangle2D::setWindingOrder( const bool aIsWindingOrderClockwise )
{
    if ( aIsWindingOrderClockwise == this->isWindingOrderClockwise() )
    {
        return;
    }

    this->toggleIsWindingOrderClockwise();
}

bool Triangle2D::isWindingOrderClockwise() const
{
    return MathUtilities::isFuzzyLessThan( this->sign(), 0.0 );
}

void Triangle2D::toggleIsWindingOrderClockwise()
{
    this->reverse();
}

Triangle2D Triangle2D::toggledWindingOrder() const
{
    Triangle2D result = *this;
    result.toggleIsWindingOrderClockwise();

    return result;
}

Triangle2D Triangle2D::havingWindingOrder( const bool aIsClockwise ) const
{
    return aIsClockwise == this->isWindingOrderClockwise()
           ? *this
           : this->toggledWindingOrder();
}

double Triangle2D::sign(
        const Vector2D& v0,
        const Vector2D& v1,
        const Vector2D& v2 )
{
    return   ( v0.x() - v2.x() ) * ( v1.y() - v2.y() )
           - ( v1.x() - v2.x() ) * ( v0.y() - v2.y() );
}

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Triangle2D& t )
{
    d << "Triangle2D(" << endl
      << "\tv0:" << t.v0() << endl
      << "\tv1:" << t.v1() << endl
      << "\tv2:" << t.v2() << ')';

    return d;
}
#endif
