#include "Size.h"

#include "../Utilities/MathUtilities.h"

#include <qmath.h>

#include <math.h>

using namespace LightCoverage::Data;

using namespace LightCoverage::Utilities;

Size::Size( const double& aWidth, const double& aHeight )
    : m_width( MathUtilities::s_invalidLength )
    , m_height( MathUtilities::s_invalidLength )
{
    this->set( aWidth, aHeight );
}

Size::Size( const int aWidth, const int aHeight )
    : m_width( MathUtilities::s_invalidLength )
    , m_height( MathUtilities::s_invalidLength )
{
    this->set( aWidth, aHeight );
}

Size::Size( const QSize& aSize )
    : m_width( MathUtilities::s_invalidLength )
    , m_height( MathUtilities::s_invalidLength )
{
    this->set( aSize.width(), aSize.height() );
}

Size::Size( const QSizeF& aSize )
    : m_width( MathUtilities::s_invalidLength )
    , m_height( MathUtilities::s_invalidLength )
{
    this->set( aSize.width(), aSize.height() );
}

Size::Size( const Vector2D& aVector2D )
    : m_width( MathUtilities::s_invalidLength )
    , m_height( MathUtilities::s_invalidLength )
{
    this->set( aVector2D.x(), aVector2D.y() );
}

Size::Size()
    : m_width( MathUtilities::s_invalidLength )
    , m_height( MathUtilities::s_invalidLength )
{
}

void Size::setWidth( const double& aWidth )
{
    if ( MathUtilities::isLengthValid( aWidth ) )
    {
        m_width = aWidth;
    }
}

void Size::setHeight( const double& aHeight )
{
    if ( MathUtilities::isLengthValid( aHeight ) )
    {
        m_height = aHeight;
    }
}

void Size::set( const double& aWidth, const double& aHeight )
{
    this->setWidth( aWidth );
    this->setHeight( aHeight );
}

bool Size::isEmpty() const
{
    return MathUtilities::isFuzzyZero( m_width )
           && MathUtilities::isFuzzyZero( m_height );
}

bool Size::isValid() const
{
    return MathUtilities::isLengthValid( m_width )
           && MathUtilities::isLengthValid( m_height );
}

void Size::clear()
{
    m_width = MathUtilities::s_invalidLength;
    m_height = MathUtilities::s_invalidLength;
}

double Size::area() const
{
    return this->isValid()
           ? m_width * m_height
           : MathUtilities::s_invalidLength;
}

void Size::inscribe( const double& boundaryWidth, const double& boundaryHeight )
{
    this->inscribe( Size( boundaryWidth, boundaryHeight ) );
}

Size Size::inscribed( const double& boundaryWidth, const double& boundaryHeight ) const
{
    Size size = *this;
    size.inscribe( boundaryWidth, boundaryHeight );

    return size;
}

void Size::inscribe( const Size& other )
{
    if ( ! this->isValid()
         || ! other.isValid() )
    {
        return;
    }

    *this *= this->inscriptionScale( other );
}

Size Size::inscribed( const Size& boundarySize ) const
{
    return this->inscribed( boundarySize.width(), boundarySize.height() );
}

void Size::expand( const double& horizontalMargin, const double& verticalMargin )
{
    *this = this->expanded( horizontalMargin, verticalMargin );
}

Size Size::expanded( const double& horizontalMargin, const double& verticalMargin ) const
{
    return Size( m_width + horizontalMargin,
                 m_height + verticalMargin );
}

void Size::expand( const double& margin )
{
    this->expand( margin, margin );
}

Size Size::expanded( const double& margin ) const
{
    return this->expanded( margin, margin );
}

double Size::minLength() const
{
    return qMin( m_width, m_height );
}

double Size::maxLength() const
{
    return qMax( m_width, m_height );
}

Size Size::minSize() const
{
    const double minLength = this->minLength();
    return Size( minLength, minLength );
}

Size Size::maxSize() const
{
    const double maxLength = this->maxLength();
    return Size( maxLength, maxLength );
}

QSize Size::toQSize() const
{
    return QSize( static_cast< int >( m_width ),
                  static_cast< int >( m_height ) );
}

Size::operator QSize() const
{
    return this->toQSize();
}

QSizeF Size::toQSizeF() const
{
    return QSizeF( m_width,
                   m_height );
}

Vector2D Size::toVector2D() const
{
    return Vector2D( m_width, m_height );
}

Size::operator QSizeF() const
{
    return this->toQSizeF();
}

Size Size::operator+( const Size& other ) const
{
    Size size = *this;
    size += other;

    return size;
}

Size& Size::operator+=( const Size& other )
{
    if ( this->isValid()
         && other.isValid() )
    {
        m_width += other.m_width;
        m_height += other.m_height;
    }

    return *this;
}

Size Size::operator-( const Size& other ) const
{
    Size size = *this;
    size -= other;

    return size;
}

Size& Size::operator-=( const Size& other )
{
    if ( this->isValid()
         && other.isValid() )
    {
        m_width -= other.m_width;
        m_height -= other.m_height;
    }

    return *this;
}

Size Size::operator*( const double& factor ) const
{
    Size size = *this;
    size *= factor;

    return size;
}

Size& Size::operator*=( const double& factor )
{
    if ( this->isValid() )
    {
        m_width *= factor;
        m_height *= factor;
    }

    return *this;
}

Size Size::operator/( const double& factor ) const
{
    Size size = *this;
    size /= factor;

    return size;
}

Size& Size::operator/=( const double& factor )
{
    if ( ! this->isValid() )
    {
        return *this;
    }

    if ( MathUtilities::isFuzzyZero( factor ) )
    {
        const double inf = std::numeric_limits< double >::infinity();
        m_width  = inf;
        m_height = inf;
    }
    else
    {
        m_width  /= factor;
        m_height /= factor;
    }

    return *this;
}

double Size::inscriptionScale( const double& boundaryWidth, const double& boundaryHeight ) const
{
    return this->inscriptionScale( Size( boundaryWidth, boundaryHeight ) );
}

double Size::inscriptionScale( const Size& other ) const
{
    if ( ! this->isValid()
         || ! other.isValid() )
    {
        return std::numeric_limits< double >::quiet_NaN();
    }

    return qMin( other.m_width / m_width,
                 other.m_height / m_height );
}

bool Size::operator==( const Size& other ) const
{
    return    MathUtilities::isFuzzyEqual( m_width,  other.m_width )
           && MathUtilities::isFuzzyEqual( m_height, other.m_height );
}

bool Size::operator!=( const Size& other ) const
{
    return ! ( *this == other );
}


#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const Size& s )
{
    return d.nospace() << "Size( " << s.width() << ", " << s.height() << " )";
}
#endif
