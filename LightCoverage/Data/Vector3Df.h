#ifndef LIGHTCOVERAGE_DATA_VECTOR3DF_H
#define LIGHTCOVERAGE_DATA_VECTOR3DF_H

#include "Vector2D.h"

#include <QDebug>

namespace LightCoverage {
namespace Data {

class Vector3Df
{
public:
    explicit Vector3Df( const float aX, const float aY, const float aZ );
    explicit Vector3Df( const Vector2D& vector2D, const float aZ = 0.0f );
    Vector3Df();

    void setX( const float aX );
    inline float x() const
    {
        return m_x;
    }

    void setY( const float aY );
    inline float y() const
    {
        return m_y;
    }

    void setZ( const float aZ );
    inline float z() const
    {
        return m_z;
    }

    void set( const float aX, const float aY, const float aZ );

    float length() const;
    bool isUnit() const;
    float distanceTo( const Vector3Df& other ) const;
    float dot( const Vector3Df& other ) const;
    Vector3Df cross( const Vector3Df& other ) const;

    bool operator==( const Vector3Df& other ) const;
    bool operator!=( const Vector3Df& other ) const;

    Vector3Df operator+( const Vector3Df& other ) const;
    Vector3Df& operator+=( const Vector3Df& other );
    Vector3Df operator-( const Vector3Df& other ) const;
    Vector3Df& operator-=( const Vector3Df& other );
    Vector3Df operator*( const float factor ) const;
    Vector3Df& operator*=( const float factor );
    Vector3Df operator/( const float factor ) const;
    Vector3Df& operator/=( const float factor );

    void normalize();
    Vector3Df normalized() const;
    Vector3Df normal( const Vector3Df& other ) const;
    float angle() const;
    float angleTo( const Vector3Df& other ) const;

private:
    float m_x;
    float m_y;
    float m_z;
};

}
}

LightCoverage::Data::Vector3Df operator*( const float factor, const LightCoverage::Data::Vector3Df& pt );

#ifndef QT_NO_DEBUG_STREAM
QDebug operator<<( QDebug d, const LightCoverage::Data::Vector3Df& pt );
#endif

#endif // LIGHTCOVERAGE_DATA_VECTOR3DF_H
