#ifndef LIGHTCOVERAGE_LIGHTCOVERAGE_PRECOMPILED_H
#define LIGHTCOVERAGE_LIGHTCOVERAGE_PRECOMPILED_H

#include <GL/glew.h>

#ifdef __cplusplus
#include <QAction>
#include <QApplication>
#include <QByteArray>
#include <QColor>
#include <QDebug>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QDir>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QGLFormat>
#include <QGLWidget>
#include <QGridLayout>
#include <QHash>
#include <QHBoxLayout>
#include <QKeySequence>
#include <QLabel>
#include <QLine>
#include <QLineF>
#include <QList>
#include <QMainWindow>
#include <QMap>
#include <qmath.h>
#include <QMatrix>
#include <QMatrix4x4>
#include <QMenu>
#include <QMenuBar>
#include <QMessageBox>
#include <QMimeData>
#include <QMouseEvent>
#include <QObject>
#include <QPainter>
#include <QPair>
#include <QPen>
#include <QPoint>
#include <QPointF>
#include <QPolygon>
#include <QPolygonF>
#include <QProcess>
#include <QPushButton>
#include <QRect>
#include <QRectF>
#include <QScopedArrayPointer>
#include <QScopedPointer>
#include <QSet>
#include <QSize>
#include <QSizeF>
#include <QStatusBar>
#include <QString>
#include <QStringBuilder>
#include <QStringList>
#include <QTabWidget>
#include <QTextStream>
#include <QtGlobal>
#include <QUrl>
#include <QVector>
#include <QWidget>

#include <limits>
#include <utility>
#endif

#include <float.h>
#include <math.h>

#endif // LIGHTCOVERAGE_LIGHTCOVERAGE_PRECOMPILED_H
