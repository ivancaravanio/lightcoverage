#include "RoomFilePathDisplay.h"

#include "Utilities/GeneralUtilities.h"

#include <QDesktopServices>
#include <QDir>
#include <QFileInfo>
#include <QHBoxLayout>
#include <QProcess>
#include <QProcess>
#include <QPushButton>
#include <QStringBuilder>
#include <QUrl>

using namespace LightCoverage;

using namespace LightCoverage::Utilities;

RoomFilePathDisplay::RoomFilePathDisplay( QWidget* const parent )
    : QWidget( parent )
{
    static const QString hyperlinkStyleSheet = "QPushButton { color: blue; text-decoration: underline color blue; }";

    m_dirPathDisplay = new QPushButton;
    m_dirPathDisplay->setFlat( true );
    m_dirPathDisplay->setStyleSheet( hyperlinkStyleSheet );
    connect( m_dirPathDisplay, SIGNAL(clicked()), this, SLOT(openFileFolder()) );

    m_fileNameDisplay = new QPushButton;
    m_fileNameDisplay->setFlat( true );
    m_fileNameDisplay->setStyleSheet( hyperlinkStyleSheet );
    connect( m_fileNameDisplay, SIGNAL(clicked()), this, SLOT(openFile()) );

    QHBoxLayout* const mainLayout = new QHBoxLayout( this );
    mainLayout->setSpacing( 0 );
    mainLayout->addWidget( m_dirPathDisplay );
    mainLayout->addWidget( m_fileNameDisplay );

    this->applyFilePath();
}

void RoomFilePathDisplay::setFilePath( const QString& aFilePath )
{
    QString existingFilePath;
    const bool isNewFilePathFilled = ! aFilePath.isEmpty();
    if ( isNewFilePathFilled )
    {
        existingFilePath = GeneralUtilities::toExistingCanonicalFilePath( aFilePath );
    }

    if ( isNewFilePathFilled && existingFilePath.isEmpty()
         || GeneralUtilities::areFilePathsEqual( existingFilePath, m_filePath ) )
    {
        return;
    }

    m_filePath = existingFilePath;
    this->applyFilePath();
}

void RoomFilePathDisplay::openFileFolder()
{
    if ( m_filePath.isEmpty() )
    {
        return;
    }

#ifdef Q_OS_WIN
    static const QString openDirSelectFileCommandTemplate = "explorer /select,";

    QProcess::startDetached( openDirSelectFileCommandTemplate + m_filePath );
#else
    QDesktopServices::openUrl( QUrl::fromLocalFile( QFileInfo( m_filePath ).canonicalPath() ) );
#endif
}

void RoomFilePathDisplay::openFile()
{
    if ( m_filePath.isEmpty() )
    {
        return;
    }

    QDesktopServices::openUrl( QUrl::fromLocalFile( m_filePath ) );
}

void RoomFilePathDisplay::applyFilePath()
{
    QString dirPath;
    QString fileName;
    if ( ! m_filePath.isEmpty() )
    {
        const QFileInfo fileInfo( m_filePath );
        dirPath = QDir::toNativeSeparators( fileInfo.canonicalPath() ) + QDir::separator();
        fileName = fileInfo.fileName();
    }

    m_dirPathDisplay->setText( dirPath );
    m_fileNameDisplay->setText( fileName );
}

