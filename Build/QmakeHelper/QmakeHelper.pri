# Needed for function calls. The following article - http://qt-project.org/doc/qt-4.8/qmake-variable-reference.html#pwd, says:
# "Note: Function calls have no effect on the value of PWD. PWD will refer to the path of the calling file."
# Calling file could differ from the current file.
QMAKEHELPER_PRI_PWD = $$PWD

LOCALE_NAMES = \
    tr \
    de \
    el \
    es \
    fr \
    it \
    nl \
    pl \
    pt \
    ru

#    af \
#    sq \
#    ar_SA \
#    ar_IQ \
#    ar_EG \
#    ar_LY \
#    ar_DZ \
#    ar_MA \
#    ar_TN \
#    ar_OM \
#    ar_YE \
#    ar_SY \
#    ar_JO \
#    ar_LB \
#    ar_KW \
#    ar_AE \
#    ar_BH \
#    ar_QA \
#    eu \
#    bg \
#    be \
#    ca \
#    zh_TW \
#    zh_CN \
#    zh_HK \
#    zh_SG \
#    hr \
#    cs \
#    da \
#    n \
#    nl_BE \
#    en \
#    en_US \
#    en_GB \
#    en_AU \
#    en_CA \
#    en_NZ \
#    en_IE \
#    en_ZA \
#    en_JM \
#    en \
#    en_BZ \
#    en_TT \
#    et \
#    fo \
#    fa \
#    fi \
#    fr \
#    fr_be \
#    fr_ca \
#    fr_ch \
#    fr_lu \
#    mk \
#    gd \
#    gd_ie \
#    de \
#    de_ch \
#    de_at \
#    de_lu \
#    de_li \
#    el_gr \
#    he \
#    hi \
#    hu \
#    is \
#    in \
#    it \
#    it_ch \
#    ja \
#    ko \
#    ko \
#    lv \
#    lt \
#    ms \
#    mt \
#    no \
#    no \
#    p \
#    pt_br \
#    pt \
#    rm \
#    ro \
#    ro_mo \
#    ru \
#    ru_mo \
#    sz \
#    sr \
#    sr \
#    sk \
#    s \
#    sb \
#    es \
#    es_mx \
#    es \
#    es_gt \
#    es_cr \
#    es_pa \
#    es_do \
#    es_ve \
#    es_co \
#    es_pe \
#    es_ar \
#    es_ec \
#    es_c \
#    es_uy \
#    es_py \
#    es_bo \
#    es_sv \
#    es_hn \
#    es_ni \
#    es_pr \
#    sx \
#    sv \
#    sv_fi \
#    th \
#    ts \
#    tn \
#    tr \
#    uk \
#    ur \
#    ve \
#    vi \
#    xh \
#    ji \
#    zu

defineTest(repopulateTranslations) {
    TRANSLATION_LOCALES = $$join( LOCALE_NAMES, : )
    TRANSLATION_FILE_NAME_FORMAT = %1_%2.%3

    DEFINES += TRANSLATION_LOCALES=\\\"$${TRANSLATION_LOCALES}\\\"
    export( DEFINES )

    TRANSLATIONS =
    for( LOCALE_NAME, LOCALE_NAMES ) {
        TRANSLATION_TXT_FILE_NAME = $$sprintf( $$TRANSLATION_FILE_NAME_FORMAT, $$TARGET, $$LOCALE_NAME, ts )
        TRANSLATIONS += $$TRANSLATION_TXT_FILE_NAME
    }

    export( TRANSLATIONS )

    return (true)
}

# argument #1 (required, value) - number
# argument #2 (optional, value) - field width (default = "0"; if the field width is less or equal to the count of the number's digits, the digit will be printed as it is)
# argument #3 (optional, value) - fill char (default = "")
defineReplace(numberToString) {
    NUMBER_TO_STRING_RESULT =
    count( ARGS, 1, >= ) : count( ARGS, 3, <= ) {
        NUMBER = $$1

        FIELD_WIDTH = 0
        count( ARGS, 2, >= ) {
            FIELD_WIDTH = $$2
        }

        greaterThan( FIELD_WIDTH, 0 ) : !lessThan( NUMBER, 0 ) {
            # Contains all possible digit counts for 32-bit signed/unsigned integer
            # 32-bit signed/unsigned long integers are not taken into consideration,
            # since lessThan, greaterThan, equals use 32-bit signed/unsigned integer.
            # It is not easy to deduce the target executable/library architecture type -
            # 32-bit or 64-bit, just by examining some of qmake's variables.
            # If the passed number exceeds the maximum unsigned 32-bit integer =>
            # 64-bit architecture is used
            POWERS_OF_TEN = 1 \ # 10 ^ 0 - added since the field width is 1-based but the POWERS_OF_TEN list is 0-based
                            1 \ # 10 ^ 0
                            10 \ # 10 ^ 1
                            100 \ # 10 ^ 2
                            1000 \ # 10 ^ 3
                            10000 \ # 10 ^ 4
                            100000 \ # 10 ^ 5
                            1000000 \ # 10 ^ 6
                            10000000 \ # 10 ^ 7
                            100000000 \ # 10 ^ 8
                            1000000000   # 10 ^ 9
            MAX_UNSIGNED_INT = 4294967295
            greaterThan(NUMBER, $$MAX_UNSIGNED_INT) {
                # the signed/unsigned integer is 64-bit
                POWERS_OF_TEN += 10000000000 \ # 10 ^ 10
                                 100000000000 \ # 10 ^ 11
                                 1000000000000 \ # 10 ^ 12
                                 10000000000000 \ # 10 ^ 13
                                 100000000000000 \ # 10 ^ 14
                                 1000000000000000 \ # 10 ^ 15
                                 10000000000000000 \ # 10 ^ 16
                                 100000000000000000 \ # 10 ^ 17
                                 1000000000000000000 \ # 10 ^ 18
                                 10000000000000000000   # 10 ^ 19
            }

            POWERS_OF_TEN_COUNT = $$size(POWERS_OF_TEN)
            lessThan( FIELD_WIDTH, $$POWERS_OF_TEN_COUNT ) {
                FILL_CHAR =
                count( ARGS, 3 ) {
                    FILL_CHAR = $$3
                }
                TARGET_POWER_OF_TEN = $$member( POWERS_OF_TEN, $$FIELD_WIDTH )
                NUMBER_TO_STRING_RESULT = $$NUMBER
                for( POWER_OF_TEN, POWERS_OF_TEN ) {
                    lessThan( POWER_OF_TEN, $$TARGET_POWER_OF_TEN ) | equals(POWER_OF_TEN, $$TARGET_POWER_OF_TEN ) {
                        greaterThan( POWER_OF_TEN, $$NUMBER ) {
                            DONT_FILL =
                            equals( POWER_OF_TEN, 1 ) {
                                equals( NUMBER, 0 ) {
                                    DONT_FILL = true
                                }
                            }
                            isEmpty( DONT_FILL ) {
                                NUMBER_TO_STRING_RESULT = $${FILL_CHAR}$${NUMBER_TO_STRING_RESULT}
                            }
                        }
                    }
                }
            }
            else {
                NUMBER_TO_STRING_RESULT = $$NUMBER
            }
        }
        else {
            NUMBER_TO_STRING_RESULT = $$NUMBER
        }
    }

    return ( $$NUMBER_TO_STRING_RESULT )
}

# argument #1 (required, value) - version parts (major|major minor|major minor patch)
# argument #2 (optional, value) - separator (default = ".")
# argument #3 (optional, value) - field width (default = "1")
# argument #4 (optional, value) - fill char (default = "")
defineReplace(versionString) {
    VERSION_STRING_RESULT =
    count( ARGS, 1, >= ) {
        VERSION_PARTS = $$eval( $$1 )

        count( VERSION_PARTS, 1, >= ) {
            SEPARATOR = .
            count( ARGS, 2, >= ) {
                SEPARATOR = $$2
            }

            FIELD_WIDTH = 1
            count( ARGS, 3, >= ) {
                FIELD_WIDTH = $$3
            }

            FILL_CHAR =
            count( ARGS, 4, >= ) {
                FILL_CHAR = $$4
            }

            VERSION_STRING_PARTS =
            for( VERSION_PART, VERSION_PARTS ) {
                VERSION_STRING_PARTS += $$numberToString( $$VERSION_PART, $$FIELD_WIDTH, $$FILL_CHAR )
            }

            VERSION_STRING_RESULT = $$join( VERSION_STRING_PARTS, $$SEPARATOR )
        }
    }

    return ( $$VERSION_STRING_RESULT )
}

# major
# minor
# patch
# fieldWidth
# fillChar - equals "0" here
# separator
defineReplace(pyVersionString) {
    str =
    count(ARGS, 6) {
        major       = $$1
        minor       = $$2
        patch       = $$3
        fieldWidth  = $$4
        # fillChar    = $$5
        separator   = $$6
        str         = $$system($${QMAKEHELPER_PRI_PWD}/QmakeHelper.py -versionString $$major $$minor $$patch $$fieldWidth $$separator)
    }

    return ($$str)
}

defineReplace(incremented) {
    str =
    count(ARGS, 1) {
        str = $$system($${QMAKEHELPER_PRI_PWD}/QmakeHelper.py -increment $$1)
    }

    return ($$str)
}

defineReplace(decremented) {
    str =
    count(ARGS, 1) {
        str = $$system($${QMAKEHELPER_PRI_PWD}/QmakeHelper.py -decrement $$1)
    }

    return ($$str)
}

defineTest(hasUnixShell) {
    win32 : isEmpty(QMAKE_SH) {
        return (false)
    } else: unix : !isEmpty(QMAKE_SH) {
        return (true)
    }

    return (false)
}

# argument #1 (required, value): path
defineReplace(whitespacesEscapedPath) {
    WHITESPACES_ESCAPED_PATH = $$1
    WHITESPACES_ESCAPED_PATH = $$quote($$join(WHITESPACES_ESCAPED_PATH, " "))
    hasUnixShell() {
        WHITESPACES_ESCAPED_PATH = $$replace(WHITESPACES_ESCAPED_PATH, "\\ ", " ")
        WHITESPACES_ESCAPED_PATH = $$replace(WHITESPACES_ESCAPED_PATH, " ", "\\ ")
    } else {
        WHITESPACES_ESCAPED_PATH = $$replace(WHITESPACES_ESCAPED_PATH, "\"", "")
        WHITESPACES_ESCAPED_PATH = $$quote(\"$${WHITESPACES_ESCAPED_PATH}\")
    }
    return ( $$WHITESPACES_ESCAPED_PATH )
}

defineTest(isDebugBuild) {
    CONFIG(debug, debug|release) {
        return(true)
    } else {
        return(false)
    }
}

# argument #1 (required, value): library name
# argument #2 (optional, variable): the library's version parts (example: {1 0 0}, {1 0}, {1}, {} )
# argument #3 (optional, value): if empty the name of the dynamic library will be composed, otherwise the name of the static one
defineReplace(libraryFullBinaryFileName) {
    LIBRARY_FULL_BINARY_FILE_NAME_RESULT =
    count( ARGS, 1, >= ) : count( ARGS, 3, <= )  {
        LIBRARY_VERSION =
        count( ARGS, 2, >= ) {
            LIBRARY_VERSION = $$versionString( $$2 )
        }

        LIBRARY_VERSION_SUFFIX =
        !isEmpty( LIBRARY_VERSION ) {
            win32 {
                LIBRARY_VERSION_SUFFIX = $$LIBRARY_VERSION
            } else {
                LIBRARY_VERSION_SUFFIX = .$$LIBRARY_VERSION
            }
        }

        NAME_TEMPLATE =
        # %1 - library name
        # %2 - library version
        win32 {
            IS_STATIC = $$3
            isEmpty(IS_STATIC) {
                NAME_TEMPLATE = %1%2.dll
            } else {
                *g++* | *gcc* {
                    NAME_TEMPLATE = lib%1%2.a
                } else {
                    NAME_TEMPLATE = %1%2.lib
                }
            }
        } else: macx {
            NAME_TEMPLATE = $${QMAKE_PREFIX_SHLIB}%1%2.$${QMAKE_EXTENSION_SHLIB}
        } else: unix : !macx {
            NAME_TEMPLATE = $${QMAKE_PREFIX_SHLIB}%1.so%2
        }
        LIBRARY_FULL_BINARY_FILE_NAME_RESULT = $$sprintf( $$NAME_TEMPLATE, $$1, $$LIBRARY_VERSION_SUFFIX )
    }

    return ( $$LIBRARY_FULL_BINARY_FILE_NAME_RESULT )
}

defineTest(isStaticLibrary) {
    ! equals( TEMPLATE, lib ) {
        return ( false )
    }

    contains( CONFIG, static ) | contains( CONFIG, staticlib ) {
        return ( true )
    }

    return ( false )
}

# argument #1 (required, value): module complete base file name
# argument #2 (required, value): version parts as a list
defineReplace(moduleFullBinaryFileName) {
    COMPLETE_BASE_FILE_NAME = $$1
    VERSION_PARTS = $$2

    isEmpty( COMPLETE_BASE_FILE_NAME ) | ! count( VERSION_PARTS, 3 ) {
        return ()
    }

    IS_LIBRARY =
    LIBRARY_VERSION_PARTS =
    win32 {
        contains( TEMPLATE, app ) {
            return ( $${COMPLETE_BASE_FILE_NAME}.exe )
        } else : contains( TEMPLATE, lib ) {
            IS_LIBRARY = 1
            LIBRARY_VERSION_PARTS = $$member( VERSION_PARTS, 0 )
        }
    } else : macx {
        equals( TEMPLATE, app ) {
            contains( CONFIG, app_bundle ) {
                isEmpty( QMAKE_BUNDLE_EXTENSION ) {
                    return ( $${COMPLETE_BASE_FILE_NAME}.app )
                } else {
                    return ( $${COMPLETE_BASE_FILE_NAME}.$${QMAKE_BUNDLE_EXTENSION} )
                }
            } else {
                return ( $$TARGET )
            }
        } else : equals( TEMPLATE, lib ) {
            contains( CONFIG, lib_bundle ) {
                isEmpty( QMAKE_BUNDLE_EXTENSION ) {
                    return ( $${COMPLETE_BASE_FILE_NAME}.framework )
                } else {
                    return ( $${COMPLETE_BASE_FILE_NAME}.$${QMAKE_BUNDLE_EXTENSION} )
                }
            } else {
                IS_LIBRARY = 1
                LIBRARY_VERSION_PARTS = $$VERSION_PARTS
            }
        }
    } else : unix : ! macx {
        equals( TEMPLATE, app ) {
            return ( $$COMPLETE_BASE_FILE_NAME )
        } else : equals( TEMPLATE, lib ) {
            IS_LIBRARY = 1
            LIBRARY_VERSION_PARTS = $$VERSION_PARTS
        }
    }

    ! isEmpty( IS_LIBRARY ) {
        IS_STATIC_LIBRARY =
        isStaticLibrary() {
            IS_STATIC_LIBRARY = true
        }

        return ( $$libraryFullBinaryFileName( $${COMPLETE_BASE_FILE_NAME}, LIBRARY_VERSION_PARTS, $$IS_STATIC_LIBRARY ) )
    }

    return ()
}

CRLF                    = $$escape_expand(\\n\\t)
WIN_DIR_SEPARATOR       = $$escape_expand(\\)
UNIX_DIR_SEPARATOR      = /

hasUnixShell() {
    ENV_DIR_SEP         = $$UNIX_DIR_SEPARATOR
    ENV_COPY            = cp -f
    ENV_COPY_FILE       = $$ENV_COPY
    ENV_COPY_DIR        = $$ENV_COPY -R
    ENV_MOVE            = mv -f
    ENV_DEL_FILE        = rm -fR
    ENV_DEL_DIR         = rmdir
    ENV_MKDIR           = mkdir -p
    ENV_CHK_DIR_EXISTS  = test -d

    ENV_SYMBOLIC_LINK   = ln -f -s
    ENV_INSTALL_FILE    = install -m 644 -p
    ENV_INSTALL_PROGRAM = install -m 755 -p
} else {
    ENV_DIR_SEP         = $$WIN_DIR_SEPARATOR
    ENV_COPY            = xcopy /y /q /c
    ENV_COPY_FILE       = $$ENV_COPY
    ENV_COPY_DIR        = xcopy /s /q /y /i /c
    ENV_MOVE            = move
    ENV_DEL_FILE        = del /q
    ENV_DEL_DIR         = rd /s /q
    ENV_MKDIR           = mkdir
    ENV_CHK_DIR_EXISTS  = IF EXIST

    ENV_SYMBOLIC_LINK   =
    ENV_INSTALL_FILE    = $$ENV_COPY_FILE
    ENV_INSTALL_PROGRAM = $$ENV_COPY_FILE
}

# argument #1 (required, value): a variable containing the path to convert
defineReplace(toNativeSeparators) {
    PATH = $$1
    !isEmpty( PATH ) {
        DIR_SEP_TO_REPLACE =
        hasUnixShell() {
            DIR_SEP_TO_REPLACE = $${WIN_DIR_SEPARATOR}$${WIN_DIR_SEPARATOR}
        } else {
            DIR_SEP_TO_REPLACE = $$UNIX_DIR_SEPARATOR
        }
        PATH = $$replace(PATH, $$DIR_SEP_TO_REPLACE, $$ENV_DIR_SEP)
    }

    return ( $$PATH )
}

# argument #1 (required, value): path
defineReplace(fixedPath) {
    return ( $$whitespacesEscapedPath( $$toNativeSeparators( $$1 ) ) )
}

defineReplace(shellWrapper) {
    SHELL_WRAPPER =
    hasUnixShell() : win32 {
        SHELL_WRAPPER = $$system_quote( $$system_path( $$QMAKE_SH ) ) -c \"%1\"
    } else {
        SHELL_WRAPPER = %1
    }

    return ( $$SHELL_WRAPPER )
}

# argument #1 (required, value): command and its arguments
defineReplace(formattedCommand) {
    return( $$sprintf( $$shellWrapper(), $$1 ) )
}

# argument #1 (required, value): source file/files path
# argument #2 (required, value): destination file/folder path
defineReplace(copyFile) {
    return ( $$formattedCommand( $$ENV_COPY_FILE $$fixedPath($$1) $$fixedPath($$2) ) )
}

# argument #1 (required, value): source folder path
# argument #2 (required, value): destination folder path
defineReplace(formattedCopyDirCommand) {
    COPY_DIR_FORMAT = %1
    hasUnixShell() {
        COPY_DIR_FORMAT += %2/.
    } else {
        COPY_DIR_FORMAT += %2
    }
    COPY_DIR_FORMAT += %3

    return ( $$sprintf( $$COPY_DIR_FORMAT, $$ENV_COPY_DIR, $$1, $$2 ) )
}

# argument #1 (required, value): source folder path
# argument #2 (required, value): destination folder path
defineReplace(copyDir) {
    return ( $$formattedCommand( $$formattedCopyDirCommand( $$fixedPath($$1), $$fixedPath($$2) ) ) )
}

# argument #1 (required, value): source file path
# argument #2 (required, value): destination file path
defineReplace(copyFileExists) {
    return ( $$formattedCommand( $$sprintf( $$fileFolderExists( $$fixedPath($$1), file, exist ), $$ENV_COPY_FILE $$fixedPath($$1) $$fixedPath($$2) ) ) )
}

# argument #1 (required): file path
defineReplace(delFile) {
    return ( $$formattedCommand( $$ENV_DEL_FILE $$fixedPath($$1) ) )
}

# argument #1 (required, value): source folder path
# argument #2 (required, value): destination folder path
defineReplace(copyDirExists) {
    return ( $$formattedCommand( $$sprintf( $$fileFolderExists( $$fixedPath($$1), directory, exist ), $$formattedCopyDirCommand( $$fixedPath($$1), $$fixedPath($$2) ) ) ) )
}

# argument #1 (required, value): folder path
defineReplace(makeDir) {
    return ( $$formattedCommand( $$ENV_MKDIR $$fixedPath($$1) ) )
}

# argument #1 (required, value): folder path
defineReplace(makeDirNotExists) {
    return ( $$formattedCommand( $$sprintf( $$fileFolderExists( $$fixedPath($$1), directory, notexist ), $$ENV_MKDIR $$fixedPath($$1) ) ) )
}

# argument #1 (required, value): folder path
defineReplace(delDir) {
    return ( $$formattedCommand( $$ENV_DEL_DIR $$fixedPath($$1) ) )
}

# argument #1 (required, value): source file path
# argument #2 (required, value): destination symbolic link file path
defineReplace(symbolicLink) {
    return ( $$formattedCommand( $$ENV_SYMBOLIC_LINK $$fixedPath($$1) $$fixedPath($$2) ) )
}

# argument #1 (required, value): file path
# argument #2 (required, value): "file" or "directory"
# argument #3 (required, value): "exist" or "notexist"
defineReplace(fileFolderExists) {
    FILE_EXISTS_RESULT =
    ARGUMENT_2_POSSIBLE_VALUES = file directory
    ARGUMENT_3_POSSIBLE_VALUES = exist notexist

    count(ARGS, 3) : contains(ARGUMENT_2_POSSIBLE_VALUES, $$2) : contains(ARGUMENT_3_POSSIBLE_VALUES, $$3) {
        FILE_EXISTS_FORMAT =
        EXIST = $$3
        hasUnixShell() {
            FILE_EXISTS_FORMAT = if [
            equals(EXIST, exist) {
                FILE_EXISTS_FORMAT += -e %1 -a

                ENTITY_TYPE = $$2
                equals(ENTITY_TYPE, file) {
                    FILE_EXISTS_FORMAT += -f
                } else: equals(ENTITY_TYPE, directory) {
                    FILE_EXISTS_FORMAT += -d
                }
            } else: equals(EXIST, notexist) {
                FILE_EXISTS_FORMAT = if [ ! -e
            }
            FILE_EXISTS_FORMAT += %1 ]; then %2; fi
        } else {
            equals(EXIST, exist) {
                FILE_EXISTS_FORMAT = IF
            } else: equals(EXIST, notexist) {
                FILE_EXISTS_FORMAT = IF NOT
            }
            FILE_EXISTS_FORMAT += EXIST %1 %2
        }

        FILE_EXISTS_RESULT = $$sprintf($$FILE_EXISTS_FORMAT, $$1)
    }

    return ($$FILE_EXISTS_RESULT)
}

# argument #1 (required, value): string, which whitespaces will be replaced by \\x20
defineReplace(quotedWhitespaceEscapedValue) {
    QUOTED_WHITESPACE_ESCAPED_VALUE = $$1
    !isEmpty( QUOTED_WHITESPACE_ESCAPED_VALUE ) {
        WHITESPACE_CHAR_CODE = $$escape_expand(\\x20)
        QUOTED_WHITESPACE_ESCAPED_VALUE = $$replace( QUOTED_WHITESPACE_ESCAPED_VALUE, " ", $$WHITESPACE_CHAR_CODE )
        QUOTED_WHITESPACE_ESCAPED_VALUE = $$join( QUOTED_WHITESPACE_ESCAPED_VALUE, $$WHITESPACE_CHAR_CODE )
        QUOTED_WHITESPACE_ESCAPED_VALUE = $$quote( $$QUOTED_WHITESPACE_ESCAPED_VALUE )
    }

    return ($$QUOTED_WHITESPACE_ESCAPED_VALUE)
}

# argument #1 (required, variable): array of elements
# argument #2 (required, value): the value that should be prepended
#                                to each element from the array
defineReplace(prependToEachArrayElement) {
    PREPEND_TO_EACH_ARRAY_ELEMENT_RESULT =
    for( ITER, $$1 ) {
        PREPEND_TO_EACH_ARRAY_ELEMENT_RESULT += $${2}$${ITER}
    }

    return ($$PREPEND_TO_EACH_ARRAY_ELEMENT_RESULT)
}

# argument #1 (required, variable): array of elements
# argument #2 (required, value): the value that should be appended
#                                to each element from the array
defineReplace(appendToEachArrayElement) {
    APPEND_TO_EACH_ARRAY_ELEMENT_RESULT =
    for( ITER, $$1 ) {
        APPEND_TO_EACH_ARRAY_ELEMENT_RESULT += $${ITER}$${2}
    }

    return ($$APPEND_TO_EACH_ARRAY_ELEMENT_RESULT)
}

# argument #1 (required, value): first string to compare
# argument #2 (required, value): second string to compare
# argument #3 (required, value): 0 - case insensitive
#                                1 - case sensitive
#                                2 - platform automatic
defineTest(compareStrings) {
    COMPARISON_MODE = $$3
    equals( COMPARISON_MODE, 0 ) {
        FIRST_STRING_LOWER = $$lower( $$1 )
        equals( FIRST_STRING_LOWER, $$lower( $$2 ) ) {
            return (true)
        }
    } else : equals( COMPARISON_MODE, 1 ) {
        FIRST_STRING = $$1
        equals( FIRST_STRING, $$2 ) {
            return (true)
        }
    } else : equals( COMPARISON_MODE, 2 ) {
        COMPARISON_MODE_NEW =
        win32 {
            COMPARISON_MODE_NEW = 0
        } else {
            COMPARISON_MODE_NEW = 1
        }

        compareStrings( $$1, $$2, $$COMPARISON_MODE_NEW ) {
            return (true)
        }
    }

    return (false)
}

QT_BINARY_TRANSLATION_FILES_FILTER = qt_*.qm
defineReplace(qtBinaryTranslationFilesPaths) {
    qtAndQtHelpTranslationFiles = $$files($$[QT_INSTALL_TRANSLATIONS]/$${QT_BINARY_TRANSLATION_FILES_FILTER})
    qtTranslationFilesOnly =
    ! isEmpty(qtAndQtHelpTranslationFiles) {
        for(translationFile, qtAndQtHelpTranslationFiles) {
            containsHelp = $$find(translationFile, _help_)
            isEmpty( containsHelp ) {
                qtTranslationFilesOnly += $$translationFile
            }
        }
    }
    return ($$qtTranslationFilesOnly)
}

# argument #1 (required, value): file paths
# argument #2 (required, value): destination dir path
# argument #3 (required, value): should auto clean destination dir path
defineTest(deployFiles) {
    SOURCE_FILE_PATHS = $$1
    isEmpty( SOURCE_FILE_PATHS ) {
        return (false)
    }

    DESTINATION_DIR_PATH = $$2
    QMAKE_POST_LINK += $$makeDirNotExists( $$DESTINATION_DIR_PATH ) $$CRLF

    for( SOURCE_FILE_PATH, SOURCE_FILE_PATHS ) {
        QMAKE_POST_LINK += $$copyFileExists( $$SOURCE_FILE_PATH, $$DESTINATION_DIR_PATH ) $$CRLF
        QMAKE_CLEAN += $$fixedPath( $${DESTINATION_DIR_PATH}/$$basename( SOURCE_FILE_PATH ) )
    }

    SHOULD_CLEAN_DESTINATION_DIR = $$3
    !isEmpty( SHOULD_CLEAN_DESTINATION_DIR ) {
        QMAKE_CLEAN += $$fixedPath( $$DESTINATION_DIR_PATH )
    }

    export( QMAKE_POST_LINK )
    export( QMAKE_CLEAN )

    return (true)
}

defineTest(deployFilesPattern) {
    SOURCE_FILES_PATTERN = $$1
    DESTINATION_DIR_PATH = $$2
    isEmpty( SOURCE_FILES_PATTERN ) | isEmpty( DESTINATION_DIR_PATH ) {
        return (false)
    }

    QMAKE_POST_LINK += $$makeDirNotExists( $$DESTINATION_DIR_PATH ) $$CRLF
    QMAKE_POST_LINK += $$copyFile( $$SOURCE_FILES_PATTERN, $$DESTINATION_DIR_PATH ) $$CRLF

    SHOULD_CLEAN_DESTINATION_DIR = $$3
    !isEmpty( SHOULD_CLEAN_DESTINATION_DIR ) {
        QMAKE_CLEAN += $$fixedPath( $$DESTINATION_DIR_PATH )
    }

    export( QMAKE_POST_LINK )
    export( QMAKE_CLEAN )

    return (true)
}

OTHER_FILES += \
    $${PWD}/QmakeHelper.py
