include( Global.pri )
include( Versions.pri )
include( QmakeHelper/QmakeHelper.pri )

# used input variables:
# ==========================
# VERSION_MAJ    (required)
# VERSION_MIN    (required)
# VERSION_PAT    (required)
# USED_LIBRARIES (required)
#
# SHOULD_DEPLOY  (optional) - explicitly enable or disable deployment

defineTest(shouldDeploy) {
    equals( SHOULD_DEPLOY, 1 ) {
        return (true)
    } else : equals( SHOULD_DEPLOY, 0 ) {
        return (false)
    }

    # default deployment
    return (false)
}

VER_MAJ = $$VERSION_MAJ
VER_MIN = $$VERSION_MIN
VER_PAT = $$VERSION_PAT
VERSION_PARTS = $$VER_MAJ $$VER_MIN $$VER_PAT
VERSION = $$versionString( VERSION_PARTS )
DEFINES *= VERSION_STRING=\\\"$$VERSION\\\"

DESTDIR = $$BUILD
OBJECTS_DIR = $${DESTDIR}/obj
MOC_DIR = $${DESTDIR}/moc
RCC_DIR = $${DESTDIR}/rcc
UI_DIR = $${DESTDIR}/ui

QMAKE_CLEAN += $$fixedPath( $${OUT_PWD}/$${BUILD}/obj/* ) \
               $$fixedPath( $${OUT_PWD}/$${BUILD}/moc/* ) \
               $$fixedPath( $${OUT_PWD}/$${BUILD}/rcc/* ) \
               $$fixedPath( $${OUT_PWD}/$${BUILD}/ui/* ) \
               $$fixedPath( $${OUT_PWD}/$${BUILD}/$${TARGET}* )

DEFAULT_DESTDIR =
win32 {
    DEFAULT_DESTDIR = $$BUILD
}

OUT_PWD_SUFFIX =
!isEmpty(DEFAULT_DESTDIR) {
    OUT_PWD_SUFFIX = /$${DEFAULT_DESTDIR}
}

TARGETFILE = $$moduleFullBinaryFileName( $$TARGET, $$VERSION_PARTS )

DEPLOY = Deploy
DEPLOYDIR = $${OUT_PWD}/../$${DEPLOY}/$${BUILD}
DEPLOY_DIR_PATHS = $$DEPLOYDIR \
                   $${OUT_PWD}/$${BUILD}

RESOURCES_DEPLOY_DIR_PATHS =
macx {
    RELATIVE_DIR_PATH =

    equals( TEMPLATE, lib ) : contains( CONFIG, lib_bundle ) {
        RELATIVE_DIR_PATH = $${TARGETFILE}/Versions/Current/Resources
    } else : equals( TEMPLATE, app ) : contains( CONFIG, app_bundle ) {
        RELATIVE_DIR_PATH = $${TARGETFILE}/Contents/MacOS
    }

    for( DEPLOY_DIR_PATH, DEPLOY_DIR_PATHS ) {
        RESOURCES_DEPLOY_DIR_PATH = $$DEPLOY_DIR_PATH
        ! isEmpty( RELATIVE_DIR_PATH ) {
            RESOURCES_DEPLOY_DIR_PATH = $${RESOURCES_DEPLOY_DIR_PATH}/$${RELATIVE_DIR_PATH}
        }

        RESOURCES_DEPLOY_DIR_PATHS += $$RESOURCES_DEPLOY_DIR_PATH
    }
} else {
    RESOURCES_DEPLOY_DIR_PATHS = $$DEPLOY_DIR_PATHS
}

QMAKE_POST_LINK += $$makeDirNotExists( $$DEPLOYDIR ) $$CRLF

# paths for binaries (libraries) and sources (include files) and linker statements
BINARY_BUILD_SUFFIX =
isDebugBuild() {
    unix: BINARY_BUILD_SUFFIX = _debug
    else: BINARY_BUILD_SUFFIX = d
}

# the library binary's naming conventions for Windows, Linux and Mac are as follows:
# () - stands for optional
# Windows:  <name>(binary_build_suffix)(ver_maj).dll
#           $${TARGET}$${VER_MAJ}.dll
# Linux:    lib<name>(binary_build_suffix).so.(ver_maj|ver_maj.ver_min|version)
#           $${QMAKE_PREFIX_SHLIB}$${TARGET}.so.$${VER_MAJ}.$${VER_MIN}.$${VER_PAT}
# Mac:      lib<name>(binary_build_suffix).(ver_maj|ver_maj.ver_min|version).dylib
#           $${QMAKE_PREFIX_SHLIB}$${TARGET}.$${VER_MAJ}.$${VER_MIN}.$${VER_PAT}.$${QMAKE_EXTENSION_SHLIB}

LIBRARIES_INFO =

unix : ! macx {
    LIBRARIES_LINUX_INSTALL_DIR_PATH = /usr/lib/$${COMPANY}/$${PRODUCT_FAMILY}
    QMAKE_RPATHDIR += $$LIBRARIES_LINUX_INSTALL_DIR_PATH
}

TRANSLATIONS_DIR_NAME = translations

# argument #1 (required, value) - file paths pattern
# argument #2 (optional, value) - deploy Qt translations
defineTest(deployTranslations) {
    return (true)

    TRANSLATION_FILE_NAME_FORMAT = $$1
    isEmpty( TRANSLATION_FILE_NAME_FORMAT ) {
        return (false)
    }

    TRANSLATIONS_REDIST_DIR_PATHS =
    for( RESOURCES_DEPLOY_DIR_PATH, RESOURCES_DEPLOY_DIR_PATHS ) {
        TRANSLATIONS_REDIST_DIR_PATHS += $${RESOURCES_DEPLOY_DIR_PATH}/$${TRANSLATIONS_DIR_NAME}
    }

    QT_BINARY_TRANSLATION_FILES_PATHS =
    SHOULD_DEPLOY_QT_TRANSLATIONS = $$2
    ! isEmpty( SHOULD_DEPLOY_QT_TRANSLATIONS ) {
        QT_BINARY_TRANSLATION_FILES_PATHS = $$qtBinaryTranslationFilesPaths()
    }

    TRANSLATIONS_SOURCE_FILE_PATHS =
    for( LOCALE_NAME, LOCALE_NAMES ) {
        TRANSLATION_FILE_PATH = $${TRANSLATION_FILE_NAME_FORMAT}_$${LOCALE_NAME}.qm
        QT_TRANSLATION_FILE_PATH = qt_$${LOCALE_NAME}.qm
        TRANSLATIONS_SOURCE_FILE_PATHS += $$TRANSLATION_FILE_PATH $$QT_TRANSLATION_FILE_PATH
    }

    for( TRANSLATIONS_REDIST_DIR_PATH, TRANSLATIONS_REDIST_DIR_PATHS ) {
        deployFiles( $$TRANSLATIONS_SOURCE_FILE_PATHS, $$TRANSLATIONS_REDIST_DIR_PATH )
        deployFiles( $$QT_BINARY_TRANSLATION_FILES_PATHS, $$TRANSLATIONS_REDIST_DIR_PATH )
    }

    QMAKE_CLEAN += $$fixedPath( $${TRANSLATIONS_REDIST_DIR_PATH} )

    unix : ! macx : ! isEmpty( COMPANY ) {
        TRANSLATIONS_DEST_DIR_PATH_INSTALL = /usr/share/$${COMPANY}/$${TRANSLATIONS_DIR_NAME}

        moduleTranslations.path = $$fixedPath( $$TRANSLATIONS_DEST_DIR_PATH_INSTALL )
        moduleTranslations.files = $$fixedPath( $$TRANSLATIONS_SOURCE_FILES_PATTERN )
        INSTALLS += moduleTranslations

        ! isEmpty( QT_BINARY_TRANSLATION_FILES_PATHS ) {
            qtTranslations.path = $$fixedPath( $$TRANSLATIONS_DEST_DIR_PATH_INSTALL )
            qtTranslations.files = $$fixedPath( $$QT_BINARY_TRANSLATION_FILES_PATHS )
            INSTALLS += qtTranslations
        }
    }

    export( QMAKE_POST_LINK )
    export( QMAKE_CLEAN )
    export( INSTALLS )

    return (true)
}

deployTranslations( $${_PRO_FILE_PWD_}/$${TARGET}, true )

USED_LIBRARIES_INFO =
for( USED_LIBRARY, USED_LIBRARIES ) {
    for( LIBRARY_INFO, LIBRARIES_INFO ) {
        equals( $${LIBRARY_INFO}.NAME, $$USED_LIBRARY ) {
            USED_LIBRARIES_INFO += $${LIBRARY_INFO}

            INCLUDEPATH += $$eval( $${LIBRARY_INFO}.INCLUDE_PATH )
            DEPENDPATH += $$eval( $${LIBRARY_INFO}.INCLUDE_PATH )

            LINKER_READY_LIBRARY_BINARY_FILE_NAME = $$eval( $${LIBRARY_INFO}.BINARY_FILE_NAME )
            win32 {
                LINKER_READY_LIBRARY_BINARY_FILE_NAME = $${LINKER_READY_LIBRARY_BINARY_FILE_NAME}$$eval( $${LIBRARY_INFO}.VER_MAJ )
            }

            LIBRARY_BINARY_DIR_PATH = $$eval( $${LIBRARY_INFO}.BINARY_DIR_PATH )
            macx : exists( $${LIBRARY_BINARY_DIR_PATH}/$${LINKER_READY_LIBRARY_BINARY_FILE_NAME}.framework ) {
                LIBS += -F$$LIBRARY_BINARY_DIR_PATH -framework $$LINKER_READY_LIBRARY_BINARY_FILE_NAME
            } else {
                LIBS += -L$$LIBRARY_BINARY_DIR_PATH -l$$LINKER_READY_LIBRARY_BINARY_FILE_NAME
            }

            DEFINES *= $$eval( $${LIBRARY_INFO}.DEFINES )

            win32 {
                FULL_LIBRARY_BINARY_FILE_NAME = $$libraryFullBinaryFileName( $$eval( $${LIBRARY_INFO}.BINARY_FILE_NAME ), $${LIBRARY_INFO}.VER_MAJ )
                LIBRARY_BINARY_SOURCE_DIR_PATH = $$eval( $${LIBRARY_INFO}.BINARY_DIR_PATH )
                FULL_LIBRARY_BINARY_FILE_PATH = $${LIBRARY_BINARY_SOURCE_DIR_PATH}/$${FULL_LIBRARY_BINARY_FILE_NAME}
                LIBRARY_TRANSLATIONS_SOURCE_DIR_PATH = $${LIBRARY_BINARY_SOURCE_DIR_PATH}/$${TRANSLATIONS_DIR_NAME}

                QMAKE_POST_LINK += IF EXIST $$fixedPath( $${FULL_LIBRARY_BINARY_FILE_PATH} ) $$copyFile( $${LIBRARY_BINARY_SOURCE_DIR_PATH}/*.dll, $$DEPLOYDIR ) $$CRLF
                QMAKE_CLEAN     += $$fixedPath( $${DEPLOYDIR}/$${FULL_LIBRARY_BINARY_FILE_NAME} )

                for( DEPLOY_DIR_PATH, DEPLOY_DIR_PATHS ) {
                    deployTranslations( $${LIBRARY_TRANSLATIONS_SOURCE_DIR_PATH}/$$eval( $${LIBRARY_INFO}.NAME ) )
                }
            }
        }

        unix : ! macx {
            QMAKE_RPATHDIR += $$eval( $${LIBRARY_INFO}.BINARY_DIR_PATH )
        }
    }
}

defineTest(isBundle) {
    equals( TEMPLATE, app ) : contains( CONFIG, app_bundle ) {
        return (true)
    }

    equals( TEMPLATE, lib ) : contains( CONFIG, lib_bundle ) {
        return (true)
    }

    return (false)
}

defineTest(deployMacModule) {
    # for mac we need to create the deploy dir as the translations dir is not under it
    QMAKE_PRE_LINK += $$makeDirNotExists( $$DEPLOYDIR ) $$CRLF

    MODULE_NAME =
    equals( TEMPLATE, app ) {
        contains( CONFIG, app_bundle ) {
            isEmpty( QMAKE_BUNDLE_EXTENSION ) {
                MODULE_NAME = $${TARGET}.app
            } else {
                MODULE_NAME = $${TARGET}.$${QMAKE_BUNDLE_EXTENSION}
            }
        } else {
            MODULE_NAME = $$TARGET
        }
    } else : equals( TEMPLATE, lib ) {
        contains( CONFIG, lib_bundle ) {
            isEmpty( QMAKE_BUNDLE_EXTENSION ) {
                MODULE_NAME = $${TARGET}.framework
            } else {
                MODULE_NAME = $${TARGET}.$${QMAKE_BUNDLE_EXTENSION}
            }
        } else {
            VERSION_PARTS = $$VER_MAJ \
                            $$VER_MIN \
                            $$VER_PAT
            MODULE_NAME = $$libraryFullBinaryFileName( $$TARGET, VERSION_PARTS )
        }
    }

    MODULE_DIR_PATH = $${OUT_PWD}/$${BUILD}
    MODULE_PATH = $${MODULE_DIR_PATH}/$${MODULE_NAME}

    isBundle() {
        QMAKE_POST_LINK += $$copyDir( $$MODULE_PATH, $${DEPLOYDIR}/$${MODULE_NAME} ) $$CRLF
    } else {
        QMAKE_POST_LINK += $$copyFile( $$MODULE_PATH, $$DEPLOYDIR ) $$CRLF

        contains( TEMPLATE, lib ) {
            VERSION_PARTS =
            SYMBOLIC_LINK_FILE_PATHS = $${DEPLOYDIR}/$$libraryFullBinaryFileName( $$TARGET, VERSION_PARTS )
            VERSION_PARTS += $$VER_MAJ
            SYMBOLIC_LINK_FILE_PATHS += $${DEPLOYDIR}/$$libraryFullBinaryFileName( $$TARGET, VERSION_PARTS )
            VERSION_PARTS += $$VER_MIN
            SYMBOLIC_LINK_FILE_PATHS += $${DEPLOYDIR}/$$libraryFullBinaryFileName( $$TARGET, VERSION_PARTS )

            for( SYMBOLIC_LINK_FILE_PATH, SYMBOLIC_LINK_FILE_PATHS ) {
                QMAKE_POST_LINK += $$symbolicLink( $$MODULE_NAME, $$SYMBOLIC_LINK_FILE_PATH ) $$CRLF
                QMAKE_CLEAN += $$fixedPath( $$SYMBOLIC_LINK_FILE_PATH )
            }
        }
    }

    MAC_DEPLOY_QT_ADDITIONAL_LIBRARY_PATHS =
    for( LIBRARY_INFO, LIBRARIES_INFO ) {
        MAC_DEPLOY_QT_ADDITIONAL_LIBRARY_PATHS += $$fixedPath( $$eval( $${LIBRARY_INFO}.BINARY_DIR_PATH ) )
    }

    MODULE_TO_DEPLOY_PATHS =
    equals( TEMPLATE, lib ) : ! isBundle() :
    else : MODULE_TO_DEPLOY_PATHS = $$MODULE_PATH

    MODULE_TO_DEPLOY_PATHS += $${DEPLOYDIR}/$${MODULE_NAME}

    for( MODULE_TO_DEPLOY_PATH, MODULE_TO_DEPLOY_PATHS ) {
        MAC_DEPLOY_QT_COMMAND = $$fixedPath( $${OUT_PWD}/../ThirdParty/macdeployqt/macdeployqt/macdeployqt ) \
                                $$fixedPath( $$MODULE_TO_DEPLOY_PATH )

        !isEmpty( MAC_DEPLOY_QT_ADDITIONAL_LIBRARY_PATHS ) {
            MAC_DEPLOY_QT_COMMAND += -additional-library-search-paths=$$join( MAC_DEPLOY_QT_ADDITIONAL_LIBRARY_PATHS, : )
        }

        isDebugBuild() {
            MAC_DEPLOY_QT_COMMAND += -use-debug-libs
        }

        QMAKE_POST_LINK += $$MAC_DEPLOY_QT_COMMAND $$CRLF
    }

    QMAKE_CLEAN += $$fixedPath( $${DEPLOYDIR}/$${MODULE_NAME} )
    QMAKE_CLEAN += $$fixedPath( $$MODULE_PATH )

    export( QMAKE_PRE_LINK )
    export( QMAKE_POST_LINK )
    export( QMAKE_CLEAN )
}

defineTest(deployWinModule) {
    USED_LIBRARIES_BINARY_DIR_PATHS =
    for( USED_LIBRARY, USED_LIBRARIES ) {
        for( LIBRARY_INFO, LIBRARIES_INFO ) {
            equals( $${LIBRARY_INFO}.NAME, $$USED_LIBRARY ) {
                USED_LIBRARIES_BINARY_DIR_PATHS += $$eval( $${LIBRARY_INFO}.BINARY_DIR_PATH )
            }
        }
    }

    ADDITIONAL_LIBRARY_SEARCH_DIR_PATHS = /i \"$$[QT_INSTALL_BINS];$$[QT_INSTALL_LIBS];$$join( USED_LIBRARIES_BINARY_DIR_PATHS, ; )\"
    DEPLOY_DIR_PATHS = $$DEPLOYDIR

    for( DEPLOY_DIR_PATH, DEPLOY_DIR_PATHS ) {
        QMAKE_POST_LINK += $$fixedPath( $${OUT_PWD}/../DeployQt/$${BUILD}/DeployQt ) $$fixedPath( $${DEPLOY_DIR_PATH}/$${TARGETFILE} ) $$ADDITIONAL_LIBRARY_SEARCH_DIR_PATHS $$CRLF
    }

    export( QMAKE_POST_LINK )
}

contains( TEMPLATE, app ) {
    unix : !macx {
        target.path = /usr/bin
        INSTALLS    += target
    } else: macx {
        deployMacModule()
    }

    ! macx {
        # output the executable file
        EXEPATH     =  $${OUT_PWD}/$${BUILD}/$${TARGETFILE}
        QMAKE_CLEAN += $$fixedPath( $$EXEPATH )

        QMAKE_POST_LINK += $$copyFile( $$EXEPATH, $$DEPLOYDIR ) $$CRLF

        QMAKE_CLEAN += $$fixedPath( $${DEPLOYDIR}/$${TARGETFILE} )

        win32 : !equals( TARGET, DeployQt ) : shouldDeploy() {
            deployWinModule()
        }
    }
}

contains( TEMPLATE, lib ) {
    # for windows copy the LIBRARY_ with the major version only
    win32 {
        LIBRARY_FILE_PATH =  $${OUT_PWD}/$${BUILD}/$${TARGETFILE}
        QMAKE_POST_LINK   += $$copyFile( $$LIBRARY_FILE_PATH, $$DEPLOYDIR ) $$CRLF
        QMAKE_CLEAN       += $$fixedPath( $${DEPLOYDIR}/$${TARGETFILE} )
        QMAKE_CLEAN       += $$fixedPath( $$LIBRARY_FILE_PATH )

        shouldDeploy() {
            deployWinModule()
        }
    } else : unix : !macx {
        VERSION_PARTS = $$VER_MAJ \
                        $$VER_MIN \
                        $$VER_PAT
        LIBRARY_FULL_VERSION_FILE_NAME = $$libraryFullBinaryFileName( $${TARGET}, VERSION_PARTS )
        QMAKE_POST_LINK += $$copyFile( $${OUT_PWD}/$${BUILD}/$${LIBRARY_FULL_VERSION_FILE_NAME}, $$DEPLOYDIR ) $$CRLF

        VERSION_PARTS =
        SYMBOLIC_LINK_FILE_NAMES = $$libraryFullBinaryFileName( $$TARGET, VERSION_PARTS )
        VERSION_PARTS += $$VER_MAJ
        SYMBOLIC_LINK_FILE_NAMES += $$libraryFullBinaryFileName( $$TARGET, VERSION_PARTS )
        VERSION_PARTS += $$VER_MIN
        SYMBOLIC_LINK_FILE_NAMES += $$libraryFullBinaryFileName( $$TARGET, VERSION_PARTS )

        for( SYMBOLIC_LINK_FILE_NAME, SYMBOLIC_LINK_FILE_NAMES ) {
            QMAKE_POST_LINK += $$symbolicLink( $$LIBRARY_FULL_VERSION_FILE_NAME, $${DEPLOYDIR}/$${SYMBOLIC_LINK_FILE_NAME} ) $$CRLF
            QMAKE_CLEAN += $$fixedPath( $${DEPLOYDIR}/$${SYMBOLIC_LINK_FILE_NAME} )
        }

        QMAKE_CLEAN += $$fixedPath( $${DEPLOYDIR}/$$libraryFullBinaryFileName( $${TARGET},  )* )

        target.path = $$LIBRARIES_LINUX_INSTALL_DIR_PATH
        INSTALLS += target
    } else : macx {
        deployMacModule()
    }
}

win32 | macx {
    include( RC/RC.pri )
}
