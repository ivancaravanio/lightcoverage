#include "Logger.h"

#include <QCoreApplication>
#include <QFileInfo>
#include <QTextStream>

using namespace DeployQt;

Logger::Logger()
{
}

void Logger::log( const QString& message )
{
    static QTextStream logStream( stdout );
    static const QString APPLICATION_FILE_BASE_NAME = QFileInfo( QCoreApplication::applicationFilePath() ).baseName();

    logStream << APPLICATION_FILE_BASE_NAME << ": " << message;
    if ( ! message.endsWith( '\n' ) && ! message.endsWith( '\r' ) )
    {
        logStream << endl;
    }
}
