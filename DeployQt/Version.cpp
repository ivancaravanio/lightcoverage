#include "Version.h"

#include <QStringList>

using namespace DeployQt;

const int Version::INVALID_VERSION_PART_VALUE = -1;

const QChar Version::VERSION_PARTS_SEPARATOR( QLatin1Char( '.' ) );

Version::Version()
    : m_major( INVALID_VERSION_PART_VALUE ),
      m_minor( INVALID_VERSION_PART_VALUE ),
      m_patch( INVALID_VERSION_PART_VALUE ),
      m_build( INVALID_VERSION_PART_VALUE )
{
}

Version::Version( const int major, const int minor, const int patch, const int build )
    : m_major( INVALID_VERSION_PART_VALUE ),
      m_minor( INVALID_VERSION_PART_VALUE ),
      m_patch( INVALID_VERSION_PART_VALUE ),
      m_build( INVALID_VERSION_PART_VALUE )
{
    this->set( major, minor, patch, build );
}

Version::Version( const QString& s )
    : m_major( INVALID_VERSION_PART_VALUE ),
      m_minor( INVALID_VERSION_PART_VALUE ),
      m_patch( INVALID_VERSION_PART_VALUE ),
      m_build( INVALID_VERSION_PART_VALUE )
{
    this->set( s );
}

int Version::major() const
{
    return m_major;
}

int Version::minor() const
{
    return m_minor;
}

int Version::patch() const
{
    return m_patch;
}

int Version::build() const
{
    return m_build;
}

bool Version::isValid() const
{
    return Version::isValid( m_major,
                             m_minor,
                             m_patch );
}

QString Version::toString() const
{
    if ( ! this->isValid() )
    {
        return QString();
    }

    const int versionParts[] =
    {
        m_major,
        m_minor,
        m_patch,
        m_build
    };

    QStringList versionPartStrings;
    const int versionPartsCount = sizeof( versionParts ) / sizeof( versionParts[ 0 ] );
    versionPartStrings.reserve( versionPartsCount );
    for ( int i = 0; i < versionPartsCount; ++ i )
    {
        const int versionNumber = versionParts[ i ];
        if ( versionNumber < 0 )
        {
            break;
        }

        versionPartStrings.append( QString::number( versionNumber ) );
    }

    return versionPartStrings.join( VERSION_PARTS_SEPARATOR );
}

void Version::set( const int major, const int minor, const int patch, const int build )
{
    if ( ! Version::isValid( major, minor, patch ) )
    {
        return;
    }

    m_major = major;
    m_minor = minor;
    m_patch = patch;

    if ( ! Version::isVersionPartValid( build ) )
    {
        return;
    }

    m_build = build;
}

void Version::set( const QString& s )
{
    if ( s.isEmpty() )
    {
        return;
    }

    const QStringList versionPartStrings = s.split( VERSION_PARTS_SEPARATOR );
    const int versionPartsCount = versionPartStrings.size();
    if ( versionPartsCount < VersionPartIndicesCount - 1
         || versionPartsCount > VersionPartIndicesCount )
    {
        return;
    }

    int versionParts[ VersionPartIndicesCount ] = { INVALID_VERSION_PART_VALUE };
    for ( int i = 0; i < versionPartsCount; ++i )
    {
        bool converted = false;
        const int versionPart = versionPartStrings[ i ].toInt( &converted );
        if ( i <= VersionPartIndexPatch
             && ( ! converted || ! Version::isVersionPartValid( versionPart ) ) )
        {
            return;
        }

        versionParts[ i ] = versionPart;
    }

    this->set( versionParts[ VersionPartIndexMajor ],
               versionParts[ VersionPartIndexMinor ],
               versionParts[ VersionPartIndexPatch ],
               versionParts[ VersionPartIndexBuild ] );
}

bool Version::isValid( const int major, const int minor, const int patch )
{
    return Version::isVersionPartValid( major )
           && Version::isVersionPartValid( minor )
           && Version::isVersionPartValid( patch );
}

bool Version::isVersionPartValid( const int versionNumber )
{
    return versionNumber >= 0;
}
