#ifndef DEPLOYQT_DEPLOYQTPRECOMPILED_H
#define DEPLOYQT_DEPLOYQTPRECOMPILED_H

#ifdef Q_OS_WIN
#include <Windows.h>
#endif // Q_OS_WIN

#ifdef __cplusplus

#include <limits>

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QFlags>
#include <QList>
#include <QPair>
#include <QProcess>
#include <QScopedArrayPointer>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <QUuid>

#endif // __cplusplus

#endif // DEPLOYQT_DEPLOYQTPRECOMPILED_H
