#ifndef DEPLOYQT_MODULEINFO_H
#define DEPLOYQT_MODULEINFO_H

#include "Version.h"

#include <QtGlobal>
#include <QString>

#ifdef Q_OS_WIN

namespace DeployQt {

class ModuleInfo
{
public:
    enum Build
    {
        BuildInvalid,
        BuildDebug,
        BuildRelease
    };

    enum Type
    {
        TypeInvalid,
        TypeApplication,
        TypeStaticLinkLibrary,
        TypeDynamicLinkLibrary
    };

    ModuleInfo();
    explicit ModuleInfo( const QString& inFilePath );

    QString filePath() const;

    bool isValid() const;

    Type type() const;
    Build build() const;
    Version version() const;

private:
    static bool moduleInfo( const QString& inFilePath,
                            Type& outType,
                            Build& outBuild,
                            Version& outVersion );

private:
    QString m_filePath;
    Type    m_type;
    Build   m_build;
    Version m_version;
};

}

#endif // Q_OS_WIN

#endif // DEPLOYQT_MODULEINFO_H
