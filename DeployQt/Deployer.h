#ifndef DEPLOYQT_DEPLOYER_H
#define DEPLOYQT_DEPLOYER_H

#include <QtGlobal>

#include <QDir>
#include <QFileInfo>
#include <QFlags>
#include <QFutureWatcher>
#include <QHash>
#include <QObject>
#include <QPair>
#include <QQueue>
#include <QSet>
#include <QString>
#include <QStringList>
#include <QTime>

namespace DeployQt {

class Deployer : public QObject
{
    Q_OBJECT

public:
    Deployer( QObject* const parent = nullptr );
    ~Deployer() /* override */;

    void deploy( const QString& targetFilePath, const QStringList& additionalLibraryIncludeDirPaths = QStringList() );

signals:
    void deployed( const QString& targetFilePath, const QStringList& additionalLibraryIncludeDirPaths );

private:
    enum DependencyWalkerResult
    {
        // Module Warnings - Application should load, but might fail during runtime.
        DependencyWalkerResultModuleWarning1 = 0x00000001, // At least one dynamic dependency module was not found.
        DependencyWalkerResultModuleWarning2 = 0x00000002, // At least one delay-load dependency module was not found.
        DependencyWalkerResultModuleWarning3 = 0x00000004, // At least one module could not dynamically locate a function in another module using the GetProcAddress function call.
        DependencyWalkerResultModuleWarning4 = 0x00000008, // At least one module has an unresolved import due to a missing export function in a delay-load dependent module.
        DependencyWalkerResultModuleWarning5 = 0x00000010, // At least one module was corrupted or unrecognizable to Dependency Walker, but still appeared to be a Windows module.
        DependencyWalkerResultModuleWarning6 = 0x00000020, // At least one module failed to load during profiling. This usually occurs when a module returns 0 from its DllMain function or generates an unhandled exception while processing the DLL_PROCESS_ATTACH message.

        // Module Errors - Application will fail to load by the operating system.
        DependencyWalkerResultModuleError1 = 0x00000100, // At least one file was not a 32-bit or 64-bit Windows module.
        DependencyWalkerResultModuleError2 = 0x00000200, // At least one required implicit or forwarded dependency was not found.
        DependencyWalkerResultModuleError3 = 0x00000400, // At least one module has an unresolved import due to a missing export function in a dependent module.
        DependencyWalkerResultModuleError4 = 0x00000800, // Modules with different CPU types were found.
        DependencyWalkerResultModuleError5 = 0x00001000, // A circular dependency was detected.
        DependencyWalkerResultModuleError6 = 0x00002000, // There was an error in a Side-by-Side configuration file.

        // Processing Errors - All or some modules could not be processed.
        DependencyWalkerResultProcessingError1 = 0x00010000, // There was an error with at least one command line option.
        DependencyWalkerResultProcessingError2 = 0x00020000, // The file you specified to load could not be found.
        DependencyWalkerResultProcessingError3 = 0x00040000, // At least one file could not be opened for reading.
        DependencyWalkerResultProcessingError4 = 0x00080000, // The format of the Dependency Walker Image (DWI) file was unrecognized.
        DependencyWalkerResultProcessingError5 = 0x00100000, // There was an error while trying to profile the application.
        DependencyWalkerResultProcessingError6 = 0x00200000, // There was an error writing the results to an output file.
        DependencyWalkerResultProcessingError7 = 0x00400000, // Dependency Walker ran out of memory.
        DependencyWalkerResultProcessingError8 = 0x00800000 // Dependency Walker encountered an internal program error.
    };
    Q_DECLARE_FLAGS( DependencyWalkerResults, DependencyWalkerResult )

private:
    static const QString QT_LIBRARY_REG_EXP_TEMPLATE;
    static const QString LIBRARY_DEBUG_REG_EXP_TEMPLATE;
    static const QString LIBRARY_RELEASE_REG_EXP_TEMPLATE;
    static const QString QT_CORE_LIBRARY_BASE_NAME;
    static const QString PLUGINS_DIR_NAME;

private slots:
    void handleLevel2DependencyResolved();

private:
    static QFileInfo qtCoreFileInfo( const QStringList& inDependenciesFilePaths );
    static QDir qtFrameworkDir( const QFileInfo& inQtCoreFileInfo );
    static QDir qtLibDir( const QFileInfo& inQtCoreFileInfo );
    static QDir qtBinDir( const QFileInfo& inQtCoreFileInfo );
    static QDir qtPluginsDir( const QFileInfo& inQtCoreFileInfo );
    static bool qtVersionBuild( const QString& qtCoreLibraryFileName, int& qtMajorVersion, bool& isDebug );

    static bool isDependencyWalkerResultOk( const int dependencyWalkerResult );
    static QStringList allDependenciesFilePaths(const QString& targetFilePath, const QStringList& additionalLibraryIncludeDirPaths = QStringList() );
    static QStringList filteredDependenciesFilePaths( const QStringList& inDependenciesFilePaths, const QStringList& additionalLibraryIncludeDirPaths = QStringList() );
    static QStringList filteredDependenciesFilePaths( const QString& targetFilePath, const QStringList& additionalLibraryIncludeDirPaths = QStringList() );
    static QList< QPair< QString, QString > > pluginsDelayLoadedDependenciesPaths( const QStringList& inDependenciesFilePaths, const QStringList& additionalLibraryIncludeDirPaths = QStringList() );
    void redistribute();
    void cleanupReportDeployed( const QString& targetFilePath, const QStringList& additionalLibraryIncludeDirPaths );
    void addDependencyFilePaths( const QStringList& filePaths );
    void addDependencyFilePaths( const QList< QPair< QString, QString > >& filePathSubDirNameMappings );
    void addDependencyFilePath( const QString& filePath, const QString& subDirName = QString() );
    static QString canonicalFilePath( const QFileInfo& fileInfo );

private:
    QQueue< QPair< QString, QStringList > >     m_deploymentRequests;
    QHash< QString, QPair< QString, QString > > m_allDependencies;
    QSet< QFutureWatcher< QStringList >* >      m_level2DependenciesResolvers;
    QTime                                       m_deploymentTime;
};

}

#endif // DEPLOYQT_DEPLOYER_H
