#ifndef DEPLOYQT_VERSION_H
#define DEPLOYQT_VERSION_H

#include <QString>

namespace DeployQt {

class Version
{
public:
    static const int INVALID_VERSION_PART_VALUE;

public:
    Version();
    Version( const int major, const int minor, const int patch, const int build = -1 );
    explicit Version( const QString& s );

    int major() const;
    int minor() const;
    int patch() const;
    int build() const;

    bool isValid() const;

    QString toString() const;

    void set( const int major, const int minor, const int patch, const int build = -1 );
    void set( const QString& s );

private:
    enum VersionPartIndex
    {
        VersionPartIndexInvalid = -1,
        VersionPartIndexMajor = 0,
        VersionPartIndexMinor = 1,
        VersionPartIndexPatch = 2,
        VersionPartIndexBuild = 3,
        VersionPartIndicesCount = 4
    };

private:
    static bool isValid( const int major, const int minor, const int patch );
    static bool isVersionPartValid( const int versionNumber );

private:
    static const QChar VERSION_PARTS_SEPARATOR;

    int m_major;
    int m_minor;
    int m_patch;
    int m_build;
};

}

#endif // DEPLOYQT_VERSION_H

