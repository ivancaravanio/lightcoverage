#include "Deployer.h"

#include "Logger.h"
#include "ModuleInfo.h"

#include <QCoreApplication>
#include <QDateTime>
#include <QDir>
#include <QFileInfo>
#include <QFuture>
#include <QList>
#include <QPair>
#include <QProcess>
#include <QString>
#include <QStringBuilder>
#if QT_VERSION >= QT_VERSION_CHECK( 5, 0, 0 )
    #include <QtConcurrent/QtConcurrentRun>
#else
    #include <QtConcurrentRun>
#endif
#include <QTextStream>
#include <QUuid>

// #define USE_PROCESS_ENVIRONMENT
#ifndef USE_PROCESS_ENVIRONMENT
    #include <limits>
    #include <Windows.h>
#endif

// The official Qt Windows deployment guidelines:
// http://doc.qt.io/qt-5/windows-deployment.html

using namespace DeployQt;

const QString Deployer::QT_LIBRARY_REG_EXP_TEMPLATE = ".*[/\\\\]?Qt(%1d?[0-9]+|[0-9]+%1d?)\\.dll$";
const QString Deployer::LIBRARY_DEBUG_REG_EXP_TEMPLATE( "^%1d\\.dll$" );
const QString Deployer::LIBRARY_RELEASE_REG_EXP_TEMPLATE( "^%1\\.dll$" );
const QString Deployer::QT_CORE_LIBRARY_BASE_NAME( "Core" );
const QString Deployer::PLUGINS_DIR_NAME( "plugins" );

Deployer::Deployer( QObject* const parent )
    : QObject( parent )
{
}

Deployer::~Deployer()
{
}

void Deployer::deploy( const QString& targetFilePath, const QStringList& additionalLibraryIncludeDirPaths )
{
    const auto cleanerDeploymentFinishedReporter = [this, targetFilePath, additionalLibraryIncludeDirPaths]()
    {
        this->cleanupReportDeployed( targetFilePath, additionalLibraryIncludeDirPaths );
    };

    if ( ! m_deploymentRequests.isEmpty() )
    {
        cleanerDeploymentFinishedReporter();
        return;
    }

    const QFileInfo targetFileInfo( targetFilePath );
    if ( ! targetFileInfo.exists() )
    {
        cleanerDeploymentFinishedReporter();
        return;
    }

    if ( m_deploymentTime.elapsed() > 0 )
    {
        m_deploymentTime.restart();
    }
    else
    {
        m_deploymentTime.start();
    }

    const QString targetCanonicalFilePath = Deployer::canonicalFilePath( targetFileInfo );
    Logger::log( "Started deploying \"" % targetCanonicalFilePath % "\" ..." );

    m_deploymentRequests.enqueue( qMakePair( targetCanonicalFilePath, additionalLibraryIncludeDirPaths ) );

    Logger::log( "Analyzing \"" % targetCanonicalFilePath % "\" ..." );
    const QStringList level1Dependencies = Deployer::filteredDependenciesFilePaths( targetCanonicalFilePath, additionalLibraryIncludeDirPaths );
    if ( level1Dependencies.isEmpty() )
    {
        m_deploymentRequests.dequeue();
        cleanerDeploymentFinishedReporter();
        return;
    }

    m_allDependencies.clear();
    this->addDependencyFilePaths( level1Dependencies );

    const QList< QPair< QString, QString > > pluginsDelayLoadedDependencies = Deployer::pluginsDelayLoadedDependenciesPaths( level1Dependencies, additionalLibraryIncludeDirPaths );
    this->addDependencyFilePaths( pluginsDelayLoadedDependencies );

    const int allDependenciesCount = m_allDependencies.size();
    Logger::log( QString( "Found %1 %2:" ).arg( allDependenciesCount ).arg( allDependenciesCount == 0 ? "dependency" : "dependencies" ) );

    int i = 0;
    m_level2DependenciesResolvers.reserve( allDependenciesCount );

    const QHash< QString, QPair< QString, QString > >::const_iterator allDependenciesIterEnd = m_allDependencies.constEnd();
    for ( QHash< QString, QPair< QString, QString > >::const_iterator allDependenciesIter = m_allDependencies.constBegin();
          allDependenciesIter != allDependenciesIterEnd;
          ++ allDependenciesIter )
    {
        const QPair< QString, QString >& filePathSubDirName = allDependenciesIter.value();

        Logger::log( QString( "Analyzing dependency #%1: \"%2\" ..." )
                     .arg( ++ i )
                     .arg( filePathSubDirName.first ) );
        const QFuture< QStringList > futureResolvedDependencies = QtConcurrent::run( static_cast< QStringList (*)( const QString&, const QStringList& ) >( & Deployer::filteredDependenciesFilePaths ),
                                                                                     filePathSubDirName.first,
                                                                                     additionalLibraryIncludeDirPaths );
        QFutureWatcher< QStringList >* const watcher = new QFutureWatcher< QStringList >( this );
        connect( watcher, SIGNAL(finished()), this, SLOT(handleLevel2DependencyResolved()) );
        m_level2DependenciesResolvers.insert( watcher );
        watcher->setFuture( futureResolvedDependencies );
    }
}

void Deployer::handleLevel2DependencyResolved()
{
    QObject* const senderObj = this->sender();
    if ( senderObj == nullptr )
    {
        return;
    }

    QFutureWatcher< QStringList >* const level2DependencyResolver = dynamic_cast< QFutureWatcher< QStringList >* >( senderObj );
    if ( level2DependencyResolver == nullptr )
    {
        return;
    }

    const QSet< QFutureWatcher< QStringList >* >::iterator level2DependencyResolverIter = m_level2DependenciesResolvers.find( level2DependencyResolver );
    if ( level2DependencyResolverIter == m_level2DependenciesResolvers.end() )
    {
        return;
    }

    this->addDependencyFilePaths( level2DependencyResolver->result() );
    Logger::log( QString( "%1 dependencies left to go." )
                 .arg( m_level2DependenciesResolvers.size() ) );

    m_level2DependenciesResolvers.erase( level2DependencyResolverIter );
    level2DependencyResolver->deleteLater();

    if ( m_level2DependenciesResolvers.isEmpty() )
    {
        this->redistribute();
    }
}

bool Deployer::isDependencyWalkerResultOk( const int dependencyWalkerResult )
{
    return ! ( DependencyWalkerResults( dependencyWalkerResult )
               & ( DependencyWalkerResults( DependencyWalkerResultProcessingError1 )
                   | DependencyWalkerResultProcessingError2
                   | DependencyWalkerResultProcessingError3
                   | DependencyWalkerResultProcessingError4
                   | DependencyWalkerResultProcessingError5
                   | DependencyWalkerResultProcessingError6
                   | DependencyWalkerResultProcessingError7
                   | DependencyWalkerResultProcessingError8 ) );
}

QFileInfo Deployer::qtCoreFileInfo( const QStringList& inDependenciesFilePaths )
{
    static const QRegExp QT_CORE_LIBRARY_REG_EXP( QT_LIBRARY_REG_EXP_TEMPLATE.arg( QT_CORE_LIBRARY_BASE_NAME ), Qt::CaseInsensitive );
    const int index = inDependenciesFilePaths.indexOf( QT_CORE_LIBRARY_REG_EXP );
    if ( index == -1 )
    {
        Logger::log( "QtCore library not found as a dependency." );

        return QFileInfo();
    }

    const QFileInfo qtCoreFileInfo( inDependenciesFilePaths[ index ] );
    if ( ! qtCoreFileInfo.exists() )
    {
        Logger::log( "QtCore library file does not exist." );

        return QFileInfo();
    }

    return qtCoreFileInfo;
}

QDir Deployer::qtFrameworkDir( const QFileInfo& inQtCoreFileInfo )
{
    QDir frameworkDir = Deployer::qtLibDir( inQtCoreFileInfo );
    if ( ! frameworkDir.cdUp() )
    {
        return QDir();
    }

    return frameworkDir;
}

QDir Deployer::qtLibDir( const QFileInfo& inQtCoreFileInfo )
{
    return inQtCoreFileInfo.dir();
}

QDir Deployer::qtBinDir( const QFileInfo& inQtCoreFileInfo )
{
    QDir binDir = Deployer::qtFrameworkDir( inQtCoreFileInfo );

    if ( ! binDir.cd( "bin" ) )
    {
        return QDir();
    }

    return binDir;
}

QDir Deployer::qtPluginsDir( const QFileInfo& inQtCoreFileInfo )
{
    QDir pluginsDir = Deployer::qtFrameworkDir( inQtCoreFileInfo );

    if ( ! pluginsDir.cd( PLUGINS_DIR_NAME ) )
    {
        return QDir();
    }

    return pluginsDir;
}

QStringList Deployer::allDependenciesFilePaths( const QString& targetFilePath, const QStringList& additionalLibraryIncludeDirPaths )
{
    const QFileInfo targetFileInfo( targetFilePath );
    if ( ! targetFileInfo.exists() )
    {
        Logger::log( "target file \"" % targetFilePath % "\" not found." );

        return QStringList();
    }

    const QDir appDir = qApp->applicationDirPath();
    QString dependsOutputFileNameSuffix = QUuid::createUuid().toString();
    dependsOutputFileNameSuffix.chop( 1 );
    dependsOutputFileNameSuffix.remove( 0, 1 );
    const QString dependsOutputFilePath = appDir.filePath( QString( "dependencyTable_%1.csv" ).arg( dependsOutputFileNameSuffix ) );
    QFile dependsOutputFile( dependsOutputFilePath );
    if ( dependsOutputFile.exists() )
    {
        dependsOutputFile.remove();
    }

#ifdef USE_PROCESS_ENVIRONMENT
    QProcess process;
    if ( ! additionalLibraryIncludeDirPaths.isEmpty() )
    {
        QProcessEnvironment processEnvironment = process.processEnvironment();
        processEnvironment.insert( QProcessEnvironment::systemEnvironment() );
        const QLatin1Char pathSeparator( ';' );
        const QString pathEnvironmentVariableName = "PATH";
        QString pathEnvironmentVariableValue = processEnvironment.value( pathEnvironmentVariableName );
        if ( ! pathEnvironmentVariableValue.isEmpty() )
        {
            pathEnvironmentVariableValue += pathSeparator;
        }

        pathEnvironmentVariableValue += additionalLibraryIncludeDirPaths.join( pathSeparator );
        processEnvironment.insert( pathEnvironmentVariableName, pathEnvironmentVariableValue );
        process.setProcessEnvironment( processEnvironment );
    }

    const QString dependenciesAnalyzerToolFilePath = QFileInfo( appDir, DEPENDS_FILE_NAME ).canonicalFilePath();
    QStringList args;
    args << "/c"
         << "/f:1"
         << "/ph:1"
         << "/pl:1"
         << "/pg:1"
         << ( "/oc:" + dependsOutputFilePath )
         << targetFilePath;
    const int dependencyWalkerResult = process.execute( dependenciesAnalyzerToolFilePath,
                                                        args );
#else
    static bool environmentUpdated = false;
    if ( ! environmentUpdated )
    {
#ifdef max
#undef max
#endif

        const int environmentVariableValueMaxByteCount = std::numeric_limits< qint16 >::max();
        QByteArray pathEnvironmentVariableValue( environmentVariableValueMaxByteCount, '\0' );
        const char PATH_ENVIRONMENT_VARIABLE_NAME[] = "PATH";
        const int charactersCount = ::GetEnvironmentVariableA( PATH_ENVIRONMENT_VARIABLE_NAME, pathEnvironmentVariableValue.data(), pathEnvironmentVariableValue.size() );
        if ( charactersCount >= 0 && ::GetLastError() != ERROR_ENVVAR_NOT_FOUND )
        {
            pathEnvironmentVariableValue.truncate( charactersCount );
            static const char ENV_VARIABLE_VALUES_SEPARATOR = ';';
            pathEnvironmentVariableValue.append( ENV_VARIABLE_VALUES_SEPARATOR );
            pathEnvironmentVariableValue += additionalLibraryIncludeDirPaths.join( QChar( QLatin1Char( ENV_VARIABLE_VALUES_SEPARATOR ) ) );
            if ( pathEnvironmentVariableValue.size() <= environmentVariableValueMaxByteCount )
            {
                if ( ::SetEnvironmentVariableA( PATH_ENVIRONMENT_VARIABLE_NAME, pathEnvironmentVariableValue.constData() ) == FALSE )
                {
                    Logger::log( QString( "Failed to set %1 environment variable\'s value." ).arg( PATH_ENVIRONMENT_VARIABLE_NAME ) );
                }
            }
            else
            {
                Logger::log( QString( "The updated %1 environment variable\'s value exceeds the maximum of %2 bytes." )
                             .arg( PATH_ENVIRONMENT_VARIABLE_NAME )
                             .arg( environmentVariableValueMaxByteCount ) );
            }
        }
        else
        {
            Logger::log( QString( "Cannot retrieve %1 environment variable\'s value." ).arg( PATH_ENVIRONMENT_VARIABLE_NAME ) );
        }

        environmentUpdated = true;
    }

    const int dependencyWalkerResult =
            QProcess::execute(
                QFileInfo( appDir, DEPENDS_FILE_NAME ).absoluteFilePath(),
                QStringList()
                << "/c"
                << "/f:1"
                << "/ph:1"
                << "/pl:1"
                << "/pg:1"
                << ( "/oc:" + dependsOutputFilePath )
                << targetFilePath );
#endif
    if ( ! isDependencyWalkerResultOk( dependencyWalkerResult ) )
    {
        Logger::log( "target file \"" % targetFilePath % "\" not found." );

        return QStringList();
    }

    if ( ! dependsOutputFile.exists() )
    {
        Logger::log( "the generated temporary file that contain the target's dependencies - \""
                     % dependsOutputFilePath
                     % "\", was not found." );

        return QStringList();
    }

    if ( ! dependsOutputFile.open( QIODevice::ReadOnly ) )
    {
        Logger::log( "the generated temporary file that contain the target's dependencies - \""
                     % dependsOutputFilePath
                     % "\", exists but cannot be opened for reading." );

        return QStringList();
    }

    QStringList filePaths;

    // column headers line
    dependsOutputFile.readLine();
    // target file line
    dependsOutputFile.readLine();
    while ( ! dependsOutputFile.atEnd() )
    {
        QString dependencyFilePath = QString::fromLatin1( dependsOutputFile.readLine() ).split( QLatin1Char( ',' ) ).at( 1 );
        if ( dependencyFilePath.isEmpty() )
        {
            continue;
        }

        dependencyFilePath.remove( QChar( QLatin1Char( '\"' ) ) );
        const QFileInfo dependencyFileInfo( dependencyFilePath );
        if ( dependencyFileInfo.exists() )
        {
            filePaths.append( Deployer::canonicalFilePath( dependencyFileInfo ) );
        }
    }

    if ( filePaths.isEmpty() )
    {
        Logger::log( "not a single dependency was found." );
    }

    dependsOutputFile.close();
    dependsOutputFile.remove();

    return filePaths;
}

QStringList Deployer::filteredDependenciesFilePaths( const QStringList& inDependenciesFilePaths, const QStringList& additionalLibraryIncludeDirPaths )
{
    if ( inDependenciesFilePaths.isEmpty() )
    {
        return QStringList();
    }

    const QFileInfo coreFileInfo = Deployer::qtCoreFileInfo( inDependenciesFilePaths );
    if ( ! coreFileInfo.exists() )
    {
        return QStringList();
    }

    const ModuleInfo qtCoreModuleInfo( coreFileInfo.filePath() );
    if ( ! qtCoreModuleInfo.isValid() )
    {
        return QStringList();
    }

    const QDir libDir = Deployer::qtLibDir( coreFileInfo );
    const QDir binDir = Deployer::qtBinDir( coreFileInfo );

    QList< QDir > dependencySearchDirs;
    if ( libDir.exists() )
    {
        dependencySearchDirs.append( libDir );
    }

    if ( libDir.exists() )
    {
        dependencySearchDirs.append( binDir );
    }

    foreach ( const QString& additionalLibraryIncludeDirPath, additionalLibraryIncludeDirPaths )
    {
        const QDir additionalLibraryIncludeDir( additionalLibraryIncludeDirPath );
        if ( additionalLibraryIncludeDir.exists() )
        {
            dependencySearchDirs.append( additionalLibraryIncludeDir );
        }
    }

    const bool isDebugBuild = qtCoreModuleInfo.build() == ModuleInfo::BuildDebug;

    QStringList filteredDepsFilePaths;
    foreach ( const QString& dependencyFilePath, inDependenciesFilePaths )
    {
        const QFileInfo dependencyFileInfo( dependencyFilePath );
        if ( ! dependencyFileInfo.exists() )
        {
            Logger::log( "the dependency library \"" % dependencyFilePath % "\" does not exist." );

            continue;
        }

        const QDir dependencyDir = dependencyFileInfo.dir();
        const QRegExp stdLibrariesRegExp( "^(msvc|mingwm|libgcc).*\\.dll$", Qt::CaseInsensitive );
        const QRegExp msvcRegExp( "^msvc(r|p)\\d+d?.dll$", Qt::CaseInsensitive );

        const QString dependencyFileName = dependencyFileInfo.fileName();
        if ( dependencySearchDirs.contains( dependencyDir )
             || stdLibrariesRegExp.exactMatch( dependencyFileName ) )
        {
            if ( msvcRegExp.exactMatch( dependencyFileName )
                 && ( isDebugBuild ^ dependencyFileInfo.completeBaseName().endsWith( QLatin1Char( 'd' ), Qt::CaseInsensitive ) ) )
            {
                continue;
            }

            filteredDepsFilePaths.append( dependencyFilePath );
        }
    }

    if ( filteredDepsFilePaths.isEmpty() )
    {
        Logger::log( "not a single dependency was filtered out." );
    }

    return filteredDepsFilePaths;
}

QStringList Deployer::filteredDependenciesFilePaths( const QString& targetFilePath, const QStringList& additionalLibraryIncludeDirPaths )
{
    return Deployer::filteredDependenciesFilePaths( Deployer::allDependenciesFilePaths( targetFilePath, additionalLibraryIncludeDirPaths ), additionalLibraryIncludeDirPaths );
}

QList< QPair< QString, QString > > Deployer::pluginsDelayLoadedDependenciesPaths( const QStringList& inDependenciesFilePaths, const QStringList& additionalLibraryIncludeDirPaths )
{
    if ( inDependenciesFilePaths.isEmpty() )
    {
        return QList< QPair< QString, QString > >();
    }

    const QFileInfo qtCoreFi = Deployer::qtCoreFileInfo( inDependenciesFilePaths );
    if ( ! qtCoreFi.exists() )
    {
        return QList< QPair< QString, QString > >();
    }

    const ModuleInfo qtCoreModuleInfo( qtCoreFi.filePath() );
    if ( ! qtCoreModuleInfo.isValid() )
    {
        return QList< QPair< QString, QString > >();
    }

    QList< QPair< QString, QString > > outPaths;

    const bool isQt5AndAbove = qtCoreModuleInfo.version().major() >= 5;
    const bool isDebugBuild = qtCoreModuleInfo.build() == ModuleInfo::BuildDebug;
    if ( isQt5AndAbove )
    {
        // TODO: make the detection of the ANGLE library used
        // as a dependency by some Qt 5 library modules automatic
        // http://qt-project.org/forums/viewthread/21269/

        QString libEGLFileName = "libEGL";
        if ( isDebugBuild )
        {
            libEGLFileName += QChar( QLatin1Char( 'd' ) );
        }

        libEGLFileName += ".dll";

        const QFileInfo eglLibraryInfo( Deployer::qtLibDir( qtCoreFi ), libEGLFileName );
        if ( eglLibraryInfo.exists() )
        {
            static const QString DEPENDENCIES_DEPLOY_RELATIVE_DIR_PATH;
            outPaths.append( QPair< QString, QString >( Deployer::canonicalFilePath( eglLibraryInfo ), DEPENDENCIES_DEPLOY_RELATIVE_DIR_PATH ) );
        }
        else
        {
            Logger::log( "the dependency library \"" % eglLibraryInfo.filePath() % "\" does not exist." );
        }
    }

    QStringList pluginTypeRelativeDirPaths;
    if ( inDependenciesFilePaths.indexOf( QRegExp( QT_LIBRARY_REG_EXP_TEMPLATE.arg( "Gui" ), Qt::CaseInsensitive ) ) >= 0 )
    {
        pluginTypeRelativeDirPaths.append( "imageformats" );
    }

    if ( inDependenciesFilePaths.indexOf( QRegExp( QT_LIBRARY_REG_EXP_TEMPLATE.arg( "Sql" ), Qt::CaseInsensitive ) ) >= 0 )
    {
        pluginTypeRelativeDirPaths.append( "sqldrivers" );
    }

    if ( inDependenciesFilePaths.indexOf( QRegExp( QT_LIBRARY_REG_EXP_TEMPLATE.arg( "Network" ), Qt::CaseInsensitive ) ) >= 0 )
    {
        pluginTypeRelativeDirPaths.append( "bearer" );
    }

    if ( inDependenciesFilePaths.indexOf( QRegExp( QT_LIBRARY_REG_EXP_TEMPLATE.arg( "Multimedia" ), Qt::CaseInsensitive ) ) >= 0 )
    {
        pluginTypeRelativeDirPaths.append( "mediaservice" );
        pluginTypeRelativeDirPaths.append( "playlistformats" );
    }

    if ( isQt5AndAbove )
    {
        pluginTypeRelativeDirPaths.append( "platforms" );
    }

    QDir pluginsDir;
    QStringList pluginDirPathCandidates;
    pluginDirPathCandidates.reserve( 1 + additionalLibraryIncludeDirPaths.size() );
    pluginDirPathCandidates << Deployer::qtPluginsDir( qtCoreFi ).canonicalPath()
                            << additionalLibraryIncludeDirPaths;
    QStringList pluginsRelativeDirPaths;
    pluginsRelativeDirPaths.reserve( 2 );
    pluginsRelativeDirPaths << "."
                            << ( QString( ".." ) + QDir::separator() + PLUGINS_DIR_NAME );
    foreach ( const QString& pluginDirPathCandidate, pluginDirPathCandidates )
    {
        QDir pluginDirCandidate( pluginDirPathCandidate );
        if ( ! pluginDirCandidate.exists() )
        {
            continue;
        }

        bool pluginDirFound = false;
        foreach ( const QString& pluginsRelativeDirPath, pluginsRelativeDirPaths )
        {
            if ( ! pluginDirCandidate.cd( pluginsRelativeDirPath ) )
            {
                continue;
            }

            bool containsAllNeededPluginFolders = true;
            foreach ( const QString& pluginTypeRelativeDirPath, pluginTypeRelativeDirPaths )
            {
                QDir pluginTypeDirCandidate = pluginDirCandidate;
                if ( ! pluginTypeDirCandidate.cd( pluginTypeRelativeDirPath ) )
                {
                    containsAllNeededPluginFolders = false;
                    break;
                }
            }

            if ( containsAllNeededPluginFolders )
            {
                pluginsDir = pluginDirCandidate;
                pluginDirFound = true;
                break;
            }
        }

        if ( pluginDirFound )
        {
            break;
        }
    }

    foreach ( const QString& pluginTypeRelativeDirPath, pluginTypeRelativeDirPaths )
    {
        QDir pluginTypeDir = pluginsDir;
        if ( ! pluginTypeDir.cd( pluginTypeRelativeDirPath ) )
        {
            Logger::log( QString( "no \"%1\" plugins subdirectory found" ).arg( pluginTypeRelativeDirPath ) );
            continue;
        }

        static const QStringList FILE_TYPES_FILTER = QStringList() << "*.dll";
        const QStringList pluginFileNames = pluginTypeDir.entryList( FILE_TYPES_FILTER );
        if ( pluginFileNames.isEmpty() )
        {
            Logger::log( QString( "no plugins found in subdirectory: \"%1\"" ).arg( pluginTypeRelativeDirPath ) );
            continue;
        }

        foreach ( const QString& pluginFileName, pluginFileNames )
        {
            const QString pluginFilePath = QDir::toNativeSeparators( pluginTypeDir.filePath( pluginFileName ) );
            const ModuleInfo pluginModuleInfo( pluginFilePath );

            bool deployPlugin = false;
            if ( pluginModuleInfo.isValid() )
            {
                deployPlugin = pluginModuleInfo.build() == qtCoreModuleInfo.build();
            }
            else
            {
                const QString pluginCompleteBaseName = QFileInfo( pluginFilePath ).completeBaseName();
                const int qtMajorVersion = qtCoreModuleInfo.version().major();
                bool isPluginDebugVersion = false;
                if ( qtMajorVersion >= 5 )
                {
                    isPluginDebugVersion = pluginCompleteBaseName.endsWith( QChar( QLatin1Char( 'd' ) ) );
                }
                else
                {
                    isPluginDebugVersion = pluginCompleteBaseName.endsWith( QString( "d%1" ).arg( qtMajorVersion ) );
                }

                const bool isQtDebugVersion = qtCoreModuleInfo.build() == ModuleInfo::BuildDebug;
                deployPlugin = isQtDebugVersion == isPluginDebugVersion;
            }

            if ( deployPlugin )
            {
                outPaths.append( QPair< QString, QString >( pluginFilePath, pluginTypeRelativeDirPath ) );
            }
        }
    }

    return outPaths;
}

void Deployer::redistribute()
{
    const QPair< QString, QStringList > fileAndAddIncludeDirPaths = m_deploymentRequests.dequeue();
    const auto cleanerDeploymentFinishedReporter = [this, fileAndAddIncludeDirPaths]()
                                                   {
                                                       this->cleanupReportDeployed( fileAndAddIncludeDirPaths.first,
                                                                                    fileAndAddIncludeDirPaths.second );
                                                   };

    const QFileInfo targetFileInfo( fileAndAddIncludeDirPaths.first );
    if ( ! targetFileInfo.exists() )
    {
        cleanerDeploymentFinishedReporter();
        return;
    }

    Logger::log( "Deploying dependencies for target module: " + Deployer::canonicalFilePath( targetFileInfo ) );

    const QDir targetFolder = targetFileInfo.absoluteDir();
    int i = 0;
    const int totalDependenciesCount = m_allDependencies.size();
    const QHash< QString, QPair< QString, QString > >::const_iterator allDependenciesIterEnd = m_allDependencies.constEnd();
    for ( QHash< QString, QPair< QString, QString > >::const_iterator allDependenciesIter = m_allDependencies.constBegin();
          allDependenciesIter != allDependenciesIterEnd;
          ++ allDependenciesIter )
    {
        const QPair< QString, QString >& filePathSubDirName = allDependenciesIter.value();

        ++ i;
        QDir targetDependencyFolder = targetFolder;

        const QString& sourceFilePath = filePathSubDirName.first;
        const QString& deploymentFolderSubDirName = filePathSubDirName.second;
        if ( ! deploymentFolderSubDirName.isEmpty() )
        {
            if ( ! targetDependencyFolder.mkpath( deploymentFolderSubDirName ) )
            {
                Logger::log( "failed to create relative directory path: "
                             % deploymentFolderSubDirName
                             % "\n in directory: "
                             % targetFolder.canonicalPath() );
                continue;
            }

            if ( ! targetDependencyFolder.cd( deploymentFolderSubDirName ) )
            {
                Logger::log( "cannot access relative directory path: "
                             % deploymentFolderSubDirName
                             % "\n in directory: "
                             % targetFolder.canonicalPath() );
            }
        }

        const QString deployedDependencyFilePath = QDir::toNativeSeparators(
                                                       targetDependencyFolder.filePath(
                                                           QFileInfo( sourceFilePath ).fileName() ) );
        const bool deployed = QFile::copy( sourceFilePath, deployedDependencyFilePath );
        QString deployResultMessage;
        QTextStream logStream( &deployResultMessage, QIODevice::WriteOnly );
        logStream << QString( "\tdeploying dependency %1/%2:" ).arg( i ).arg( totalDependenciesCount ) << endl
                  << "\tfrom:\t" << sourceFilePath << endl
                  << "\tto:\t" << deployedDependencyFilePath << endl
                  << "\tstatus:\t" << ( deployed ? "success" : "failure" ) << endl;
        Logger::log( deployResultMessage );
    }

    const QTime deploymentTimePrintable = QTime().addMSecs( m_deploymentTime.elapsed() );
    Logger::log( QString( "Deployment finished in %1 minutes." ).arg( deploymentTimePrintable.toString( "hh:mm:ss.zzz" ) ) );

    cleanerDeploymentFinishedReporter();
}

void Deployer::cleanupReportDeployed( const QString& targetFilePath, const QStringList& additionalLibraryIncludeDirPaths )
{
    m_allDependencies.clear();
    emit deployed( targetFilePath, additionalLibraryIncludeDirPaths );
}

void Deployer::addDependencyFilePaths( const QStringList& filePaths )
{
    foreach ( const QString& filePath, filePaths )
    {
        this->addDependencyFilePath( filePath );
    }
}

void Deployer::addDependencyFilePaths( const QList< QPair< QString, QString > >& filePathSubDirNameMappings )
{
    const int filePathSubDirNameMappingsCount = filePathSubDirNameMappings.size();
    for ( int i = 0; i < filePathSubDirNameMappingsCount; ++ i )
    {
        const QPair< QString, QString >& filePathSubDirName = filePathSubDirNameMappings[ i ];
        this->addDependencyFilePath( filePathSubDirName.first, filePathSubDirName.second );
    }
}

void Deployer::addDependencyFilePath( const QString& filePath, const QString& subDirName )
{
    const QFileInfo fileInfo( filePath );
    if ( ! fileInfo.exists() )
    {
        return;
    }

    const QString fileName = fileInfo.fileName();
    const QString uniqueFileName =
#ifdef Q_OS_WIN
            fileName.toLower();
#else
            fileName;
#endif

    m_allDependencies.insert(
                uniqueFileName,
                qMakePair( Deployer::canonicalFilePath( fileInfo ), subDirName ) );
}

QString Deployer::canonicalFilePath( const QFileInfo& fileInfo )
{
    return QDir::toNativeSeparators( fileInfo.canonicalFilePath() );
}

bool Deployer::qtVersionBuild( const QString& qtCoreLibraryFileName, int& qtMajorVersion, bool& isDebug )
{
    static const QString QT_5_AND_ABOVE_LIBRARY_REG_EXP_TEMPLATE = QString( "^Qt[5-9][0-9]*%1d?\\.dll$" ).arg( QT_CORE_LIBRARY_BASE_NAME );
    static const QString QT_4_AND_BELOW_LIBRARY_REG_EXP_TEMPLATE = QString( "^Qt%1d?[0-4]\\.dll$" ).arg( QT_CORE_LIBRARY_BASE_NAME );

    static const QRegExp QT_5_AND_ABOVE_LIBRARY_REG_EXP( QT_5_AND_ABOVE_LIBRARY_REG_EXP_TEMPLATE, Qt::CaseInsensitive );
    static const QRegExp QT_4_AND_BELOW_LIBRARY_REG_EXP( QT_4_AND_BELOW_LIBRARY_REG_EXP_TEMPLATE, Qt::CaseInsensitive );
    if ( qtCoreLibraryFileName.contains( QT_5_AND_ABOVE_LIBRARY_REG_EXP ) )
    {
        const int startIndex = sizeof( "Qt" ) / sizeof( char );
        const int endIndex = qtCoreLibraryFileName.indexOf( QRegExp( "[a-zA-Z]" ), startIndex );
        const QString versionString = qtCoreLibraryFileName.mid( startIndex, endIndex - startIndex + 1 );
        bool converted = false;
        const int qtMajorVersionCandidate = versionString.toInt( & converted );
        if ( ! converted )
        {
            return false;
        }

        qtMajorVersion = qtMajorVersionCandidate;
    }
    else if ( qtCoreLibraryFileName.contains( QT_4_AND_BELOW_LIBRARY_REG_EXP ) )
    {
        const int startIndex = sizeof( "Qt" ) / sizeof( char );
        const int endIndex = qtCoreLibraryFileName.indexOf( QRegExp( "[a-zA-Z]" ), startIndex );
        const QString versionString = qtCoreLibraryFileName.mid( startIndex, endIndex - startIndex + 1 );
        bool converted = false;
        const int qtMajorVersionCandidate = versionString.toInt( & converted );
        if ( ! converted )
        {
            return false;
        }

        qtMajorVersion = qtMajorVersionCandidate;
    }

    isDebug = qtCoreLibraryFileName.endsWith( "d.dll", Qt::CaseInsensitive );

    return true;
}
